# -*- coding: utf-8 -*-
{
    'name': "NHCP Employee Profiling",

    'summary': """
        Breaks Employee Name Into Prefix (optional), Firstname, Middlemane (optional), Lastname, Suffix (optional).
        """,

    'description': """
        Breaks Employee Name Into Prefix (optional), Firstname, Middlemane (optional), Lastname, Suffix (optional).
        It also predicts name as you type so may just select the correct name and prevent typo-error.
    """,

    'author': "Daryll Gay Bangoy",
    'website': "www.linkedin.com/in/lyradb",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1.1',
    'price':21.08,
    'currency':'USD',
    'license':'LGPL-3',
    # any module necessary for this one to work correctly
    'depends': ['base','hr','ohrms_loan','base_automation'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee.xml',
        'views/res_partner.xml',
        'data/pds_questions.xml',
        'report/nchp_profiling_report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    "images":['static/description/Banner.png'],
}