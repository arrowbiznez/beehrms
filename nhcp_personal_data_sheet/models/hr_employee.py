# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class HrEmployeeSuggest(models.Model):
    _inherit = 'hr.employee'

    sss = fields.Char(string='SSS No')
    gsis =fields.Char(string='GSIS No')
    philhealth = fields.Char(string='Phil Health ID No')
    hdmf = fields.Char(string='HDMF')
    tin_id = fields.Char(string='TIN ID No')
    mobile = fields.Char(string='Mobile')
    phone = fields.Char(string='Telephone')
    identification_id = fields.Char(string='Agency Employee No')


    spouse_id = fields.Many2one(comodel_name='res.partner', string='Spouse Name', domain="[('company_type','=','person')]", context="{'default_company_type':'person'}")
    # spouse_id = fields.Char(string='Spouse Name')
    spouse_lastname = fields.Char()
    spouse_middlename = fields.Char()
    spouse_firstname = fields.Char()
    spouse_occupation = fields.Char(string='Occupation')
    spouse_phone = fields.Char(string='Spouse Telephone')
    # spouse_employer_id = fields.Many2one(comodel_name='res.partner', string='Spouse Employer', domain="[('company_type','=','company')]", context="{'default_company_type':'company'}")
    spouse_employer_id = fields.Char(string="Employer/Business Name")

    # mother_id = fields.Many2one(comodel_name='res.partner', string='Mother Maidem Name', domain="[('company_type','=','person')]", context="{'default_company_type':'person'}")
    # mother_id = fields.Char(string='Mother Maiden Name')
    mother_lastname = fields.Char()
    mother_middlename = fields.Char()
    mother_firstname = fields.Char()
    # father_id = fields.Many2one(comodel_name='res.partner', string='Father Name', domain="[('company_type','=','person')]", context="{'default_company_type':'person'}")
    # father_id = fields.Char(string='Father Name')
    father_lastname = fields.Char()
    father_middlename = fields.Char()
    father_firstname = fields.Char()
    children_ids = fields.One2many(comodel_name='hr.employee.children', inverse_name='employee_id', string='Children')
    education_ids = fields.One2many(comodel_name='hr.employee.education', inverse_name='employee_id', string='Children')
    workexp_ids = fields.One2many(comodel_name='hr.employee.work.experience', inverse_name='employee_id', string='Work Experience')
    organization_ids = fields.One2many(comodel_name='hr.employee.organization', inverse_name='employee_id', string='Organization')
    cs_ids = fields.One2many(comodel_name='hr.employee.civil.service', inverse_name='employee_id', string='Civil Service')
    learning_dev_ids = fields.One2many(comodel_name='hr.employee.learning.development', inverse_name='employee_id', string='Learning and Development')
    other_skills_ids = fields.One2many(comodel_name='hr.employee.other.skills', inverse_name='employee_id', string='Skills')
    other_recognition_ids = fields.One2many(comodel_name='hr.employee.other.recognition', inverse_name='employee_id', string='Non Academic Distinction/Recognition')
    other_association_ids = fields.One2many(comodel_name='hr.employee.other.skills', inverse_name='employee_id', string='Membership in Association/Organization')
    reference_ids = fields.One2many(comodel_name='hr.employee.references', inverse_name='employee_id', string='References')
    qa_ids = fields.One2many(comodel_name='hr.employee.qa', inverse_name='employee_id', string='QA', default=lambda self: self._insert_pds_question())

    signature = fields.Binary(string='Employee Signature')
    # workinfo
    position_title = fields.Char(string='Position Title')
    employee_num = fields.Char(string='Employee Number')
    item_num = fields.Char(string='Item Number')
    salary_grade = fields.Many2one(comodel_name='hr.salary.grade', string='Salary Grade')

    account_number = fields.Char(string='Account Number')
    
    # year_effective = fields.Date

    private_address = fields.Char(string='Address')
    private_address_block = fields.Char()
    private_address_street = fields.Char()
    private_address_subd = fields.Char()
    private_address_bgry = fields.Char()
    private_address_city = fields.Char()
    private_address_province = fields.Char()
    private_address_zip = fields.Char()
    private_phone = fields.Char(string="Telephone")
    private_email_add = fields.Char(string="Email")
    # private_address = fields.Char(string='Address')
    @api.model
    def year_selection(self):
        year = 2010  # replace 2000 with your a start year
        year_list = []
        while year != 2100:  # replace 2030 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list

    # year_effective = fields.Date(string="Year Effective")
    effectivity_year = fields.Date(string="Year Effective")

    basic_salary = fields.Monetary(string="Basic Salary", currency_field="company_currency_id")
    level_of_employment = fields.Selection(string="Level of Employment",
                                           selection=[('first_lvl', '1st Level'),
                                                      ('second_lvl', '2nd Level'),
                                                      ('refuse', '3rd Level')])
    level = fields.Selection(string="Level", selection=[('key_position', 'Key Position'),
                                                      ('services', 'Services'),
                                                      ('admin', 'Administrative'),
                                                      ('technical', 'Technical)'),
                                                        ])
    date_of_orig_apntmnt = fields.Date(string="Date of Original Appointment")
    date_of_last_promo = fields.Date(string="Date of Last Promotion")
    # year_of_promo = fields.Date(string="Year of Promotion")
    promotion_year = fields.Date(string="Year of Promotion")
    steps = [
        ('1', 'Step 1'), ('2', 'Step 2'),
        ('3', 'Step 3'), ('4', 'Step 4'),
        ('5', 'Step 5'), ('6', 'Step 6'),
        ('7', 'Step 7'), ('8', 'Step 8')]

    step_increment = fields.Selection(steps, string="Step Increment")
    company_currency_id = fields.Many2one('res.currency', related='company_id.currency_id')

    contact_person_fname = fields.Char(string="First Name")
    contact_person_lname = fields.Char(string="Last Name")
    contact_person_mname = fields.Char(string="Middle Name")
    contact_person_num = fields.Char(string="Contact Number")
    contact_relationship = fields.Char(string="Relationship")


    def _insert_pds_question(self):
        qa_ids = []
        que = self.env['hr.employee.question'].search([('active','=',True)])
        for rec in que:
            qa_ids.append((0,0,{'question':rec.id}))

        # self.qa_ids = qa_ids
        return qa_ids

class HrEmployeeReferences(models.Model):
    _name = 'hr.employee.references'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    # partner_id = fields.Many2one(comodel_name='res.partner', string='Name')
    partner_id = fields.Char(string='Name')
    address = fields.Char(string='Address')
    contact = fields.Char(string='Tel. No.')


# class HrEmployeeOtherQA(models.Model):
#     _name = 'hr.employee.other.qa'

class HrEmployeeQuestion(models.Model):
    _name = 'hr.employee.question'

    general = fields.Text(string='General')
    name = fields.Text(string='Question')
    case_if = fields.Char(string='Case')
    active = fields.Boolean(string='Active', default=True)
class HrEmployeeQA(models.Model):
    _name = 'hr.employee.qa'
    _description = "PDS Questions"

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    question = fields.Many2one(comodel_name='hr.employee.question', string='Question',
    domain="[('active','=',True)]")
    general = fields.Text(string='General', related='question.general')
    case_if = fields.Char(string='Case', related='question.case_if')
    answer = fields.Selection(string='Your Answer', selection=[('no', 'No'), ('yes', 'Yes'),])
    support_answer = fields.Text(string='Support Your Answer')

class HrEmployeeOtherAssoc(models.Model):
    _name = 'hr.employee.other.association'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    name = fields.Char(string='Association/Organization')

class HrEmployeeOtherRecognition(models.Model):
    _name = 'hr.employee.other.recognition'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    name = fields.Char(string='Distinction/Recognition')
class HrEmployeeOtherSkills(models.Model):
    _name = 'hr.employee.other.skills'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    name = fields.Char(string='Skills/Hobbies')


class HrEmployeeLearningDev(models.Model):
    _name = 'hr.employee.learning.development'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    title =  fields.Char(string='Title')
    date_from = fields.Date(string='Date From')
    date_to = fields.Date(string='Date To')
    hours_no = fields.Float(string='Number of Hours')
    ld_type = fields.Char(string='Type of L.D.')
    sponsor = fields.Char(string='Conducted/Sponsor By')

class HrEmployeeCS(models.Model):
    _name = 'hr.employee.civil.service'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    career_service =  fields.Char(string='Career Service')
    exam_rating = fields.Float(string='Rating')
    exam_date = fields.Date(string='Date of Examination')
    exam_place = fields.Char(string='Place of Examination')
    license_no = fields.Char(string='License Number')
    license_expiry_date = fields.Char(string='Date of Validity')

class HrEmployeeWorkExperience(models.Model):
    _name = 'hr.employee.work.experience'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    date_from = fields.Date(string='Date From')
    date_to = fields.Date(string='Date To')
    position = fields.Char(string='Position Title')
    job = fields.Char(string='Dept/Agency/Office/Company')
    salary = fields.Float(string='Monthly Salary')
    salary_grade = fields.Char(string='Salary Grade')
    app_status = fields.Char(string='Status of Appointment')
    govt_service = fields.Boolean(string='Government Service')

class HrEmployeeOrganization(models.Model):
    _name = 'hr.employee.organization'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    # partner_id = fields.Many2one(comodel_name='res.partner', string='Organization Name', domain="[('company_type','=','company')]", context="{'default_company_type':'company'}")
    partner_id = fields.Char(string='Organization Name')
    date_from = fields.Date(string='Date From')
    date_to = fields.Date(string='Date To')
    hours_no = fields.Float(string='Number of Hours')
    position = fields.Char(string='Position/Nature of Work')

class HrEmployeeSchool(models.Model):
    _name = 'hr.employee.education'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    level = fields.Selection(string='Level', selection=[('elementary', 'Elementary'),
    ('secondary', 'Secondary'),
    ('vocational', 'Vocational/Trade School'),
    ('college','College'),
    ('graduate','Graduate Studies')])
    # school_name = fields.Many2one(comodel_name='school.name', string='School Name')
    school_name = fields.Char(string='School Name')
    education = fields.Many2one(comodel_name='school.education', string='Education')
    school_year_from = fields.Integer(string='School From')
    school_year_to = fields.Integer(string='School To')
    # unit_earned = fields.Many2one(comodel_name='school.academic.units', string='Highest Level/Unit Earned')
    unit_earned = fields.Char(string='Highest Level/Unit Earned')
    year_graduated = fields.Integer(string='Year Gruaduated')
    # school_award = fields.Many2one(comodel_name='school.academic.award', string='Academic Award')
    school_award = fields.Char(string='Academic Award')


class SchoolName(models.Model):
    _name = 'school.name'

    name = fields.Char(string='School Name')

class SchoolEducation(models.Model):
    _name = 'school.education'

    name = fields.Char(string='Education')
class SchoolAcademicUnits(models.Model):
    _name = 'school.academic.units'

    name = fields.Char(string='Highest Level/Unit Earned')
class SchoolAcademicAward(models.Model):
    _name = 'school.academic.award'

    name = fields.Char(string='Scholarship/Academic Award')

class HrEmployeeChildren(models.Model):
    _name = 'hr.employee.children'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    order = fields.Integer(string='Sequence')
    # child_name = fields.Many2one(comodel_name='res.partner', string='Child Name', domain="[('company_type','=','person')]", context="{'default_company_type':'person'}")
    birthdate = fields.Date(string='Date of Birth')
    child_name = fields.Char(string='Child Name')


