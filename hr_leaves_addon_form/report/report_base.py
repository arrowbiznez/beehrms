from odoo import fields, api, _, models
from odoo.exceptions import ValidationError, UserError

import time
from dateutil.parser import parse
from odoo.exceptions import UserError

class HRLeaveBase(models.AbstractModel):
    _name = 'report.hr_leaves_addon_form.request_form_template'
    
    @api.model
    def _get_report_values(self, docids, data=None):
  
        docargs = {
            'doc_ids': docids,
            'doc_model': 'hr.leave',
            'docs': self,
            'data':data
        }
        print("*******************************************************")
        print(docargs)
        return docargs
    
    # @api.model
    # def render_html(self, docids, data):
    #     docargs = {
    #         'doc_ids': self.ids,
    #         'doc_model': 'hr.leave',
    #         'dataInput': data['data'],
    #     }
    #     return self.env['report'].render('hr_leaves_addon_form.request_form_template', docargs)
