from odoo import models, fields, api, _
from datetime import date, datetime

class HrEmployeeExclude(models.Model):
    _inherit = 'hr.leave'
    
    def generate_leave_form(self):
        data = {
                'emp_name':self.employee_id.name,
                'emp_dept':self.employee_id.department_id.name,
                'emp_post':self.employee_id.job_id.name,
                'emp_salary':'',
                'date_filed':str(date.today()),
                'num_of_days':self.number_of_days,
                'included_dates': str(self.request_date_from)+" to " +str(self.request_date_to),
                'leave_type':self.holiday_status_id.code,
                'leave_6a_descrip':self.leave_to_avail_description,
                'leave_6b_description':self.leave_to_avail_details_code,
                'leave_6b_details':self.leave_to_avail_spes_details,
            }
        print("*******************************************************")
        print(data)
        return self.env.ref('hr_leaves_addon_form.custom_leave_form_base').report_action(self, data=data)