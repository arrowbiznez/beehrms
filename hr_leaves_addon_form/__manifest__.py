# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'HR Leave Addons Form',
    'category': 'ohrms',
    'description': """
Addon fo the custom form for leave
==========================

""",
    'depends': ['base', 'hr'],
    'data': [
        'views/hr_leave.xml',
        'report/leave_form_template.xml',
        'report/leave_view.xml',
        'report/template_base.xml',
        'data/leave_type_data.xml',
        # 'views/snippets.xml',
    ],
    # 'qweb': [
    #     'static/src/xml/*.xml',
    # ],
    'images': [
        'static/src/img/logo.png',
    ],
    'auto_install': False
}
