from odoo import models, fields, api, _

class HrEmployeeExclude(models.Model):
    _inherit = 'hr.leave'

    selection_6a_value_reference = {    'VL':'Vacation Leave',
			                        	'MFL':'Mandatory/Forced Leave',
			                        	'SL':'Sick Leave',
				                        'ML':'Maternity Leave',
				                        'PL':'Paternity Leave',
				                        'SPL':'Special Privilage Leave',
				                        'SOLPL':'Solo Parent Leave',
				                        'STUDL':'Study Leave',
				                        'VAWC':'10-Day VAWC Leave',
				                        'REHAB':'Rehabilitation Privilage',
				                        'SLBW':'Special Leave Benifits for Women',
				                        'SECL':'Special Emergency (Calamity) Leave',
				                        'ADOPTL':'Adoption Leave',
				                        'OTHERS':'Others',
				                     }
    selection_6b_value_reference = {
                                     'ph':'Within the Philippines',
                                     'abroad':'Abroad',
                                     'in_hospital':'In Hospital Illness',
                                     'out_patient':'Out Patient',
                                     'ma_completion':"Completion of Master's Degree",
                                     'exam':'BAR/Board Examination Review',
                                     'money':'Monetization of Leave Credits',
                                     'terminal':'Terminal Leave',
                                 }


    # ========================================================================
    # Donot use the following fields for reports as they dont save any data

    request_employee_name = fields.Char(string="Employee", related="employee_id.name", store=False)
    request_employee_dept = fields.Char(string="Office/Department", related="employee_id.department_id.name", store=False)
    request_employee_post = fields.Char(string="Position", related="employee_id.job_id.name", store=False)

    vacation_leave_details = fields.Selection(string="Description", selection=[('ph','Within the Philippines'),('abroad','Abroad')], store=False)
    sick_leave_details = fields.Selection(string="Description", selection=[('in_hospital','In Hospital Illness'),('out_patient','Out Patient')], store=False)
    study_leave_details = fields.Selection(string="Description", selection=[('ma_completion',"Completion of Master's Degree"),('exam','BAR/Board Examination Review')], store=False)
    other_leave_details = fields.Selection(string="Description", selection=[('money','Monetization of Leave Credits'),('terminal','Terminal Leave')], store=False)
    
    #=========================================================================

    type_of_leave_to_avail = fields.Char(string="Type of Leave", related="holiday_status_id.code")
    leave_to_avail_description = fields.Text(string="Decribe your leave")

    @api.onchange('vacation_leave_details')
    def _onchange_vl(self):
        if self.type_of_leave_to_avail:
            if self.type_of_leave_to_avail == 'VL' and self.vacation_leave_details:
                self.leave_to_avail_details = self.selection_6b_value_reference[self.vacation_leave_details]
                self.leave_to_avail_details_code = self.vacation_leave_details
                self.sick_leave_details = False
                self.sick_leave_details = False
                self.other_leave_details = False

    @api.onchange('sick_leave_details')
    def _onchange_sl(self):
        if self.type_of_leave_to_avail:
            if self.type_of_leave_to_avail == 'SL' and self.sick_leave_details:
                self.leave_to_avail_details = self.selection_6b_value_reference[self.sick_leave_details]
                self.leave_to_avail_details_code = self.sick_leave_details
                self.vacation_leave_details = False
                self.sick_leave_details = False
                self.other_leave_details = False


    @api.onchange('study_leave_details')
    def _onchange_studl(self):
        if self.type_of_leave_to_avail:
            if self.type_of_leave_to_avail == 'STUDL' and self.study_leave_details:
                self.leave_to_avail_details = self.selection_6b_value_reference[self.study_leave_details]
                self.leave_to_avail_details_code = self.study_leave_details
                self.vacation_leave_details = False
                self.sick_leave_details = False
                self.other_leave_details = False


    @api.onchange('other_leave_details')
    def _onchange_other(self):
        if self.type_of_leave_to_avail:
            if self.type_of_leave_to_avail == 'OTHERS' and self.other_leave_details:
                self.leave_to_avail_details = self.selection_6b_value_reference[self.other_leave_details]
                self.leave_to_avail_details_code = self.other_leave_details
                self.vacation_leave_details = False
                self.sick_leave_details = False
                self.sick_leave_details = False

    leave_to_avail_details_code = fields.Char()
    leave_to_avail_details = fields.Char(string="Description")
    leave_to_avail_spes_details = fields.Text(string="Details")
