# -*- coding: utf-8 -*-
{
    "name": "Code Backend Theme ",
    "description": """Minimalist and elegant backend theme for Odoo 14, Backend Theme, Theme""",
    "summary": "Code Backend Theme V14 is an attractive theme for backend",
    "category": "Theme/Backend",
    "version": "1.1",
    'author': 'Cybrosys Techno Solutions',
    'company': 'Cybrosys Techno Solutions',
    'maintainer': 'Cybrosys Techno Solutions',
    'website': "https://www.cybrosys.com",
    "depends": ['base', 'web', 'mail'],
    "data": [
        'assets/assets.xml',
        # 'views/icons.xml',
        # 'views/layout.xml',
    ],
    "qweb": [
        # 'static/src/xml/sidebar.xml',
        # 'static/src/xml/styles.xml',
        # 'static/src/xml/top_bar.xml',
    ],
    'images': [
        # 'static/description/banner.png',
        # 'static/description/theme_screenshot.png',
    ],
    'license': 'LGPL-3',
    'pre_init_hook': 'test_pre_init_hook',
    'post_init_hook': 'test_post_init_hook',
    'installable': True,
    'application': False,
    'auto_install': False,
}
