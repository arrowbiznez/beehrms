from odoo import models, fields, api, _

class HrDepartment(models.Model):
    _inherit = 'auditlog.rule'
    
    def auditlog_subscribe(self):
        logs = self.search([('state','=','draft')])
        print('Auditlog Start:', logs)
        for rec in logs:
            print('Auditlog:', rec.state)
            rec.subscribe()