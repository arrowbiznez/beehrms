from odoo import models, fields, api, _


class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    employee_id = fields.Many2one('hr.employee',
                                  string='Related Employee', ondelete='restrict',
                                  help='Employee-related data of the user')
                                  
    #legal_count = fields.Integer(string='Legal Count')
    
    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        for rec in self:
            rec.name = rec.employee_id.name
            rec.login = rec.employee_id.work_email
            rec.email = rec.employee_id.work_email
    
    
    @api.model
    def create(self, vals):
        print('vals',vals)
        ret = super(ResUsersInherit, self).create(vals)
        employee_id = False
        if 'employee_id' in vals:
            if vals['employee_id']:
                employee_id = vals['employee_id']
                employee = self.env['hr.employee'].sudo().browse(employee_id)
                employee.user_id = ret
        return ret
    
    # @api.multi
    def write(self, vals):
        print('vals',vals)
        employee_id = False
        if 'employee_id' in vals:
            if vals['employee_id']:
                employee_id = vals['employee_id']
                self.employee_id.user_id = False
                employee = self.env['hr.employee'].sudo().browse(employee_id)
                employee.user_id = self.id
        return super(ResUsersInherit, self).write(vals)
    