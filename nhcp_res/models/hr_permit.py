from odoo import models, fields, api, _

office_stay = 'Stay in the Office'
office_report = 'Report in the Office'
office_leave = 'Official Leave The Office'
office_leave_personal = 'Personal Leave The Office'

class HrPermitEmployee(models.Model):
    _inherit = 'hr.permit'

    def _open_permit_all_to_stay(self):
        res = super()._open_permit_all_to_stay()

        has_group_hr_admin = self.env.user.has_group('nhcp_res.group_nhcp_hr_admin')

        if has_group_hr_admin:
            res['domain'] = str([('state','!=','draft')])
        return res

    def _open_permit_all_to_report(self):
        res = super()._open_permit_all_to_stay()

        has_group_hr_admin = self.env.user.has_group('nhcp_res.group_nhcp_hr_admin')

        if has_group_hr_admin:
            res['domain'] = str([('state','!=','draft')])
        return res

    def _open_permit_all_to_leave(self):
        res = super()._open_permit_all_to_stay()

        has_group_hr_admin = self.env.user.has_group('nhcp_res.group_nhcp_hr_admin')

        if has_group_hr_admin:
            res['domain'] = str([('state', '!=', 'draft')])
        return res

    def _open_permit_all_to_leave_personal(self):
        res = super()._open_permit_all_to_stay()

        has_group_hr_admin = self.env.user.has_group('nhcp_res.group_nhcp_hr_admin')

        if has_group_hr_admin:
            res['domain'] = str([('state', '!=', 'draft')])
        return res