from odoo import models, fields, api, _

class HrDepartment(models.Model):
    _inherit = 'hr.department'
    
    dept_code = fields.Char(string='Department Initials', required=True, help="For Document Series")
    
    # def init(self):
    #     self._cr.execute("""
    #     with dept as (select id, unnest(string_to_array(name,' ')) initials
    # from hr_department where dept_code isnull);
    #     update hr_department a
    #     set dept_code = d.initials
    #     from dept d
    #     where d.id = a.id
    #     and a.dept_code isnull;
    #     """)
    
class Sequencer(models.Model):
    _inherit = 'ir.sequence'
    
    def next_by_code_by_employee(self,model_code,model_name,employee):
        print('param','model_code','model_name','employee')
        code = '%s' % (model_name)
        if employee.department_id:
            dept_code = employee.department_id.dept_code
            if dept_code:
                name = '%s for %s' % (model_name,dept_code.lower())
                code = '%s.%s' % (model_name,dept_code.lower())
                prefix = '%s%s%s' % (model_code,dept_code.upper(),'%(year)s-')
                found = self.sudo().search([('code','=',code)])
                if not found:
                    self.sudo().create({
                    'name': name,
                    'code': code,
                    'prefix': prefix,
                    'number_next': 1,
                    'number_increment': 1,
                    'padding': 4,
                    })
                    self.env.cr.commit()
        
        print('Sequencer code', 'code')
        return self.next_by_code(code) or '/'

class HREmployeeEvaluation(models.Model):
    _inherit = 'hr.employee.evaluation'

    @api.model
    def create(self, values):
        seq = False
        if 'employee_id' not in values:
            emp_id = self.env.user.employee_id.id
        else:
            emp_id = values['employee_id']
        emp = self.env['hr.employee'].browse(emp_id)
        # print('emp',emp)
        if emp:
            if emp.department_id and 'dept_code' in self.env['hr.department']._fields:
                if emp.department_id.dept_code:
                    seq = self.env['ir.sequence'].next_by_code_by_employee("EVAL",self._name,emp) or '/'
        if not seq:
            seq = self.env['ir.sequence'].next_by_code('hr.employee.evaluation') or '/'
        values['name'] = seq
        
        return super(HREmployeeEvaluation, self.sudo()).create(values)