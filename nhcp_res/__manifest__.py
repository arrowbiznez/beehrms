# -*- coding: utf-8 -*-
{
    'name': "NHCP Core",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Daryll Gay Bangoy",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'web',
        'hr',
        'hr_holidays',
        'hr_employee_name_breakdown',
        'hr_gamification',
        'hr_employee_updation',
        'hr_recruitment',
        'hr_attendance',
        'hr_payroll_community',
        'hr_expense',
        'hr_leave_request_aliasing',
        'hr_timesheet',
        # 'oh_employee_creation_from_user',
        'oh_employee_documents_expiry',
        'hr_multi_company',
        'ohrms_loan',
        # 'ohrms_loan_accounting',
        'ohrms_salary_advance',
        'hr_reminder',
        'hr_reward_warning',
        'hr_permit_employee',
        'hr_document_request',
        'hr_travel_order',
        'hr_training_request',
        'ohrms_overtime',
        'hrms_dashboard',
        'hr_employee_evaluation',
        'ohrms_overtime_approval',
        'ohrms_holidays_approval_additionals',
        'employee_orientation',
        'website',
        'hr_salary_grade',
        'nhcp_personal_data_sheet',
        'nhcp_personal_info',
        'timesheet_based_attendance',
        'base_automation',
        # 'hr_payroll_account_community',
        'auditlog',
        'nhcp_core',
    ],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/hr_employee_evaluation_views.xml',
        'views/hr_employee_function_core_views.xml',
        'views/menu.xml',
        'views/templates.xml',
        'views/dashboard.xml',
        'data/auditlog_data.xml',
        'views/remove_import.xml',
    ],
    'qweb': [
        # "static/src/xml/apps.xml",
        # "static/src/templates/side_bar.xml",
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
