# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime

class special_payroll(models.Model):
    _name = 'special.payroll'
    _description = 'special payroll'

    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    generate_date = fields.Date(string='Date', default=datetime.now())
    contract_id = fields.Many2one('hr.contract', string='Contract', readonly=True)
    total_amount = fields.Float(string="Total Amount", compute='_onchange_salary_rule')
    salary_rule_ids = fields.One2many('hr.payslip.special.payroll', 'special_id')
    deduction_ids = fields.One2many('hr.deductions.special.payroll', 'special_deduction_id')

    @api.onchange('employee_id')
    def _onchange_employee(self):
        for line in self:
            line.contract_id = line.employee_id.contract_id

    @api.depends('salary_rule_ids')
    def _onchange_salary_rule(self):
        total = 0
        total_deduct = 0
        for line in self.salary_rule_ids:
            total += line.amount
        for deduct in self.deduction_ids:
            total_deduct += deduct.amount
        self.total_amount = total - total_deduct



class HrPayslipSpecialPayroll(models.Model):
    _name = 'hr.payslip.special.payroll'

    salary_rule_id = fields.Many2one('hr.salary.rule', string='Salary Rule', domain=[('category_id.code','=','ALW')])
    desc = fields.Char(string="Description")
    code = fields.Char(string="Code")
    amount = fields.Float(string="Amount")
    contract_id = fields.Many2one('hr.contract')
    payslip_id = fields.Many2one('hr.payslip', string="Payslip ID")
    special_id = fields.Many2one('special.payroll')

    @api.onchange('salary_rule_id')
    def _compute_input_details(self):
        for line in self:
            line.name = line.salary_rule_id.name
            line.code = line.salary_rule_id.code

class HrDeductionsSpecialPayroll(models.Model):
    _name = 'hr.deductions.special.payroll'

    salary_rule_id = fields.Many2one('hr.salary.rule', string='Salary Rule', domain=[('category_id.code','=','DED')])
    desc = fields.Char(string="Description")
    code = fields.Char(string="Code")
    amount = fields.Float(string="Amount")
    contract_id = fields.Many2one('hr.contract')
    payslip_id = fields.Many2one('hr.payslip', string="Payslip ID")
    special_deduction_id = fields.Many2one('special.payroll')

    @api.onchange('salary_rule_id')
    def _compute_input_details(self):
        for line in self:
            line.name = line.salary_rule_id.name
            line.code = line.salary_rule_id.code
