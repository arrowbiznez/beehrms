# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import float_compare
from datetime import datetime, timedelta, time
from dateutil.relativedelta import relativedelta
import calendar
from pytz import timezone
import pytz


class Hrleave(models.Model):
    _inherit = 'hr.leave'
    
    late_timesheet_id = fields.Many2one('attendance.timesheet', string='Late Timesheet Attendance')   
    absent_timesheet_id = fields.Many2one('attendance.timesheet', string='Absent Timesheet Attendance')   
    
    # def date_convert_into_local(datetime, timezone):
    #     ks_tz = timezone and pytz.timezone(timezone) or pytz.UTC
    #     return pytz.UTC.localize(datetime.replace(tzinfo=None), is_dst=False).astimezone(ks_tz).replace(tzinfo=None)

    def _leave_update_timesheet(self):
        if 'absent_timesheet_id' in self._fields:   
            if self.absent_timesheet_id and self.state == 'confirm':
                if not self.absent_timesheet_id.absent_leave_id: 
                    self.absent_timesheet_id.absent_leave_id = self.id
                   
        if 'late_timesheet_id' in self._fields:    
            if self.late_timesheet_id and self.state == 'confirm':
                if not self.late_timesheet_id.late_leave_id:
                    self.late_timesheet_id.late_leave_id = self.id
        
        if 'late_timesheet_id' in self._fields  and 'absent_timesheet_id' in self._fields: 
            if self.state == 'validate' and not self.late_timesheet_id and not self.absent_timesheet_id:    
                    timesheet_code = self.holiday_status_id.timesheet_code
                    if not timesheet_code:
                        return False
                        
                    employee = self.employee_id
                    employee_id = self.employee_id.id

                    datefrom = datetime.combine(self.request_date_from, time.min)
                    dateto = datetime.combine(self.request_date_to, time.max)

                    ndays = int((dateto-datefrom).days)+1
                    leave = 0.0
                    
                    tz = pytz.timezone(self.env.user.tz)
                    if self.employee_id.user_id.tz:
                        tz = pytz.timezone(employee.user_id.tz)
                    
                    for day in range(ndays):
                        leave_date = (datefrom+timedelta(days=day))
                        # if xday:
                        #     leave_date = (datefrom+timedelta(days=day))
                        # else:
                        #     leave_date = datefrom
                            
                        leave_date = pytz.utc.localize(leave_date).astimezone(tz)

                        timesheet = self.env['attendance.timesheet'].search([('employee_id','=',employee_id),
                            ('attendance_date','=',leave_date)], limit=1)
                        
                        if not timesheet:
                            year = leave_date.strftime('%Y')
                            mm = leave_date.strftime('%m')
                            dd = str(calendar.monthrange(int(year),int(mm))[1])
                            date_from = "%s-%s-01" % (year,mm)
                            date_end = "%s-%s-%s" % (year,mm,dd.zfill(2))
                            nextmonth = datetime.strptime(date_from,'%Y-%m-%d')+relativedelta(months=1)
                            payroll_date_from = nextmonth.strftime('%Y-%m-%d')
                            payroll_date_end = "%s-%s-%s" % (year, mm, dd)
                            weekyr = int(leave_date.strftime('%U'))
                            dayofweek = leave_date.strftime('%A')
            
                            data = {
                                'attendance_date': leave_date,
                                'employee_id': employee_id,
                                'reference2': timesheet_code,
                                'weekofyear':weekyr,
                                'dayofweek': dayofweek,
                                'payroll_date_from': payroll_date_from,
                                'payroll_date_end': payroll_date_end
                            }
                            
                            found = self.env['attendance.timesheet.base'].search([('employee_id','=',employee_id),
                                ('date_from','=',date_from), ('date_end','=',date_end),])
                            
                            if not found:
                                emp_data = {
                                    'date_from': date_from,
                                    'date_end': date_end,
                                    'payroll_date_from': payroll_date_from,
                                    'payroll_date_end': payroll_date_end,
                                    'employee_id': employee_id,
                                    'attendance_ids': [(0,0,data)]
                                }

                                ret = self.env['attendance.timesheet.base'].create(emp_data)
                                self.env.cr.commit()
                                timesheet = self.env['attendance.timesheet'].search([('employee_id','=',employee_id),
                                    ('attendance_date','=',leave_date)], limit=1)
                            else:
                                for att in found.attendance_ids:
                                    if att.attendance_date == leave_date and att.state == 'draft':
                                        data['attendance_timesheet_id'] = found.id
                                        att.write(data)
                                        break
                                
                                timesheet = self.env['attendance.timesheet'].search([('employee_id','=',employee_id),
                                    ('attendance_date','=',leave_date)], limit=1)
                        
                        if not timesheet:
                            continue

                        if timesheet.state == 'draft':
                            if self.absent_timesheet_id:
                                leave += self.number_of_days
                            if self.late_timesheet_id:
                                leave += self.number_of_days
                                
                            if self.request_unit_half:
                                leave += 0.5
                            elif self.request_unit_hours:
                                leave += round((self.request_hour_to - self.request_hour_from)/8,2)
                            else:
                                leave += 1
                                
                            if timesheet.reference2 != 'rd':
                                timesheet.reference2 = timesheet_code
                            timesheet.leave = leave
                            timesheet.leave_id = self.id
    
class AttendanceTimesheet(models.Model):
    _inherit = 'attendance.timesheet'
    
    # Override previous field and change compute
    absence = fields.Float(string='Absences', readonly=True, compute="_compute_absences", store=True, digits="Absences")
    leave_id = fields.Many2one(comodel_name='hr.leave', string='Leave', readonly=True, store=True) #Remove: compute=_compute_att_check_in_out
    leave = fields.Float(string='Leave', readonly=True, compute="_compute_leave", store=True)
    # New field
    late_leave_id = fields.Many2one(comodel_name='hr.leave', string='Late Leave')
    absent_leave_id = fields.Many2one(comodel_name='hr.leave', string='Absent Leave')
    
    def init(self):
        self._cr.execute("""
             update attendance_timesheet set absence = round(1-(case when reference2 in ('ua','holiday','sholiday') then 0 when reference2 = 'rd' then 1 else worked_hours/8 end)-leave,3) where state = 'draft';
            """)

    @api.depends('worked_hours', 'leave','reference2')
    def _compute_absences(self):
        for rec in self:
            if rec.reference2 in ['sholiday','holiday'] and not rec.leave_id:
                rec.absence = 0
            else:
                absence = 1 if rec.reference2 != 'rd' else 0
                worked_hours = rec.worked_hours/8 if rec.reference2 not in ['ua','rd','holiday','sholiday'] else 0
                rec.absence = round(absence-worked_hours-rec.leave,3)
    
    @api.depends('absent_leave_id','late_leave_id','leave_id')
    def _compute_leave(self):
        for rec in self:
            print('xxx', rec.leave_id, rec.late_leave_id, rec.absent_leave_id)
            leave_total = 0.0
            leave_code = ''
            
            if 'leave_id' in self._fields: 
                if rec.leave_id:
                    for ll in rec.leave_id:
                        if ll.holiday_status_id.timesheet_code and ll.state == 'validate':
                            leave_code = ll.holiday_status_id.timesheet_code
                            if ll.request_unit_half:
                                leave = 0.5
                            elif ll.request_unit_hours:
                                leave = round((ll.request_hour_to - ll.request_hour_from)/8,2)
                                if leave > 1.0: # if greater 8 hours
                                    leave = 1 # max of 8 hours only
                            else:
                                leave = 1
                            leave_total += leave
                        
            if 'late_leave_id' in self._fields:
                if rec.late_leave_id:
                    for ll in rec.late_leave_id:
                        if ll.late_timesheet_id and ll.state == 'confirm':
                            leave_total += ll.number_of_days
            
                        
            if 'absent_leave_id' in self._fields: 
                if rec.absent_leave_id:
                    for ll in rec.absent_leave_id:
                        if ll.absent_timesheet_id:
                            leave_total += ll.number_of_days
                    
            rec.leave = leave_total
            
            if rec.reference2 in ['at',False]:
                if leave_code:
                    rec.reference2 = leave_code
                    
    def _auto_leave_tardy_undertime(self):
        for rec in self:
            if rec.state == 'approve':
                if rec.late_leave_id:
                    rec.late_leave_id.action_validate()
                if rec.absent_leave_id and rec.absent_leave_id.state == 'confirm':
                    rec.absent_leave_id.action_validate()
            elif rec.state == 'draft' and (rec.late or rec.undertime) and rec.employee_id.remaining_leaves:
                res = self.env['hr.leave.type'].sudo().search([('code','in',['VL','SL']),('active','=',True)], order="code desc, id desc", limit=1)
                for found in res:
                    
                    # mapped_days = self.env['hr.leave'].mapped('holiday_status_id').get_employees_days(self.mapped('employee_id').ids)
                    # leave_days = mapped_days[rec.employee_id.id][found.id]
                    
                    # if float_compare(leave_days['remaining_leaves'], 0, precision_digits=2) == -1 or float_compare(leave_days['virtual_remaining_leaves'], 0, precision_digits=2) == -1:
                    #     continue
                    
                    alloc = self.env['hr.leave.allocation'].sudo().search([
                                    ('holiday_status_id','=',found.id),
                                    ('employee_id','=',rec.employee_id.id),('state','=','validate')],order='id desc')
                    
                                    # ('remaining_leaves','>',0.0),
                                    
                    if not alloc:
                        continue
                    
                    if rec.reference2 not in ['late','under']: 
                        if rec.late_leave_id:
                            rec.late_leave_id.state = 'cancel'
    
                        # if rec.leave_id:
                        #     rec.leave_id.state = 'cancel'
                    late = (rec.late + rec.undertime)/480 # *((1/8)/60)
                    # if alloc.max_leaves >=  late:
                    if late:
                        reason = ''
                        sep = ''
                        if rec.late:
                            reason = 'Tardiness'
                            sep=' and '
                        if rec.undertime:
                            reason = '%s%s%s' % (reason,sep,'Undertime')
                        name = 'Automatic application of %s for "%s" as of %s' % (found.name, reason, rec.attendance_date)
                        holiday_data = {
                            'employee_id': rec.employee_id.id,
                            'holiday_type': 'employee',
                            'holiday_status_id': found.id,
                            'request_date_from': rec.attendance_date,
                            'request_date_to': rec.attendance_date,
                            'date_from': rec.attendance_date,
                            'date_to': rec.attendance_date,
                            'number_of_days':late,
                            'report_note': name,
                            'vac_special_leave': 'within',
                            'state': 'confirm',
                            'late_timesheet_id': rec.id,
                        }
                        
                        ret = self.env['hr.leave'].sudo()
                        if not rec.late_leave_id:
                            ret.create(holiday_data)
                            rec.reference2 = found.code.lower()
                        else:
                            rec.late_leave_id.number_of_days = late
    
    def _unauthorized_absent_to_leave(self):
        for rec in self:
            if rec.state == 'draft' and rec.reference2 == 'ua' and rec.absent_leave_id:
                rec.absent_leave_id.action_validate()  
            if rec.state == 'draft' and rec.absence and rec.employee_id.remaining_leaves:
                res = self.env['hr.leave.type'].sudo().search([('code','in',['VL','SL']),('active','=',True)], order="code desc, id desc")
                for found in res:
                    
                    # mapped_days = self.env['hr.leave'].mapped('holiday_status_id').get_employees_days(self.mapped('employee_id').ids)
                    # leave_days = mapped_days[rec.employee_id.id][found.id]
                    
                    # if float_compare(leave_days['remaining_leaves'], 0, precision_digits=2) == -1 or float_compare(leave_days['virtual_remaining_leaves'], 0, precision_digits=2) == -1:
                    #     continue
                    
                    alloc = self.env['hr.leave.allocation'].sudo().search([
                                    ('holiday_status_id','=',found.id),
                                    ('employee_id','=',rec.employee_id.id),('state','=','validate')],order='id desc',limit=1)
                    
                                    # ('remaining_leaves','>=',0.0),
                                    
                    if not alloc:
                        continue
                    
                    if rec.reference2 != 'ua': 
                        if rec.absent_leave_id:
                            rec.absent_leave_id.state = 'cancel'
                            break
                    absence = rec.absence * (1 if rec.absence >= 1 else rec.absence)/480 #((1/8)/60))
                    
                    if absence and rec.reference2 == 'ua':
                        name = 'Automatic application of %s for "Absences" as of %s' % (found.name,  rec.attendance_date)
                        holiday_data = {
                            'employee_id': rec.employee_id.id,
                            'holiday_type': 'employee',
                            'holiday_status_id': found.id,
                            'request_date_from': rec.attendance_date,
                            'request_date_to': rec.attendance_date,
                            'date_from': rec.attendance_date,
                            'date_to': rec.attendance_date,
                            'number_of_days':absence,
                            'report_note': name,
                            'vac_special_leave': 'within',
                            'state': 'confirm',
                            'absent_timesheet_id': rec.id,
                        }
                        
                        ret = self.env['hr.leave'].sudo()
                        if not rec.absent_leave_id:
                            try:
                                ret.create(holiday_data)
                                rec.reference2 = found.code.lower()
                            except:
                                print('No %s Allocation for %s.' %  (found.name, rec.employee_id.name))
                        else:
                            rec.absent_leave_id.number_of_days = absence
    
    
    def _auto_leave_absent(self):
        attendance_date = datetime.now().date()-timedelta(days=1)
        for rec in self.search([('attendance_date','=',attendance_date),('state','=','draft'),('absence','>=',1)]):
            print('_auto_leave_absent',rec.id, rec.state)
            if rec.state == 'draft' and rec.absent_leave_id:
                print('validate')
                rec.absent_leave_id.action_validate()  
            elif rec.state == 'draft' and rec.absence:
                print('absences', rec.absence)
                res = self.env['hr.leave.type'].sudo().search([('code','in',['VL','SL']),('active','=',True)], order="code desc, id desc", limit=1)
                for found in res:
                    
                    # mapped_days = self.env['hr.leave'].mapped('holiday_status_id').get_employees_days(self.mapped('employee_id').ids)
                    # leave_days = mapped_days[rec.employee_id.id][found.id]
                    
                    # if float_compare(leave_days['remaining_leaves'], 0, precision_digits=2) == -1 or float_compare(leave_days['virtual_remaining_leaves'], 0, precision_digits=2) == -1:
                    #     continue
                    
                    alloc = self.env['hr.leave.allocation'].sudo().search([
                                    ('holiday_status_id','=',found.id),
                                    ('employee_id','=',rec.employee_id.id),
                                    ('max_leaves','>',0.0),('state','=','validate')],order='id desc',limit=1)
                    if not alloc:
                        continue
                    
                    if rec.reference2 not in ['late','under']: 
                        if rec.late_leave_id:
                            rec.late_leave_id.state = 'cancel'
                    absence = rec.absence * (1 if rec.absence >= 1 else rec.absence)/480 # ((1/8)/60))
                    
                    if absence and rec.reference2 == 'ua':
                        name = 'Automatic application of %s for "Absences" as of %s' % (found.name,  rec.attendance_date)
                        holiday_data = {
                            'employee_id': rec.employee_id.id,
                            'holiday_type': 'employee',
                            'holiday_status_id': found.id,
                            'request_date_from': rec.attendance_date,
                            'request_date_to': rec.attendance_date,
                            'date_from': rec.attendance_date,
                            'date_to': rec.attendance_date,
                            'number_of_days':absence,
                            'report_note': name,
                            'vac_special_leave': 'within',
                            'state': 'confirm',
                            'absent_timesheet_id': rec.id,
                        }
                        
                        ret = self.env['hr.leave'].sudo()
                        if not rec.absent_leave_id:
                            try:
                                ret.create(holiday_data)
                                rec.reference2 = found.code.lower()
                            except:
                                print('No %s Allocation for %s.' %  (found.name, rec.employee_id.name))
                        else:
                            rec.absent_leave_id.number_of_days = absence
    
    def _process_uanauthorized_absences_previous_day(self):
        attendance_date = datetime.now().date()-timedelta(years=1)
        for rec in self.search([('attendance_date','>=',attendance_date),('reference2','in',['',False]),'|',('state','=','draft'),('undertime','>',0),('absence','>=',1)]):
            if rec.state == 'draft' and rec.absence and not rec.leave_id and not rec.late_leave_id:
                rec.reference2 = 'ua'
            elif rec.state == 'draft' and rec.undertime and not rec.leave_id and not rec.late_leave_id:
                rec.reference2 = 'under'