from calendar import c
from odoo import fields, api, models, _
from datetime import datetime, timedelta, time
from dateutil.relativedelta import relativedelta
from pytz import timezone
from odoo.exceptions import ValidationError
import pytz
import mysql.connector
from mysql.connector import Error

class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    
    device_id = fields.Char(string='Biometric User ID', help="Biometric User ID")
    
    @api.onchange('device_id')
    def _onchange_device_id(self):
        self.barcode = self.device_id
        
    def init(self):
        self.env.cr.execute("""
        update hr_employee set barcode = device_id
        """)
        self.env.cr.commit()
    
class HrBioclockUsers(models.Model):
    _name = 'hr.bioclock.user'
    
    name = fields.Char(string='Biometric User Name', index=True)
    device_id = fields.Char(string='Biometric User ID', help="Biometric User ID", unique=True, required=True)
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Related Employee')
    active = fields.Boolean(string='Active', default=True)
    
    
    @api.constrains('device_id', 'name')
    def _check_validity(self):
        device_id = self.device_id
        name = self.name
        id = self.id
        
        found = self.search([('id','!=',id),('device_id','=',device_id),('name','=',name)])
        if found:
            raise ValidationError('Biometric User ID: %s with Name: %s is Already Exists.' % (device_id, name))
        
        found = self.search([('id','!=',id), ('device_id','=',device_id)], limit=1)
        if found:
            raise ValidationError('Biometric User ID: %s is already owned by %s.' % (device_id, name))
        
    
    def _cron_append_biometric_user(self):
        for rec in self.env['hr.bioclock.logs'].search([('employee_id','=',False),('device_id','!=',False)], order='device_id, punching_time asc'):
            device_id = rec.device_id
            user = self.search([('device_id','=',device_id)])
            if user:
                if user.employee_id:
                    rec.employee_id = user.employee_id.id
                continue
                
            res = self.create({
                'device_id': device_id,
                'name': 'New User %s' % device_id,
            })


class HrBioclockLogsFetch(models.TransientModel):
    _name = 'hr.bioclock.logs.fetch'
    _description = 'Bioclock Logs Fetcher'

    date_from = fields.Date(string='Date From', required=True, default=lambda self: fields.datetime.now().date()-timedelta(days=15))
    date_to = fields.Date(string='Date To',  required=True, default=lambda self: fields.datetime.now().date())

    rec_to_fetch = fields.Integer('Biometric', readonly=True)
    rec_fetched = fields.Integer('Fetched', readonly=True)
    
    @api.onchange('date_from','date_to')
    def _onchange_report_date(self):
        date_from = self.date_from
        date_to = self.date_to

        if not date_from or not date_to:
            return False 
        
        if date_to < date_from:
            return False
        
        xdate_from = datetime.combine(date_from, time.min)
        xdate_to = datetime.combine(date_to, time.max)

        self.rec_fetched = self.env['hr.bioclock.logs'].sudo().search_count([('punching_time','>=',xdate_from),('punching_time','<=',xdate_to)])

        host = self.env['ir.config_parameter'].sudo().get_param('alpeta.host')
        if host:
            user = self.env['ir.config_parameter'].sudo().get_param('alpeta.user')
            passwd = self.env['ir.config_parameter'].sudo().get_param('alpeta.passwd') 
            db = self.env['ir.config_parameter'].sudo().get_param('alpeta.db')

            mydb = mysql.connector.connect(host=host,user=user,password=passwd, database=db)
        else:
            mydb = mysql.connector.connect(host="51.79.206.91",user="odoo",password="root", database='ucdb')
        
        start_event_time = datetime.strftime(xdate_from,'%Y-%m-%d %H:%M:%S')
        end_event_time =  datetime.strftime(xdate_to,'%Y-%m-%d %H:%M:%S') #datetime.now().strftime('%Y-%m-%d 23:59:59')
        limit_date = datetime.strptime(date_to.strftime('%Y-%m-%d'),'%Y-%m-%d')
        yy = int(start_event_time[:4])
        mm = int(start_event_time[5:7])
        
        mycursor = mydb.cursor()

        test_date = '%s-%s-01' % (str(yy),str(mm).rjust(2,'0'))
        test_date = datetime.strptime(test_date,'%Y-%m-%d')

        count = 0
        while test_date <= limit_date: 
            res = False
            sql = """select count(*) rec from auth_logs_%s%s where event_time between '%s' and '%s';""" % (str(yy),str(mm).rjust(2,'0'), start_event_time, end_event_time)
            try:
                req = mycursor.execute(sql)
                res = mycursor.fetchall()
            except:
                print("An exception occurred")

            if res:
                for rec in res:
                    count += rec[0]
            
            mm += 1
            if mm > 12:
                mm = 1
                yy += 1
            test_date = '%s-%s-01' % (str(yy),str(mm).rjust(2,'0'))
            test_date = datetime.strptime(test_date,'%Y-%m-%d') 
        self.rec_to_fetch = count
        mycursor.close()
        mydb.close()
    
    def action_fetch_alpeta_logs(self):
        for rec in self:
            self.env['hr.bioclock.logs'].fetch_alpeta_logs(rec.date_from)

        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        } 

class HrBioclockLogs(models.Model):
    _name = 'hr.bioclock.logs'
    _description = 'Bioclock Logs'
    _order = 'biometric_time desc'
    
    name = fields.Char(string='Biometric User Name', index=True, related="employee_id.name", store=True, readonly=True)
    device_id = fields.Char(string='Biometric User ID', 
    help="Biometric User ID", required=True, readonly=False, index=True)
    punch_type = fields.Selection([('0', 'Check In'),
                                   ('1', 'Check Out'),
                                   ('7', 'Trash Log'),
                                   ('X', 'Raw Log (X)'),
                                   ('O', 'Raw Log (O)')],
                                  string='Punching Type', readonly=True, index=True)
                                  
                                #    ('2', 'Break Out'),
                                #    ('3', 'Break In'),
                                #    ('4', 'Overtime In'),
                                #    ('5', 'Overtime Out'),

    attendance_type = fields.Selection([('1', 'Finger'),
                                        ('15', 'Face'),
                                        ('2', 'Type_2'),
                                        ('3', 'Password'),
                                        ('4', 'Card')], string='Category', 
                                        help="Select the attendance type", readonly=True)
                                        
    punching_time = fields.Datetime(string='Punching Time', help="Give the punching time", required=False, readonly=True, index=True)
        
    punching_time_text = fields.Char(string='From Biometric Punching Time', readonly=True, index=True)
    
    biometric_time = fields.Datetime(string='Biometric Attendance', help="Give the punching time", 
                required=False, readonly=False, index=True)
    
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', readonly=True, index=True)
    
    attendance_id = fields.Many2one(comodel_name='hr.attendance', string='Attendance',  readonly=True, index=True)
   
    _sql_constraints = [
        (
            "user_punching_time_uniq",
            "unique (device_id,punching_time)",
            "Punching time must be unique for each user !",
        ),
        (
            "user_biometric_attendance_uniq",
            "unique (device_id,biometric_time)",
            "Biometric time must be unique for each user !",
        )
    ] 
    
    @api.constrains('device_id', 'punching_time', 'biometric_time')
    def _check_validity(self):
        device_id = self.device_id
        punching_time = self.punching_time
        biometric_time = self.biometric_time
        id = self.id
        
        if punching_time and not biometric_time: 
            found = self.search([('id','!=',id),('device_id','=',device_id),('punching_time','=',punching_time)])
            
            if found:
                raise ValidationError('Duplicate record for Biometric User ID: %s with Punching Time: %s.' % (device_id,punching_time))
        
        if not punching_time and biometric_time: 
            found = self.search([('id','!=',id),('device_id','=',device_id),('biometric_time','=',biometric_time)])
            
            if found:
                raise ValidationError('Duplicate record for Biometric User ID: %s with Biometric Time: %s.' % (device_id,biometric_time))
            
    def init(self):
        self._cr.execute("""
DO
$do$
BEGIN
    IF NOT EXISTS (
        SELECT FROM pg_catalog.pg_roles
        WHERE  rolname = 'postbio') THEN
        create role postbio login password 'root';
    END IF;
    GRANT SELECT, INSERT, UPDATE, DELETE
        ON hr_bioclock_logs 
            TO postbio;
    GRANT USAGE, SELECT 
        ON SEQUENCE hr_bioclock_logs_id_seq 
            TO postbio;
END
$do$;
        """)

    def fetch_alpeta_logs(self, date_from=False, employee_id=False):
        self._cron_fetch_alpeta_logs(date_from=date_from)
        self._cron_convert_bioclock_logs()
        self._cron_analyze_raw_logs()
        
    def _cron_fetch_alpeta_logs(self, date_from=False, employee_id=False):
        host = self.env['ir.config_parameter'].sudo().get_param('alpeta.host')
        if host:
            user = self.env['ir.config_parameter'].sudo().get_param('alpeta.user')
            passwd = self.env['ir.config_parameter'].sudo().get_param('alpeta.passwd') 
            db = self.env['ir.config_parameter'].sudo().get_param('alpeta.db')

            mydb = mysql.connector.connect(host=host,user=user,password=passwd, database=db)
        else:
            mydb = mysql.connector.connect(host="51.79.206.91",user="odoo",password="root", database='ucdb')
        
        start_event_time =  datetime.now().strftime('%Y-%m-01 00:00:00')
        end_event_time =  datetime.now().strftime('%Y-%m-%d 23:59:59')
        limit_date = datetime.strptime(datetime.now().strftime('%Y-%m-%d'),'%Y-%m-%d')
        yy = int(end_event_time[:4])
        mm = int(end_event_time[5:7])
        dd = int(end_event_time[8:10])
        
        print('fetching logs... 3')
        if date_from:
            test_date = date_from.strftime('%Y-%m-01')
            test_date = datetime.strptime(test_date,'%Y-%m-%d')
            while test_date <= limit_date:
                print('Creating timesheet... %s' % test_date.strftime('%Y-%m-%d'))
                self.env['attendance.timesheet'].sudo().create_timesheet(date=test_date)
                test_date = test_date+relativedelta(months=1)
            start_event_time = datetime.strftime(date_from,'%Y-%m-%d %H:%M:%S')
        
        found = self.search([],order='punching_time desc', limit=1)
        
        if not date_from and found:
            start_event_time = found.punching_time.strftime('%Y-%m-%d %H:%M:%S')

        yy = int(start_event_time[:4])
        mm = int(start_event_time[5:7])

        if date_from and found:
            end_event_time = found.punching_time.strftime('%Y-%m-%d %H:%M:%S')

        print('event_time',start_event_time, end_event_time)
        
        mycursor = mydb.cursor()
        test_date = '%s-%s-01' % (str(yy),str(mm).rjust(2,'0'))
        test_date = datetime.strptime(test_date,'%Y-%m-%d')

        while test_date <= limit_date:  
            res = False
            if employee_id:
                res = self.env['hr.bioclock.user'].sudo().search([('employee_id','=',employee_id)], order='id desc', limit=1)
                user_id = res.device_id
                sql = """select user_id, event_time from auth_logs_%s%s where user_id = %s and event_time between '%s' and '%s';""" % (str(yy),str(mm).rjust(2,'0'), user_id, start_event_time, end_event_time)
            else:
                sql = """select user_id, event_time from auth_logs_%s%s where event_time between '%s' and '%s';""" % (str(yy),str(mm).rjust(2,'0'), start_event_time, end_event_time)

            res = False
            try:
                req = mycursor.execute(sql)
                res = mycursor.fetchall()
            except:
                print("An exception occurred")
            
            if res:
                for rec in res:
                    print(rec)
                    device_id = rec[0]
                    punching_time_text = rec[1].strftime('%Y-%m-%d %H:%M:%S')

                    sql = """select * from hr_bioclock_logs where device_id = '%s' and punching_time = '%s'""" % (device_id, punching_time_text)
                    self.env.cr.execute(sql)
                    found = self.env.cr.fetchall()
                    if found:
                        print('bioclock log exists', found)
                        continue

                    sql = """insert into hr_bioclock_logs (device_id, punching_time, punch_type) values('%s','%s','O')""" % (device_id, punching_time_text)                
                    try:
                        print('bioclock log inserted', device_id, punching_time_text)
                        self.env.cr.execute(sql)
                    except:
                        print("An exception occurred")
                    else:
                        self.env.cr.commit()
                    print(sql)
                    print('xxx_logs_xx',device_id, punching_time_text)
            mm += 1
            if mm > 12:
                mm = 1
                yy += 1
            test_date = '%s-%s-01' % (str(yy),str(mm).rjust(2,'0'))
            test_date = datetime.strptime(test_date,'%Y-%m-%d') 
        mycursor.close()
        mydb.close()
        
    def _cron_analyze_bioclock_logs(self):
        tz = self.env.user.tz
        if self.env.user.tz:
            tz = pytz.timezone(self.env.user.tz)
        current_user = False
        prev_log = False
        delay = 15
        att_logs = self.search([('attendance_id','=',False),('punch_type','!=','7')], order='punching_time asc', limit=1000)
        for att in att_logs:
            device_id = att.device_id
            biometric_time = att.biometric_time 
            punching_time = att.punching_time 
            bioclock_log_id = att.id
            found = att_logs.filtered(lambda r: r.id<bioclock_log_id and r.device_id == device_id and r.punching_time == punching_time)
            if found:
                att.punch_type = '7' # Trash
                self.env.cr.commit()
                continue
            found = self.search([('attendance_id','!=',False),('punch_type','in',['0','1']),('id','<',bioclock_log_id),('device_id','=',device_id),('punching_time','=',punching_time)])
            if found:
                att.punch_type = '7' # Trash
                self.env.cr.commit()
                continue
            if current_user != device_id:
                current_user = device_id
                prev_log = False
            fmt = "%Y-%m-%d %H:%M:%S"
            # Current time in UTC
            now_utc = datetime.now(timezone('UTC'))
            # Convert to current user time zone
            now_timezone = now_utc.astimezone(tz)
            punching_time_text = att.punching_time_text
            if not punching_time_text and att.punching_time:
                YY = att.punching_time.strftime('%Y')
                mm = att.punching_time.strftime('%m')
                dd = att.punching_time.strftime('%d')
                HH = att.punching_time.strftime('%H')
                MM = att.punching_time.strftime('%M')
                SS = att.punching_time.strftime('%S')
                punching_time_text = '%s-%s-%s %s:%s:%s' % (YY, mm, dd, HH, MM, SS)
                punching_time_text = att.punching_time_text
            if punching_time_text and not att.biometric_time:
                UTC_OFFSET_TIMEDELTA = datetime.strptime(now_utc.strftime(fmt), fmt) - datetime.strptime(now_timezone.strftime(fmt), fmt)
                local_datetime = datetime.strptime(punching_time_text, fmt)
                biometric_time = local_datetime + UTC_OFFSET_TIMEDELTA
                att.biometric_time = biometric_time
                self.env.cr.commit()
                trashtime = biometric_time+relativedelta(seconds=delay)
                found = self.search([('id','>',att.id),('device_id','=',device_id),('biometric_time','>=',biometric_time),('biometric_time','<=',trashtime)])
                if found:
                    att.punch_type = '7' # Trash
                    biometric_time = False
                    self.env.cr.commit()
            if biometric_time and prev_log:
                if biometric_time.strftime('%Y-%m-%d %H:%M') == prev_log.strftime('%Y-%m-%d %H:%M'):
                    att.punch_type = '7' # 
                    biometric_time = False
                elif biometric_time <= (prev_log+timedelta(seconds=delay)):
                    att.punch_type = '7' # Trash
                    prev_log = biometric_time
                    biometric_time = False
                self.env.cr.commit()
            if not biometric_time: 
                continue
            prev_log = biometric_time
            users = self.env['hr.bioclock.user'].search([('employee_id','!=',False),('device_id','=',device_id),('active','=',True)])
            if not users:
                found = self.env['hr.bioclock.user'].search([('device_id','=',device_id)], limit=1)
                if not found:
                    try:
                        res = self.env['hr.bioclock.user'].create({'device_id': device_id,
                        'name': 'New User %s' % device_id})
                        self.env.cr.commit()
                    except:
                        print('New User Already Exists.')
                continue
            
            employee_id = users.employee_id.id
            
            found = self.env['hr.attendance'].search([('employee_id','=',employee_id),('check_out','=',False)], limit = 1,order='id desc')
            att.employee_id = employee_id
            self.env.cr.commit()
            if found:
                date1 = pytz.utc.localize(found.check_in).astimezone(tz)
                date2 = pytz.utc.localize(att.biometric_time).astimezone(tz)
                if date1.date() != date2.date():
                    found = False
            if not found:
                res = self.env['hr.attendance'].create({
                    'employee_id': employee_id,
                    'check_in': biometric_time,
                    'biometric_log_id': att.id,
                })
                att.attendance_id = res
                att.punch_type = '0'
                self.env.cr.commit()
            else:
                found.check_out  = biometric_time
                found.biometric_log_out_id = att.id
                self.env.cr.commit()
                att.punch_type = '1'
                att.attendance_id = found.id
                self.env.cr.commit()

    def _cron_analyze_raw_logs(self):
        tz = self.env.user.tz
        if self.env.user.tz:
            tz = pytz.timezone(self.env.user.tz)
        current_user = False
        prev_log = False
        delay = 15
        att_logs = self.search([('attendance_id','=',False),('employee_id','!=',False),('biometric_time','!=',False),('punch_type','in',[False,'X','O'])], order='device_id, biometric_time asc')
        for att in att_logs:
            device_id = att.device_id
            biometric_time = att.biometric_time 
            bioclock_log_id = att.id
            found = att_logs.filtered(lambda r: r.id<bioclock_log_id and r.device_id == device_id and r.biometric_time == biometric_time)
            if found: #Trash if duplicate
                att.punch_type = '7' # Trash
                self.env.cr.commit()
                continue
            found = self.search([('attendance_id','!=',False),('punch_type','in',['0','1']),('id','<',bioclock_log_id),('device_id','=',device_id),('biometric_time','=',biometric_time)])
            if found: #Trash if Already Exists
                att.punch_type = '7' # Trash
                self.env.cr.commit()
                continue
            if current_user != device_id:
                current_user = device_id
                prev_log = False
            fmt = "%Y-%m-%d %H:%M:%S"
            # Current time in UTC
            if biometric_time:
                trashtime = biometric_time+relativedelta(seconds=delay)
                found = self.search([('id','>',bioclock_log_id),('device_id','=',device_id),('biometric_time','>=',biometric_time),('biometric_time','<=',trashtime)])
                if found: # Trash if invalid logs/duplicate punch for less than or 15 seconds delay
                    att.punch_type = '7' # Trash
                    biometric_time = False
                    self.env.cr.commit()
            if biometric_time and prev_log:
                if biometric_time.strftime('%Y-%m-%d %H:%M') == prev_log.strftime('%Y-%m-%d %H:%M'): #Trash if duplicate
                    att.punch_type = '7' # 
                    biometric_time = False
                elif biometric_time <= (prev_log+timedelta(seconds=delay)): #Trash if 15 seconds diferrence
                    att.punch_type = '7' # Trash
                    prev_log = biometric_time
                    biometric_time = False
                self.env.cr.commit()
            if not biometric_time or not att.employee_id: 
                continue
            employee_id = att.employee_id.id
            # Check Attendance
            found = self.env['hr.attendance'].search([('employee_id','=',employee_id),('check_in','<',biometric_time),('check_out','=',False)],order='id desc', limit = 1)
            if found:
                date1 = pytz.utc.localize(found.check_in).astimezone(tz)
                date2 = pytz.utc.localize(att.biometric_time).astimezone(tz)
                if date1.date() != date2.date():
                    found = False
            if not found:
                res = self.env['hr.attendance'].create({
                    'employee_id': employee_id,
                    'check_in': biometric_time,
                    'biometric_log_id': bioclock_log_id,
                })
                self.env.cr.commit()
                att.attendance_id = res
                att.punch_type = '0' # Checkin
                self.env.cr.commit()
            else:
                found.check_out  = biometric_time
                found.biometric_log_out_id = bioclock_log_id
                self.env.cr.commit()
                att.punch_type = '1' # Checkout
                att.attendance_id = found.id
                self.env.cr.commit()

    def convert_trash_to_raw_logs(self):
        for rec in self:
            if rec.punch_type == '7':
                rec.punch_type = 'O'
        self._cron_analyze_raw_logs()
        
    def _cron_convert_bioclock_logs(self):
        tz = self.env.user.tz
        if self.env.user.tz:
            tz = pytz.timezone(self.env.user.tz)
        delay = 15
        att_logs = self.search([('attendance_id','=',False),('biometric_time','=',False),('punch_type','in',[False,'X','O'])], order='punching_time asc')
        for att in att_logs:
            device_id = att.device_id
            biometric_time = att.biometric_time 
            fmt = "%Y-%m-%d %H:%M:%S"
            # Current time in UTC
            now_utc = datetime.now(timezone('UTC'))
            # Convert to current user time zone
            now_timezone = now_utc.astimezone(tz)
            punching_time_text = att.punching_time_text
            if not punching_time_text and att.punching_time:
                YY = att.punching_time.strftime('%Y')
                mm = att.punching_time.strftime('%m')
                dd = att.punching_time.strftime('%d')
                HH = att.punching_time.strftime('%H')
                MM = att.punching_time.strftime('%M')
                SS = att.punching_time.strftime('%S')
                punching_time_text = '%s-%s-%s %s:%s:%s' % (YY, mm, dd, HH, MM, SS)
                att.punching_time_text = punching_time_text
            if punching_time_text and not att.biometric_time:
                UTC_OFFSET_TIMEDELTA = datetime.strptime(now_utc.strftime(fmt), fmt) - datetime.strptime(now_timezone.strftime(fmt), fmt)
                local_datetime = datetime.strptime(punching_time_text, fmt)
                biometric_time = local_datetime + UTC_OFFSET_TIMEDELTA
                att.biometric_time = biometric_time
                self.env.cr.commit()
            self.env.cr.commit()
            users = self.env['hr.bioclock.user'].search([('employee_id','!=',False),('device_id','=',device_id),('active','=',True)])
            if not users:
                found = self.env['hr.bioclock.user'].search([('device_id','=',device_id)], limit=1)
                if not found:
                    try:
                        res = self.env['hr.bioclock.user'].create({'device_id': device_id,
                        'name': 'New User %s' % device_id})
                        self.env.cr.commit()
                    except:
                        print('New User Already Exists.')
                continue
            if not att.employee_id:
                att.employee_id = users.employee_id.id
                self.env.cr.commit()
                
        # Tag Employee
        device_id = False
        users = False
        att_logs = self.search([('employee_id','=',False)], order='device_id desc, punching_time desc')
        for att in att_logs:
            if device_id != att.device_id or not users:
                device_id = att.device_id
                users = self.env['hr.bioclock.user'].search([('employee_id','!=',False),('device_id','=',device_id),('active','=',True)], limit=1)
            if users:
                att.employee_id = users.employee_id.id
                self.env.cr.commit()

    def validate_trash_logs(self):
        if self.punch_type != '7':
            return 0
        # print('trash logs')
        # tz = pytz.timezone(self.env.user.tz)
        for att in self:
            biometric_time = att.biometric_time
            device_id = att.device_id
            yy = biometric_time.year
            mm = biometric_time.month
            dd = biometric_time.day
            limit_datetime = datetime(yy,mm,dd,23,59)
            if att.employee_id:
                employee_id = att.employee_id.id
            else:
                users = self.env['hr.bioclock.user'].search([('employee_id','!=',False),('device_id','=',device_id),('active','=',True)])
                if not users:
                    continue
                employee_id = users.employee_id.id
            found = self.env['hr.attendance'].search([('employee_id','=',employee_id),('check_in','>',biometric_time),('check_in','<=',limit_datetime),('check_out','=',False)], limit = 1, order='id asc')
            if found:
                if found.attendance_timesheet_id.attendance_timesheet_id.state != 'draft':
                    raise ValidationError('Timesheet is already Approved.')
                att.employee_id = employee_id
                found.write({'check_in':biometric_time,'check_out': found.check_in, 'biometric_log_id': att.id})
                found.biometric_log_id.punch_type = '1'
                att.attendance_id = found.id
                att.punch_type = '0'
                # self.env.cr.commit()
                # found._update_timesheet_attendance() 
            else:
                raise ValidationError('Invalid, logs is incomplete.')
                
        
    def _cron_validate_trash_logs(self):
        # print('trash logs')
        tz = pytz.timezone(self.env.user.tz)
        now = fields.datetime.now()
        yy = now.year
        mm = now.month
        dd = now.day
        date_end = datetime(yy,mm,dd,23,59,59)
        before = fields.datetime.now()-timedelta(days=15)
        yy = before.year
        mm = before.month
        dd = before.day
        date_start = datetime(yy,mm,dd,0,0,0)
        for att in self.search([('punch_type','=','7'),('attendance_id','=',False),('biometric_time','>=',date_start),('biometric_time','<=',date_end)],order='device_id, biometric_time desc', limit=3000):
            biometric_time = att.biometric_time
            device_id = att.device_id
            yy = biometric_time.year
            mm = biometric_time.month
            dd = biometric_time.day
            limit_datetime = datetime(yy,mm,dd,23,59)
            bioclock_log_id = att.id
            found = self.search([('attendance_id','!=',False),('id','!=',bioclock_log_id),('device_id','=',device_id),('biometric_time','=',biometric_time)])
            if found:
                continue
            if att.employee_id:
                employee_id = att.employee_id.id
            else:
                users = self.env['hr.bioclock.user'].search([('employee_id','!=',False),('device_id','=',device_id),('active','=',True)])
                if not users:
                    continue
                employee_id = users.employee_id.id
            found = self.env['hr.attendance'].search([('employee_id','=',employee_id),('check_in','>',biometric_time),('check_in','<=',limit_datetime),('check_out','=',False)], limit = 1, order='id asc')
            if found:
                if found.attendance_timesheet_id.attendance_timesheet_id.state != 'draft':
                    # print('Timesheet %s is already Approved.' % (found.attendance_timesheet_id.attendance_timesheet_id.name))
                    continue
                att.employee_id = employee_id
                found.write({'check_in':biometric_time,'check_out': found.check_in, 'biometric_log_id': att.id})
                found.biometric_log_id.punch_type = '1'
                att.attendance_id = found.id
                att.punch_type = '0'
                self.env.cr.commit()
            else:
                att.employee_id = employee_id
                self.env.cr.commit()

        expiry_date = fields.datetime.now()-timedelta(days=90)
        expired_logs = self.search([('punch_type','=','7'),('attendance_id','=',False),('biometric_time','=',False),('punching_time','<',expiry_date)], order='punching_time asc', limit=3000)
        for trash_log in expired_logs:
            trash_log.unlink() # Delete Trash Logs

class HrAttendance(models.Model):
    _inherit = 'hr.attendance'
    
    biometric_log_id = fields.Many2one(comodel_name='hr.bioclock.logs', string='Biometric Log (Check In)', readonly=True, index=True, ondelete='set null')
    checkout_type = fields.Selection(string='Check In Type', selection=[('bio', 'Biometric'), ('web', 'Web Login'),], 
        default='web', compute="_compute_log_type")
    biometric_log_out_id = fields.Many2one(comodel_name='hr.bioclock.logs', string='Biometric Log (Check Out)', readonly=True, index=True, ondelete='set null')
    checkin_type = fields.Selection(string='Check Out Type', selection=[('bio', 'Biometric'), ('web', 'Web Login'),], 
        default='web', compute="_compute_log_type")
    
    def init(self):
        self._cr.execute("""
        update hr_attendance a set biometric_log_out_id = b.id
        from hr_bioclock_logs b
            where a.employee_id = b.employee_id 
            and b.biometric_time = a.check_out
            and a.biometric_log_out_id isnull;
        update hr_attendance a set biometric_log_id = b.id
        from hr_bioclock_logs b
            where a.employee_id = b.employee_id 
            and b.biometric_time = a.check_in
            and a.biometric_log_id isnull;
        """)
    
    @api.depends('biometric_log_id','biometric_log_out_id')
    def _compute_log_type(self):
        for rec in self:
            rec.checkin_type = 'bio' if rec.biometric_log_id else 'web'
            rec.checkout_type = 'bio' if rec.biometric_log_out_id else 'web'

    @api.constrains('check_in', 'check_out', 'employee_id', 'biometric_log_id')
    def _check_validity(self):
        """Overide Check Validity to allow attendance without checkout."""
        if self.biometric_log_id:
            return True
        else: # Work From Home
            rec = allowed = False
            print('employee_id',self.employee_id)
            employee_id = self.employee_id.id
            if self.check_out:
                weekday = self.check_out.strftime('%a').lower()
                attendance_date = self.check_out.strftime('%Y-%m-%d')
            else:
                weekday = self.check_in.strftime('%a').lower()
                attendance_date = self.check_in.strftime('%Y-%m-%d')
            print('employee_id',employee_id)
            found = self.env['hr.attendance.web'].sudo().search([('employee_id','=',employee_id),'|'
                                                                 ,('date_start','<=',attendance_date),('date_end','=',False),('date_start','<=',attendance_date)],order='date_start desc')
            
            for rec in found:
                if weekday == 'sun' and rec.sun:
                    allowed = True
                    print('sun', rec.sun)
                elif weekday == 'mon' and rec.mon:
                    allowed = True
                    print('mon', rec.mon)
                elif weekday == 'tue' and rec.tue:
                    allowed = True
                    print('tue', rec.tue)
                elif weekday == 'wed' and rec.wed:
                    allowed = True
                    print('wed', rec.wed)
                elif weekday == 'thu' and rec.thu:
                    allowed = True
                    print('thu', rec.thu)
                elif weekday == 'fri' and rec.fri:
                    allowed = True
                    print('fri', rec.fri)
                elif weekday == 'sat' and rec.sat:
                    allowed = True
                    print('sat', rec.sat)
            print('Check IN/OUT', attendance_date, weekday, allowed)            
            if not allowed:
                raise ValidationError('You are not allowed to "Check In/Check Out" via Web.')
            
            context = self._context
            return super(HrAttendance, self.with_context(context))._check_validity()
