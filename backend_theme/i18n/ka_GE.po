msgid ""
msgstr ""
"Project-Id-Version: openeducat11enttranslation\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-12 06:14+0000\n"
"PO-Revision-Date: 2018-03-06 00:45-0500\n"
"Last-Translator: aakash.kodwani <aakash@techreceptives.com>\n"
"Language-Team: Georgian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: openeducat11enttranslation\n"
"X-Crowdin-Language: ka\n"
"X-Crowdin-File: /v11_crowdin/backend_theme/i18n/backend_theme.pot\n"
"Language: ka_GE\n"

#. module: backend_theme
#: model:ir.ui.view,arch_db:backend_theme.home_page
msgid "<span class=\"oe_topbar_name\" style=\"color:#fff;\">Menu</span>\n"
"                        <b class=\"caret\" style=\"color:#fff;\"/>"
msgstr ""

#. module: backend_theme
#: model:ir.ui.view,arch_db:backend_theme.home_page
msgid "About"
msgstr ""

#. module: backend_theme
#: model:ir.ui.view,arch_db:backend_theme.webclient_bootstrap
msgid "Apps"
msgstr ""

#. module: backend_theme
#: model:ir.ui.view,arch_db:backend_theme.home_page
msgid "Documentation"
msgstr ""

#. module: backend_theme
#: model:ir.ui.view,arch_db:backend_theme.home_page
msgid "Log out"
msgstr ""

#. module: backend_theme
#: model:ir.ui.view,arch_db:backend_theme.home_page
msgid "My Odoo.com account"
msgstr ""

#. module: backend_theme
#: model:ir.ui.view,arch_db:backend_theme.webclient_bootstrap
msgid "Search..."
msgstr ""

#. module: backend_theme
#: model:ir.ui.view,arch_db:backend_theme.home_page
msgid "Support"
msgstr ""

