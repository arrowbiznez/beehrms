# -*- coding: utf-8 -*-
{
    'name': "HR Salary Grade",

    'summary': """
        Salary Grade""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Daryll Gay S. Bangoy",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['hr','hr_contract','hr_payroll_community','base_automation'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/hr_salary_grade.xml',
        'views/hr_salary_grade.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
