# -*- coding: utf-8 -*-

# from os import PRIO_PGRP
import re
from odoo import models, fields, api, _
from odoo.exceptions import Warning, ValidationError
import pytz
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from calendar import monthrange

steps = [
    ('1', 'Step 1'), ('2', 'Step 2'),
    ('3', 'Step 3'), ('4', 'Step 4'),
    ('5', 'Step 5'), ('6', 'Step 6'),
    ('7', 'Step 7'), ('8', 'Step 8')]

class HRSalaryGrade(models.Model):
    _name = 'hr.salary.grade'
    _inherit = 'mail.thread'
    _description = 'HR Salary Grade'
    
    name = fields.Char(string='Salary Grade', required=True, track_visibility="onchange")
    active = fields.Boolean(string='Active', default=True, track_visibility="onchange")
    
    @api.constrains('name')
    def _check_name(self):
        name = self.name
        id = self.id
        found = self.search([('name','=',name),('id','!=',id)])
        if found:
            raise ValidationError("Salary Grade Already Exists.")

    
class HRSalaryMatrixAdjustments(models.Model):
    _name = 'hr.salary.matrix.adjustments'
    _order ='effectivity_date desc, salary'
    _description = 'HR Salary Matrix Adjustments'
      
    salary_grade = fields.Many2one(comodel_name='hr.salary.matrix', string='Salary Matrix')
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 related='salary_grade.company_id')
    grade = fields.Many2one(comodel_name='hr.salary.grade', string='Grade', track_visibility="onchange", related='salary_grade.grade', store=True)
    step = fields.Selection(string='Step', selection=steps, default='1', track_visibility="onchange", related='salary_grade.step', store=True)
    name = fields.Char(string='Description', track_visibility="onchange", required=False)
    effectivity_date = fields.Date(string='Effectivity Date', required=True, track_visibility="onchange", default=lambda self: fields.datetime.now(), readonly=False)
    salary = fields.Monetary(string='Salary',  track_visibility="onchange", help="Salary Adjustmet.")
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id)
    # active = fields.Boolean(string='Active', default=True, store=True)  
    
    @api.model
    def create(self, values):
        datenow = datetime.now().date()
        effectivity_date = values.get('effectivity_date') or datenow.strftime('%Y-%m-%d')
        salary = values.get('salary')
        salary_grade = values.get('salary_grade')
        ret = super(HRSalaryMatrixAdjustments, self).create(values)
        if ret:
            if datetime.strptime(effectivity_date,'%Y-%m-%d').date() <= datetime.now().date():
                found = self.env['hr.salary.matrix'].browse(salary_grade)
                if found:
                    found._compute_salary_ids()
        return ret
    
    # @api.multi
    def write(self, values):
        effectivity_date = values.get('effectivity_date') or datetime.strftime(self.effectivity_date,'%Y-%m-%d')
        salary = values.get('salary') or self.salary
        salary_grade = values.get('salary_grade') or self.salary_grade.id
        ret = super(HRSalaryMatrixAdjustments, self).write(values)
        if ret:
            if datetime.strptime(effectivity_date,'%Y-%m-%d').date() <= datetime.now().date():
                found = self.env['hr.salary.matrix'].browse(salary_grade)
                if found:
                    found._compute_salary_ids()
        return ret
    
    
    @api.depends('effectivity_date')
    def _compute_salary_grade(self):
        res = {}
        vals = []
        name = []
        effectivity_date = self.effectivity_date
        for rec in self.env['hr.salary.matrix'].sudo().search([('active','=',True),('effectivity_date','<=',effectivity_date)],order='effectivity_date desc, name asc'):
            if rec.name in name:
                continue
            name.append(rec.name)
            vals.append(rec.id)
        res['domain'] = {'salary_grade': [('id', 'in', vals)]}
        return res
    
    
    @api.onchange('salary_grade')
    def _onchange_salary_grade(self):
        if not self.salary:
            self.salary = self.salary_grade.salary
    
    
    @api.constrains('salary')
    def validate_salary(self):
        if self.salary < 1:
            raise ValidationError("Salary must be greater than zero (0).")
        id = self.id
        salary_grade = self.salary_grade.id
        effectivity_date = self.effectivity_date
        exists = self.search([('id','!=',id),('salary_grade','=',salary_grade),('effectivity_date','=',effectivity_date)])
        if exists:
            raise ValidationError("Salary grade adjustments with same effectivity date already exists.")
            
class HRSalaryMatrix(models.Model):
    _name = 'hr.salary.matrix'
    _inherit = 'mail.thread'
    _order ='effectivity_date desc, grade, step'
    _description = 'HR Salary Matrix'
    
    salary_ids = fields.One2many(comodel_name='hr.salary.matrix.adjustments', inverse_name='salary_grade', string='Salary Adjustments')
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id, track_visibility="onchange")
    name = fields.Char(string='Description', track_visibility="onchange", compute="_compute_name", store=True)
    effectivity_date = fields.Date(string='Effectivity Date', required=True, track_visibility="onchange", default=lambda self: fields.datetime.now())
    adjustment_date = fields.Date(string='Adjustment Date', compute='_compute_salary_ids')
    grade = fields.Many2one(comodel_name='hr.salary.grade', string='Grade', required=True, track_visibility="onchange")
    step = fields.Selection(string='Step', 
    selection=steps, default=1, required=True, track_visibility="onchange")
    salary = fields.Monetary(string='Salary',  track_visibility="onchange", store=True, help="Salary of Employee base on grade and step.")
    wage = fields.Monetary(string='Wage',  track_visibility="onchange", compute='_compute_salary_ids', help="Salary of Employee base on grade and step.", store=False)
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id)
    active = fields.Boolean(string='Active', default=True, store=True)
    contract_ids = fields.One2many(comodel_name='hr.contract', inverse_name='salary_grade', string='contracts')
    
    
    
    def cron_check_salary_grade(self):
        for rec in self.search([('active','=',True)]):
            salary_grade = rec.id
            salary = wage = rec.salary
            adjustment_date = False
            date_from = False
            datenow = datetime.now().date()
            for mat in rec.salary_ids.filtered(lambda r: r.effectivity_date <= datenow).sorted(key=lambda r: r.effectivity_date, reverse=True):
                if mat.effectivity_date <= datenow:
                    wage = mat.salary
                    adjustment_date = mat.effectivity_date
                    break
            rec.adjustment_date = adjustment_date
            data = []
            # update Contract and salary Differential
            if salary != wage:
                rec.salary = wage
                for contract in rec.contract_ids:
                    salary_diff = wage - contract.wage
                    contract.wage = wage
                    contract_id = contract.id
                    adj_date = date(rec.effectivity_date.year,1,1)
                    payslips = self.env['hr.payslip'].sudo().search([('contract_id','=',contract_id),('date_from','>=',adj_date),('state','=','done')])
                    ctr = 0
                    for payslip in payslips:
                        if ctr == 0:
                            date_from = payslip.date_from
                        date_to = payslip.date_to
                        ctr += 1
                    salary_diff = salary_diff #*ctr
                    print('salary_diff', salary_diff)
                    if salary_diff != 0.00 and not date_from:
                        payroll_date = date(fields.datetime.now().year,fields.datetime.now().month, monthrange(fields.datetime.now().year,fields.datetime.now().month)[1])+relativedelta(months=1)
                        data.append({
                                'contract_id': contract_id,
                                'date_start': date_from,
                                'date_end': date_to,
                                'payroll_date': payroll_date,
                                'amount': salary_diff,
                                })
            if data:      
                print('data', data)
                self.env['hr.payslip.adjustment.sdiff'].sudo().create(data)
    
    # @api.depends('salary_ids')
    def _compute_salary_ids(self):
        for rec in self:
            salary = wage = rec.salary
            adjustment_date = False
            datenow = datetime.now().date()
            for mat in rec.salary_ids.filtered(lambda r: r.effectivity_date <= datenow).sorted(key=lambda r: r.effectivity_date, reverse=True):
                if mat.effectivity_date <= datenow:
                    wage = mat.salary
                    adjustment_date = mat.effectivity_date
                    break
            rec.wage = wage
            rec.adjustment_date = adjustment_date
            # if salary != wage:
            #     rec.salary = wage
            #     for contract in rec.contract_ids:
            #         contract.wage = wage
                    
    
    # @api.depends('wage')
    # def _compute_wage(self):
    #     print('wage', self.salary, self.wage)
    #     if self.salary != self.wage:
    #         self.salary = self.wage
            
    # def write(self, values):
    #     salary_grade = self.id
    #     ret = super(HRSalaryMatrix, self).write(values)
    #     if ret:
    #         contracts = self.env['hr.contract'].sudo().search([('salary_grade','=',salary_grade),('state','=','open')])
    #         if contracts:
    #             data = ()
    #             for contract in contracts:
    #                 print('contract',salary_grade,contract.name, self.salary, self.wage, contract.wage)
    #                 contract.wage = self.salary
    #                 contract_id = contract.id
    #                 salary_diff = self.salary - contract.wage
    #                 adj_date = date(self.effectivity_date.year,1,1)
    #                 payslips = self.env['hr.payslip'].sudo().search([('contract_id','=',contract_id),('date_from','>=',adj_date),('state','=','done')])
    #                 ctr = 0
    #                 for payslip in payslips:
    #                     if ctr == 0:
    #                         date_from = payslip.date_from
    #                     date_to = payslip.date_to
    #                     ctr += 1
    #                 salary_diff = salary_diff*ctr
    #                 print('salary_diff', salary_diff)
    #                 if salary_diff != 0.00:
    #                     payroll_date = date(fields.datetime.now().year,fields.datetime.now().month, monthrange(fields.datetime.now().year,fields.datetime.now().month)[1])+relativedelta(months=1)
    #                     data.append({
    #                             'contract_id': contract_id,
    #                             'date_start': date_from,
    #                             'date_to': date_to,
    #                             'payroll_date': payroll_date,
    #                             'amount': salary_diff,
    #                             })
    #             if data:      
    #                 print('data', data)
    #                 self.env['hr.payslip.adjustment.sdiff'].sudo().create(data)
                        
        # return ret
        
    
    @api.depends('grade','step')
    def _compute_name(self):
    
        step = self.step
        step_name = ''
        for rec in steps:
            if step == rec[0]:
                step_name = rec[1]
                break
        name = '%s-%s' % (self.grade.name, step_name)
        self.name = name
        pass
    
    @api.constrains('effectivity_date', 'grade', 'step', 'company_id')
    def validate_matrix(self):
        company_id = self.company_id.id
        effectivity_date = self.effectivity_date
        grade = self.grade.id
        step = self.step
        id = self.id
        rec = self.search([('effectivity_date', '=', effectivity_date),
                                   ('grade', '=', grade),
                                   ('step', '=', step),
                                   ('company_id', '=', company_id),
                                   ('id', '!=', id),
                                   ])

        if rec:
            raise ValidationError("Salary Matrix you defined is already exists.")
                     
                     
class HRJob(models.Model):
    _inherit = 'hr.job'
    
    job_grade = fields.Many2one(comodel_name='hr.salary.grade', string='Salary Grade', domain="[('active','=',True)]", track_visibility="onchange")
    struct_id  = fields.Many2one(comodel_name='hr.payroll.structure', string='Salary Structure')
    
class HRContract(models.Model):
    _inherit = 'hr.contract'
    # salary_step = fields.Selection(string='Salary Step', tracking=True,
    # selection=steps, default="1", required=True)
    
    job_grade = fields.Many2one(comodel_name='hr.salary.grade', string='Grade', related="job_id.job_grade", 
    domain="[('active','=',True)]", tracking=False)
        
    salary_grade = fields.Many2one(comodel_name='hr.salary.matrix', string='Salary Grade', 
    domain="[('grade','=',job_grade),('active','=',True)]")
    
    salary_step = fields.Selection(string='Step', tracking=False,
        selection=steps, related="salary_grade.step")
        
    adjustment_date = fields.Date(string='Adjustment Date', related="salary_grade.adjustment_date")
    
    next_incentive_date = fields.Date(string='Next Date of Salary Step Upgrade', compute='_compute_next_incentive_date', store=False)
    
    prev_incentive_date = fields.Date(string='Previous Date of Salary Step Upgrade', compute='_compute_next_incentive_date', store=False)

    salary_grade_ids = fields.One2many('hr.contract.salary.grade', 'contract_id', string='Salary Grade History')
    
    def init(self):
        self.env.cr.execute("""with sal1 as (select id as new_id, name
                    from hr_salary_matrix
                    where active = True),
                    sal2 as (select id as old_id, name
                    from hr_salary_matrix
                    where active = False),
                    sal3 as (select new_id, a.name, old_id from sal1 a inner join sal2 b on a.name = b.name)
                    update hr_contract a
                    set salary_grade = m.new_id
                    from sal3 m
                    where a.salary_grade = m.old_id and a.salary_grade != m.new_id
                    and a.state = 'open';""")
        self.env.cr.commit()
        
    # @api.depends('contract_id.date_start')
    def _compute_next_incentive_date(self):
        for contract in self:
            if contract.state != 'open':
                continue
            if contract and contract.date_start:
                contract_start = contract.date_start
                start_date = fields.Date.from_string(contract.date_start.replace(day=1))
                start_yy = contract_start.year
                current_date = fields.Date.context_today(self)
                current_yy = current_date.year
                years_since_start = current_yy - start_yy
                # years_since_start = (current_date - start_date).days // 365
                next_incentive_years = ((years_since_start // 3) + 1) * 3
                next_incentive_date = start_date + timedelta(days=next_incentive_years * 365)
                next_incentive_date = start_date.replace(year=next_incentive_date.year)
                prev_incentive_date = next_incentive_date
                if next_incentive_date < current_date:
                    next_incentive_date = start_date.replace(year=next_incentive_date.year+3)
                if contract.date_end: 
                    if contract.date_end <= next_incentive_date:
                        continue 
                contract.next_incentive_date = next_incentive_date
                contract.prev_incentive_date = prev_incentive_date
                
    
    def cron_check_salary_grade_upgrade(self):
        incentive_date = fields.Date.context_today(self)+timedelta(days=1)
        for contract in self.search([('state','=','open')]):
            if contract.next_incentive_date:
                if incentive_date == contract.next_incentive_date:
                    grade = contract.salary_grade.grade.id
                    step = str(int(contract.salary_step)+1)
                    found = self.env['hr.salary.matrix'].sudo().search([('grade','=',grade),('step','=',step),('active','=',True)])
                    if found:
                        contract.salary_grade = found.id
                        
    def check_salary_grade_upgrade(self):
        incentive_date = fields.Date.context_today(self)+timedelta(days=1)
        start_date = fields.Date.context_today(self).replace(month=1,day=1)
        for contract in self.search([('state','=','open')]):
            if contract.prev_incentive_date:
                if contract.prev_incentive_date >= start_date and contract.prev_incentive_date <= incentive_date:
                    grade = contract.salary_grade.grade.id
                    step = str(int(contract.salary_step)+1)
                    found = self.env['hr.salary.matrix'].sudo().search([('grade','=',grade),('step','=',step),('active','=',True)])
                    if found:
                        contract.salary_grade = found.id
    
    # @api.onchange('job_grade','salary_step')
    # def _onchange_job_grade(self):
       
    #     res = self.env['hr.salary.matrix'].sudo().search([('grade','=',self.job_grade.id),('step','=',self.salary_step)],order='effectivity_date desc',limit=1)
    
    #     return {'domain':{'subtype': [('type','=',self.type)]}}
    
    @api.onchange('job_id')
    def _onchange_job_id(self):
        self.struct_id = self.job_id.struct_id.id or False
        
    @api.onchange('salary_grade')
    def _onchange_salary_grade(self):
        self.wage = self.salary_grade.salary
        
    @api.model
    def create(self, values):
    
        if values.get('salary_grade'):
            salary_grade = values['salary_grade']
            res = self.env['hr.salary.matrix'].browse(salary_grade)
            values['wage'] = res.salary
            
        return super(HRContract, self).create(values)
    
    def write(self, values):
        if values.get('salary_grade'):
            salary_grade = values['salary_grade']
            res = self.env['hr.salary.matrix'].browse(salary_grade)
            values['wage'] = res.salary
            
        return super(HRContract, self).write(values)
    
class HRContractSalaryGrade(models.Model):
    _name = 'hr.contract.salary.grade'
    _decription = 'HR Contract Salary Grade History'
    _order = 'effectivity_date desc'
    
    contract_id = fields.Many2one('hr.contract', string='Contract')
    effectivity_date = fields.Datetime('Effectivity Date', default=lambda self: fields.datetime.now())
    transaction_date = fields.Datetime('Transaction Date', default=lambda self: fields.datetime.now())
    salary_grade = fields.Many2one(comodel_name='hr.salary.matrix', string='Salary Matrix')
    wage = fields.Monetary(string='Wage', help="Salary of Employee base on grade and step.")
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id)
    
    def update_salary_grade_history(self, record_id):
        for contract in self.env['hr.contract'].browse(record_id):  
            data = {'contract_id': contract.id,
                    'salary_grade': contract.salary_grade.id,
                    'wage': contract.wage}
            
            if contract.next_incentive_date:
                if contract.next_incentive_date == fields.Date.context_today(self)+timedelta(days=1):
                    data['effectiviy_date'] = contract.next_incentive_date
            
            self.sudo().create(data)
    
        
        
        
    
    
    
    