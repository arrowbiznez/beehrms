# -*- coding: utf-8 -*-
{
    'name': "Philippine - Localization",
    'summary': """
        Philippine base location (source: http://www.philgis.org/):
        """,
    'description': """
        Long description of module's purpose
    """,
    'author': "Aurel Balanay - Evanscor Technology Solutions Inc.",
    'website': "http://www.evanscor.com",
    'category': 'Localization',
    'version': '0',
    'depends': ['base','mail','contacts'],
    'data': [
        'security/ir.model.access.csv',
        'data/res_country_state_data.xml',
        'data/res_state_city_data.xml',
       'data/res_city_barangay_data_batch_1.xml',
       'data/res_city_barangay_data_batch_1_5.xml',
       'data/res_city_barangay_data_batch_1_5_1.xml',
       'data/res_city_barangay_data_batch_1_5_2.xml',
       'data/res_city_barangay_data_batch_2.xml',
       'data/res_city_barangay_data_batch_2_5.xml',
       'data/res_city_barangay_data_batch_2_5_1.xml',
       'data/res_city_barangay_data_batch_2_5_2.xml',
       'data/res_city_barangay_data_batch_3.xml',
       'data/res_city_barangay_data_batch_3_5.xml',
       'data/res_city_barangay_data_batch_3_5_1.xml',
       'data/res_city_barangay_data_batch_3_5_2.xml',
      'data/res_city_barangay_data_batch_4.xml',
       'data/res_city_barangay_data_batch_4_5.xml',
       'data/res_city_barangay_data_batch_4_5_1.xml',
       'data/res_city_barangay_data_batch_4_5_2.xml',
        'data/local_dialect_data.xml',
        'views/ph_localization_views.xml',
        'views/menuitem_views.xml',
        'views/res_partner_views.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}