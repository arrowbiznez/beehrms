from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class localDialect(models.Model):
    _name = 'local.dialect'

    name = fields.Char(string='Name')
    
    @api.constrains('name')
    def _check_unique_name(self):
        names = self.search([]) - self
        value = [x.name.lower() for x in names]

        if self.name and self.name.lower() in value:
            raise ValidationError(_('Duplication of Dialect!'))