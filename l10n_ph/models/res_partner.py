from odoo import fields, models, api, tools

class ResPartner(models.Model):
    _inherit = 'res.partner'

    city_id = fields.Many2one('res.state.city',string="City",domain="[('state_id','=',state_id)]")
    barangay_id = fields.Many2one('res.city.barangay',string="Barangay",domain="[('city_id','=',city_id)]")
    country_code = fields.Char(related='country_id.code',string="Country Code")

    @api.onchange('city_id')
    def onchange_city_id(self):
        self.city = self.city_id.name

    def _get_res_state_city(self, city_id=None, barangay_id=None):
        city = self.env['res.state.city'].browse(city_id).name if city_id else False
        barangay = self.env['res.city.barangay'].browse(barangay_id).name if barangay_id else False
        return {
            'city': city,
            'barangay': barangay
        }

    @api.model
    def create(self, values):
        if 'city_id' in values:
            city_id = values['city_id']
            barangay_id = values['barangay_id']
            values['city'] = self._get_res_state_city(city_id)['city']
            values['street2'] = self._get_res_state_city(barangay_id)['barangay']
        return super(ResPartner, self).create(values)

    def write(self, values):
        if 'barangay_id' in values:
            barangay = self.env['res.city.barangay'].browse(values['barangay_id'])
            values['street2'] = barangay.name
        return super(ResPartner, self).write(values)