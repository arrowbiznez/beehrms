from odoo import api, fields, models, tools, _
from datetime import datetime, date
import calendar


class HRContracts(models.Model):
    _inherit = 'hr.contract'

    # basic wage
    @api.model
    def get_basic_wage(self, payslip, worked_days):
        date_from = datetime.strptime(payslip.date_from, '%Y-%m-%d').date()
        date_to = datetime.strptime(payslip.date_to, '%Y-%m-%d').date()
        emp_sched_pay = self.schedule_pay
        emp_paid_pay = self.paid_payroll
        result = 0.00
        days_per_year = self.company_id.partner_id.days_per_year
        if self.department_id:
            if self.department_id.inter_company_id:
                days_per_year = self.department_id.inter_company_id.partner_id.days_per_year
        else:
            days_per_year = self.company_id.partner_id.days_per_year
        date_start = datetime.strptime(self.date_start, '%Y-%m-%d').date()
        if self.employee_id.date_separated:
            date_separated = datetime.strptime(self.date_end, '%Y-%m-%d').date()
            if date_start <= date_from:
                if date_separated > date_from and date_separated < date_to:
                    number_of_days = self._get_number_of_days(days_per_year, date_from, date_separated)
                    salary = self.wage
                    result = ((salary * 12) / days_per_year) * number_of_days
        else:
            if date_start <= date_from:
                salary = self.wage
                if emp_paid_pay == 'daily':
                    result = (worked_days.WORK100.number_of_days * salary)
                else:
                    if emp_sched_pay == 'weekly':
                        result = round((salary * days_per_year) / 12, 2)
                    elif emp_sched_pay == 'monthly':
                        result = salary
                    elif emp_sched_pay == 'semi-monthly':
                        result = salary
            else:
                previous_contract = self.search([('id', '!=', self.id), ('employee_id', '=', self.employee_id.id)],
                                                order='date_start desc', limit=1)
                salary = previous_contract.wage
                if emp_paid_pay == 'daily':
                    if emp_sched_pay == 'weekly':
                        if worked_days:
                            result = round(salary * payslip.company_id.partner_id.days_per_year / 12, 2)
                    elif emp_sched_pay == 'semi-monthly':
                        result = salary * 15
                    elif emp_sched_pay == 'monthly':
                        result = salary * 30
                else:
                    if emp_sched_pay == 'monthly':
                        result = salary
                    elif emp_sched_pay == 'semi-monthly':
                        result = salary
        return round(result, 2)

    # contribution of employee sss/philhealth/pagibig
    def get_contribution(self, payslip, worked_days, schedpay, table):
        emp_sched_pay = self.schedule_pay
        emp_paid_pay = self.paid_payroll
        emp_wage = self.get_basic_wage(payslip, worked_days)
        result = 0
        cont = True
        if emp_sched_pay == 'weekly':
            if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                emp_wage = self.get_gross_wage(payslip, worked_days) + self.get_all_month_wage(payslip)
            weekly_wage = self.get_gross_wage(payslip, worked_days)
            if weekly_wage > 0.0:
                cont = True
            else:
                cont = False
        if cont:
            if table == 'hr.philhealth.table':
                if worked_days:
                    if self.paid_payroll == 'monthly':
                        if not self.employee_id.date_separated:
                            emp_wage = self.wage
                    contri = self.env['hr.philhealth.table'].search([('minrange', '<=', emp_wage),
                                                                     ('maxrange', '>=', emp_wage),
                                                                     ('active', '=', True),
                                                                     ('state', '=', 'default')])
                    if contri:
                        if not self.employee_id.date_separated:
                            if contri.based_monthly_prem == False:
                                result = contri.eeshare * 2
                            else:
                                result = emp_wage * (contri.monthly_prem / 100.0)
                            if emp_sched_pay == 'weekly' and emp_paid_pay == 'daily':
                                result = contri.eeshare
                        else:
                            if contri.based_monthly_prem == False:
                                result = contri.eeshare
                            else:
                                result = emp_wage * (contri.monthly_prem / 100.0)
                            if emp_sched_pay == 'weekly' and emp_paid_pay == 'daily':
                                result = contri.eeshare
                    # print(result)

            if table == 'hr.sss.table':
                if worked_days:
                    contri = self.env['hr.sss.table'].search([('minrange', '<=', emp_wage),
                                                              ('maxrange', '>=', emp_wage),
                                                              ('active', '=', True),
                                                              ('state', '=', 'default')])

                    result = contri.eeshare + contri.mpf_ee
                    # print(result)

            if table == 'hr.hdmf.table':
                if worked_days:
                    if emp_paid_pay == 'daily' and emp_sched_pay == 'weekly':
                        if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                            emp_wage = self.get_gross_wage(payslip, worked_days) + self.get_all_month_wage(payslip)
                        else:
                            emp_wage = (self.get_gross_wage(payslip, worked_days) * schedpay)
                    contri = self.env[table].search([('minrange', '<=', emp_wage),
                                                     ('maxrange', '>=', emp_wage),
                                                     ('active', '=', True),
                                                     ('state', '=', 'default')])

                    result = (emp_wage * (contri.eeshare / 100))
                    # print result

        return round(result, 2)

    # contribution of employeer sss/philhealth/pagibig
    def get_contributioner(self, payslip, worked_days, schedpay, table):
        emp_sched_pay = self.schedule_pay
        emp_paid_pay = self.paid_payroll
        emp_wage = self.get_basic_wage(payslip, worked_days)
        result = 0
        cont = True
        if emp_sched_pay == 'weekly':
            if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                emp_wage = self.get_gross_wage(payslip, worked_days) + self.get_all_month_wage(payslip)
            weekly_wage = self.get_gross_wage(payslip, worked_days)
            if weekly_wage > 0.0:
                cont = True
            else:
                cont = False

        if cont:
            if table == 'hr.philhealth.table':
                if worked_days:
                    contri = self.env['hr.philhealth.table'].search([('minrange', '<=', emp_wage),
                                                                     ('maxrange', '>=', emp_wage),
                                                                     ('active', '=', True),
                                                                     ('state', '=', 'default')])

                    if contri:
                        if emp_paid_pay == 'monthly':
                            if not self.employee_id.date_separated:
                                emp_wage = self.wage
                                if contri.based_monthly_prem == False:
                                    result = contri.eeshare * 2
                                else:
                                    result = emp_wage * (contri.monthly_prem / 100.0)
                            else:
                                if contri.based_monthly_prem == False:
                                    result = contri.eeshare
                                else:
                                    result = emp_wage * (contri.monthly_prem / 100.0)
                        if emp_paid_pay == 'daily':
                            if not self.employee_id.date_separated:
                                if emp_sched_pay == 'weekly':
                                    result = contri.eeshare
                                else:
                                    if contri.based_monthly_prem == False:
                                        result = contri.eeshare * 2
                                    else:
                                        result = emp_wage * (contri.monthly_prem / 100.0)
                            else:
                                if emp_sched_pay == 'weekly':
                                    result = contri.eeshare
                                else:
                                    if contri.based_monthly_prem == False:
                                        result = contri.eeshare
                                    else:
                                        result = emp_wage * (contri.monthly_prem / 100.0)
                    # print result

            if table == 'hr.sss.table':
                if worked_days:
                    contri = self.env['hr.sss.table'].search([('minrange', '<=', emp_wage),
                                                                  ('maxrange', '>=', emp_wage),
                                                                  ('active', '=', True),
                                                                  ('state', '=', 'default')])
                    result = contri.ershare + contri.mpf_er
                    # print result

            if table == 'hr.hdmf.table':
                if worked_days:
                    # if emp_paid_pay == 'daily' and emp_sched_pay == 'weekly':
                    #     emp_wage = self.get_gross_wage(payslip, worked_days)
                    contri = self.env[table].search([('minrange', '<=', emp_wage),
                                                         ('maxrange', '>=', emp_wage),
                                                         ('active', '=', True),
                                                         ('state', '=', 'default')])

                    if contri.ershare == 50.00:
                        result = (emp_wage * (contri.eeshare / 100))
                    elif contri.ershare == 100.00:
                        result = contri.ershare
                    else:
                        result = 0.0
                    # print result

        return round(result, 2)

    # contribution of employee sss ec
    def get_contributionec(self,payslip, worked_days, schedpay, table):
        emp_sched_pay = self.schedule_pay
        emp_paid_pay = self.paid_payroll
        emp_wage = self.get_basic_wage(payslip, worked_days)
        result = 0.0
        cont = True
        if emp_sched_pay == 'weekly' and emp_paid_pay == 'daily':
            if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                emp_wage = self.get_gross_wage(payslip, worked_days) + self.get_all_month_wage(payslip)
            weekly_wage = self.get_gross_wage(payslip, worked_days)
            if weekly_wage > 0.0:
                cont = True
            else:
                cont = False
        if cont:
            contri = self.env[table].search([('minrange', '<=', emp_wage),
                                             ('maxrange', '>=', emp_wage),
                                             ('active', '=', True),
                                             ('state', '=', 'default')])

            result = contri.ec_er

        return round(result, 2)

    def get_gross_wage(self, payslip, worked_days):
        date_start = datetime.strptime(payslip.date_from, '%Y-%m-%d')
        date_end = datetime.strptime(payslip.date_to, '%Y-%m-%d')
        basic = self.basic_wage(payslip, worked_days)
        absences = 0.0
        total_absences = self.env['hr.absences.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                  ('date_start', '>=', date_start),
                                                                  ('date_end', '<=', date_end)])
        if total_absences:
            for total_absent in total_absences:
                absences += total_absent.total_absences
        result = basic - absences
        return round(result, 2)

    def get_all_month_wage(self, payslip):
        date_payment = payslip.date_from
        date_start = date(date_payment.year, date_payment.month, '01')
        date_end = date(date_payment.year, date_payment.month,  calendar.monthrange(date_payment.year, date_payment.month)[1])
        all_contributions = self.env['hr.payslip'].search([('id', '!=', payslip.id),
                                                           ('employee_id', '=', self.employee_id.id),
                                                           ('date_from', '>=', date_start),
                                                           ('date_from', '<=', date_end)])
        basic = 0.0
        absences = 0.0
        if all_contributions:
            for all in all_contributions:
                for line in all.line_ids:
                    if line.code == 'BASIC':
                        basic += line.total

        total_absences = self.env['hr.absences.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                  ('date_start', '>=', date_start),
                                                                  ('date_end', '<=', date_end)])
        if total_absences:
            for total_absent in total_absences:
                absences += total_absent.total_absences
        result = basic - absences
        return round(result, 0)

    # contribution of employee sss/philhealth/pagibig
    def get_pay_vale(self, worked_days, payslip, schedpay, table):
        date_from = datetime.strptime(payslip.date_from, "%Y-%m-%d").date()
        # fvale = False
        # svale = False
        #
        # if date_from.day >= 1 and date_from.day <= 15:
        #     fvale = True
        #     svale = False
        # else:
        #     fvale = False
        #     svale = True

        # if comp_partner.pay_vale == 'firstvale':
        if date_from.day >= 1 and date_from.day <= 15:
            # if fvale:
            result = self.get_contribution(payslip, worked_days, schedpay, table)
            # else:
            #     result = 0.00

        # elif comp_partner.pay_vale == 'secondvale':
        else:
            # if svale:
            result = self.get_contribution(payslip, worked_days, schedpay, table)
            # else:
            #     result = 0.00

        return round(result, 2)

    #     how to get deduction and adjustment
    # def get_deduct_adjust(self,payslip,table1,table2,emp_ids):
    #     date_from = datetime.strptime(payslip.date_from, "%Y-%m-%d").date()
    #     date_to = datetime.strptime(payslip.date_to, "%Y-%m-%d").date()
    #
    #
    #     chck_list = self.env[table1].search([('payment_date', '<=', date_from),
    #                                          ('payment_date', '>=', date_to),
    #                                          ('state', '=', 'done')])
    #
    #     if not chck_list:
    #         result = 0.00
    #     else:
    #         result = 0
    #         for xx in chck_list:
    #             for x in xx.emp_list_ids:
    #                 list = x.id
    #                 emp_list = self.env[table2].search([('id', '=', list),
    #                                                     ('emp_id', '=', emp_ids)])
    #
    #                 result = result + emp_list.amount
    #
    #     return round(result,2)

    #     how to get rate
    def _get_rate(self, paid_rate):
        sal_wage = self.wage
        shift_sched = self.env['resource.calendar.attendance'].search([('calendar_id', '=', self.working_hours.id)])
        hrs_per_week = 0

        for x in shift_sched:
            hrs_per_week += x.total_hours

        if hrs_per_week == 48:
            workingdays_per_year = 313
        elif hrs_per_week == 40:
            workingdays_per_year = 261
        else:
            workingdays_per_year = 365

        months_per_year = 12
        hrs_per_day = 8
        result = 0.00
        if self.paid_payroll == 'daily':
            if paid_rate == 'hour':
                result = (sal_wage / hrs_per_day)
            elif paid_rate == 'daily':
                result = sal_wage

        elif self.paid_payroll == 'monthly':
            if paid_rate == 'hour':
                result = (((sal_wage * months_per_year) / workingdays_per_year) / hrs_per_day)
            elif paid_rate == 'daily':
                result = ((sal_wage * months_per_year) / workingdays_per_year)

        return round(result, 2)

    # get previous contribution for WTAX!

    def get_previous_basic_wage(self, worked_days, old_salary):
        salary = old_salary
        emp_sched_pay = self.schedule_pay
        emp_paid_pay = self.paid_payroll
        result = 0.00
        if emp_paid_pay == 'daily':
            result = worked_days.WORK100.number_of_days * salary
        else:
            if emp_sched_pay == 'monthly':
                result = salary
            elif emp_sched_pay == 'semi-monthly':
                result = salary / 2

        return round(result, 2)

    def get_previousphcont(self, end, old_salary):
        table = 'hr.philhealth.table'
        result = self.get_previous_contribution(end, table, old_salary) if self.philhealth_cont else 0.00
        return round(result, 4)

    def get_previousssscont(self, end, old_salary):
        table = 'hr.sss.table'
        result = self.get_previous_contribution(end, table, old_salary) if self.sss_cont else 0.00
        return round(result, 2)

    def get_previoushdmfcont(self, end, old_salary):
        table = 'hr.hdmf.table'
        result = self.get_previous_contribution(end, table, old_salary) if self.hdmf_cont else 0.00
        return round(result, 2)

    def get_previous_contribution(self, end, table, old_salary):
        emp_sched_pay = self.schedule_pay
        emp_paid_pay = self.paid_payroll
        emp_wage = old_salary
        result = 0
        if table == 'hr.philhealth.table':
            contri = self.env['hr.philhealth.table'].search([('minrange', '<=', emp_wage),
                                                             ('maxrange', '>=', emp_wage),
                                                             ('dateeffect', '<=', end),
                                                             ('active', '=', True), '|',
                                                             ('date_end', '>=', end), ('date_end', '=', False)], limit=1, order="dateeffect")
            if contri:
                if contri.based_monthly_prem:
                    result = emp_wage * (contri.monthly_prem / 100.0) / 2
                else:
                    result = contri.eeshare
                if emp_sched_pay == 'weekly' and emp_paid_pay == 'daily':
                    result = contri.eeshare

        if table == 'hr.sss.table':
            contri = self.env['hr.sss.table'].search([('minrange', '<=', emp_wage),
                                                      ('maxrange', '>=', emp_wage),
                                                      ('dateeffect', '<=', end),
                                                      ('active', '=', True), '|',
                                                      ('date_end', '>=', end), ('date_end', '=', False)], limit=1, order="dateeffect desc")

            result = contri.eeshare

        if table == 'hr.hdmf.table':
            contri = self.env[table].search([('minrange', '<=', emp_wage),
                                             ('maxrange', '>=', emp_wage),
                                             ('dateeffect', '<=', end),
                                             ('active', '=', True), '|',
                                             ('date_end', '>=', end), ('date_end', '=', False)], limit=1, order="dateeffect desc")

            result = (emp_wage * (contri.eeshare / 100))

        return round(result, 4)