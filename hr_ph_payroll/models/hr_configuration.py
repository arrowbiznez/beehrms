# -*- coding: utf-8 -*-


from odoo import models, fields, api, _
from datetime import date,datetime, timedelta
import time,calendar
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import except_orm,Warning,RedirectWarning,ValidationError
from dateutil.relativedelta import relativedelta
import odoo.addons.decimal_precision as dp

WEEKLIST = [('0', 'Monday'),
            ('1', 'Tuesday'),
            ('2', 'Wednesday'),
            ('3', 'Thursday'),
            ('4', 'Friday'),
            ('5', 'Saturday'),
            ('6', 'Sunday')]

def daterange(start_date, end_date):
    end_date = end_date + timedelta(days=1)
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

strftime = datetime.strptime
# contribution


class HrHDMF(models.Model):
    _name = 'hr.hdmf.matrix'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'HDMF Matrix'

    # @api.multi
    @api.depends('dateeffect')
    def compute_hdmf_name(self):
        for record in self:
            if record.dateeffect:
                name = "HDMF[%s]" % record.dateeffect
                record.update({'name': name})

    name = fields.Char(string="Description", required=False, compute="compute_hdmf_name", store=True)
    active = fields.Boolean(string="Active", default=True)
    dateeffect = fields.Date(string="Start Date", required=True, )
    date_end = fields.Date(string="End Date", required=False, )
    state = fields.Selection(string="Status", selection=[('default', 'Default'), ('non', 'Non-Default'), ],
                             required=False, default='default')
    hdmf_line_ids = fields.One2many(comodel_name="hr.hdmf.table", inverse_name="hdmf_id",
                                    string="HDMF Table Matrix", required=False, )
    user_id = fields.Many2one(comodel_name="res.users", string="Encoded By", required=False, default=lambda self: self.env.user,
                              track_visibility="onchange")

    @api.model
    def create(self, values):
        res = super(HrHDMF, self).create(values)
        if res:
            res.set_as_default()
        return res

    # @api.multi
    def set_as_default(self):
        self.state = 'default'
        for record in self.search([('id', '!=', self.id)]):
            if record:
                record.write({'state': 'non'})
        model_obj = self.env['ir.model.data']
        data_id = model_obj._get_id('hrms_payroll', 'hr_hdmf_matrix_tree_view')
        view_id = model_obj.sudo().browse(data_id).res_id
        return {'type': 'ir.actions.client',
                'tag': 'reload',
                'name': _('HDMF Matrix'),
                'res_model': 'hr.hdmf.matrix',
                'view_type': 'tree',
                'view_mode': 'tree',
                'view_id': view_id,
                'target': 'current',
                'nodestroy': True}

    def set_as_non_default(self):
        self.state = 'non'
        model_obj = self.env['ir.model.data']
        data_id = model_obj._get_id('hrms_payroll', 'hr_hdmf_matrix_tree_view')
        view_id = model_obj.sudo().browse(data_id).res_id
        return {'type': 'ir.actions.client',
                'tag': 'reload',
                'name': _('HDMF Matrix'),
                'res_model': 'hr.hdmf.matrix',
                'view_type': 'tree',
                'view_mode': 'tree',
                'view_id': view_id,
                'target': 'current',
                'nodestroy': True}


class HrHdmfTable(models.Model):
    _name = 'hr.hdmf.table'
    _description = 'HDMF Matrix Table'

    hdmf_id = fields.Many2one(comodel_name="hr.hdmf.matrix", string="HDMF Matrix", required=False, )
    active = fields.Boolean(string='Active', related='hdmf_id.active', store=True)
    state = fields.Selection(string="Status", selection=[('default', 'Default'), ('non', 'Non-Default'), ],
                             required=False, related='hdmf_id.state', store=True)
    dateeffect = fields.Date(string='Start Date',related='hdmf_id.dateeffect', store=True)
    date_end = fields.Date(string='End Date', related='hdmf_id.date_end', store=True)
    minrange = fields.Float(string='Minimum Range')
    maxrange = fields.Float(string='Maximum Range')
    ershare = fields.Float(string='Employer Share(%)')
    eeshare = fields.Float(string='Employee Share(%)')


class HrSSS(models.Model):
    _name = 'hr.sss.matrix'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'SSS Matrix'

    # @api.multi
    @api.depends('dateeffect')
    def compute_sss_name(self):
        for record in self:
            if record.dateeffect:
                name = "SSS[%s]" % record.dateeffect
                record.update({'name': name})

    name = fields.Char(string="Description", required=False, compute="compute_sss_name", store=True)
    active = fields.Boolean(string="Active", default=True)
    dateeffect = fields.Date(string="Date Effect", required=True, )
    date_end = fields.Date(string="Date Ended", required=False, )
    state = fields.Selection(string="Status", selection=[('default', 'Default'), ('non', 'Non-Default'), ],
                             required=False, default='default')
    sss_line_ids = fields.One2many(comodel_name="hr.sss.table", inverse_name="sss_id",
                                    string="SSS Table Matrix", required=False, )
    user_id = fields.Many2one(comodel_name="res.users", string="Encoded By", required=False,
                              default=lambda self: self.env.user,
                              track_visibility="onchange")

    @api.model
    def create(self, values):
        res = super(HrSSS, self).create(values)
        if res:
            res.set_as_default()
        return res

    # @api.multi
    def set_as_default(self):
        self.state = 'default'
        for record in self.search([('id', '!=', self.id)]):
            if record:
                record.write({'state': 'non'})
        model_obj = self.env['ir.model.data']
        data_id = model_obj._get_id('hrms_payroll', 'hr_sss_matrix_tree_view')
        view_id = model_obj.sudo().browse(data_id).res_id
        return {'type': 'ir.actions.client',
                'tag': 'reload',
                'name': _('SSS Matrix'),
                'res_model': 'hr.sss.matrix',
                'view_type': 'tree',
                'view_mode': 'tree',
                'view_id': view_id,
                'target': 'current',
                'nodestroy': True}

    def set_as_non_default(self):
        self.state = 'non'
        model_obj = self.env['ir.model.data']
        data_id = model_obj._get_id('hrms_payroll', 'hr_sss_matrix_tree_view')
        view_id = model_obj.sudo().browse(data_id).res_id
        return {'type': 'ir.actions.client',
                'tag': 'reload',
                'name': _('SSS Matrix'),
                'res_model': 'hr.sss.matrix',
                'view_type': 'tree',
                'view_mode': 'tree',
                'view_id': view_id,
                'target': 'current',
                'nodestroy': True}


class HrSSSTable(models.Model):
    _name = 'hr.sss.table'
    _description = 'SSS Matrix Table'

    sss_id = fields.Many2one(comodel_name="hr.sss.matrix", string="SSS Matrix", required=False, )
    active = fields.Boolean(string='Active', related='sss_id.active', store=True)
    state = fields.Selection(string="Status", selection=[('default', 'Default'), ('non', 'Non-Default'), ],
                             required=False, related='sss_id.state', store=True)
    dateeffect = fields.Date(string='Start Date',related='sss_id.dateeffect', store=True)
    date_end = fields.Date(string='End Date', related='sss_id.date_end', store=True)
    minrange = fields.Float(string='Minimum Range')
    maxrange = fields.Float(string='Maximum Range')
    monthly_sal_credit = fields.Float(string='Monthly Salary Credit')
    ershare = fields.Float(string = 'ER Share')
    eeshare = fields.Float(string='EE Share')
    ec_er = fields.Float(string='EC-ER')
    mpf_ee = fields.Float(string="MPF-EE",  required=False, )
    mpf_er = fields.Float(string="MPF-ER",  required=False, )
    total_cont = fields.Float(string='Total Contribution', compute='_get_total_cont')
    se_vm_ofw_total_cont = fields.Float(string='SE/VM/OFW Total Contribution', compute='_get_sstotalcont')

    @api.onchange('ershare', 'eeshare', 'ec_er', 'mpf_ee', 'mpf_er')
    def _get_total_cont(self):
        for r in self:
            r.total_cont = ((r.ershare + r.eeshare) + r.ec_er + (r.mpf_ee + r.mpf_er))

    def _get_sstotalcont(self):
        for r in self:
            r.se_vm_ofw_total_cont = (r.ershare + r.eeshare)

class HrGSIS(models.Model):
    _name = 'hr.gsis.matrix'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'GSIS Matrix'

    # @api.multi
    @api.depends('dateeffect')
    def compute_gsis_name(self):
        for record in self:
            if record.dateeffect:
                name = "GSIS[%s]" % record.dateeffect
                record.update({'name': name})

    name = fields.Char(string="Description", required=False, compute="compute_gsis_name", store=True)
    active = fields.Boolean(string="Active", default=True)
    dateeffect = fields.Date(string="Date Effect", required=True, )
    date_end = fields.Date(string="Date Ended", required=False, )
    state = fields.Selection(string="Status", selection=[('default', 'Default'), ('non', 'Non-Default'), ],
                             required=False, default='default')
    gsis_line_ids = fields.One2many(comodel_name="hr.gsis.table", inverse_name="gsis_id",
                                    string="GSIS Table Matrix", required=False, )
    user_id = fields.Many2one(comodel_name="res.users", string="Encoded By", required=False,
                              default=lambda self: self.env.user,
                              track_visibility="onchange")

    @api.model
    def create(self, values):
        res = super(HrGSIS, self).create(values)
        if res:
            res.set_as_default()
        return res

    # @api.multi
    def set_as_default(self):
        self.state = 'default'
        for record in self.search([('id', '!=', self.id)]):
            if record:
                record.write({'state': 'non'})
        model_obj = self.env['ir.model.data']
        data_id = model_obj._get_id('hrms_payroll', 'hr_gsis_matrix_tree_view')
        view_id = model_obj.sudo().browse(data_id).res_id
        return {'type': 'ir.actions.client',
                'tag': 'reload',
                'name': _('GSIS Matrix'),
                'res_model': 'hr.gsis.matrix',
                'view_type': 'tree',
                'view_mode': 'tree',
                'view_id': view_id,
                'target': 'current',
                'nodestroy': True}

    def set_as_non_default(self):
        self.state = 'non'
        model_obj = self.env['ir.model.data']
        data_id = model_obj._get_id('hrms_payroll', 'hr_gsis_matrix_tree_view')
        view_id = model_obj.sudo().browse(data_id).res_id
        return {'type': 'ir.actions.client',
                'tag': 'reload',
                'name': _('GSIS Matrix'),
                'res_model': 'hr.gsis.matrix',
                'view_type': 'tree',
                'view_mode': 'tree',
                'view_id': view_id,
                'target': 'current',
                'nodestroy': True}


class HrGSISTable(models.Model):
    _name = 'hr.gsis.table'
    _description = 'GSIS Matrix table'

    gsis_id = fields.Many2one(comodel_name="hr.gsis.matrix", string="GSIS Matrix", required=False, )
    active = fields.Boolean(string='Active', related='gsis_id.active', store=True)
    state = fields.Selection(string="Status", selection=[('default', 'Default'), ('non', 'Non-Default'), ],
                             required=False, related='gsis_id.state', store=True)
    dateeffect = fields.Date(string='Start Date',related='gsis_id.dateeffect', store=True)
    date_end = fields.Date(string='End Date', related='gsis_id.date_end', store=True)
    minrange = fields.Float(string='Minimum Range')
    maxrange = fields.Float(string='Maximum Range')
    monthly_sal_credit = fields.Float(string='Monthly Salary Credit')
    ershare = fields.Float(string = 'ER Share')
    eeshare = fields.Float(string='EE Share')
    ec_er = fields.Float(string='EC-ER')
    mpf_ee = fields.Float(string="MPF-EE",  required=False, )
    mpf_er = fields.Float(string="MPF-ER",  required=False, )
    total_cont = fields.Float(string='Total Contribution', compute='_get_total_cont')
    se_vm_ofw_total_cont = fields.Float(string='SE/VM/OFW Total Contribution', compute='_get_sstotalcont')

    @api.onchange('ershare', 'eeshare', 'ec_er', 'mpf_ee', 'mpf_er')
    def _get_total_cont(self):
        for r in self:
            r.total_cont = ((r.ershare + r.eeshare) + r.ec_er + (r.mpf_ee + r.mpf_er))

    def _get_sstotalcont(self):
        for r in self:
            r.se_vm_ofw_total_cont = (r.ershare + r.eeshare)


class HrPhilHealth(models.Model):
    _name = 'hr.philhealth.matrix'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'PhilHealth Matrix'

    # @api.multi
    @api.depends('dateeffect')
    def compute_philhealth_name(self):
        for record in self:
            if record.dateeffect:
                name = "PHIC[%s]" % record.dateeffect
                record.update({'name': name})

    name = fields.Char(string="Description", required=False, compute="compute_philhealth_name", store=True)
    active = fields.Boolean(string="Active", default=True)
    dateeffect = fields.Date(string="Start Date", required=True, )
    date_end = fields.Date(string="End Date", required=False, )
    state = fields.Selection(string="Status", selection=[('default', 'Default'), ('non', 'Non-Default'), ],
                             required=False, default='default')
    phic_line_ids = fields.One2many(comodel_name="hr.philhealth.table", inverse_name="phic_id",
                                    string="PHILHEALTH Table Matrix", required=False, )
    user_id = fields.Many2one(comodel_name="res.users", string="Encoded By", required=False,
                              default=lambda self: self.env.user,
                              track_visibility="onchange")

    @api.model
    def create(self, values):
        res = super(HrPhilHealth, self).create(values)
        if res:
            res.set_as_default()
        return res

    # @api.multi
    def set_as_default(self):
        self.state = 'default'
        for record in self.search([('id', '!=', self.id)]):
            if record:
                record.write({'state': 'non'})
        model_obj = self.env['ir.model.data']
        data_id = model_obj._get_id('hrms_payroll', 'hr_philhealth_matrix_tree_view')
        view_id = model_obj.sudo().browse(data_id).res_id
        return {'type': 'ir.actions.client',
                'tag': 'reload',
                'name': _('PHIC Matrix'),
                'res_model': 'hr.philhealth.matrix',
                'view_type': 'tree',
                'view_mode': 'tree',
                'view_id': view_id,
                'target': 'current',
                'nodestroy': True}

    def set_as_non_default(self):
        self.state = 'non'
        model_obj = self.env['ir.model.data']
        data_id = model_obj._get_id('hrms_payroll', 'hr_philhealth_matrix_tree_view')
        view_id = model_obj.sudo().browse(data_id).res_id
        return {'type': 'ir.actions.client',
                'tag': 'reload',
                'name': _('PHIC Matrix'),
                'res_model': 'hr.philhealth.matrix',
                'view_type': 'tree',
                'view_mode': 'tree',
                'view_id': view_id,
                'target': 'current',
                'nodestroy': True}


class HrPhilhealthTable(models.Model):
    _name = 'hr.philhealth.table'
    _description = 'PhilHealth Matrix Table'

    phic_id = fields.Many2one(comodel_name="hr.philhealth.matrix", string="PhilHealth Matrix", required=False, )
    active = fields.Boolean(string='Active', related='phic_id.active', store=True)
    state = fields.Selection(string="Status", selection=[('default', 'Default'), ('non', 'Non-Default'), ],
                             required=False, related='phic_id.state', store=True)
    dateeffect = fields.Date(string='Start Date', related='phic_id.dateeffect', store=True)
    date_end = fields.Date(string='End Date', related='phic_id.date_end', store=True)
    minrange = fields.Float(string='Minimum Range')
    maxrange = fields.Float(string='Maximum Range')
    ershare = fields.Float(string = 'Employer Share')
    eeshare = fields.Float(string='Employee Share')
    monthly_prem = fields.Float(string="Monthly Premium", digits=(12, 4))
    based_monthly_prem = fields.Boolean(string="Based on Monthly Premium",  )


class HrTaxAnnual(models.Model):
    _name = 'hr.tax.annual'
    _description = 'Tax Annual'

    active = fields.Boolean(string='Active',default=True)
    dateeffect = fields.Date(string='Start Date')
    date_ended = fields.Date(string="End Date", required=False, )
    minrange = fields.Float(string='Salary From')
    maxrange = fields.Float(string='Salary To')
    fixed_tax = fields.Float(string='Fixed Tax')
    rate = fields.Float(string='Rate(%)')


class HrTaxCompensation(models.Model):
    _name = 'hr.tax.compensation'
    _description = 'Tax Compensation'

    active = fields.Boolean(string='Active', default=True)
    date_effect = fields.Date(string='Date Effect')
    schedule_pay = fields.Selection(string="Compensation Type", selection=[('daily', 'Daily'),
                                                                    ('weekly', 'Weekly'),
                                                                    ('semi-monthly', 'Semi-Monthly'),
                                                                    ('monthly', 'Monthly')], required=False, )
    minrange = fields.Float(string='Salary From')
    maxrange = fields.Float(string='Salary To')
    fixed_tax = fields.Float(string='Fixed Tax')
    rate = fields.Float(string='Prescribed Percentage(%)')