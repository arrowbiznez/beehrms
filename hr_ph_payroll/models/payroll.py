from odoo import api, fields, models, _
from datetime import date, datetime, timedelta
from comm_methods import get_basic_wage,get_contribution,get_contributioner,\
    get_contributionec,get_pay_vale, _get_rate, get_previousphcont,get_previousssscont,get_previoushdmfcont
from odoo.exceptions import ValidationError,UserError
from dateutil.relativedelta import relativedelta
import calendar
import math

class HrSalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    computed_in_payslip = fields.Boolean(string="Computed in Salary")


class HrPayslipWorked_days(models.Model):
    _inherit = 'hr.payslip.worked_days'

    # in 1 day = 8 hrs only
    @api.onchange('number_of_days')
    def _onchange_number_ofdays(self):
        self.number_of_hours = self.number_of_days * 8


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'
    _order = 'date_from desc,name'

    @api.one
    @api.constrains('hr_period_id', 'company_id')
    def _check_period_company(self):

        contract_ids = self.env['hr.contract'].search([('employee_id', '=', self.employee_id.id),
                                                       ('id', '=', self.contract_id.id),('state','=','open')])

        company_id = self.env['res.partner'].search([('id', '=', contract_ids.partner_ids.id)])
        if self.hr_period_id:
            if self.hr_period_id.company_id != company_id:
                raise ValidationError(
                    "The company on the selected period must "
                    "be the same as the company on the "
                    "payslip."
                )

        return True

    @api.onchange('payslip_run_id')
    def onchange_payslip_run_id(self):

        payslip_run = self.payslip_run_id

        if payslip_run:
            period = payslip_run.hr_period_id
            self.hr_period_id = period.id
            if period:
                self.name = self.employee_id.name
                self.department_id = self.employee_id.department_id.id

    @api.multi
    def compute_total_paid_loan(self):
        total = 0.00
        for line in self.loan_ids:
            # if line.paid == True:
            total += line.paid_amount

        self.total_loan_amount_paid = total

    loan_ids = fields.One2many('hr.loan.line', 'payroll_id', string="Loans")
    total_loan_amount_paid = fields.Float(string="Total Loan Amount", compute='compute_total_paid_loan')
    department_id = fields.Many2one('hr.department', string='Department')
    job_id = fields.Many2one(comodel_name="hr.job", string="Job Position", required=False, )
    payslip_run_id = fields.Many2one('hr.payslip.run', string='Payslip Batches', readonly=True,
                                     copy=False, states={'draft': [('readonly', False)]})
    deduction_list_ids = fields.One2many('hr.deduction.employee','payroll_ded_id', string="Deductions")
    adjustment_list_ids = fields.One2many('hr.adjustment.employee', 'payroll_adj_id',string="Adjustments")
    deduction_installment_ids = fields.One2many('hr.deduction.installment.line', 'payslip_id', string="Deductions w/ Installment")
    compensation_ids = fields.One2many(comodel_name="hr.payroll.compensation", inverse_name="payslip_id", string="Compensation(s)", required=False, )
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True,
                                  default=lambda self: self.env.user.company_id.currency_id)
    set_as_regular_payroll = fields.Boolean(string="Set as Regular Payroll", related='payslip_run_id.set_as_regular_payroll', store=True)
    log_ids = fields.One2many(comodel_name="hr.payslip.log", inverse_name="payslip_id", string="Logs Transaction", required=False, )

    @api.model
    def create(self, values):
        res = super(HrPayslip, self).create(values)
        if res and 'contract_id' in values:
            contract_id = values['contract_id']
            employee_id = values['employee_id']
            contract = self.env['hr.contract'].browse(contract_id)
            employee = self.env['hr.employee'].browse(employee_id)
            if contract.department_id:
                values['department_id'] = contract.department_id.id if contract.department_id else employee.department_id.id
            if contract.job_id:
                values['job_id'] = contract.job_id.id if contract.job_id else employee.job_id.id
            payslip = self.browse(res.id)
            if payslip:
                vals = {'user_id': self.env.user.id, 'payslip_id': payslip.id}
                self.env['hr.payslip.log'].create(vals)
        return res

    @api.multi
    def write(self, values):
        res = super(HrPayslip, self).write(values)
        if res:
            vals = {'user_id': self.env.user.id, 'payslip_id': self.id}
            self.env['hr.payslip.log'].create(vals)
        return res

    # from odoo payroll
    @api.multi
    def compute_sheet(self):
        for payslip in self:
            print('',payslip.employee_id.identification_id)
            payslip.get_loan()
            payslip.get_hr_adjustment()
            payslip.get_hr_deduction()
            number = payslip.number or self.env['ir.sequence'].next_by_code('salary.slip')
            # delete old payslip lines
            payslip.line_ids.unlink()
            # set the list of contract for which the rules have to be applied
            # if we don't give the contract, then the rules to apply should be for all current contracts of the employee
            contract_ids = payslip.contract_id.ids or self.get_contract(payslip.employee_id, payslip.date_from, payslip.date_to)
            lines = [(0, 0, line) for line in self.get_payslip_lines(contract_ids, payslip.id)]
            payslip.write({'line_ids': lines, 'number': number})
            # payslip.get_hr_compensation()
            # payslip.get_annual_payslip()
        return True

    @api.onchange('line_ids')
    def _onchange_line_ids(self):
        for record in self:
            if record.line_ids:
                total_basic = 0.0
                total_deduction = 0.0
                total_absences = 0.0
                for line in record.line_ids:
                    if line.code in ('SPCHDY', 'REGHDY', 'TOTADJ', 'OT', 'BASIC'):
                        total_basic += line.total
                    if line.code == 'ABS':
                        total_absences += line.total
                    if line.code in ('SSSEE', 'PHEE', 'HDMFEE', 'WHTAX', 'LOAN', 'NETDED'):
                        total_deduction += line.total
                total_gross = total_basic - total_absences
                total_amount = total_gross - total_deduction
                for lines in record.line_ids:
                    if lines.code == 'GROSS':
                        lines.write({'amount': total_gross})
                    if lines.code == 'TOTDED':
                        lines.write({'amount': total_deduction})
                    if lines.code == 'NET':
                        lines.write({'amount': total_amount})

    # For All Employees
    @api.model
    def get_contract(self, employee, date_from, date_to):
        """
        @param employee: recordset of employee
        @param date_from: date field
        @param date_to: date field
        @return: returns the ids of all the contracts for the given employee that need to be considered for the given dates
        """
        # a contract is valid if it ends between the given dates
        clause_1 = ['&', ('date_end', '<=', date_to), ('date_end', '>=', date_from)]
        # OR if it starts between the given dates
        clause_2 = ['&', ('date_start', '<=', date_to), ('date_start', '>=', date_from)]
        # OR if it starts before the date_from and finish after the date_end (or never finish)
        clause_3 = ['&', ('date_start', '<=', date_from), '|', ('date_end', '=', False), ('date_end', '>=', date_to)]
        clause_final = [('employee_id', '=', employee.id), '|', '|'] + clause_1 + clause_2 + clause_3
        return self.env['hr.contract'].sudo().search(clause_final).ids

    def get_loan(self):
        array = []
        if self.contract_id:
            if self.contract_id.schedule_pay == 'semi-monthly':
                date_start = datetime.strptime(self.date_from, '%Y-%m-%d')
                date_end = datetime.strptime(self.date_to, '%Y-%m-%d')
            elif self.contract_id.schedule_pay == 'monthly':
                date_start = datetime.strptime(self.date_from, '%Y-%m-%d')
                date_end = datetime.strptime(self.date_to, '%Y-%m-%d')
            else:
                date_start = datetime.strptime(self.payslip_run_id.date_payment, '%Y-%m-%d')
                date_end = datetime.strptime(self.payslip_run_id.date_payment, '%Y-%m-%d')
            loan_ids = self.env['hr.loan.line'].sudo().search([('employee_id', '=', self.employee_id.id),
                                                               ('paid', '=', False),
                                                               ('paid_date', '>=', date_start),
                                                               ('paid_date', '<=', date_end),
                                                               ('loan_id.state', '=', 'approve')])

            for loan in loan_ids:
                if loan.loan_type_id.set_as_priority:
                    array.append(loan.id)

                # loan.update({'paid': True})
            self.loan_ids = array
            print ('LOAN', array)
        return array

    def get_hr_deduction(self):
        array = []
        if self.contract_id.schedule_pay == 'semi-monthly':
            date_start = datetime.strptime(self.date_from, '%Y-%m-%d')
            date_end = datetime.strptime(self.date_to, '%Y-%m-%d')
        elif self.contract_id.schedule_pay == 'monthly':
            date_start = datetime.strptime(self.date_from, '%Y-%m-%d')
            date_end = datetime.strptime(self.date_to, '%Y-%m-%d')
        else:
            date_start = datetime.strptime(self.payslip_run_id.date_payment, '%Y-%m-%d')
            date_end = datetime.strptime(self.payslip_run_id.date_payment, '%Y-%m-%d')
        deductions = self.env['hr.deduction.employee'].sudo().search([('emp_id', '=', self.employee_id.id),
                                                                      ('payment_date', '<=', date_end),
                                                                      ('payment_date', '>=', date_start)], order='id')

        for deduction in deductions:
            array.append(deduction.id)

            # deduction.update({'paid': True})
        self.deduction_list_ids = array
        print ('DEDUCTION', array)
        return array

    # For Weekly Employees
    def get_absences(self):
        start_date = datetime.strptime(self.date_from, '%Y-%m-%d')
        end_date = datetime.strptime(self.date_to, '%Y-%m-%d')
        work = 0.00
        wage = 0.00
        if self.contract_id:
            wage = self.contract_id.wage
        worked_days = self.env['hr.payslip.worked_days'].search([('code', '=', 'WORK100')], limit=1)
        if worked_days:
            work = worked_days.number_of_days

        total_absences = self.env['hr.absences.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                  ('date_start', '>=', start_date),
                                                                  ('date_end', '<=', end_date)])
        results = 0
        if total_absences:
            for total_absent in total_absences:
                results += total_absent.total_absences
        result = work - (results / wage)
        return math.ceil(results), round(result, 2),

    # For Weekly Employees
    def get_late_undertime(self):
        start_date = datetime.strptime(self.date_from, '%Y-%m-%d')
        end_date = datetime.strptime(self.date_to, '%Y-%m-%d')
        absences = self.env['hr.absences.employee'].search([('employee_id', '=', self.employee_id.id),
                                                            ('date_start', '>=', start_date),
                                                            ('date_end', '<=', end_date)])
        result = 0.00
        # work = 0.00
        if absences:
            for absent in absences:
                result += absent.total_late + absent.total_undertime

        return math.ceil(result)

    @api.multi
    def get_hr_adjustment(self):
        array = []
        if self.contract_id.schedule_pay == 'semi-monthly':
            date_start = datetime.strptime(self.date_from, '%Y-%m-%d')
            date_end = datetime.strptime(self.date_to, '%Y-%m-%d')
        elif self.contract_id.schedule_pay == 'monthly':
            date_start = datetime.strptime(self.date_from, '%Y-%m-%d')
            date_end = datetime.strptime(self.date_to, '%Y-%m-%d')
        else:
            date_start = datetime.strptime(self.payslip_run_id.date_payment, '%Y-%m-%d')
            date_end = datetime.strptime(self.payslip_run_id.date_payment, '%Y-%m-%d')
        emp_ids = self.env['hr.adjustment.employee'].sudo().search([('emp_id', '=', self.employee_id.id),
                                                                    ('payment_date', '<=', date_end),
                                                                    ('payment_date', '>=', date_start)])
        for emp_adj_ids in emp_ids:
            array.append(emp_adj_ids.id)
            # emp_adj_ids.write({'paid': True})
        self.adjustment_list_ids = array
        print ('ADJ', array)
        return array

    @api.model
    def hr_verify_sheet(self):
        self.compute_sheet()
        array = []
        for line in self.loan_ids:
            if line.paid:
                array.append(line.id)
                # line.action_paid_amount()
            else:
                line.payroll_id = False
        self.loan_ids = array

        return self.write({'state': 'verify'})

    @api.multi
    def action_payslip_draft(self):
        if self.env.user.has_group('hr_payroll.group_hr_payroll_user') or self.env.user.has_group('hr_payroll.group_hr_payroll_manager'):
            self.write({'state': 'draft'})
        else:
            raise ValidationError(_("You are not allowed to post the payroll(s). Please contact the administrator."))
        return True

    @api.multi
    def action_payslip_done(self):
        if self.env.user.has_group('hr_payroll.group_hr_payroll_user') or self.env.user.has_group('hr_payroll.group_hr_payroll_manager'):
            if self.contract_id.schedule_pay == 'semi-monthly':
                date_start = datetime.strptime(self.date_from, '%Y-%m-%d')
                date_end = datetime.strptime(self.date_to, '%Y-%m-%d')
            elif self.contract_id.schedule_pay == 'monthly':
                date_start = datetime.strptime(self.date_from, '%Y-%m-%d')
                date_end = datetime.strptime(self.date_to, '%Y-%m-%d')
            else:
                date_start = datetime.strptime(self.payslip_run_id.date_payment, '%Y-%m-%d')
                date_end = datetime.strptime(self.payslip_run_id.date_payment, '%Y-%m-%d')
            absences = self.env['hr.absences.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                ('date_payment', '>=', date_start),
                                                                ('date_payment', '<=', date_end)])
            if absences:
                for absence in absences:
                    absence.write({'paid': True, 'payslip_id': self.id})

            holidays = self.env['hr.payslip.holidays.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                        ('date_payment', '>=', date_start),
                                                                        ('date_payment', '<=', date_end)])
            if holidays:
                for holiday in holidays:
                    holiday.write({'paid': True, 'payslip_id': self.id})

            overtimes = self.env['hr.payslip.overtime.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                         ('date_payment', '>=', date_start),
                                                                         ('date_payment', '<=', date_end)])
            if overtimes:
                for overtime in overtimes:
                    overtime.write({'paid': True, 'payslip_id': self.id})

            deductions = self.env['hr.deduction.employee'].sudo().search([('emp_id', '=', self.employee_id.id),
                                                                          ('payment_date', '<=', date_end),
                                                                          ('payment_date', '>=', date_start)], order='id')
            if deductions:
                for deduction in deductions:
                    if deduction.payroll_ded_id:
                        if deduction.unsettled_deduction > 0.000:
                            if deduction.deduction_type_id.loan_type_id:
                                loan_line = self.env['hr.loan.line']
                                employee_loan_line = loan_line.search([('employee_id', '=', deduction.emp_id.id),
                                                                  ('loan_type_id', '=', deduction.deduction_type_id.loan_type_id.id),
                                                                  ('paid_date', '=', deduction.payment_date),
                                                                  ('paid_amount', '=', deduction.actual_deduction)])
                                if employee_loan_line:
                                    loan_line.create({
                                        'paid_date': deduction.ud_payment_date,
                                        'paid_amount': deduction.unsettled_deduction,
                                        'employee_id': deduction.emp_id.id,
                                        'loan_id': loan_line.loan_id.id,
                                        'loan_type_id': deduction.deduction_type_id.loan_type_id.id})
                                employee_loan_line.write({'paid': True})
                            if deduction.hr_deduction_installment_line_id:
                                deduction_installment = self.env['hr.deduction.installment.line']
                                installment = deduction_installment.browse(deduction.hr_deduction_installment_line_id.id)
                                if installment:
                                    deduction_installment.create({
                                        'name': str(installment.deduction_id.name),
                                        'date': deduction.ud_payment_date,
                                        'amount': deduction.unsettled_deduction,
                                        'employee_id': deduction.emp_id.id,
                                        'deduction_id': installment.deduction_id.id,
                                        'deduction_type': installment.deduction_id.deduction_type.id})
                                    installment.write({'paid': True})
                    deduction.write({'paid': True})

            adjustments = self.env['hr.adjustment.employee'].sudo().search([('emp_id', '=', self.employee_id.id),
                                                                            ('payment_date', '<=', date_end),
                                                                            ('payment_date', '>=', date_start)])
            for adjustment in adjustments:
                adjustment.write({'paid': True})

            loan_ids = self.env['hr.loan.line'].sudo().search([('employee_id', '=', self.employee_id.id),
                                                               ('paid_date', '>=', date_start),
                                                               ('paid_date', '<=', date_end)])

            for loan in loan_ids:
                if loan.loan_type_id.set_as_priority:
                    loan.write({'paid': True})
            date_to = datetime.strptime(self.date_to, '%Y-%m-%d').date()
            date_start = date(date_to.year, 01, 01)
            date_end = date(date_to.year, 12, 31)
            annual_period = self.env['hr.payroll.annual.period']
            annual_period_res = annual_period.search([('company_id', '=', self.company_id.partner_id.id),
                                                      ('date_start', '=', date_start),
                                                      ('date_end', '=', date_end),
                                                      ('state', '=', 'open')], limit=1)
            thirteen_month_period = annual_period_res
            if thirteen_month_period:
                annual_emp_payslip = self.env['hr.payroll.annual.payslip']
                annual_employee = annual_emp_payslip.search([('employee_id', '=', self.employee_id.id),
                                                             ('annual_period_id', '=', thirteen_month_period.id)], limit=1)
                if annual_employee:
                    annual_employee.compute_sheet()
                else:
                    values = {
                        'name': '13th Month Salary of %s' % self.employee_id.name,
                        'employee_id': self.employee_id.id,
                        'annual_period_id': thirteen_month_period.id,
                        'company_id': self.employee_id.company_id.id,
                        'department_id': self.employee_id.department_id.id,
                        'date_start': thirteen_month_period.date_start,
                        'date_end': thirteen_month_period.date_end,
                        'date_payment': thirteen_month_period.date_payment,
                        'currency_id': self.employee_id.company_id.currency_id.id,
                    }
                    annual_emp_payslip_res = annual_emp_payslip.create(values)
                    annual_emp_payslip_res.compute_sheet()
            self.write({'state': 'done'})
        else:
            raise ValidationError(_("You are not allowed to post the payroll(s). Please contact the administrator."))

        return True

    def withholding_tax_contract(self, employee_id, date_start, date_end):
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        curyear = d_to.year
        clause_1 = ['&', ('date_end', '<=', date_to), ('date_end', '>=', date_from)]
        clause_2 = ['&', ('date_start', '<=', date_to), ('date_start', '>=', date_from)]
        clause_3 = ['&', ('date_start', '<=', date_from), '|', ('date_end', '=', False), ('date_end', '>=', date_to)]
        clause_final = [('employee_id', '=', employee_id), '|', '|'] + clause_1 + clause_2 + clause_3
        contracts = self.env['hr.contract'].sudo().search(clause_final)
        contract_list = []

        for contract in contracts:
            if contract.paid_payroll == 'monthly':
                salary_wage = contract.wage
            else:
                salary_wage = (contract.wage * contract.partner_ids.days_per_year) / 12.0
            get_date_start = datetime.strptime(contract.date_start, "%Y-%m-%d").date()
            if contract.date_end:
                get_date_end = datetime.strptime(contract.date_end, "%Y-%m-%d").date()
                end_of_the_year = date(curyear, 12, 31)
                if len(contracts) == 1 and end_of_the_year > get_date_end:
                    get_date_end = date(curyear, 12, 31)
            else:
                get_date_end = date(curyear, 12, 31)
            old_date_start_month = get_date_start.month
            old_date_start_year = get_date_start.year
            if old_date_start_year != curyear:
                old_date_start_month = 1
                if contract.date_end:
                    old_date_end_month = get_date_end.month
                else:
                    old_date_end_month = 12
            else:
                if contract.date_end:
                    old_date_end_month = get_date_end.month
                else:
                    old_date_end_month = 12

            no_of_months = (old_date_end_month - old_date_start_month) + 1
            beginning_month = date(old_date_start_year, old_date_start_month, get_date_start.day)
            ending_month = date(old_date_start_year, old_date_end_month, calendar.monthrange(old_date_start_year, old_date_end_month)[1])
            months = "%s - %s %s" % (beginning_month.strftime("%b"), ending_month.strftime("%b"), str(curyear))
            contract_list.append((months, salary_wage, no_of_months, salary_wage * no_of_months))
        return contract_list

    def withholding_tax_tardiness(self, employee_id, date_start, date_end):
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        payslips = self.search([('employee_id', '=', employee_id),
                                ('date_from', '>=', date_from), ('date_to', '<=', date_to)])
        result = 0.0
        if payslips:
            for p in payslips:
                for line in p.line_ids:
                    if line.code == 'ABS':
                        result += line.total
        # absences = self.env['hr.absences.employee'].search([('employee_id', '=', employee_id),
        #                                                     ('absent_list_id.date_start', '>=', date_from),
        #                                                     ('absent_list_id.date_end', '<=', date_to)])
        # result = 0.00
        # if absences:
        #     for absent in absences:
        #         result += absent.amount

        return round(result, 2)

    def withholding_tax_premium_pay(self, employee_id, date_start, date_end):
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        holiday_list = []
        holidays = self.env['hr.payslip.holidays.employee'].sudo().search([('employee_id', '=', employee_id),
                                                                    ('date_payment', '>=', date_from),
                                                                    ('date_payment', '<=', date_to)])

        if holidays:
            for holiday in holidays:
                holiday_found = self.env['hr.holidays.public.line'].search([('date', '=', holiday.date_premium)])
                date_premium = datetime.strptime(holiday.date_premium, '%Y-%m-%d').date()
                holiday_name = "%s %s" % (str(date_premium.strftime("%b %d, %Y")), holiday_found.name)
                holiday_list.append((holiday_name, holiday.regular_premium + holiday.special_premium))
        return holiday_list

    def withholding_tax_adjustment(self, employee_id, date_start, date_end):
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        adjustment_list = []
        adjustments = self.env['hr.adjustment.employee'].sudo().search([('emp_id', '=', employee_id),
                                                                 ('payment_date', '>=', date_from),
                                                                 ('payment_date', '<=', date_to)])

        if adjustments:
            for adjustment in adjustments:
                print adjustment.amount
                if adjustment.adjustment_type_id.is_taxable:
                    adjustment_name = "%s" % adjustment.adjustment_type_id.description
                    adjustment_list.append((adjustment_name, adjustment.amount))
        return adjustment_list

    def withholding_tax_overtime(self, employee_id, date_start, date_end):
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        payslips = self.search([('employee_id', '=', employee_id),
                                ('date_from', '>=', date_from), ('date_to', '<=', date_to)])
        result = 0.0
        if payslips:
            for p in payslips:
                for line in p.line_ids:
                    if line.code == 'OT':
                        result += line.total

        return round(result, 2)

    def withholding_tax_contributions(self, employee_id, date_start, date_end):
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        curyear = d_to.year
        clause_1 = ['&', ('date_end', '<=', date_to), ('date_end', '>=', date_from)]
        clause_2 = ['&', ('date_start', '<=', date_to), ('date_start', '>=', date_from)]
        clause_3 = ['&', ('date_start', '<=', date_from), '|', ('date_end', '=', False), ('date_end', '>=', date_to)]
        clause_final = [('employee_id', '=', employee_id), '|', '|'] + clause_1 + clause_2 + clause_3
        contracts = self.env['hr.contract'].sudo().search(clause_final)
        contract_list = []
        for contract in contracts:
            if contract.paid_payroll == 'monthly':
                salary_wage = contract.wage
            else:
                salary_wage = (contract.wage * contract.partner_ids.days_per_year) / 12.0
            get_date_start = datetime.strptime(contract.date_start, "%Y-%m-%d").date()
            if contract.date_end:
                get_date_end = datetime.strptime(contract.date_end, "%Y-%m-%d").date()
                end_of_the_year = date(curyear, 12, 31)
                if len(contracts) == 1 and end_of_the_year > get_date_end:
                    get_date_end = date(curyear, 12, 31)
            else:
                get_date_end = date(curyear, 12, 31)
            old_date_start_month = get_date_start.month
            old_date_start_year = get_date_start.year
            if old_date_start_year != curyear:
                old_date_start_month = 1
                if contract.date_end:
                    old_date_end_month = get_date_end.month
                else:
                    old_date_end_month = 12
            else:
                if contract.date_end:
                    old_date_end_month = get_date_end.month
                else:
                    old_date_end_month = 12

            # no_of_months = (old_date_end_month - old_date_start_month) + 1
            beginning_month = date(old_date_start_year, old_date_start_month, get_date_start.day)
            ending_month = date(old_date_start_year, old_date_end_month, calendar.monthrange(old_date_start_year, old_date_end_month)[1])

            months = "%s - %s %s" % (beginning_month.strftime("%b"), ending_month.strftime("%b"), str(curyear))
            # payslip = self.browse(self.id)
            sss_contribution = 0.0
            hdmf_contribution = 0.0
            phic_contribution = 0.0
            for month_of_year in range(beginning_month.month, ending_month.month + 1):
                end = date(curyear, month_of_year, calendar.monthrange(curyear, month_of_year)[1])
                sss_contribution += contract.get_previousssscont(end, salary_wage)
                hdmf_contribution += contract.get_previoushdmfcont(end, salary_wage)
                phic_contribution += (contract.get_previousphcont(end, salary_wage))
            contract_list.append(("SSS", ("%s %s" % ("SSS", months)), sss_contribution))
            contract_list.append(("PH", ("%s %s" % ("PH", months)), phic_contribution))
            contract_list.append(("HDMF", ("%s %s" % ("HDMF", months)), hdmf_contribution))
        return contract_list

    def withholding_tax_base(self, compensation,  date_end):
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_to = d_to.strftime("%Y-%m-%d")
        records = self.env['hr.tax.annual'].sudo().search([('minrange', '<=', compensation),
                                                           ('maxrange', '>=', compensation),
                                                           ('dateeffect', '<=', date_to),
                                                           ('active', '=', True)], limit=1)
        print records.minrange
        return records

    def withholding_tax_less(self, paylsip_id, employee_id, company_id, date_start, date_end):
        str_date = " "
        previous_withholding_tax = 0.0
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        start = datetime.strptime(date_from, "%Y-%m-%d").date()
        clause_1 = ['&', ('date_end', '<=', date_to), ('date_end', '>=', date_from)]
        clause_2 = ['&', ('date_start', '<=', date_to), ('date_start', '>=', date_from)]
        clause_3 = ['&', ('date_start', '<=', date_from), '|', ('date_end', '=', False), ('date_end', '>=', date_to)]
        clause_final = [('employee_id', '=', employee_id), '|', '|'] + clause_1 + clause_2 + clause_3
        contracts = self.env['hr.contract'].sudo().search(clause_final)
        contract_start = []
        # starting_date = start
        for contract in contracts:
            get_date_start = datetime.strptime(contract.date_start, "%Y-%m-%d").date()
            contract_start.append(get_date_start)
        min_date = min(contract_start)
        if min_date > start:
            starting_date = min_date
        else:
            starting_date = start

        period = self.env['hr.period'].search([('company_id', '=', company_id.partner_id.id),
                                               ('date_start', '=', d_from),
                                               ('date_stop', '=', d_to)], limit=1)
        number = period.number
        if number == 1:
            previous_new_year = date(d_from.year, 01, 01) - relativedelta(years=1)
            fiscal_year = self.env['hr.fiscalyear'].search([('company_id', '=', company_id.partner_id.id),
                                                            ('date_start', '=', previous_new_year)], limit=1)
            if fiscal_year:
                previous_payslip = self.env['hr.period'].search([('company_id', '=', company_id.partner_id.id),
                                                                 ('schedule_pay', '=', period.schedule_pay),
                                                                 ('fiscalyear_id', '=', fiscal_year.id),
                                                                 ('number', '=', len(fiscal_year.period_ids))], limit=1)
                if previous_payslip:
                    ending_date = datetime.strptime(previous_payslip.date_stop, "%Y-%m-%d").date()
                    if number > 1:
                        str_date = "%s - %s" % (starting_date.strftime("%b %d"), ending_date.strftime("%b %d, %Y"))
                        # if ending_date < starting_date:
                        #     str_date = ""
        else:
            previous_payslip = self.env['hr.period'].search([('company_id', '=', company_id.partner_id.id),
                                                             ('schedule_pay', '=', period.schedule_pay),
                                                             ('number', '=', number - 1)], limit=1)
            if previous_payslip:
                ending_date = datetime.strptime(previous_payslip.date_stop, "%Y-%m-%d").date()
                if number > 1:
                    str_date = "%s - %s" % (starting_date.strftime("%b %d"), ending_date.strftime("%b %d, %Y"))
                    # if ending_date < starting_date:
                    #     str_date = ""
        payslips = self.search([('id', '!=', paylsip_id), ('employee_id', '=', employee_id),
                                ('date_from', '>=', date_from), ('date_to', '<=', date_to)])
        if payslips:
            for p in payslips:
                for line in p.line_ids:
                    if line.code == 'WHTAX':
                        previous_withholding_tax += line.total
        print (number, starting_date, ending_date, previous_withholding_tax)
        return str_date, previous_withholding_tax

    def withholding_tax_remaining_vale(self, company_id, date_start, date_end):
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        period = self.env['hr.period'].search([('company_id', '=', company_id.partner_id.id),
                                               ('date_start', '=', d_from),
                                               ('date_stop', '=', d_to)], limit=1)
        fiscal_year = period.fiscalyear_id
        actual_period = 0
        if fiscal_year:
            actual_period = len(fiscal_year.period_ids)
        result = (actual_period - period.number) + 1
        return result

    @api.multi
    def subsidiary_ledger(self, employee_id, date_start, date_end):
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        payslip = self.env['hr.payslip'].search([('employee_id', '=', employee_id),
                                                 ('date_from', '>=', date_from),
                                                 ('set_as_regular_payroll', '=', True),
                                                 ('date_to', '<=', date_to)], order='date_from')
        record = []
        for rec in payslip:
            date_from = rec.date_from
            basic = 0
            cola = 0
            otadj = 0
            ot = 0
            holregular = 0
            holspecial = 0
            absences = 0
            late = 0
            gross = 0
            wtax = 0
            sss = 0
            hdmf = 0
            phhealth = 0
            # loan = 0
            otded = 0
            totded = 0
            net = 0
            # if not rec.adjustment_list_ids:
            adjustments = self.env['hr.adjustment.employee'].search([('emp_id', '=', employee_id),
                                                                     ('payment_date', '>=', rec.date_from),
                                                                     ('payment_date', '<=', rec.date_to)])

            if adjustments:
                for adj in adjustments:
                    if adj.adjustment_type_id.to_payroll_journal:
                        otadj += adj.amount
            payslip_lines = self.env['hr.payslip.line'].search([('slip_id', '=', rec.id)])
            for line in payslip_lines:
                if line.code == 'BASIC':
                    basic = line.total
                if line.code == 'COLA':
                    cola = line.total
                if line.code == 'OT':
                    ot = line.total
                if line.code == 'REGHDY':
                    holregular = line.total
                if line.code == 'SPCHDY':
                    holspecial = line.total
                if line.code in ['ABS', 'LATE']:
                    absences += line.total
                if line.code == 'GROSS':
                    gross = line.total
                if line.code == 'WHTAX':
                    wtax = line.total
                if line.code == 'SSSEE':
                    sss = line.total
                if line.code == 'HDMFEE':
                    hdmf = line.total
                if line.code == 'PHEE':
                    phhealth = line.total
                # if line.code == 'LOAN':
                #     loan = line.total
                if line.code == 'NETDED':
                    otded = line.total
                if line.code == 'TOTDED':
                    totded = line.total
                if line.code == 'NET':
                    net = line.total

            record.append({'date_from': rec.date_from,
                           'date_to': rec.date_to,
                           'basic': basic,
                           'cola': cola,
                           'otadj': otadj,
                           'ot': ot,

                           'holregular': holregular,
                           'holspecial': holspecial,
                           'absences': absences,
                           'late': late,

                           'gross': gross,
                           'wtax': wtax,
                           'sss': sss,
                           'hdmf': hdmf,

                           'phhealth': phhealth,
                           # 'loan': loan,
                           'otded': otded,
                           'totded': totded,
                           'net': net,
                           })
        return record

    def loan_list(self, employee_id, date_start, date_end):
        result = []
        d_from = datetime.strptime(date_start, "%Y-%m-%d").date()
        d_to = datetime.strptime(date_end, "%Y-%m-%d").date()
        date_from = d_from.strftime("%Y-01-01")
        date_to = d_to.strftime("%Y-%m-%d")
        records = self.env['hr.payslip'].search([('employee_id', '=', employee_id),
                                                 ('date_from', '>=', date_from),
                                                 ('set_as_regular_payroll', '=', True),
                                                 ('date_to', '<=', date_to)], order='date_from')
        if records:
            for rec in records:
                for loan in rec.loan_ids:
                    if (loan.loan_type_id.id, loan.loan_type_id.description) not in result:
                        result.append((loan.loan_type_id.id, loan.loan_type_id.description))
        return result

    @api.multi
    def print_withholding_tax(self):
        self.ensure_one()
        return self.env['report'].get_action(self, 'hrms_payroll.report_withholding_tax_template')

    @api.multi
    def print_subsidiary_ledger(self):
        self.ensure_one()
        return self.env['report'].get_action(self, 'hrms_payroll.report_payrollsubsidiary')


class HrPayslipLog(models.Model):
    _name = 'hr.payslip.log'
    _order = 'date_time desc'

    user_id = fields.Many2one(comodel_name="res.users", string="User(s)", required=False, default=lambda self: self.env.user)
    date_time = fields.Datetime(string="Date and Time", required=False, related='create_date', store=True)
    payslip_id = fields.Many2one(comodel_name="hr.payslip", string="Payslip", required=False, )


class HrPayslipLine(models.Model):
    _inherit = 'hr.payslip.line'

    amount_difference = fields.Float(string="Difference",  required=False, default=0.0)

    # @api.depends('quantity', 'amount', 'rate', 'amount_difference')
    # def _compute_total(self):
    #     for line in self:
    #         line.total = float(line.quantity) * (line.amount - line.amount_difference) * line.rate / 100

class HrEmployeeExclude(models.Model):
    _name = 'hr.employee.exclude'
    _description = "Exclude Employee"

    name = fields.Many2one('hr.employee', string="Employee")
    date_effect = fields.Date(string="Date")


class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'
    _order = 'date_payment desc'

    def _get_employee_id(self):
        user_id = self.env.user.id
        res_resource = self.env['resource.resource'].search([('user_id', '=', user_id)])
        if not res_resource and user_id != 1:
            raise ValidationError("No related employee assigned to current user. Please consult System Admin")
        else:
            resource_id = res_resource.id
            res_employee = self.env['hr.employee'].search([('resource_id', '=', resource_id)])
            employee_id = res_employee.id

        return employee_id

    payslip_count = fields.Integer(compute='_compute_payslip_count', string="Payslip generated")
    annual_paid = fields.Boolean(string="Include 13th Month Pay",)
    structure_id = fields.Many2one(comodel_name='hr.payroll.structure', string='Structure')
    
    @api.multi
    # def generate_adjustments(self):
    #     fixed_adjustment = self.env['payroll.fixed.adjustment']
    #     fixed_adjustment_employee = self.env['hr.adjustment.employee']
    #     adjustment_listing = self.env['hr.adjustment.list']
    #     adjustment_type = fixed_adjustment.get_adjustment_type(self.date_start, self.date_end, self.hr_period_id.vale_period)
    #     # fixed_records = fixed_adjustment.get_fixed_adjustment(self.date_start, self.date_end, self.hr_period_id.vale_period)
    #     clause_1 = ['&', ('date_ended', '<=', self.date_end,), ('date_ended', '>=', self.date_start)]
    #     clause_2 = ['&', ('date_start', '<=', self.date_end,), ('date_start', '>=', self.date_start)]
    #     clause_3 = ['&', ('date_start', '<=', self.date_start), '|', ('date_ended', '=', False), ('date_ended', '>=', self.date_end,)]
    #     clause_final = [('payment_type', 'in', [str(self.hr_period_id.vale_period), '3']), '|', '|'] + clause_1 + clause_2 + clause_3
    #     records = fixed_adjustment.search(clause_final)
    #     if adjustment_type:
    #         for adj_type in adjustment_type:
    #             adjustment_list_res = adjustment_listing.search([('adjustment_type', '=', adj_type.id),
    #                                                             ('payment_date', '=', self.date_payment),
    #                                                             ('state', '=', 'done')], limit=1)
    #             if not adjustment_list_res:
    #                 vals = {
    #                     'name': adj_type.description + "[" + str(self.date_payment) + "]",
    #                     'date_from': self.date_start,
    #                     'date_to': self.date_end,
    #                     'payment_date': self.date_payment,
    #                     'adjustment_type': adj_type.id,
    #                     'state': 'done',
    #                     'user_ids': self._get_employee_id()
    #                 }
    #                 adjustment_list = adjustment_listing.create(vals)
    #             else:
    #                 adjustment_list = adjustment_list_res
    #             if records:
    #                 for record in records:
    #                     # print str(record.employee_id.first_name)
    #                     if record.adjustment_type.id == adjustment_list.adjustment_type.id \
    #                             and record.employee_id.company_id.partner_id.id == self.company_id.id:
    #                         if str(record.payment_type) != '3':
    #                             amount = record.amount * 2.0
    #                         else:
    #                             amount = record.amount
    #                         adjustment_list_emp_res = fixed_adjustment_employee.search([('emp_id', '=', record.employee_id.id),
    #                                                                                     ('adjustment_type_id', '=', adj_type.id),
    #                                                                                     ('payment_date', '=', self.date_payment),
    #                                                                                     ('amount', '=', amount)], limit=1)
    #                         if not adjustment_list_emp_res:
    #                             # print (str(record.employee_id.first_name), amount)
    #                             values = {
    #                                 'identification_id': record.employee_id.identification_id,
    #                                 'emp_id': record.employee_id.id,
    #                                 'adjustment_list_id': adjustment_list.id,
    #                                 'adjustment_type_id': adj_type.id,
    #                                 'payment_date': self.date_payment,
    #                                 'amount': amount,
    #                             }
    #                             fixed_adjustment_employee.sudo().create(values)
    #             adjustment_list.action_done()

    # def generate_deduction_loan(self):
    #     deduction_employee = self.env['hr.deduction.employee']
    #     deduction_loan_type = self.env['hr.loan.type'].search([('set_as_priority', '=', False)])
    #     if deduction_loan_type:
    #         deduction_loan = self.env['hr.loan.line'].search([('loan_type_id', 'in', deduction_loan_type.ids),
    #                                                           ('paid_date', '>=', self.date_start),
    #                                                           ('paid_date', '<=', self.date_end)])
    #         if deduction_loan:
    #             ewa_deduction_types = self.env['hr.deduction.type'].search([('ewa_loan', '=', True)])
    #             deduction_list = self.env['hr.deduction.list']
    #             if ewa_deduction_types:
    #                 for ewa_deduction_type in ewa_deduction_types:
    #                     deduction_list_res = deduction_list.search([('deduction_type', '=', ewa_deduction_type.id),
    #                                                                 ('payment_date', '=', self.date_payment),
    #                                                                 ('state', '=', 'done')], limit=1)
    #                     if not deduction_list_res:
    #                         vals = {
    #                             'deduction_type': ewa_deduction_type.id,
    #                             'name': ewa_deduction_type.description + "[" + str(self.date_payment) + "]",
    #                             'payment_date': self.date_payment,
    #                             'state': 'done',
    #                             'user_ids': self._get_employee_id()
    #                         }
    #                         ewa_loan_list = deduction_list.create(vals)
    #                     else:
    #                         ewa_loan_list = deduction_list_res
    #                     if ewa_loan_list:
    #                         loans = self.env['hr.loan.line'].search([('loan_type_id', '=', ewa_deduction_type.loan_type_id.id),
    #                                                                  ('paid_date', '>=', self.date_start),
    #                                                                  ('paid_date', '<=', self.date_end)])
    #                         for loan in loans:
    #                             if loan.employee_id.company_id.partner_id.id == self.company_id.id:
    #                                 emp_res = deduction_employee.search([('payment_date', '=',  self.date_payment),
    #                                                                      ('emp_id', '=', loan.employee_id.id),
    #                                                                      ('amount', '=', loan.paid_amount)], limit=1)
    #                                 if not emp_res:
    #                                     values = {
    #                                         'deduction_list_id': ewa_loan_list.id,
    #                                         'identification_id': loan.employee_id.identification_id,
    #                                         'emp_id': loan.employee_id.id,
    #                                         'payment_date': self.date_payment,
    #                                         'amount': loan.paid_amount,
    #                                         'actual_deduction': loan.paid_amount,
    #                                     }
    #                                     deduction_employee.create(values)

    # def get_unsettled_deduction(self):
    #     deduction_employee = self.env['hr.deduction.employee']
    #     emp_ids = deduction_employee.search([('ud_payment_date', '>=', self.date_start),
    #                                          ('ud_payment_date', '<=', self.date_end),
    #                                          ('unsettled_deduction', '>', 0.0)], order='id')
    #     deduct_list = []
    #     if emp_ids:
    #         for rec in emp_ids:
    #             if rec.deduction_list_id.deduction_type.id not in deduct_list:
    #                 deduct_list.append(rec.deduction_list_id.deduction_type.id)
    #         result = list(set(deduct_list))
    #         deduction_type = self.env['hr.deduction.type'].search([('id', 'in', result)])
    #         deduction_list = self.env['hr.deduction.list']
    #         for ded_type in deduction_type:
    #             deduction_list_res = deduction_list.search([('payment_date', '<=', self.date_start),
    #                                                         ('payment_date', '>=', self.date_end),
    #                                                         ('deduction_type', '=', ded_type.id)], limit=1)
    #             if deduction_list_res:
    #                 ded_list = deduction_list_res
    #             else:
    #                 ded_list = deduction_list.sudo().create(
    #                     {'deduction_type': ded_type.id,
    #                      'name': ded_type.description,
    #                      'payment_date': self.date_payment,
    #                      'state': 'done'})
    #             if ded_list:
    #                 for emp_ded_ids in emp_ids:
    #                     if emp_ded_ids.deduction_list_id.deduction_type.id == ded_type.id and \
    #                             emp_ded_ids.emp_id.company_id.partner_id.id == self.company_id.id:
    #                         deduction_employee_res = deduction_employee.search([('emp_id', '=', emp_ded_ids.emp_id.id),
    #                                                                             ('payment_date', '=', self.date_payment),
    #                                                                             ('amount', '=', emp_ded_ids.unsettled_deduction),
    #                                                                             ('actual_deduction', '=', emp_ded_ids.unsettled_deduction)], limit=1)
    #                         if not deduction_employee_res:
    #                             values = {
    #                                 'identification_id': emp_ded_ids.emp_id.identification_id,
    #                                 'emp_id': emp_ded_ids.emp_id.id,
    #                                 'payment_date': emp_ded_ids.ud_payment_date,
    #                                 'deduction_list_id': ded_list.id,
    #                                 'amount': emp_ded_ids.unsettled_deduction,
    #                                 'actual_deduction': emp_ded_ids.unsettled_deduction,
    #                                 }
    #                             deduction_employee.sudo().create(values)

    # @api.multi
    # def generate_batch_adjustments(self):
    #     self.generate_adjustments()
        # self.generate_deduction_loan()
        # self.get_unsettled_deduction()

    @api.multi
    def _compute_payslip_count(self):
        for payslip in self:
            payslip.payslip_count = len(payslip.slip_ids)

    def act_payslips(self):
        return {
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.payslip',
            'domain': [
                ('payslip_run_id', '=', self.id),
            ],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'nodestroy': True,
            'name': 'Included Employee'}

    # @api.model
    # def create(self, values):
    #     res = super(HrPayslipRun, self).create(values)
    #     if res:
    #         res.generate_batch_adjustments()
    #         res.get_unsettled_deduction()
    #     return res
    #
    # @api.multi
    # def write(self, values):
    #     res = super(HrPayslipRun, self).write(values)
    #     if res:
    #         self.generate_batch_adjustments()
    #         self.get_unsettled_deduction()
    #     return res


class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'

    emp_count = fields.Integer(string="Count")

    # sample context
    @api.model
    def default_get(self, fields):
        rec = super(HrPayslipEmployees, self).default_get(fields)
        context = self._context
        active_id = self.env.context.get('active_id')

        if 'company_id' in context:
            company_id = context['company_id']
        else:
            company_id = ''

        if 'schedule_pay' in context:
            schedule_pay = context['schedule_pay']
        else:
            schedule_pay = ''

        if 'date_start' in context:
            date_start = context['date_start']
        else:
            date_start = ''

        if 'date_end' in context:
            date_end = context['date_end']
        else:
            date_end = ''

        # accept open contract only
        # contract_ids = self.env['hr.contract'].search([('partner_ids', '=', company_id),
        #                                                ('date_start', '<=', date_end), '|',
        #                                                ('date_end', '>=', date_start),
        #                                                ('date_end', '=', False), ('state', '=', 'open')])

        # print contract_ids
        company = self.env['res.company'].search([('partner_id', '=', company_id)], limit=1)
        employee_ids = self.env['hr.employee'].search([('company_id', '=', company.id), ('state', 'not in', ['relieved','terminate'])])

        emp_ids = []
        count = 0
        for rec_emp in employee_ids:
            payslip_id = self.env['hr.payslip'].search([('employee_id', '=', rec_emp.id),
                                                        ('date_from', '=', date_start),
                                                        ('date_to', '=', date_end),
                                                        ('state', '=', 'draft')])
            if not payslip_id:
                contract = self.env['hr.contract']
                clause_1 = ['&', ('date_end', '<=', date_end), ('date_end', '>=', date_start)]
                clause_2 = ['&', ('date_start', '<=', date_end), ('date_start', '>=', date_start)]
                clause_3 = ['&', ('date_start', '<=', date_start), '|', ('date_end', '=', False),
                            ('date_end', '>=', date_end)]
                clause_final = [('employee_id', '=', rec_emp.id), ('schedule_pay', '=', schedule_pay), '|',
                                '|'] + clause_1 + clause_2 + clause_3
                cont_res = contract.search(clause_final, limit=1, order='date_start desc')
                previous_contract = self.env['hr.contract'].search([('state', '=', 'close'),
                                                                    ('schedule_pay', '=', schedule_pay),
                                                                    ('employee_id', '=', rec_emp.id)],
                                                                   order='date_start desc', limit=1)
                if cont_res:
                    if rec_emp.id not in emp_ids:
                        emp_ids.append(rec_emp.id)
                if not cont_res and previous_contract:
                    if rec_emp.id not in emp_ids:
                        emp_ids.append(rec_emp.id)

            count += 1
        rec['emp_count'] = count
        rec['employee_ids'] = emp_ids

        return rec

    @api.onchange('company_id')
    def _onchange_company_id(self):
        active_id = self.env.context.get('active_id')
        run_data = self.env['hr.payslip.run'].browse(active_id)
        if self.company_id:
            result = {}
            emp = []
            contracts = self.env['hr.contract'].search([('schedule_pay', '=', run_data.schedule_pay),
                                                        ('date_start', '<=', run_data.date_end), '|',
                                                        ('date_end', '>=', run_data.date_start),
                                                        ('date_end', '=', False), ('state', '=', 'open')])
            for contract in contracts:
                if contract.employee_id.id not in emp:
                    emp.append(contract.employee_id.id)
            result['domain'] = {'employee_ids': [('id', 'in', emp), ('company_id.partner_id', '=', run_data.company_id.id), ('active', '=', True)]}
            return result

    @api.multi
    def compute_sheet(self):
        payslips = self.env['hr.payslip']
        [data] = self.read()
        active_id = self.env.context.get('active_id')
        batch = self.env['hr.payslip.run'].browse(active_id)
        if active_id:
            [run_data] = self.env['hr.payslip.run'].browse(active_id).read(['date_start', 'date_end', 'credit_note'])
        from_date = run_data.get('date_start')
        to_date = run_data.get('date_end')
        if not data['employee_ids']:
            raise UserError(_("You must select employee(s) to generate payslip(s)."))
        # batch.generate_batch_adjustments()
        # batch.get_unsettled_deduction()
        for employee in self.env['hr.employee'].browse(data['employee_ids']):
            # if employee.contract_id and employee.contract_id.struct_id and employee.contract_id.date_start <= from_date:
            exclude = self.env['hr.employee.exclude'].search([('name', '=', employee.id),
                                                              ('date_effect', '>=', from_date),
                                                              ('date_effect', '<=', to_date)])
            if not exclude:

                emp_payslip = payslips.sudo().search([('employee_id', '=', employee.id),
                                                      ('date_from', '=', from_date),
                                                      ('date_to', '=', to_date)])
                if emp_payslip:
                    emp_payslip.compute_sheet()
                if not emp_payslip:
                    slip_data = self.env['hr.payslip'].onchange_employee_id(from_date, to_date, employee.id, contract_id=False)
                    res = {
                            'employee_id': employee.id,
                            'name': slip_data['value'].get('name'),
                            'struct_id': slip_data['value'].get('struct_id'),
                            'contract_id': slip_data['value'].get('contract_id'),
                            'payslip_run_id': active_id,
                            'input_line_ids': [(0, 0, x) for x in slip_data['value'].get('input_line_ids')],
                            'worked_days_line_ids': [(0, 0, x) for x in slip_data['value'].get('worked_days_line_ids')],
                            'date_from': from_date,
                            'date_to': to_date,
                            'credit_note': run_data.get('credit_note'),
                            'department_id': employee.department_id.id,
                            'company_id': employee.company_id.id,
                    }
                    payslips += self.env['hr.payslip'].create(res)
        payslips.compute_sheet()
        return {'type': 'ir.actions.act_window_close'}


class HrPayslipConfirmWizard(models.TransientModel): # Draft to Done
    _name = 'hr.payslip.confirm.wizard'

    @api.multi
    def multi_payslip_confirm(self):
        payslip_ids = self.env['hr.payslip'].browse(self._context.get(
                'active_ids'))
        for payslip in payslip_ids:
            if payslip.state == 'draft':
                payslip.action_payslip_done()


class HrPayslipDraftWizard(models.TransientModel): # Done to Draft
    _name = 'hr.payslip.draft.wizard'

    @api.multi
    def multi_payslip_draft(self):
        payslip_ids = self.env['hr.payslip'].browse(self._context.get(
                'active_ids'))
        for payslip in payslip_ids:
            if payslip.state == 'done':
                payslip.action_payslip_draft()


class HrPayslipReprocessWizard(models.TransientModel): # Reprocess All
    _name = 'hr.payslip.reprocess.wizard'

    @api.multi
    def multi_payslip_reprocess(self):
        payslip_ids = self.env['hr.payslip'].browse(self._context.get(
                'active_ids'))
        for payslip in payslip_ids:
            if payslip.state == 'draft':
                payslip.compute_sheet()


class HrPayslipPayrollAdjustmentWizard(models.TransientModel):
    _name = 'hr.payslip.payroll.adjustment.wizard'

    @api.multi
    def multi_payslip_payroll_adjustment(self):
        payslip_ids = self.env['hr.payslip'].browse(self._context.get(
                'active_ids'))
        for payslip in payslip_ids:
            if payslip.state == 'draft':
                # payslip.get_hr_deduction()
                payslip.get_hr_adjustment()
                ded = payslip.deduction_list_ids
                ded.update({'paid': True})
                adj = payslip.adjustment_list_ids
                adj.update({'paid': True})


class PayslipSequence(models.Model):
    _inherit = 'ir.sequence'

    payroll = fields.Boolean("Payroll")

    @api.model
    def default_get(self, fields):
        context = self._context
        print context
        result = super(PayslipSequence, self).default_get(fields)
        if context:
            if 'payroll' in context:
                if context['payroll'] == True:
                    result['payroll'] = True

        return result


class HrPayrollAccount(models.Model):
    _name = 'hr.payroll.account'

    @api.multi
    @api.depends('company_id', 'account_id')
    def compute_name(self):
        for record in self:
            name = ''
            if record.company_id:
                name = "%s" % record.company_id.partner_id.abbreviation
            if record.company_id and record.account_id:
                name = "%s [%s]" % (record.company_id.partner_id.abbreviation, record.account_id.acc_number)
            record.update({'name': name})

    name = fields.Char(string="Name", required=False, compute="compute_name", store=True)
    company_id = fields.Many2one(comodel_name="res.company", string="Company", required=False, )
    account_id = fields.Many2one(comodel_name="res.partner.bank", string="Account Number", required=False, domain="[('company_use', '=', True)]")
    account_name = fields.Char(string="Account Holder", required=False, related="account_id.partner_id.name", store=True)
    active = fields.Boolean(string="Active", default=True)

    @api.constrains('company_id', 'account_id')
    def check_payroll_account(self):
        for record in self:
            if not record.company_id and record.account_id:
                account = self.search([('id', '!=', record.id),
                                        ('company_id', '=', record.company_id.id),
                                        ('account_id', '=', record.account_id.id)])
                if account:
                    raise ValidationError(_('The record is already exists.'))