from odoo import models, fields, api, _

class HrEmployeeExclude(models.Model):
    _inherit = 'hr.employee'
    
    sss = fields.Char(string='SSS')
    gsis =fields.Char(string='GSIS')
    philhealth = fields.Char(string='Phil Health')
    hdmf = fields.Char(string='HDMF')
    tin_id = fields.Char(string='TIN ID')