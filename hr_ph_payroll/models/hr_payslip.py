from odoo import api, fields, models, _

class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'
    _order = 'date_payment desc'
    
    structure_id = fields.Many2one(comodel_name='hr.payroll.structure', string='Structure')