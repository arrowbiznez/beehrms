from odoo import api, fields, models


SCHEDULEPAY = [
        ('weekly', 'Weekly'),
        ('semi-monthly', 'Semi-Monthly'),
        ('monthly', 'Monthly'),
    ]
PAYVALE = [
        ('firstvale','First Vale'),
        ('secondvale','Second Vale')
    ]

PAYMENTWEEKDAY = [
            ('0', 'Sunday'),
            ('1', 'Monday'),
            ('2', 'Tuesday'),
            ('3', 'Wednesday'),
            ('4', 'Thursday'),
            ('5', 'Friday'),
            ('6', 'Saturday')
    ] 

class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def default_get(self, fields):
        result = super(ResPartner, self).default_get(fields)
        # 178 is the id of Ph (not sure :D )
        result['country_id'] = 178
        return result

    owned_company = fields.Boolean(string='Owned Company')
    abbreviation = fields.Char(string="Abbreviation")
    days_per_year = fields.Integer(string="Days / Year", required=False, )
    
    pay_vale = fields.Selection(PAYVALE, string="Date of Vale")
    schedule_pay = fields.Selection(SCHEDULEPAY, string='Scheduled Pay', default='monthly')
    payment_weekday = fields.Selection(PAYMENTWEEKDAY, 'Day of Payment')
