from odoo import models, fields, api, _
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
import calendar

def daterange(start_date, end_date):
    end_date = end_date + timedelta(days=1)
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


SCHEDULEPAY = [
    ('daily', 'Daily'),
    ('weekly', 'Weekly'),
    ('semi-monthly', 'Semi-Monthly'),
    ('monthly', 'Monthly'),
    ('annually', 'Annually'),
]

PAIDPAYROLL = [
    ('daily', 'Daily'),
    ('monthly', 'Monthly')
]

class HrContracts(models.Model):
    _inherit = 'hr.contract'
    
    
    paid_payroll = fields.Selection(PAIDPAYROLL,string='Paid Payroll Type', default='monthly')
    schedule_pay = fields.Selection(SCHEDULEPAY, string='Scheduled Pay', default='monthly')
    
    #Contributions Deductions - must be in Salary Structure
    philhealth_cont = fields.Boolean(string='Philhealth', default=True)
    sss_cont = fields.Boolean(string='SSS', default=True)
    gsis_cont = fields.Boolean(string='GSIS', default=True)
    hdmf_cont = fields.Boolean(string='Pagi-ibig', default=True)
    wholdtax_cont = fields.Boolean(string='Withholding Tax', default=True)
    
    cola = fields.Monetary('COLA')
    allowance = fields.Monetary('PERA')
    
    #@api.multi
    def count_contract_of_days(self):
        result = 0.0
        if self.working_hours:
            days = []
            for line in self.working_hours.attendance_ids:
                if int(line.dayofweek) not in days:
                    days.append(int(line.dayofweek))
            start = date(date.today().year, 1, 1)
            end = date(date.today().year, 12, 31)
            for dates in daterange(start, end):
                if dates.weekday() in days:
                    result += 1
        return result

    def _get_number_of_days(self, days_per_year, date_from, date_to):
        result = 0
        if days_per_year and date_from and date_to:
            if days_per_year == 313:
                restday = [6]
            else:
                restday = [5, 6]

            for single_day in daterange(date_from, date_to):
                if single_day.weekday() not in restday:
                    result += 1
        return result

    def basic_wage(self, payslip, worked_days):
        date_from = payslip.date_from.date()
        date_to = payslip.date_to.date()
        emp_sched_pay = self.schedule_pay
        emp_paid_pay = self.paid_payroll
        result = 0.00
        date_start = self.date_start
        days_per_year = self.company_id.partner_id.days_per_year
        if self.department_id:
            if self.department_id.inter_company_id:
                days_per_year = self.department_id.inter_company_id.partner_id.days_per_year
        else:
            days_per_year = self.company_id.partner_id.days_per_year
        if self.employee_id.date_separated and self.date_end:
            date_separated = self.date_end
            if date_start <= date_from:
                if date_separated > date_from and date_separated < date_to:
                    number_of_days = self._get_number_of_days(days_per_year, date_from, date_separated)
                    salary = self.wage
                    result = ((salary * 12) / days_per_year) * number_of_days
        else:
            if date_start <= date_from:
                salary = self.wage
                if emp_paid_pay == 'daily':
                    if emp_sched_pay == 'weekly':
                        if worked_days:
                            result = (worked_days.WORK100.number_of_days * salary)
                    elif emp_sched_pay == 'semi-monthly':
                        result = (self._get_number_of_days(days_per_year, date_from, date_to) * salary)
                    elif emp_sched_pay == 'monthly':
                        result = (self._get_number_of_days(days_per_year, date_from, date_to) * salary)
                else:
                    if emp_sched_pay == 'monthly':
                        result = salary
                    elif emp_sched_pay == 'semi-monthly':
                        result = salary / 2
                    elif emp_sched_pay == 'weekly':
                        if worked_days:
                            emp_wage = round((salary * 12) / days_per_year, 2)
                            result = (worked_days.WORK100.number_of_days * emp_wage)
            else:
                previous_contract = self.search([('id', '!=', self.id), ('employee_id', '=', self.employee_id.id)],
                                                order='date_start desc', limit=1)
                salary = previous_contract.wage
                if emp_paid_pay == 'daily':
                    if emp_sched_pay == 'weekly':
                        if worked_days:
                            result = (self._get_number_of_days(days_per_year, date_from, date_to) * salary)
                    elif emp_sched_pay == 'semi-monthly':
                        result = salary * self._get_number_of_days(days_per_year, date_from, date_to)
                    elif emp_sched_pay == 'monthly':
                        result = salary * self._get_number_of_days(days_per_year, date_from, date_to)
                else:
                    if emp_sched_pay == 'monthly':
                        result = salary
                    elif emp_sched_pay == 'semi-monthly':
                        result = salary / 2
                    elif emp_sched_pay == 'weekly':
                        if worked_days:
                            emp_wage = round((salary * 12) / days_per_year, 2)
                            result = (self._get_number_of_days(days_per_year, date_from, date_to) * emp_wage)
        return round(result, 2)

    # employee philhealth
    def phcontribution(self, worked_days, payslip):
        cont_for = 'philhealth'
        share_for = 'eeshare'
        emp_sched_pay = self.schedule_pay
        rpartner = self.partner_ids.id
        table = 'hr.philhealth.table'
        result = 0.00
        code = 'PHEE'

        if self.philhealth_cont:
            # comp_partner = self.env['res.partner'].search([('id', '=', rpartner)])
            if self.paid_payroll == 'monthly':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(phic, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(phic, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    contr = round(phic / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = phic - (self.get_previous_contributions(payslip, code))
                        else:
                            result = phic
                    else:
                        result = contr

                    if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
                            self.employee_id.date_separated > payslip.date_from and \
                            self.employee_id.date_separated < payslip.date_to:
                        result = results

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    contr = round(phic / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

            if self.paid_payroll == 'daily':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(phic, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(phic, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
                            self.employee_id.date_separated > payslip.date_from and \
                            self.employee_id.date_separated < payslip.date_to:
                        schedpay = 1
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    contr = round(phic / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = phic - (self.get_previous_contributions(payslip, code))
                        else:
                            result = phic
                    else:
                        result = contr

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    contr = round(phic / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

        result = result + self.getcontributionadjustment(payslip, cont_for, share_for)
        #print('PHEE', round(result, 2))

        return round(result, 2)

    # employee sss
    def ssscontribution(self, worked_days, payslip):
        cont_for = 'sss'
        share_for = 'eeshare'
        emp_sched_pay = self.schedule_pay
        rpartner = self.partner_ids.id
        table = 'hr.sss.table'
        code = 'SSSEE'
        result = 0.00
        if self.sss_cont:
            # comp_partner = self.env['res.partner'].search([('id', '=', rpartner)])
            if self.paid_payroll == 'monthly':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        additional_cont = results - (
                            self.get_additional_contribution(payslip, worked_days, schedpay, code))
                        result = round(additional_cont, rounds)
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(results, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = results - (self.get_previous_contributions(payslip, code))
                        else:
                            result = results
                    else:
                        result = contr
                    if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
                            self.employee_id.date_separated > payslip.date_from and  \
                            self.employee_id.date_separated < payslip.date_to:
                        result = results

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

            if self.paid_payroll == 'daily':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        additional_cont = results - (
                            self.get_additional_contribution(payslip, worked_days, schedpay, code))
                        result = round(additional_cont, rounds)
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(results, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = results - (self.get_previous_contributions(payslip, code))
                        else:
                            result = results
                    else:
                        result = contr

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

        result = result + self.getcontributionadjustment(payslip, cont_for, share_for)
        #print 'SSSEE', round(result, 2)

        return round(result, 2)

    # employee hdmf
    def hdmfcontribution(self, worked_days, payslip):
        cont_for = 'hdmf'
        share_for = 'eeshare'
        emp_wage = self.basic_wage(payslip, worked_days)
        rpartner = self.partner_ids.id
        emp_sched_pay = self.schedule_pay
        table = 'hr.hdmf.table'
        code = 'HDMFEE'
        result = 0.00
        if self.hdmf_cont:
            if self.paid_payroll == 'monthly':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    hdmf = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(hdmf, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(hdmf, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = results - (self.get_previous_contributions(payslip, code))
                        else:
                            result = results
                    else:
                        result = contr

                    if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
                            self.employee_id.date_separated > payslip.date_from and \
                            self.employee_id.date_separated < payslip.date_to:
                        result = results

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

            if self.paid_payroll == 'daily':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    hdmf = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(hdmf, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(hdmf, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = results - (self.get_previous_contributions(payslip, code))
                        else:
                            result = results
                    else:
                        result = contr

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contribution(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

        result = result + self.getcontributionadjustment(payslip, cont_for, share_for)
        #print 'HDMFEE', round(result, 2)
        return round(result, 2)

    # employer philhealth
    def phcompanycont(self, worked_days, payslip):
        cont_for = 'philhealth'
        share_for = 'ershare'
        emp_sched_pay = self.schedule_pay
        table = 'hr.philhealth.table'
        code = 'PHER'
        result = 0.0
        if self.philhealth_cont:
            if self.paid_payroll == 'monthly':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    #print schedpay
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(phic, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(phic, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    contr = round(phic / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = phic - (self.get_previous_contributions(payslip, code))
                        else:
                            result = phic
                    else:
                        result = contr
                    if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
                            self.employee_id.date_separated > payslip.date_from and \
                            self.employee_id.date_separated < payslip.date_to:
                        result = results

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    contr = round(phic / schedpay, 2)
                    result = contr
                else:
                    result = 0.00

            if self.paid_payroll == 'daily':
                if emp_sched_pay == 'weekly':

                    schedpay = 4
                    #print schedpay
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(phic, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(phic, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    contr = round(phic / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = phic - (self.get_previous_contributions(payslip, code))
                        else:
                            result = phic
                    else:
                        result = contr

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    phic = round(results / schedpay, 2)
                    contr = round(phic / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

            result = result + self.getcontributionadjustment(payslip, cont_for, share_for)
        #print 'PHER', round(result, 2)
        return round(result, 2)

    # ec_er and ershare
    def sssercompanycont(self, worked_days, payslip):
        cont_for = 'sss'
        share_for = 'ershare'
        emp_sched_pay = self.schedule_pay
        table = 'hr.sss.table'
        code = 'SSSER'
        result = 0.0
        if self.sss_cont:
            if self.paid_payroll == 'monthly':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    print('ok')
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    print('ok')
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        additional_cont = results - (
                            self.get_additional_contribution(payslip, worked_days, schedpay, code))
                        result = round(additional_cont, rounds)
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(results, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = results - (self.get_previous_contributions(payslip, code))
                        else:
                            result = results
                    else:
                        result = contr
                    if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
                            self.employee_id.date_separated > payslip.date_from and \
                            self.employee_id.date_separated < payslip.date_to:
                        result = results

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr
                else:
                    result = 0.00

            if self.paid_payroll == 'daily':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        additional_cont = results - (
                            self.get_additional_contribution(payslip, worked_days, schedpay, code))
                        result = round(additional_cont, rounds)
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(results, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = results - (self.get_previous_contributions(payslip, code))
                        else:
                            result = results
                    else:
                        result = contr

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr
                else:
                    result = 0.00

            result = result + self.getcontributionadjustment(payslip, cont_for, share_for)
        #print 'SSSER', round(result, 2)
        return round(result, 2)
        
    def ssseccompanycont(self, worked_days, payslip):
        emp_sched_pay = self.schedule_pay
        table = 'hr.sss.table'
        code = 'SSSEC'
        result = 0.0
        if self.sss_cont:
            if self.paid_payroll == 'monthly':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contributionec(payslip, worked_days, schedpay, table)
                    sss = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(sss, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(sss, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contributionec(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        result = contr
                    else:
                        result = results - contr
                    if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
                            self.employee_id.date_separated > payslip.date_from and \
                            self.employee_id.date_separated < payslip.date_to:
                        result = results

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contributionec(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr

                else:
                    result = 0.00
            if self.paid_payroll == 'daily':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contributionec(payslip, worked_days, schedpay, table)
                    sss = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(sss, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(sss, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contributionec(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        result = contr
                    else:
                        result = results - contr

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contributionec(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

        #print 'SSSEC', round(result, 2)
        return round(result, 2)
        
    # # GSIS Contribution
    # def gsiscompanycont(self, worked_days, payslip, code, share_for):
    #     cont_for = 'sss'
    #     # share_for = 'ershare'
    #     emp_sched_pay = self.schedule_pay
    #     table = 'hr.sss.table'
    #     # code = 'SSSER'
    #     result = 0.0
    #     if self.sss_cont:
    #         if self.paid_payroll == 'monthly':
    #             if emp_sched_pay == 'weekly':
    #                 schedpay = 4
    #                 results = self.get_contributioner(payslip, worked_days, schedpay, table)
    #                 if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
    #                     rounds = 2
    #                     additional_cont = results - (
    #                         self.get_additional_contribution(payslip, worked_days, schedpay, code))
    #                     result = round(additional_cont, rounds)
    #                 if payslip.payslip_run_id.hr_period_id.vale_period < 4:
    #                     rounds = 0
    #                     result = round(results, rounds)

    #             elif emp_sched_pay == 'semi-monthly':
    #                 schedpay = 2
    #                 results = self.get_contributioner(payslip, worked_days, schedpay, table)
    #                 contr = round(results / schedpay, 2)
    #                 if payslip.payslip_run_id.hr_period_id.vale_period == 2:
    #                     if self.get_previous_contributions(payslip, code) > 0.0:
    #                         result = results - (self.get_previous_contributions(payslip, code))
    #                     else:
    #                         result = results
    #                 else:
    #                     result = contr
    #                 if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
    #                         self.employee_id.date_separated > payslip.date_from and \
    #                         self.employee_id.date_separated < payslip.date_to:
    #                     result = results

    #             elif emp_sched_pay == 'monthly':
    #                 schedpay = 1
    #                 results = self.get_contributioner(payslip, worked_days, schedpay, table)
    #                 contr = round(results / schedpay, 2)
    #                 result = contr
    #             else:
    #                 result = 0.00

    #         # if self.paid_payroll == 'daily':
    #         #     if emp_sched_pay == 'weekly':
    #         #         schedpay = 4
    #         #         results = self.get_contributioner(payslip, worked_days, schedpay, table)
    #         #         if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
    #         #             rounds = 2
    #         #             additional_cont = results - (
    #         #                 self.get_additional_contribution(payslip, worked_days, schedpay, code))
    #         #             result = round(additional_cont, rounds)
    #         #         if payslip.payslip_run_id.hr_period_id.vale_period < 4:
    #         #             rounds = 0
    #         #             result = round(results, rounds)

    #         #     elif emp_sched_pay == 'semi-monthly':
    #         #         schedpay = 2
    #         #         results = self.get_contributioner(payslip, worked_days, schedpay, table)
    #         #         contr = round(results / schedpay, 2)
    #         #         if payslip.payslip_run_id.hr_period_id.vale_period == 2:
    #         #             if self.get_previous_contributions(payslip, code) > 0.0:
    #         #                 result = results - (self.get_previous_contributions(payslip, code))
    #         #             else:
    #         #                 result = results
    #         #         else:
    #         #             result = contr

    #         #     elif emp_sched_pay == 'monthly':
    #         #         schedpay = 1
    #         #         results = self.get_contributioner(payslip, worked_days, schedpay, table)
    #         #         contr = round(results / schedpay, 2)
    #         #         result = contr
    #         #     else:
    #         #         result = 0.00

    #         result = result + self.getcontributionadjustment(payslip, cont_for, share_for)
    #     #print 'SSSER', round(result, 2)
    #     return round(result, 2)

    # hdmf
    def hdmfercompanycont(self, worked_days, payslip):
        cont_for = 'hdmf'
        share_for = 'ershare'
        emp_wage = self.basic_wage(payslip, worked_days)
        emp_sched_pay = self.schedule_pay
        table = 'hr.hdmf.table'
        code = 'HDMFER'
        rpartner = self.partner_ids.id

        # if worked_days.ATTN.number_of_days == 0.00:
        #     result = 0.00
        # else:
        # comp_partner = self.env['res.partner'].search([('id', '=', rpartner)])
        result = 0.0
        if self.hdmf_cont:
            if self.paid_payroll == 'monthly':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    hdmf = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        contr = round(hdmf, rounds)
                        resultz = results - (round(hdmf, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(hdmf, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = results - (self.get_previous_contributions(payslip, code))
                        else:
                            result = results
                    else:
                        result = contr

                    if payslip.payslip_run_id.hr_period_id.vale_period == 1 and \
                            self.employee_id.date_separated > payslip.date_from and \
                            self.employee_id.date_separated < payslip.date_to:
                        result = results

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

            if self.paid_payroll == 'daily':
                if emp_sched_pay == 'weekly':
                    schedpay = 4
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    hdmf = round(results / schedpay, 2)
                    if schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
                        rounds = 2
                        resultz = results - (round(hdmf, rounds) * (schedpay - 1))
                        additional_cont = results - (
                                    self.get_additional_contribution(payslip, worked_days, schedpay, code) + resultz)
                        result = resultz + additional_cont
                    if payslip.payslip_run_id.hr_period_id.vale_period < 4:
                        rounds = 0
                        result = round(hdmf, rounds)

                elif emp_sched_pay == 'semi-monthly':
                    schedpay = 2
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    if payslip.payslip_run_id.hr_period_id.vale_period == 2:
                        if self.get_previous_contributions(payslip, code) > 0.0:
                            result = results - (self.get_previous_contributions(payslip, code))
                        else:
                            result = results
                    else:
                        result = contr

                elif emp_sched_pay == 'monthly':
                    schedpay = 1
                    results = self.get_contributioner(payslip, worked_days, schedpay, table)
                    contr = round(results / schedpay, 2)
                    result = contr

                else:
                    result = 0.00

            result = result + self.getcontributionadjustment(payslip, cont_for, share_for)
        #print 'HDMFER', round(result, 2)
        return round(result, 2)

    # for semi-monthly schedule
    def get_previous_contributions(self, payslip, code):
        result = 0.0
        date_payment = payslip.date_from
        date_start = date(date_payment.year, date_payment.month, 1)
        date_end = date(date_payment.year, date_payment.month, 15)

        all_contributions = self.env['hr.payslip'].search([('id', '!=', payslip.id),
                                                           ('employee_id', '=', self.employee_id.id),
                                                           ('date_from', '>=', date_start),
                                                           ('date_to', '<=', date_end)])
        if all_contributions:
            for all in all_contributions:
                for line in all.line_ids:
                    if line.code == code:
                        result += line.total
        #print ("PREVIOUS CONT.", code, result)
        return round(result, 2)

    # for weekly schedule
    def get_additional_contribution(self, payslip, worked_days, schedpay, code):
        result = 0.0
        weekly_wage = self.get_gross_wage(payslip, worked_days)
        if weekly_wage > 0.0:
            cont = True
        else:
            cont = False
        if cont and schedpay == payslip.payslip_run_id.hr_period_id.vale_period:
            date_payment = payslip.date_from
            date_start = date(date_payment.year, date_payment.month, 1)
            date_end = date(date_payment.year, date_payment.month,
                            calendar.monthrange(date_payment.year, date_payment.month)[1])
            all_contributions = self.env['hr.payslip'].search([('id', '!=', payslip.id),
                                                               ('employee_id', '=', self.employee_id.id),
                                                               ('date_from', '>=', date_start),
                                                               ('date_from', '<=', date_end)])
            if all_contributions:
                for all in all_contributions:
                    for line in all.line_ids:
                        if line.code == code:
                            result += line.total
        return round(result, 2)

    def compute_net_payslip(self):
        result = True
        if self.paid_payroll == 'daily' and self.schedule_pay == 'weekly':
            result = False
        return result

    def getcontributionadjustment(self, payslip, cont_for, share_for):
        emp_ids = self.employee_id.id
        date_from = payslip.date_from
        date_to = payslip.date_to
        result = 0.0
        check_lists = self.env['hr.contribution.adjustment.list'].search([('datefrom', '<=', date_from),
                                                                          ('dateto', '>=', date_to),
                                                                          ('state', '=', 'done'),
                                                                          ('contribution_for', '=', cont_for),
                                                                          ('share', '=', share_for)])
        if check_lists:
            for check_list in check_lists:
                for line in check_list.line_ids:
                    if emp_ids == line.line.emp_id.id:
                        result = result + line.amount

        return round(result, 2)

    # =============================withholdingtax=============================
    # annual tax

    def withholdingtax(self, payslip, worked_days):
        dfrom = payslip.date_from
        dto = payslip.date_to
        date_from = dfrom.strftime("%Y-01-01")
        date_to = dto.strftime("%Y-%m-%d")
        curyear = dto.year
        result = 0.0
        if self.wholdtax_cont:
            employee_id = self.employee_id.id
            clause_1 = ['&', ('date_end', '<=', date_to), ('date_end', '>=', date_from)]
            clause_2 = ['&', ('date_start', '<=', date_to), ('date_start', '>=', date_from)]
            clause_3 = ['&', ('date_start', '<=', date_from), '|', ('date_end', '=', False),
                        ('date_end', '>=', date_to)]
            clause_final = [('employee_id', '=', employee_id), '|', '|'] + clause_1 + clause_2 + clause_3
            contract_ids = self.env['hr.contract'].sudo().search(clause_final)

            if contract_ids:
                basic = 0.0
                sss_contribution = 0.0
                hdmf_contribution = 0.0
                phic_contribution = 0.0
                for contract in contract_ids:
                    if contract.paid_payroll == 'monthly':
                        salary_wage = contract.wage
                    else:
                        days_per_year = contract.company_id.partner_id.days_per_year
                        if contract.department_id:
                            if contract.department_id.inter_company_id:
                                days_per_year = contract.department_id.inter_company_id.partner_id.days_per_year

                        salary_wage = (contract.wage * days_per_year) / 12.0
                    get_date_start = contract.date_start
                    if contract.date_end:
                        get_date_end = contract.date_end
                        end_of_the_year = date(curyear, 12, 31)
                        if len(contract_ids) == 1 and end_of_the_year > get_date_end:
                            get_date_end = date(curyear, 12, 31)

                    else:
                        get_date_end = date(curyear, 12, 31)
                    old_date_start_month = get_date_start.month
                    old_date_start_year = get_date_start.year
                    if old_date_start_year != curyear:
                        old_date_start_month = 1
                        if contract.date_end:
                            old_date_end_month = get_date_end.month
                        else:
                            old_date_end_month = 12
                    else:
                        if contract.date_end:
                            old_date_end_month = get_date_end.month
                        else:
                            old_date_end_month = 12

                    get_old_date_count = (old_date_end_month - old_date_start_month) + 1
                    basic += salary_wage * get_old_date_count
                    beginning_month = date(old_date_start_year, old_date_start_month, get_date_start.day)
                    ending_month = date(old_date_start_year, old_date_end_month,
                                        calendar.monthrange(old_date_start_year, old_date_end_month)[1])

                    for month_of_year in range(beginning_month.month, ending_month.month + 1):
                        end = date(curyear, month_of_year, calendar.monthrange(curyear, month_of_year)[1])
                        sss_contribution += contract.get_previousssscont(end, salary_wage)
                        hdmf_contribution += contract.get_previoushdmfcont(end, salary_wage)
                        phic_contribution += (contract.get_previousphcont(end, salary_wage))
                contributions = sss_contribution + hdmf_contribution + phic_contribution
                absences = self.getabsence(payslip)
                premium_pay = 0
                adjustment = 0.0
                holidays = self.env['hr.payslip.holidays.employee'].sudo().search([('employee_id', '=', employee_id),
                                                                                   ('date_payment', '>=', date_from),
                                                                                   ('date_payment', '<=', date_to)])

                if holidays:
                    for holiday in holidays:
                        premium_pay += holiday.regular_premium + holiday.special_premium
                adjustments = self.env['hr.adjustment.employee'].sudo().search([('emp_id', '=', employee_id),
                                                                                ('payment_date', '>=', date_from),
                                                                                ('payment_date', '<=', date_to)])

                if adjustments:
                    for adj in adjustments:
                        if adj.adjustment_type_id.is_taxable:
                            adjustment += adj.amount
                overtime = self._get_overtime(payslip)
                previous_withholding_tax = 0.0
                # if payslip.adjustment_list_ids:
                #     for adj in payslip.adjustment_list_ids:
                #         if adj.adjustment_type_id.is_taxable:
                #             adjustment += adj.amount
                previous_payslips = self.env['hr.payslip'].search(
                    [('id', '!=', payslip.id), ('employee_id', '=', employee_id),
                     ('date_from', '>=', date_from), ('date_to', '<=', date_to)])
                if previous_payslips:
                    for p in previous_payslips:
                        for line in p.line_ids:
                            if line.code == 'OT':
                                overtime += line.total
                            if line.code == 'ABS':
                                absences += line.total
                            if line.code == 'WHTAX':
                                previous_withholding_tax += line.total

                total_wage = (((basic - absences) + (premium_pay + adjustment + overtime)) - contributions)
                #print (basic - absences, premium_pay, adjustment, overtime, contributions, total_wage)
                tax_annual = self.env['hr.tax.annual'].sudo().search([('minrange', '<=', total_wage),
                                                                      ('maxrange', '>=', total_wage),
                                                                      ('dateeffect', '<=', date_to)])
                if tax_annual:
                    #print (tax_annual.rate, total_wage, "PREV WHTAX", previous_withholding_tax)
                    if tax_annual.rate > 0.00:
                        annual_rate = tax_annual.rate / 100.00
                        annual_fix_tax = tax_annual.fixed_tax
                        # check if annual wage is less than 250,000 automatic 0.00
                        # if total_wage >= 250000.00:
                        total_less_base_range = total_wage - round(tax_annual.minrange, 2)
                        total_time_rate = total_less_base_range * annual_rate
                        total_add_base = total_time_rate + annual_fix_tax

                        tax_still_due = total_add_base - previous_withholding_tax
                        #     divided by remaining # of vales
                        remain_vales = self.getremainingvale(payslip)
                        #print 'REMVALES', remain_vales
                        result = tax_still_due / remain_vales

                else:
                    result = 0.00

        #print 'WHTAX', round(result, 2)
        if result < 0.0000:
            result = 0.00
        return round(result, 2)

    def getremainingvale(self, payslip):
        fiscal_year = payslip.payslip_run_id.hr_period_id.fiscalyear_id
        actual_period = 0
        if fiscal_year:
            actual_period = len(fiscal_year.period_ids)
        result = (actual_period - payslip.payslip_run_id.hr_period_id.number) + 1
        return result

    def seventy_percent_gross(self, payslip, worked_days):
        addititions = self.basic_wage(payslip, worked_days) + self._get_allowance(payslip, worked_days) + \
                      self.getadjustment(payslip) + self._get_regular_holiday(payslip) + \
                      self._get_special_holiday(payslip)
        subtractions = self.getabsence(payslip)
        gross = (addititions - subtractions)
        #print (gross)
        contribution = self.hdmfcontribution(worked_days, payslip) + self.ssscontribution(worked_days,
                                                                                          payslip) + self.phcontribution(
            worked_days, payslip)
        loans = self.get_loans(payslip)
        #print ("LOANS", loans)
        if not self.employee_id.date_separated:
            if loans:
                result = (gross - (contribution + loans)) * (70.0 / 100.0)
            else:
                result = (gross - contribution) * (70.0 / 100.0)
        else:
            if loans:
                result = (gross - (contribution + loans))
            else:
                result = (gross - contribution)
        return result

    # =============================end of withholdingtax=============================
    # get from employee

    def getdeduction(self, payslip, worked_days):
        result = 0.0
        if self.schedule_pay == 'semi-monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        elif self.schedule_pay == 'monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        else:
            date_start = payslip.payslip_run_id.date_payment
            date_end = payslip.payslip_run_id.date_payment
        emp_ids = self.env['hr.deduction.employee'].sudo().search([('emp_id', '=', self.employee_id.id),
                                                                   ('payment_date', '<=', date_end),
                                                                   ('payment_date', '>=', date_start)], order='id')
        seventy_percent = self.seventy_percent_gross(payslip, worked_days)
        if emp_ids and seventy_percent > 0.0:
            if self.schedule_pay == 'monthly':
                add_days = relativedelta(months=1)
            elif self.schedule_pay == 'semi-monthly':
                add_days = relativedelta(days=16)
            else:
                add_days = relativedelta(days=7)

            deduction = 0.0
            for emp_ded_ids in emp_ids:
                date_payment = emp_ded_ids.payment_date + add_days
                remaining_balance = seventy_percent - deduction
                if remaining_balance > 0.0:
                    if remaining_balance > emp_ded_ids.amount:
                        result += emp_ded_ids.amount
                        deduction += emp_ded_ids.amount
                        if emp_ded_ids.hr_deduction_installment_line_id:
                            installment = self.env['hr.deduction.installment.line'].browse(
                                emp_ded_ids.hr_deduction_installment_line_id.id)
                            if installment:
                                installment.write({'payslip_id': payslip.id})
                        emp_ded_ids.write({'payroll_ded_id': payslip.id})
                    else:
                        actual_deduction = emp_ded_ids.amount - remaining_balance
                        deduction += remaining_balance
                        result += actual_deduction
                        unsettled_deduction = emp_ded_ids.amount - actual_deduction
                        emp_ded_ids.write({'payroll_ded_id': payslip.id, 'amount': actual_deduction,
                                           'unsettled_deduction': unsettled_deduction, 'ud_payment_date': date_payment})

                else:
                    emp_ded_ids.write({'paid': False, 'amount': 0.0, 'unsettled_deduction': emp_ded_ids.amount,
                                       'ud_payment_date': date_payment})

            #print "NETDED", result
        return round(result, 2)

    def getadjustment(self, payslip):
        result = 0.0
        adj_list = payslip.adjustment_list_ids
        if adj_list:
            for ids in adj_list:
                if ids.paid:
                    result += ids.amount
                #print 'Adjustment'
        return round(result, 2)

    def get_loans(self, payslip):
        result = 0.0
        if self.schedule_pay == 'semi-monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        elif self.schedule_pay == 'monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        else:
            date_start = payslip.payslip_run_id.date_payment
            date_end = payslip.payslip_run_id.date_payment
        loan_ids = self.env['hr.loan.line'].sudo().search([('employee_id', '=', self.employee_id.id),
                                                           ('paid_date', '>=', date_start),
                                                           ('paid_date', '<=', date_end),
                                                           ('loan_id.state', '=', 'approve')])
        #print loan_ids

        for loan in loan_ids:
            if loan.loan_type_id.set_as_priority:
                result += loan.paid_amount
        # loan_list = payslip.loan_ids
        # if loan_list:
        #     for ids in loan_list:
        #         if ids.paid:
        #             result += ids.amount
        #print ('LOAN', result)
        return round(result, 2)

    #     how to get absence

    def getabsence(self, payslip):
        if self.schedule_pay == 'semi-monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        elif self.schedule_pay == 'monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        else:
            date_start = payslip.payslip_run_id.date_payment
            date_end = payslip.payslip_run_id.date_payment
        absences = self.env['hr.absences.employee'].search([('employee_id', '=', self.employee_id.id),
                                                            ('date_payment', '>=', date_start),
                                                            ('date_payment', '<=', date_end),
                                                            ('paid', '=', False)])
        result = 0.00
        if absences:
            for absent in absences:
                result += absent.amount

        return round(result, 2)

    def get_undeducted_absence(self, payslip):
        date_payment = payslip.payslip_run_id.date_payment
        undeducted_absences = self.env['hr.absences.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                       ('date_payment', '>=', date_payment),
                                                                       ('date_payment', '<=', date_payment),
                                                                       ('paid', '=', False)])
        if undeducted_absences:
            result = undeducted_absences.undeducted_absences
        else:
            result = 0.00

        return round(result, 2)

    def _get_regular_holiday(self, payslip):
        if self.schedule_pay == 'semi-monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        elif self.schedule_pay == 'monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        else:
            date_start = payslip.payslip_run_id.date_payment
            date_end = payslip.payslip_run_id.date_payment
        holidays = self.env['hr.payslip.holidays.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                    ('date_payment', '>=', date_start),
                                                                    ('date_payment', '<=', date_end),
                                                                    ('paid', '=', False)])
        result = 0.00
        if holidays:
            for holiday in holidays:
                result += holiday.regular_premium

        return round(result, 2)

    def _get_special_holiday(self, payslip):
        if self.schedule_pay == 'semi-monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        elif self.schedule_pay == 'monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        else:
            date_start = payslip.payslip_run_id.date_payment
            date_end = payslip.payslip_run_id.date_payment
        holidays = self.env['hr.payslip.holidays.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                    ('date_payment', '>=', date_start),
                                                                    ('date_payment', '<=', date_end),
                                                                    ('paid', '=', False)])
        result = 0.00
        if holidays:
            for holiday in holidays:
                result += holiday.special_premium

        return round(result, 2)

    def _get_overtime(self, payslip):
        if self.schedule_pay == 'semi-monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        elif self.schedule_pay == 'monthly':
            date_start = payslip.date_from
            date_end = payslip.date_to
        else:
            date_start = payslip.payslip_run_id.date_payment
            date_end = payslip.payslip_run_id.date_payment
        overtime = self.env['hr.payslip.overtime.employee'].search([('employee_id', '=', self.employee_id.id),
                                                                    ('date_payment', '>=', date_start),
                                                                    ('date_payment', '<=', date_end),
                                                                    ('paid', '=', False)])
        result = 0.00
        if overtime:
            for ot in overtime:
                result += ot.amount
        if self.schedule_pay == 'weekly':
            rounds = 0
        else:
            rounds = 2

        return round(result, rounds)

    # EWA
    def _get_ewa_monthly_due(self, payslip):
        result = 0.0
        employee = self.employee_id
        if employee.state == 'employment':
            date_from = payslip.date_from
            date_to = payslip.date_to
            ewa_line = self.env['hr.ewa.line'].search([('employee_id', '=', employee.id), ('active', '=', True)],
                                                      limit=1)
            if date_from.day >= 1 and date_to.day <= 15 and ewa_line:
                result = float(ewa_line.paid_amount)
            else:
                result = 0.00
        #print 'EWA', round(result, 2)
        return round(result, 2)

    # COLA
    def _get_cola(self, payslip, worked_days):
        result = 0.00
        if self.cola:
            cola = self.cola
            days = self.count_contract_of_days()
            working_days = worked_days.WORK100.number_of_days
            result = ((cola * 12) / days) * working_days

        #print 'COLA', result
        return round(result, 2)

    def _get_allowance(self, payslip, worked_days):
        payslip_date = payslip.date_from
        emp_sched_pay = self.schedule_pay
        emp_paid_pay = self.paid_payroll
        result = 0.00
        date_start = self.date_start
        if date_start <= payslip_date:
            allowance = self.allowance
            if emp_paid_pay == 'daily':
                if emp_sched_pay == 'weekly':
                    if worked_days:
                        # result = worked_days.ATTN.number_of_days * allowance
                        result = (self.basic_wage(payslip, worked_days) / self.wage) * allowance
                elif emp_sched_pay == 'semi-monthly':
                    result = allowance * 15
                elif emp_sched_pay == 'monthly':
                    result = allowance * 30
            else:
                if emp_sched_pay == 'monthly':
                    result = allowance
                elif emp_sched_pay == 'semi-monthly':
                    result = allowance / 2
                elif emp_sched_pay == 'weekly':
                    if worked_days:
                        days_per_year = self.company_id.partner_id.days_per_year
                        if self.department_id:
                            if self.department_id.inter_company_id:
                                days_per_year = self.department_id.inter_company_id.partner_id.days_per_year
                        emp_wage = round((self.wage * 12) / days_per_year, 2)
                        result = (worked_days.WORK100.number_of_days * emp_wage)
        return round(result, 2)