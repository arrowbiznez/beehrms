from odoo import models, fields, api, _


class HrWorkEntry(models.Model):
    _inherit = 'hr.work.entry'
    
    payroll_id = fields.Many2one(comodel_name='hr.payroll.run', string='Payroll')
    source_model = fields.Reference(selection=[('hr.attendance','Attendance'), 
    ('hr.timesheet','Timesheet'),('hr.overtime','Overtime'),('hr.leave','Time Off')], string='Source')
    
    # def fix_cmonflict(self):
    #     for rec in self:
            
            
class HrLeave(models.Model):
    _inherit = 'hr.leave'
    
    shift_id = fields.Many2one(comodel_name='resource.calendar.attendance', string='Shift')
    
    @api.onchange('employee_id','date_from')
    def _compute_shift_id(self):
        # Identify the shift
        for rec in self:
            if not rec.employee_id:
                continue
            dow = int(rec.date_from.strftime("%u"))-1
            vals = rec.date_from.strftime("%H:%M")
            t, hours = divmod(float(vals[0]), 24)
            t, minutes = divmod(float(vals[1]), 60)
            minutes = minutes / 60.0
            time = hours+minutes
            employee_id = rec.employee_id.id or False
            cal = self.env['hr.contract'].sudo().search([('employee_id','=',employee_id),('state','=','open'),('date_start','<=',rec.date_from)],order='id desc', limit=1)
            if cal and hours > 0:
                res = self.env['resource.calendar.attendance'].sudo().search([('resource_id','=',cal.resource_calendar_id),('dayofweek','=',dow),('hours_from','<=',time),('hours_to','<=',time)],order='id desc', limit=1)
                if res:
                    rec.shift_id = res.id
    
    def _update_work_entry(self):
        for rec in self:
            valid_leaves = (self.env.ref('hr_holidays.holiday_status_cl').id, self.env.ref('hr_holidays.holiday_status_comp').id)
            if rec.state != 'validate' or rec.holiday_status_id not in valid_leaves:
                continue
            employee_id = rec.employee_id.id
            check_in = False
            if rec.date_from:
                check_in = rec.date_from
            
            name = 'LVE: %s - %s' % (rec.employee_id.name, check_in.strftime('%a - %b %d, %Y'))
                
            check_out = False
            if rec.check_out:
                check_out = rec.date_to
                
            source_model = '%s,%s' % ('hr.leave',rec.id)
            res = self.env['hr.work.entry'].sudo().search([('source_model','=',source_model)])
            
            work_entry_type = self.env.ref('hr_work_entry.work_entry_type_attendance').id
            
            data = []
            if rec.holiday_status_id == self.env.ref('hr_holidays.holiday_status_comp').id:
                delta = rec.date_from - rec.date_to
                worked_hours = delta.total_seconds() / 3600.0
                data = {
                    'employee_id': employee_id, 
                    'date_start': check_in, 
                    'date_stop': check_out,
                    'duration':worked_hours,
                    'source_model': source_model,
                    'work_entry_type_id': work_entry_type,
                    'name': name }
            else:
                worked_hours = rec.duration * rec.shift_id.resource_id.hours_per_day
                data = {
                    'employee_id': employee_id, 
                    'date_start': check_in, 
                    'date_stop': check_out,
                    'duration':worked_hours,
                    'source_model': source_model,
                    'work_entry_type_id': work_entry_type,
                    'name': name }
            
            if not res:
                self.env['hr.work.entry'].sudo().create(data)
            else:
                res.write({data})
    
    
class HrAttendance(models.Model):
    _inherit = 'hr.attendance'
    
    payslip_status = fields.Boolean('Reported in last payslips', help='Green this button when the time off has been taken into account in the payslip.', copy=False)
    shift_id = fields.Many2one(comodel_name='resource.calendar.attendance', string='Shift')
    
    @api.onchange('employee_id', 'check_in')
    def _compute_shift_id(self):
        # Identify the shift
        for rec in self:
            if not rec.employee_id:
                continue
            dow = int(rec.check_in.strftime("%u"))-1
            vals = rec.check_in.strftime("%H:%M")
            t, hours = divmod(float(vals[0]), 24)
            t, minutes = divmod(float(vals[1]), 60)
            minutes = minutes / 60.0
            time = hours+minutes
            employee_id = rec.employee_id.id or False
            cal = self.env['hr.contract'].sudo().search([('employee_id','=',employee_id),('state','=','open'),('date_start','<=',rec.check_in)],order='id desc', limit=1)
            if cal:
                res = self.env['resource.calendar.attendance'].sudo().search([('resource_id','=',cal.resource_calendar_id),('dayofweek','=',dow),('hours_from','<=',time),('hours_to','<=',time)],order='id desc', limit=1)
                if res:
                    rec.shift_id = res.id
                
    @api.depends('check_in', 'check_out')
    def _compute_worked_hours(self):
        # Compute the worked hours base on shift
        for att in self:
            overtime = 0
            if not att.shift_id:
                if att.check_out:
                    delta = att.check_out - att.check_in
                    att.worked_hours = delta.total_seconds() / 3600.0
                else:
                    att.worked_hours = False
            elif att.shift_id:
                vals = att.check_in.strftime("%H:%M")
                t, hours = divmod(float(vals[0]), 24)
                t, minutes = divmod(float(vals[1]), 60)
                minutes = minutes / 60.0
                hours_from = hours+minutes
                if hours_from < att.shift_id.hours_from:
                    hours_from = att.shift_id.hours_from
                
                vals = att.check_out.strftime("%H:%M")
                t, hours = divmod(float(vals[0]), 24)
                t, minutes = divmod(float(vals[1]), 60)
                minutes = minutes / 60.0
                hours_to = hours+minutes
                if hours_to in (att.shift_id.hours_to, att.shift_id.hours_to+0.5):
                    hours_to = att.shift_id.hours_to
                else:       
                    dow = att.check_out.strftime("%u")-1
                    employee_id = att.employee_id.id
                    cal = self.env['hr.contract'].sudo().search([('employee_id','=',employee_id),('state','=','open'),('date_start','<=',att.check_in)],order='id desc', limit=1)
                    if cal:
                        res = self.env['resource.calendar.attendance'].sudo().search([('resource_id','=',cal.resource_calendar_id),('dayofweek','=',dow),('hours_from','<=',hours_to),('hours_to','<=',hours_to)],order='id desc', limit=1)
                        if res:
                            time1 = (res.shift_id.hours_to-res.shift_id.hours_from)+(att.shift_id.hours_to-att.shift_id.hours_from)
                            if time1 > 0:
                                hours_to = res.hours_to
                                overtime = (hours_to-hours_from)-time1
                att.worked_hours = (hours_to-hours_from)-overtime
        
    def _update_work_entry(self):
        for rec in self: 
            if not rec.check_out:
                continue
            employee_id = rec.employee_id.id
            check_in = False
            if rec.check_in:
                check_in = rec.check_in
            
            name = 'ATT: %s - %s' % (rec.employee_id.name, check_in.strftime('%a - %b %d, %Y'))
                
            check_out = False
            if rec.check_out:
                check_out = rec.check_out
                     
            source_model = '%s,%s' % ('hr.attendance',rec.id)
            res = self.env['hr.work.entry'].sudo().search([('source_model','=',source_model)])
            
            worked_hours = rec.worked_hours
            work_entry_type = self.env.ref('hr_work_entry.work_entry_type_attendance').id
            
            if not res:
                self.env['hr.work.entry'].sudo().create({
                    'employee_id': employee_id, 
                    'date_start': check_in, 
                    'date_stop': check_out,
                    'duration':worked_hours,
                    'source_model': source_model,
                    'work_entry_type_id': work_entry_type,
                    'name': name })
            else:
                res.write({
                    'employee_id': employee_id, 
                    'date_start': check_in,
                    'date_stop': check_out,
                    'duration':worked_hours, 
                    'name': name})
                
            