# -*- coding: utf-8 -*-
{
    'name': "Philippines Payroll",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Payroll',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['hr_payroll_community','hr_work_entry','base_automation'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        # 'data/hr_payroll_data.xml',
        # 'security/hr_payroll_security.xml',
        'data/hr.contribution.register.csv',
        'views/res_partner.xml',
        'views/hr_configuration.xml',
        'views/hr_contract.xml',
        'views/templates.xml',
        'views/hr_work_entry.xml',
        'views/hr_employee.xml',
        'views/hr_payslip.xml',
        'data/salary_rule.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}