from datetime import datetime, time
from pytz import timezone
from odoo import api, fields, models, tools, _
import math
import babel


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'
    signature = fields.Binary(string='Signature')
    att_timesheet_id = fields.Many2one(comodel_name='attendance.timesheet.base', string='Timesheet')
    
    
    @api.onchange('employee_id', 'date_from', 'date_to')
    def onchange_employee(self):
        
        print('inside employee')
        if (not self.employee_id) or (not self.date_from) or (not self.date_to):
            return
        print('Name',self.employee_id.name)

        employee = self.employee_id
        date_from = self.date_from
        date_to = self.date_to
        contract_ids = []

        ttyme = datetime.combine(fields.Date.from_string(date_from), time.min)
        locale = self.env.context.get('lang') or 'en_US'
        self.name = _('Salary Slip of %s for %s') % (
        employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))
        self.company_id = employee.company_id
        
        print('Contract 1', self.env.context.get('contract'), self.contract_id)
        if not self.env.context.get('contract') or not self.contract_id:
            print('get contract')
            contract_ids = self.get_contract(employee, date_from, date_to)
            print('Contract d', contract_ids)
            if not contract_ids:
                return
            self.contract_id = self.env['hr.contract'].browse(contract_ids[0])

        if not self.contract_id.struct_id:
            return
        self.struct_id = self.contract_id.struct_id
        if self.contract_id:
            contract_ids = self.contract_id.ids
        # computation of the salary input
        contracts = self.env['hr.contract'].browse(contract_ids)
        print('Contracts 2', contracts)
        worked_days_line_ids = self.get_worked_day_lines(contracts, date_from, date_to)
        print('Worked Lines',worked_days_line_ids)
        worked_days_lines = self.worked_days_line_ids.browse([])
        for r in worked_days_line_ids:
            worked_days_lines += worked_days_lines.new(r)
        self.worked_days_line_ids = worked_days_lines

        input_line_ids = self.get_inputs(contracts, date_from, date_to)
        input_lines = self.input_line_ids.browse([])
        for r in input_line_ids:
            input_lines += input_lines.new(r)
        self.input_line_ids = input_lines
        
        print('exit Name',self.employee_id.name)
        return
    
    @api.model
    def get_worked_day_lines(self, contracts, date_from, date_to):
        
        """
        @param contract: Browse record of contracts
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """
        
        print('Worked Days')
        res = []
        # fill only if the contract as a working schedule linked
        for contract in contracts.filtered(
                lambda contract: contract.resource_calendar_id):
            day_from = datetime.combine(fields.Date.from_string(date_from),
                                        time.min)
            day_to = datetime.combine(fields.Date.from_string(date_to),
                                      time.max)

            # compute leave days
            leaves = {}
            calendar = contract.resource_calendar_id
            tz = timezone(calendar.tz)
            day_leave_intervals = contract.employee_id.list_leaves(
                day_from, day_to, calendar=contract.resource_calendar_id)

            for day, hours, leave in day_leave_intervals:
                holiday = leave.holiday_id
                current_leave_struct = leaves.setdefault(
                    holiday.holiday_status_id, {
                        'name': holiday.holiday_status_id.name or _(
                            'Global Leaves'),
                        'sequence': 5,
                        'code': holiday.holiday_status_id.code or 'GLOBAL',
                        'number_of_days': 0.0,
                        'number_of_hours': 0.0,
                        'contract_id': contract.id,
                    })
                current_leave_struct['number_of_hours'] += hours
                work_hours = calendar.get_work_hours_count(
                    tz.localize(datetime.combine(day, time.min)),
                    tz.localize(datetime.combine(day, time.max)),
                    compute_leaves=False,
                )
                if work_hours:
                    current_leave_struct['number_of_days'] += hours / work_hours
            # hour = self.env['attendance.timesheet'].search([(
            #     'employee_id', '=', self.employee_id.id),
            #     ('check_in', '>=', self.date_from),
            #     ('check_out', '<=', self.date_to),
            #     ('state', '=', 'approve')])
            
            # compute worked days
            work_data = contract.employee_id.get_work_days_data(day_from, day_to,
                                                                calendar=contract.resource_calendar_id)
            print("work data",work_data)
            attendances = {
                'name': _("Normal Working Days paid at 100%"),
                'sequence': 1,
                'code': 'WORK100',
                'number_of_days': work_data['days'],
                'number_of_hours': work_data['hours'],
                'contract_id': contract.id,
            }
            print('attendances', attendances)

            res.append(attendances)
            
            # hour = self.env['attendance.timesheet.base'].search([(
            #     'employee_id', '=', contract.employee_id.id),
            #     ('payroll_date_from', '>=', date_from),
            #     ('payroll_date_end', '<=', date_to),
            #     ('state', '=', 'approve')])
            
            hour = self.env['attendance.timesheet'].search([(
                'employee_id', '=', contract.employee_id.id),
                ('payroll_date_from', '>=', date_from),
                ('payroll_date_end', '<=', date_to),
                ('state', '=', 'approve')])
            
            print('hour',hour)
            total_hour = []
            if hour:
                for att in hour:
                    if att.absence > 0:
                        total_hour.append(att.absence*8)
                    att.payslip_id = self.id
                    att.payslip_status = True
                    absences_date = att.attendance_date or False
                    print('absences_date', type(absences_date))
                    #total_hour.append(att.absences_hours)
                # self.att_timesheet_id = hour.id
                
                sum_of_hour = sum(total_hour)
                mat = math.modf(sum_of_hour)
                mats = list(mat)
                cal = int(mats[1]) * .125
                rounded = round(mats[0], 2)
                dec = int(rounded * 100)
                minute = 0
                if dec >= 60:
                    rem = dec - 60
                    cal = cal + 1
                    dec = rem
                if 0 < dec <= 6:
                    minute = dec * .002
                elif 6 < dec <= 18:
                    minute = (dec * .002) + .001
                elif 19 <= dec <= 30:
                    minute = (dec * .002) + .002
                elif 31 <= dec <= 42:
                    minute = (dec * .002) + .003
                elif 43 <= dec <= 54:
                    minute = (dec * .002) + .004
                elif 55 <= dec <= 59:
                    minute = (dec * .002) + .005
                days = minute + cal
                rounded_hours = round(sum_of_hour, 1)
                
                ttyme = datetime.combine(fields.Date.from_string(absences_date), time.min)
                locale = self.env.context.get('lang') or 'en_US'
                abs_desc = _('Absences from %s') % (tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))
                
                absences = {
                    'name': abs_desc,
                    'sequence': 2,
                    'code': 'ABS100',
                    'number_of_days': days,
                    'number_of_hours': rounded_hours,
                    'contract_id': contract.id,
                }
                
                print('absences', absences)
            
                res.append(absences)
                res.extend(leaves.values())
        
        print('Exit Worked Days', res)
        return res
