from odoo import api, models, fields


class BookPartner(models.Model):
    _inherit = 'hr.employee'

    attendance_timesheet_ids = fields.One2many("attendance.timesheet",
                                               "employee_id",
                                               string='Timesheet Reference')
    attendance_timesheet_base_ids = fields.One2many("attendance.timesheet.base",
                                               "employee_id",
                                               string='Timesheet Base Reference')
    
    # badge_number = fields.Char(string='Badge Number', help="Bioclock Badge Number")
    
