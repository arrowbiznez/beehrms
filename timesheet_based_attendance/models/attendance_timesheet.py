# -*- coding: utf-8 -*-
from weakref import WeakKeyDictionary
from pkg_resources import register_finder
from odoo import fields, api, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta, date, time
from dateutil.relativedelta import relativedelta
import calendar
from pytz import timezone
import pytz

from odoo import api, fields, models

office_leave = 'Official Leave The Office'
office_leave_code = 'OPLO'
office_leave_personal = 'Personal Leave The Office'

leaves_list = ['vl', 'sl', 'mat', 'pat', 'spl', 'par', 'std', 
               '10', 'reb', 'slw', 'emer', 'adopt', 'sholiday', 'holiday']

def daterange(start_date, end_date):
    end_date = end_date + timedelta(days=1)
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)
        
def get_month_week(date):
    weekctr = 0
    weekyr = 0
    year = date.strftime('%Y')
    mm = date.strftime('%m')
    d = date.strftime('%d')
    dd = str(calendar.monthrange(int(year),int(mm))[1])
    
    start = "%s-%s-01" % (year,mm)
    end = "%s-%s-%s" % (year,mm,dd.zfill(2))
    
    for dates in daterange(datetime.strptime(start,'%Y-%m-%d'), datetime.strptime(end,'%Y-%m-%d')):
        if weekyr != int(dates.strftime('%U')):
            weekyr = int(dates.strftime('%U'))
            weekctr += 1
        if date == dates.date():
            break
            
    print('weekctr:', date, weekctr)      
    return weekctr

class HrLeaveType(models.Model):
    _inherit = 'hr.leave.type'
    
    timesheet_code = fields.Selection(string='Timesheet Appearance as', selection=[('vl', 'Vacation Leave'), 
    ('vl', 'Vacation Leave'),
    ('sl', 'Sick Leave'),
    ('mat', 'Maternity Leave'),
    ('pat', 'Paternity Leave'),
    ('spl', 'Special Previlige Leave'),
    ('par', 'Solo Parent Leave'),
    ('std', 'Study Leave'),
    ('10', '10-Day VAWC Leave'),
    ('reb', 'Rehabilitation Leave'),
    ('slw', 'Special Leave Benifits for Women'),
    ('emer', 'Special Emergency (Calamity) Leave'),
    ('adopt', 'Adoption Leave'),])


class AttendanceTimesheetApproval(models.TransientModel):
    _name = 'attendance.timesheet.approval'
    _description = 'Attendance Timesheet Approval'
    
    name = fields.Char(string='Approval')
    attendance_ids = fields.Many2many(comodel_name='attendance.timesheet.base', string='Attendance Timesheet')
    
    @api.model
    def default_get(self, field_list):
        print('field_list', field_list)
        res = super(AttendanceTimesheetApproval, self).default_get(field_list)
        print('context', self.env.context.get('active_ids'))
        active_id = self.env.context.get('active_id')

        active_ids = self._context.get('active_ids',[]) or []
        print('active_ids', active_ids)
        for active_id in active_ids:
            print('active_id',active_id)
        return res
    
    def update_state(self):
        active_ids = self._context.get('active_ids', [])
        print('active_ids', active_ids)
        for record in self.env['attendance.timesheet.base'].browse(active_ids):
            record.action_approve()

class HrBioclockLogs(models.Model):
    _inherit = 'hr.bioclock.logs'
    
    timesheet_base_id = fields.Many2one(comodel_name='attendance.timesheet.base', string='Timesheet', readonly=True) #, compute="_compute_timesheet_base_ids")
    
    def _get_timesheet_base_cron(self):
        print('_get_timesheet_base_cron')
        for rec in self.search([('employee_id','!=',False),('timesheet_base_id','=',False)]):
            if rec.employee_id and not rec.timesheet_base_id:
                employee_id = rec.employee_id.id
                bioclock_date = datetime.strftime(rec.biometric_time,'%Y-%m-%d')
                print('bioclock_date',employee_id,bioclock_date)
                found = self.env['attendance.timesheet.base'].search([('employee_id','=',employee_id),
                                                                             ('date_from','<=',bioclock_date),
                                                                             ('date_end','>=',bioclock_date)],
                                                                            order='date_from desc, date_end asc',limit=1)
                for res in found:
                    print('found',res.id)
                    rec.timesheet_base_id = res.id

class AttendanceTimesheetBase(models.Model):
    _name = "attendance.timesheet.base"
    _description = "Attendance Timesheet Base"
    _rec_name = "employee_id"
    _order = 'date_from desc, date_end desc, payroll_date_from desc, payroll_date_end desc'
    
    _sql_constraints = [
        (
            "timesheet_uniq",
            "unique (employee_id,date_from, date_end)",
            "A date range must be unique per employee !",
        )
    ]
    
    name = fields.Char(string='Series')
    employee_id = fields.Many2one(comodel_name="hr.employee", string='Employee', required=1, readonly=True, default=lambda self: self.env.user.employee_id.id, index=True)
    employee = fields.Char(string='Employee', readonly=True, related='employee_id.name', store=True, index=True)
    worked_hours = fields.Float(string='Worked Hours')
    state = fields.Selection([('draft', 'Draft'), 
    ('approve', 'Approved'), ('post', 'Posted'),
                              ('cancel', 'Cancelled')],
                             default='draft',
                             string="Status", tracking=True, index=True)
    
    attendance_ids = fields.One2many(comodel_name='attendance.timesheet', inverse_name='attendance_timesheet_id', string='Attendances')
    date_from = fields.Date(string='Date From', required=True, index=True)
    date_end = fields.Date(string='Date To', required=True, index=True)
    payroll_date_from = fields.Date(string='Payroll Date From', index=True)
    payroll_date_end = fields.Date(string='Payroll Date To', index=True)
    
    total_working_hours = fields.Float(string='Total Working Hours', readonly=True, default=(5*4*8))
    total_worked_hours = fields.Float(string='Approved Worked Hours', compute="_compute_attendance_ids", help="Approved Worked Hours")
    absences_hours = fields.Float(string='Total Absences', compute='_compute_absences', store=True, digits="Absences")
    
    total_vl = fields.Float(string='Vacation Leave', readonly=True, compute="_compute_attendance_ids")
    total_sl = fields.Float(string='Sick Leave', readonly=True, compute="_compute_attendance_ids")
    total_spl = fields.Float(string='Special Leave', readonly=True, compute="_compute_attendance_ids")
    
    total_absences = fields.Float(string='Absences', readonly=True, compute="_compute_attendance_ids", digits="Total Absences", help="Total of Unauthorized Absences (Remarks dependent).")
    total_late = fields.Float(string='Lates/Tardy', readonly=True, compute="_compute_attendance_ids")
    total_undertime = fields.Float(string='Undertime', readonly=True, compute="_compute_attendance_ids")
    
    payslip_id = fields.Many2one(comodel_name='hr.payslip', string='Payslip')
    contract_id = fields.Many2one(comodel_name='hr.contract', string='Contract', compute="_compute_get_contract", store=True)    
    
    bioclock_logs = fields.One2many(comodel_name='hr.bioclock.logs', inverse_name='timesheet_base_id', string='Timesheet')
    hr_attendance_ids = fields.One2many(comodel_name='hr.attendance', inverse_name='attendance_timesheet_base_id', string='Attendances')
    
    # def _fix_negative_timesheet(self):
    #     for rec in self.search([('state','=','draft'),('total_absences','!=',0.0)]):
    #         for att in rec.attendance_ids.filtered(lambda r: r.undertime < 0.0 or r.late < 0.0 or r.absence < 0.0):
    #             rec.action_fix_attendance()
    #             break
    
    def action_fetch_employee_logs_list(self):
        for id in self.web_progress_iter(self.ids, msg="Fetching Data"):
            rec = self.browse(id)
            date_from = rec.date_from
            employee_id = rec.employee_id.id
            self.env['hr.bioclock.logs'].fetch_alpeta_logs(date_from=date_from, employee_id=employee_id)

    def action_fetch_employee_logs(self):
        date_from = self.date_from
        employee_id = self.employee_id.id
        self.env['hr.bioclock.logs'].fetch_alpeta_logs(date_from=date_from, employee_id=employee_id)

    def _fix_employee_attendance(self):
        date_from = datetime.now().date().strftime('%Y-%m-01')
        for rec in self.search([('date_from','=',date_from),('state','=','draft')]):
            self.env['attendance.timesheet'].sudo().create_timesheet(date=date_from, employee_id=rec.employee_id.id)
            for att in rec.attendance_ids:
                att._compute_check_in_out()
                att._compute_att_check_in_out()

    def action_fix_attendance_list(self):
        for id in self.ids:
            rec = self.browse(id)
            self.env['attendance.timesheet'].sudo().create_timesheet(date=rec.date_from, employee_id=rec.employee_id.id)
            for att in rec.attendance_ids:
                att._compute_check_in_out()
                att._compute_att_check_in_out()
                att._compute_absences()

    def action_fix_attendance(self):
        for rec in self:
            self.env['attendance.timesheet'].sudo().create_timesheet(date=rec.date_from, employee_id=rec.employee_id.id)
            for att in rec.attendance_ids:
                att._compute_check_in_out()
                att._compute_att_check_in_out()
                att._compute_absences()

    @api.depends('date_end')
    def _compute_get_contract(self):
        for rec in self:
            employee_id = rec.employee_id.id
            res = self.env['hr.contract'].sudo().search([('state','=','open'),
            ('date_start','<=',rec.date_end),('employee_id','=',employee_id)], order="date_start desc, id desc", limit=1)
            rec.contract_id = res.id or False
    
    def _get_working_hours(self):
        employee_id = self.employee_id.id
        res = self.env['hr.contract'].sudo().search([('employee_id','=',employee_id),('state','=','open')], order="id desc", limit=1)
        if res:
            self.total_working_hours = ((5*4)*res.resource_calendar_id.hours_per_day)
        else:
            self.total_working_hours = ((5*4)*8)
    
    @api.model
    def create(self, values):
        res = False
        if 'date_from' in values and 'date_end' in values:
            date_from = '%s %s' % (values['date_from'],'00:00:00')
            date_end =  '%s %s' % (values['date_end'],'23:59:59')
            employee_id = values['employee_id'] if values.get('employee_id') else self.env.user.employee_id.id
            print('timesheet',values)
            res = self.env['attendance.timesheet'].sudo().search([('attendance_timesheet_id','=',False),
            ('employee_id','=',employee_id),
            ('check_in','>=',date_from),('check_out','<=',date_end)])
        
        ret = super(AttendanceTimesheetBase, self).create(values)
        if res and ret:
            for rec in res:
                res.attendance_id = ret
        return ret
        
    # @api.multi
    def write(self, values):
        state = values.get('state') or self.state
        if state == 'draft':
            date_from = '%s %s' % (values['date_from'],'00:00:00') if values.get('date_from') else '%s %s' % (self.date_from,'00:00:00')
            date_end = '%s %s' % (values['date_end'],'23:59:59') if values.get('date_end') else '%s %s' % (self.date_end,'23:59:59')
            employee_id = values['employee_id'] if values.get('employee_id') else self.employee_id.id
            res = self.env['attendance.timesheet'].sudo().search([('attendance_timesheet_id','=',False),
            ('employee_id','=',employee_id),
            ('att_check_in','>=',date_from),('att_check_out','<=',date_end)])
            for rec in res:
                rec.attendance_id = self.id
        
        return super(AttendanceTimesheetBase, self).write(values)
    
    
    @api.depends('attendance_ids','total_worked_hours','total_working_hours')
    def _compute_absences(self):
        for rec in self:
            rec.absences_hours = rec.total_working_hours - rec.total_worked_hours
           
    @api.depends('attendance_ids')
    def _compute_attendance_ids(self):
        for rec in self:
            total_worked_hours = 0.0
            total_late = 0.0
            total_undertime = 0.0
            total_absences = 0.000
            total_vl = 0.0
            total_sl = 0.0
            total_spl = 0.0
            undertime = 0.0
            
            for att in rec.attendance_ids:
                # total_absences += 1 if att.reference2=='ua' and att.absence>=1 else 0
                total_absences += 1 if att.absence>=1 else 0
                if att.reference2 not in ['ua','rd','']:
                    total_worked_hours += att.worked_hours
                    total_late += 1 if att.late != 0.0 else 0
                    total_undertime += 1 if att.undertime>0 or ( att.absence >=0.25 and att.absence<=0.75) else 0
                    total_vl += 1 if att.reference2 in ['vl','oplo','mat','pat'] else 0
                    total_sl += 1 if att.reference2=='sl' else 0
                    total_spl += 1 if att.reference2=='spl' else 0
                    
            rec.total_worked_hours = total_worked_hours
            rec.total_late = total_late
            rec.total_undertime = total_undertime
            rec.total_absences = total_absences
            
            rec.total_vl = total_vl
            rec.total_sl = total_sl
            rec.total_spl = total_spl
    
    def action_approve(self):
        for res in self:
            # if not res.contract_id:
            #     raise ValidationError('Employee has no appointment.')
            print('Approve',res.id,res.state)
            for rec in res.attendance_ids:
                if rec.state == 'draft': # and rec.reference2 != 'ua' and rec.worked_hours > 0:
                    rec.state = 'approve'
            res.state = 'approve'

    def unlink(self):
        if self.state == 'approve':
            raise ValidationError(
                _("You Cannot Delete %s as it is in Done State" % self.id))
        return super(AttendanceTimesheetBase, self).unlink()
    
    
    def _open_timesheet_approval(self):
        value = {
            'view_mode': 'form',
            'res_model': 'attendance.timesheet.approval',
            'src_model': 'attendance.timesheet.base',
            'type': 'ir.actions.act_window',
            'name': _('Timesheets Approval'),
            'res_id': self.id,
            'target': 'new'
        }
        return value
    
class AttendanceTimesheet(models.Model):
    _name = "attendance.timesheet"
    _description = "Attendance Timesheet"
    _order = "employee_id, attendance_date, id"
    _rec_name = "name"
    
    _sql_constraints = [
        (
            "timesheet_attendance_uniq",
            "unique (employee_id,attendance_date)",
            "Attendance must be unique per employee !",
        )
    ]
    
    name = fields.Char(string='Name')
    employee_id = fields.Many2one(comodel_name="hr.employee", string='Employee', index=True)
    attendance_date = fields.Date(string='Attendance Date', index=True)
    
    month_week = fields.Integer(string='Month Week', compute='_compute_month_week', store=False, readonly=True)
    
    payroll_date_from = fields.Date(string='Payroll Date From', readonly=True, index=True)
    payroll_date_end = fields.Date(string='Payroll Date To', readonly=True, index=True)
    
    am_check_in = fields.Float(string='AM Check-In')
    am_check_in_id = fields.Many2one(comodel_name='hr.attendance', string='Related Attendance AM In')
    am_check_out = fields.Float(string='AM Check-Out')
    am_check_out_id = fields.Many2one(comodel_name='hr.attendance', string='Related Attendance AM Out')
    am_shift_id = fields.Many2one(comodel_name='resource.calendar.attendance', string='AM Shift')
    
    pm_check_in = fields.Float(string='PM Check-In')
    pm_check_in_id = fields.Many2one(comodel_name='hr.attendance', string='Related Attendance PM In')
    pm_check_out = fields.Float(string='PM Check-Out')
    pm_check_out_id = fields.Many2one(comodel_name='hr.attendance', string='Related Attendance PM Out')
    pm_shift_id = fields.Many2one(comodel_name='resource.calendar.attendance', string='PM Shift')
    
    att_am_check_in = fields.Float(string='AM In', compute='_compute_check_in_out', store=True)
    att_am_check_out = fields.Float(string='AM Out', compute='_compute_check_in_out', store=True)
    att_pm_check_in = fields.Float(string='PM In', compute='_compute_check_in_out', store=True)
    att_pm_check_out = fields.Float(string='PM Out', compute='_compute_check_in_out', store=True)
    
    att_check_out_time = fields.Float(string='Flexitime Check Out time', store=True)

    check_in = fields.Datetime(string='Checkin')
    check_out = fields.Datetime(string='Checkout')
    att_check_in = fields.Datetime(string='Attendance In', ) #compute='_compute_check_in_out', store=True)
    att_check_out = fields.Datetime(string='Attendance Out',) # compute='_compute_check_in_out', store=True)
    
    dayofweek = fields.Char(string='Day of the Week', compute='_compute_att_dayofweek', store=False)
    worked_hours = fields.Float(string='Worked Hours', readonly=True, compute="_compute_att_check_in_out", store=True, digits="Absences")
    late = fields.Float(string='Lates/Tardy', readonly=True, compute="_compute_att_check_in_out", store=True, digits="Absences")
    undertime = fields.Float(string='Undertime', readonly=True, compute="_compute_att_check_in_out", store=True, digits="Absences")
    #Refactored
    absence = fields.Float(string='Absences', readonly=True, compute="_compute_absences", store=True, digits="Absences")
    
    state = fields.Selection([('draft', 'Draft'), 
                            ('approve', 'Approved'),
                            ('disapprove', 'Disapproved'),
                            ('cancel', 'Cancelled'),],
                            default='draft',
                            string="Status", tracking=True, index=True)
    attendance_timesheet_id = fields.Many2one(comodel_name='attendance.timesheet.base', string='Attendance Timesheet')
    attendance_id = fields.Many2one(comodel_name='hr.attendance', string='Attendance Reference', readonly=True)
    
    resource_attendance_id = fields.Many2one(comodel_name='resource.calendar.attendance', string='Shift', readonly=True)
    weekofyear = fields.Integer(string='Week #', compute='_compute_check_in_out', store=True)
    reference = fields.Reference(selection="_get_models",string="Source", readonly=True, required=False)
    reference2 = fields.Selection(string='Remarks', selection=[('vl', 'Vacation Leave'), 
                        ('pplo', 'Personal Permit to Leave'), 
                        ('oplo', 'Personnel Pass Slip'), 
                        ('to', 'Travel Order'),
                        ('ua', 'UnAuthorized Absent'),
                        ('at', 'Attendance'),
                        ('vl', 'Vacation Leave'),
                        ('sl', 'Sick Leave'),
                        ('mat', 'Maternity Leave'),
                        ('pat', 'Paternity Leave'),
                        ('spl', 'Special Previlige Leave'),
                        ('par', 'Solo Parent Leave'),
                        ('std', 'Study Leave'),
                        ('10', '10-Day VAWC Leave'),
                        ('reb', 'Rehabilitation Leave'),
                        ('slw', 'Special Leave Benifits for Women'),
                        ('emer', 'Special Emergency (Calamity) Leave'),
                        ('adopt', 'Adoption Leave'),
                        ('late', 'Late'),
                        ('under', 'Undertime'),
                        ('holiday', 'Holiday'),
                        ('sholiday', 'Special Holiday'),
                        ('rd', 'Rest Day'),
                        ('inc', 'Incomplete')], index=True)
    
    payslip_id = fields.Many2one(comodel_name='hr.payslip', string='Payslip')
    payslip_status = fields.Boolean('Reported in last payslips', help='Green this button when the time off has been taken into account in the payslip.', copy=False)
    
    late_leave_id = fields.Many2one(comodel_name='hr.leave', string='Late Leave')
    leave_id = fields.Many2one(comodel_name='hr.leave', string='Leave/Holiday', readonly=True, store=True) #compute=_compute_att_check_in_out
    # Refactored
    leave = fields.Float(string='Leave/Holiday', readonly=True, compute="_compute_leave", store=True, digits="Absences")    
    
    @api.depends('worked_hours', 'leave','reference2')
    def _compute_absences(self):
        for rec in self:
            leave = rec.leave if rec.worked_hours <= 8 else 0.0
            if not rec.leave_id and rec.reference2 in leaves_list+['sholiday','holiday']:
                leave = 1
            if not rec.leave_id and rec.reference2 in ['oplo','pplo','to']: #['sholiday','holiday']:
                rec.absence = 0
            else:
                absence = 1 if rec.reference2 != ['rd','oplo','pplo','to'] else 0
                #worked_hours = round(rec.worked_hours/8,2) if rec.reference2 not in ['rd','ua','oplo','pplo','to','sholiday','holiday'] else 0
                xleaves_list = leaves_list + ['rd','ua','oplo','pplo','to','sholiday','holiday']
                worked_hours = rec.worked_hours/8 if rec.reference2 not in xleaves_list else 0
                if absence and (worked_hours or leave):
                    absence = round(absence-worked_hours-(leave if (absence-worked_hours)>0 else 0),3) if absence > 0.0 else 0
                rec.absence = absence if absence > 0.000 else 0
                
    
    @api.depends('late_leave_id', 'leave_id', 'worked_hours')
    def _compute_leave(self):
        for rec in self:
            leave_total = 0.0
            leave_code = ''
            
            if rec.state == 'draft' and rec.worked_hours >= 8:
                rec.leave = 0.0
            
            if rec.state == 'draft' and rec.worked_hours < 8:
                if rec.leave_id:
                    for ll in rec.leave_id:
                        if ll.holiday_status_id.timesheet_code and ll.state == 'validate':
                            leave_code = ll.holiday_status_id.timesheet_code
                            if ll.request_unit_half:
                                leave = 0.5
                            elif ll.request_unit_hours:
                                leave = round((ll.request_hour_to - ll.request_hour_from)/8,2)
                                if leave > 1.0: # if greater 8 hours
                                    leave = 1 # max of 8 hours only
                            else:
                                leave = 1
                            leave_total += leave
                            
                if rec.late_leave_id:
                    for ll in rec.late_leave_id:
                        if ll.late_timesheet_id and ll.state == 'confirm':
                            leave_total += ll.number_of_days
                            
                rec.leave = leave_total
                # if rec.reference2 in ['at',False]:
                if leave_code:
                    rec.reference2 = leave_code

    def _auto_leave_tardy_undertime(self):
        for rec in self:
            late = (rec.late + rec.undertime)/480 # *((1/8)/60)
            if rec.state == 'approve' and rec.late_leave_id:
                rec.late_leave_id.action_validate()
            elif rec.state == 'draft' and (rec.late or rec.undertime):
                if rec.dayofweek in ['Saturday','Sunday']:
                    continue
                res = self.env['hr.leave.type'].sudo().search([('code','in',['VL','SL'])], order="code desc, id desc", limit=1)
                for found in res:
                    
                    # mapped_days = self.env['hr.leave'].mapped('holiday_status_id').get_employees_days(self.mapped('employee_id').ids)
                    # leave_days = mapped_days[rec.employee_id.id][found.id]
                    
                    # if float_compare(leave_days['remaining_leaves'], 0, precision_digits=2) == -1 or float_compare(leave_days['virtual_remaining_leaves'], 0, precision_digits=2) == -1:
                    #     continue
                    
                    alloc = self.env['hr.leave.allocation'].sudo().search([
                                    ('holiday_status_id','=',found.id),
                                    ('employee_id','=',rec.employee_id.id),
                                    ('max_leaves','>=',late),('state','=','validate')],order='id desc',limit=1)
                    if not alloc:
                        # print('skip')
                        continue
                    
                    if rec.reference2 not in ['late','under']: 
                        if rec.late_leave_id:
                            rec.late_leave_id.state = 'cancel'
    
                    if late:
                        reason = ''
                        sep = ''
                        if rec.late:
                            reason = 'Tardiness'
                            sep=' and '
                        if rec.undertime:
                            reason = '%s%s%s' % (reason,sep,'Undertime')
                        name = 'Automatic application of %s for "%s" as of %s' % (found.name, reason, rec.attendance_date)
                        holiday_data = {
                            'employee_id': rec.employee_id.id,
                            'holiday_type': 'employee',
                            'holiday_status_id': found.id,
                            'request_date_from': rec.attendance_date,
                            'request_date_to': rec.attendance_date,
                            'date_from': rec.attendance_date,
                            'date_to': rec.attendance_date,
                            'number_of_days':late,
                            'report_note': name,
                            'vac_special_leave': 'within',
                            'state': 'confirm',
                            'late_timesheet_id': rec.id,
                        }
                        
                        # context = {'leave_skip_state_check':True}
                        # self = self.with_context(context)
                        ret = self.env['hr.leave'].sudo()
                        if not rec.late_leave_id:
                            ret.create(holiday_data)
                        else:
                            rec.late_leave_id.number_of_days = late
                        # rec.leave += late
    
    @api.onchange('reference2')
    def _onchange_reference2(self):
        for rec in self:
            state = rec.state
            if state not in ['approve','cancel']:
                if rec.reference2 in ['rd','ua']:
                    state = 'disapprove'
                else:
                    state = 'draft'
                rec.state = state
    
    def init(self):
        self._cr.execute("""
        update attendance_timesheet_base a
            set name = e.name
            from hr_employee e
            where e.id = a.employee_id and a.name isnull;
        update attendance_timesheet a
            set name = e.name
            from hr_employee e
            where e.id = a.employee_id and a.name isnull;
        update attendance_timesheet
            set att_am_check_out = att_am_check_in
            where att_am_check_in > att_am_check_out;
        update attendance_timesheet
            set att_pm_check_out = att_pm_check_in
            where att_pm_check_in > att_pm_check_out;
        """)
    
    def name_get(self):
        res = []
        for rec in self:
            name = rec.name
            if not name:
                name = rec.employee_id.name
            res.append((rec.id, name))

        return res
        
    @ api.model
    def _get_models(self):   
       models = self.env['ir.model'].sudo().search ([])
       return [(model.model, model.name) for model in models]
       
    
    def _cron_timesheet_leave_checker(self):
        attendance_date = fields.datetime.now().date()
        date_from = datetime.combine(attendance_date, time.min)
        date_to = datetime.combine(attendance_date, time.max)
        
        tz = pytz.timezone(self.env.user.tz)
        if self.employee_id.user_id.tz:
            tz = pytz.timezone(self.employee_id.user_id.tz)
        date_from = pytz.utc.localize(date_from).astimezone(tz)
        date_to = pytz.utc.localize(date_to).astimezone(tz)

        leave_found = self.env['hr.leave'].sudo().search([
                    ('date_from','<=',date_from),('date_to','>=',date_to),
                    ('state','=','validate')],order='id desc')
                    
        if leave_found:
            for rec in leave_found:
                employee_id = rec.employee_id.id
                ref = leave_found.holiday_status_id.timesheet_code
                if ref:
                    att  = self.env['attendance.timesheet'].search([('state','=','draft'),('employee_id','=',employee_id),('attendance_date','=',attendance_date)],limit=1)
                    if att:
                        if not att.reference2:
                            att.reference2 = ref
                        if att.reference2 == 'at':
                            att.reference2 = ref
                    
    def _cron_create_timesheet(self):
        date = fields.datetime.now().date()
        self.create_timesheet(date=date)

    def create_timesheet(self, date=False, employee_id=False):
        if not date:
            date = fields.datetime.now().date()
        att_date = [date]
        year = date.strftime('%Y')
        mm = date.strftime('%m')
        dd = str(calendar.monthrange(int(year),int(mm))[1])
        
        date_from = "%s-%s-01" % (year,mm)
        date_end = "%s-%s-%s" % (year,mm,dd.zfill(2))
        att_date.append(datetime.strptime(date_from,'%Y-%m-%d')+relativedelta(months=1))
        att_date.append(datetime.strptime(date_from,'%Y-%m-%d')-relativedelta(months=1))
        
        ## fix payrolldate
        fix_date =  '%s-%s-01' % (year, mm)
        filter = [('payslip_id','=',False),('state','=','draft'),('date_from','>=',date_from)]
        if employee_id:
            filter.append(('employee_id','=',employee_id))
        found = self.env['attendance.timesheet.base'].search(filter,order='payroll_date_from desc')
        for att in found:
            payroll_date_from = att.payroll_date_from or datetime.strptime(date_from,'%Y-%m-%d')+relativedelta(months=1)
            payroll_date_end = att.payroll_date_end or datetime.strptime(date_end,'%Y-%m-%d')+relativedelta(months=1)
            nextpayroll = payroll_date_from+relativedelta(months=1)
            for rec in att.attendance_ids: #att.attendance_ids.filtered(lambda r: r.month_week >= 4):
                print(self.env.cr.dbname,' payroll_date_from', rec.attendance_date, att.payroll_date_from, rec.payroll_date_from)
                if get_month_week(rec.attendance_date) >= 4:
                    print('swak', rec.attendance_date)
                    year = nextpayroll.strftime('%Y')
                    mm = nextpayroll.strftime('%m')
                    dd = str(calendar.monthrange(int(year),int(mm))[1])
                    payroll_date_from = nextpayroll
                    payroll_date_end = "%s-%s-%s" % (year, mm, dd)
                    
                if rec.payroll_date_from != payroll_date_from:
                    rec.write({'payroll_date_from':payroll_date_from,
                    'payroll_date_end':payroll_date_end,})
                
        weekyr = 0
        for date in att_date:
            week_ctr = 1
            year = date.strftime('%Y')
            mm = date.strftime('%m')
            dd = str(calendar.monthrange(int(year),int(mm))[1])
            
            date_from = "%s-%s-01" % (year,mm)
            date_end = "%s-%s-%s" % (year,mm,dd.zfill(2))
        
            nextmonth = datetime.strptime(date_from,'%Y-%m-%d')+relativedelta(months=1)
            nextpayroll = nextmonth+relativedelta(months=1)
            year = nextmonth.strftime('%Y')
            mm = nextmonth.strftime('%m')
            dd = str(calendar.monthrange(int(year),int(mm))[1])
            
            payroll_date_from = nextmonth.strftime('%Y-%m-01')
            payroll_date_end = "%s-%s-%s" % (year, mm, dd)
            
            emp_data = {
                'date_from': date_from,
                'date_end': date_end,
                'payroll_date_from': payroll_date_from,
                'payroll_date_end': payroll_date_end,
            }    
            
            emp = []
            for rec in self.env['hr.contract'].search([('state','=','open'),'|',('date_start','<=',date_end),('date_end','>=',date_end),('date_end','=',False)]):
                employee_id = rec.employee_id.id
                print(self.env.cr.dbname,' timesheet cron employee', employee_id)
                data = []
                #avoid duplicate
                if employee_id in emp:
                    continue
                else:
                    emp.append(employee_id)
                emp_data['employee_id'] = employee_id
                
                base = self.env['attendance.timesheet.base'].sudo().search([('employee_id','=',employee_id),
                ('date_from','=',date_from),('date_end','=',date_end),], limit=1)
                
                if not base:
                    print(self.env.cr.dbname,' timesheet date', date_from, date_end)
                    att_data = []
                    for day in range(int((datetime.strptime(date_end,'%Y-%m-%d') - datetime.strptime(date_from,'%Y-%m-%d') + timedelta(days=1)).days)):
                        attendance_date = datetime.strptime(date_from,'%Y-%m-%d') + timedelta(day)
                        
                        if weekyr != int(attendance_date.strftime('%U')):
                            weekyr = int(attendance_date.strftime('%U'))
                            week_ctr += 1  
                            
                        if week_ctr >= 4:
                            year = nextpayroll.strftime('%Y')
                            mm = nextpayroll.strftime('%m')
                            dd = str(calendar.monthrange(int(year),int(mm))[1])
        
                            payroll_date_from = nextpayroll.strftime('%Y-%m-%d')
                            payroll_date_end = "%s-%s-%s" % (year, mm, dd)
                            # payroll_date_from = nextpay_date_from
                            # payroll_date_end = nextpay_date_end
                            
                        att_data ={
                            'attendance_date': attendance_date.strftime('%Y-%m-%d'),
                            'employee_id': employee_id,
                            'weekofyear': weekyr,
                            'payroll_date_from': payroll_date_from,
                            'payroll_date_end': payroll_date_end,}
                        
                        weekday = str(int(attendance_date.strftime('%w'))-1)
                        rd = True
                        for cal in rec.resource_calendar_id.attendance_ids.filtered(lambda r:r.dayofweek == weekday):
                            if cal:
                                rd = False
                        if rd:
                            att_data['reference2'] = 'rd'
                        
                        data.append((0,0,att_data))
                            
                    if data:   
                        emp_data['attendance_ids'] = data
                    print(self.env.cr.dbname,' source1',emp_data)
                    self.env['attendance.timesheet.base'].sudo().create(emp_data)
                    self.env.cr.commit()
                else:
                    attendance_ids = []
                    for day in range(int((datetime.strptime(date_end,'%Y-%m-%d') - datetime.strptime(date_from,'%Y-%m-%d')+ timedelta(days=1)).days)):
                        attendance_date = (datetime.strptime(date_from,'%Y-%m-%d') + timedelta(day)).strftime('%Y-%m-%d')
                        att_exist = False
                        for att in base.attendance_ids: #base.attendance_ids.filtered(lambda r: r.attendance_date == attendance_date):
                            print(self.env.cr.dbname,' Attendance', att.attendance_date, attendance_date)
                            if att.attendance_date.strftime('%Y-%m-%d') == attendance_date:
                                att_exist = att
                                break
                        print(att_exist)
                        if not att_exist:
                            if weekyr != int(datetime.strptime(attendance_date,'%Y-%m-%d').strftime('%U')):
                                weekyr = int(datetime.strptime(attendance_date,'%Y-%m-%d').strftime('%U'))
                                week_ctr += 1 
                            
                            if week_ctr >= 4:
                                year = nextpayroll.strftime('%Y')
                                mm = nextpayroll.strftime('%m')
                                dd = str(calendar.monthrange(int(year),int(mm))[1])
            
                                payroll_date_from = nextpayroll.strftime('%Y-%m-%d')
                                payroll_date_end = "%s-%s-%s" % (year, mm, dd)
                                # data['payroll_date_from'] = nextpay_date_from
                                # data['payroll_date_end'] = nextpay_date_end
                                
                            # 'attendance_timesheet_id': base.id,
                            data = {'employee_id': employee_id,
                            'attendance_date': attendance_date,
                            'weekofyear':weekyr,
                            'payroll_date_from': payroll_date_from,
                            'payroll_date_end': payroll_date_end}
                                
                            weekday = str(int(datetime.strptime(attendance_date,'%Y-%m-%d').strftime('%w'))-1)
                            rd = True
                            for cal in rec.resource_calendar_id.attendance_ids.filtered(lambda r:r.dayofweek == weekday):
                                if cal:
                                    rd = False
                            if rd:
                                data['reference2'] = 'rd'
                            
                            hr_permit_employee = self.env['ir.module.module'].sudo().search(
                            [('name', '=', 'hr_permit_employee')],
                            limit=1).state or False
                            
                            if hr_permit_employee:
                                if hr_permit_employee == 'installed':
                                    permit = self.env['hr.permit'].sudo().search([('employee_id','=',employee_id),
                                                        ('request_date','=',attendance_date),
                                                        ('state','=','approve'),('permit_type','in',[office_leave,office_leave_personal])])
                                    if not permit:
                                        permit = self.env['hr.permit'].sudo().search([('employee_id','=',employee_id),
                                                        ('transaction_date','=',attendance_date),
                                                        ('state','=','approve'),('permit_type','in',[office_leave,office_leave_personal])])
                                        
                                    if permit:
                                        state = 'oplo'
                                        if permit.permit_type == office_leave:
                                            state = 'pplo'
                                        data['reference2'] = state
                                        
                            # self.env['attendance.timesheet'].sudo().create(data)            
                            attendance_ids.append((0,0,data))
                            
                    if attendance_ids:
                        print(self.env.cr.dbname,' source2', attendance_ids)
                        base.attendance_ids = attendance_ids   
                        self.env.cr.commit
                            
    def _compute_month_week(self):
    
        for rec in self:
            rec.month_week = get_month_week(rec.attendance_date)
            # year = rec.attendance_date.year #.strftime('%Y')
            # mm = rec.attendance_date.month #.strftime('%m')
            # dd = str(calendar.monthrange(year,mm)[1])
            # payroll_date_from = date(year,mm,1)
            # payroll_date_end = date(year,mm,dd)
            # paymonth = 1
            # if rec.month_week >= 4:
            #     paymonth = 2
            # rec.payroll_date_from = payroll_date_from+relativedelta(months=paymonth)
            # rec.payroll_date_end = payroll_date_end+relativedelta(months=paymonth)
    
    @api.depends('am_check_in','am_check_out','pm_check_in','pm_check_out')
    def _compute_check_in_out(self):
        for rec in self:
            if rec.am_check_out or rec.pm_check_out:
            
                att = rec.attendance_date
                contract = rec.attendance_timesheet_id.contract_id
                cal = rec.attendance_timesheet_id.contract_id.resource_calendar_id
                
                if not contract:
                    # raise ValidationError(_("Employee has no appointment."))
                    return False

                elif not cal:
                    # raise ValidationError(_("Please see appointment, working hours is not set."))
                    return False
                    
                am_check_in = rec.am_check_in
                am_check_out = rec.am_check_out
                
                pm_check_in = rec.pm_check_in
                pm_check_out = rec.pm_check_out
                
                # For no between logs
                auto_fill = eval(self.env['ir.config_parameter'].sudo().get_param('timesheet.auto_fill_between_logs'))
                if auto_fill:
                    if am_check_out == 0.0 and am_check_in > 0.0 and pm_check_out > 0.0:
                        am_check_out = 12.0
                    if pm_check_in == 0.0 and am_check_in > 0.0 and pm_check_out > 0.0:
                        pm_check_in = 13.0
                
                att_am_check_in = am_check_in
                att_am_check_out = am_check_out 
                
                att_pm_check_in = pm_check_in
                att_pm_check_out = pm_check_out
                
                att_check_out_time = rec.att_check_out_time
                
                dayofweek = att.strftime('%A')
                print('dow',att.strftime('%A'))
                
                weekday = str(int(att.strftime('%w'))-1)
                weekyr = int(att.strftime('%U'))
                
                if not dayofweek:
                    rec.dayofweek = weekday
                    rec.weekofyear = weekyr
                    
                fmt ='%Y-%m-%d %H:%M:%S'
                for cat in cal.attendance_ids.filtered(lambda r:r.dayofweek == weekday):
                    if cat.dayofweek == weekday:
                        
                        hour_from = cat.hour_from
                        hour_to = cat.hour_to
                        
                        start_hour_from = cat.start_hour_from
                        last_hour_from = cat.last_hour_from
                        
                        flexitime = cal.flexitime
                        hours_per_day = cal.hours_per_day
                        
                        if 'morning' == cat.day_period and am_check_in != 0.0 and am_check_out != 0.0:
                            rec.am_shift_id = cat.id
                            
                            if not flexitime:
                                #Checkin
                                if am_check_in < hour_from:
                                    att_am_check_in = hour_from
                            else:
                                #CheckIn Flexitime
                                att_check_out_time = last_hour_from+hours_per_day+1
                                
                                if am_check_in < start_hour_from:
                                    att_am_check_in = start_hour_from
                                    
                                if att_am_check_in >= start_hour_from and att_am_check_in <= last_hour_from:
                                    att_check_out_time = att_am_check_in+hours_per_day+1
                    
                                rec.att_check_out_time = att_check_out_time
                            
                            #CheckOut
                            if am_check_out > hour_to:
                                att_am_check_out = hour_to
                                
                        if 'afternoon' == cat.day_period and pm_check_in != 0.0 and pm_check_out != 0.0:
                            rec.pm_shift_id = cat.id
                            #CheckIn
                            
                            att_check_out_time = hour_to if not att_check_out_time else att_check_out_time

                            if pm_check_in < cat.hour_from:
                                att_pm_check_in = hour_from
                                
                            if not flexitime:
                                #CheckOut
                                if pm_check_out > hour_to:
                                    att_pm_check_out = hour_to
                            else:
                                if att_pm_check_out > att_check_out_time:
                                    att_pm_check_out = att_check_out_time

                if att_am_check_in > att_am_check_out and att_am_check_out != 0.0:
                    att_am_check_out = att_am_check_in                   
                rec.att_am_check_in = att_am_check_in
                rec.att_am_check_out = att_am_check_out

                if att_pm_check_in > att_pm_check_out and att_pm_check_out != 0.0:
                    att_pm_check_out = att_pm_check_in
                rec.att_pm_check_in = att_pm_check_in
                rec.att_pm_check_out = att_pm_check_out

                if not rec.dayofweek:
                    rec.dayofweek = dayofweek
                if not rec.payroll_date_from:
                    attendance_date = rec.attendance_date
                    nextmonth = attendance_date+relativedelta(months=1)
                    nextpayroll = nextmonth+relativedelta(months=1)
                    year = nextmonth.strftime('%Y')
                    mm = nextmonth.strftime('%m')
                    dd = str(calendar.monthrange(int(year),int(mm))[1])
                    
                    payroll_date_from = nextmonth.strftime('%Y-%m-%d')
                    payroll_date_end = "%s-%s-%s" % (year, mm, dd)
                    
                    weekyr = rec.weekofyear if rec.weekofyear > 0 else int(attendance_date.strftime('%U'))
                    
                    if get_month_week(attendance_date) >= 4: # weekyr%4 == 0:
                        year = nextpayroll.strftime('%Y')
                        mm = nextpayroll.strftime('%m')
                        dd = str(calendar.monthrange(int(year),int(mm))[1])
            
                        payroll_date_from = nextpayroll.strftime('%Y-%m-%d')
                        payroll_date_end = "%s-%s-%s" % (year, mm, dd)
                        # payroll_date_from = nextpay_date_from
                        # payroll_date_end = nextpay_date_end
                    
                    rec.payroll_date_from = payroll_date_from
                    rec.payroll_date_end = payroll_date_end
                    rec.weekofyear = weekyr
                    
    @api.depends('attendance_date')
    def _compute_att_dayofweek(self):
        for rec in self:
            dayofweek = ''
            if rec.attendance_date:
                dayofweek = rec.attendance_date.strftime('%A')
            rec.dayofweek = dayofweek
    
    @api.depends('reference2','attendance_date','att_am_check_in','att_am_check_out','att_pm_check_in','att_pm_check_out')
    def _compute_att_check_in_out(self):
        now = datetime.now().date()
        # ntoz=lambda a: (abs(a)+a)/2
        for rec in self:
            worked_hours = 0.0
            late = 0.0
            undertime = 0.0
            leave = 0.0
            # leave_total = 0.0
            leave_code = ''
            # absence = 1 if rec.reference2 != 'rd' else 0
            dayofweek = ''
            employee_id = rec.employee_id.id
            attendance_date = rec.attendance_date

            year = attendance_date.strftime('%Y')
            mm = attendance_date.strftime('%m')
            dd = attendance_date.strftime('%d')
            hr, min = divmod(rec.pm_shift_id.hour_to*60, 60)
            valid_datetime = "%s-%s-%s %s:%s" % (year,mm,dd,hr,min)
            

            if rec.state == 'draft':
                leave_id = False
                date_from = datetime.combine(attendance_date, time.min)
                date_to = datetime.combine(attendance_date, time.max)
        

                tz = pytz.timezone(self.env.user.tz)
                if rec.employee_id.user_id.tz:
                    tz = pytz.timezone(rec.employee_id.user_id.tz)
                date_from = pytz.utc.localize(date_from).astimezone(tz)
                date_to = pytz.utc.localize(date_to).astimezone(tz)
                year = date_to.strftime('%Y')
                mm = date_to.strftime('%m')
                dd = date_to.strftime('%d')
                valid_datetime = "%s-%s-%s %s:%s" % (year,mm,dd,hr,min)
        
                leave_found = self.env['hr.leave'].sudo().search([('employee_id','=',employee_id),
                    ('date_from','<=',date_from),('date_to','>=',date_to),],
                    order='id desc', limit=1)    
    
                for ll in leave_found:
                    if ll.holiday_status_id.timesheet_code and ll.state == 'validate':
                        leave_id = ll.id
                        break
                
                rec.leave_id = leave_id
                rec._compute_leave()

            if attendance_date:
                rec.dayofweek = dayofweek = rec.attendance_date.strftime('%A')
                if rec.reference2 in leaves_list+['sholiday','holiday']:
                    rec.leave = 1
                    continue
                if rec.reference2 in ['oplo','pplo','to']:
                    rec.absence = 0
                    continue
            if rec.reference2 != 'rd':
                if rec.att_am_check_in != 0.0 and rec.att_am_check_out != 0.0 :
                    d1 = rec.att_am_check_out
                    d2 = rec.att_am_check_in
                    if d2 > d1 and d1 != 0.0:
                        d2 = d1
                        # rec.att_am_check_out = d2
                    worked_hours += (d1 - d2)
                    if rec.am_shift_id and d1 != d2:
                        if not rec.am_shift_id.flexitime:
                            late =  d2 - rec.am_shift_id.hour_from
                        else:
                            if d2 > rec.am_shift_id.last_hour_from:
                                late =  d2 - rec.am_shift_id.last_hour_from
                        undertime += rec.am_shift_id.hour_to - d1
                    
                if rec.att_pm_check_in != 0.0  and rec.att_pm_check_out != 0.0 :
                    d1 = rec.att_pm_check_out
                    d2 = rec.att_pm_check_in
                    if d2 > d1 and d1 != 0.0:
                        d2 = d1
                        # rec.att_pm_check_out = d2
                    worked_hours += (d1 - d2)
                    if rec.pm_shift_id and d1 != d2:
                        if d2 > rec.pm_shift_id.hour_from:
                            late +=  d2 - rec.pm_shift_id.hour_from
                        if not rec.pm_shift_id.flexitime:
                            undertime += rec.pm_shift_id.hour_to - d1
                        else:
                            if rec.att_check_out_time > 0:
                                if d1 < rec.att_check_out_time:
                                    undertime += rec.att_check_out_time - d1
                            elif d1 < rec.pm_shift_id.hour_to:
                                undertime += rec.pm_shift_id.hour_to - d1

                # For Half-day undertime
                if worked_hours > 0.0:
                    shift_sched = rec.attendance_timesheet_id.contract_id.resource_calendar_id.attendance_ids
                    # for Undertime PM
                    if rec.am_shift_id and not rec.pm_shift_id:
                        day = '%s %s' % (rec.dayofweek,'Afternoon')
                        pm_shift_id = False
                        for pm_shift_id in shift_sched: 
                            if pm_shift_id.name == day:
                                break
                        if pm_shift_id:
                            hr, min = divmod(pm_shift_id.hour_to*60, 60)
                            valid_datetime = "%s-%s-%s %s:%s" % (year,mm,dd,int(hr),int(min))
                            matured_datetime = datetime.strptime(valid_datetime,"%Y-%m-%d %H:%M")
                            if datetime.now() > matured_datetime:
                                undertime += abs(pm_shift_id.hour_from-pm_shift_id.hour_to)
                    
                    # for Undertime AM
                    if rec.pm_shift_id and not rec.am_shift_id:
                        day = '%s %s' % (rec.dayofweek,'Morning')
                        am_shift_id = False
                        for am_shift_id in shift_sched: 
                            if am_shift_id.name == day:
                                break
                        if am_shift_id:
                            hr, min = divmod(am_shift_id.hour_to*60, 60)
                            valid_datetime = "%s-%s-%s %s:%s" % (year,mm,dd,int(hr),int(min))
                            matured_datetime = datetime.strptime(valid_datetime,"%Y-%m-%d %H:%M")
                            if datetime.now() > matured_datetime:
                                undertime += abs(am_shift_id.hour_from-am_shift_id.hour_to)

                if not rec.leave_id and worked_hours > 0.0: 
                    if rec.reference2 == 'ua' and rec.attendance_date < now:
                        rec.reference2 = 'at'
                    elif rec.reference2 == 'ua':
                        rec.absence = 1
                        rec.leave = 0
                        rec.worked_hours = 0
                        continue 
                         
                if rec.reference2 in ['oplo','pplo','to','rd']:
                    worked_hours = 0.0

                rec.dayofweek = dayofweek       
                rec.worked_hours = worked_hours
                rec.late = late
                rec.undertime = undertime
                
                if rec.leave_id and rec.leave >= 1 and rec.reference2 == 'ua':
                    rec.reference2 = rec.leave_id.holiday_status_id.timesheet_code
                    
                """Refactored move to def _compute_absences(self)"""
                # rec.absence = absence-(round(worked_hours/8,3) if rec.reference2 not in ['ua','rd'] else 0)-leave
                if rec.reference2 in ['at','under',False]:
                    reference2 = 'at' if worked_hours and not rec.reference2 else rec.reference2
                    reference2 = 'at' if reference2 == 'under' and not undertime else rec.reference2
                    if leave_code:
                        reference2 = leave_code
                    elif rec.absence == 1 and attendance_date < now and not rec.reference2:
                        reference2 = 'ua'
                    elif late > 0:
                        reference2 = 'late'
                    elif undertime > 0:
                        reference2 = 'under'
                    elif not reference2 and \
                        ((rec.am_check_in and not rec.am_check_out) or \
                        (not rec.am_check_in and rec.am_check_out) or \
                        (rec.pm_check_in and not rec.pm_check_out) or \
                        (not rec.pm_check_in and rec.pm_check_out)) :
                        reference2 = 'inc'

                    rec.reference2 = reference2

                
    def action_approve(self):
        self.state = 'approve'

    def unlink(self):
        if self.state == 'approve':
            raise ValidationError(
                _("You Cannot Delete %s as it is in Done State" % self.id))
        return super(AttendanceTimesheet, self).unlink()
        
class HrAttendanceWeb(models.Model):
    _name = 'hr.attendance.web'
    _order = 'date_start desc'
    _description = 'Attendance Web Permission'
    
    employee_id = fields.Many2one('hr.employee', string="Employee", default=lambda self: self.env.user.employee_id.id, required=True, ondelete='cascade', index=True)        
    date_start = fields.Date(string='Date From', required = True)
    date_end = fields.Date(string='Date To')
    sun = fields.Boolean(string='Sunday')
    mon = fields.Boolean(string='Monday')
    tue = fields.Boolean(string='Tuesday')
    wed = fields.Boolean(string='Wednesday')
    thu = fields.Boolean(string='Thursday')
    fri = fields.Boolean(string='Friday')
    sat = fields.Boolean(string='Saturday')
    
class HrAttendance(models.Model):
    _inherit = 'hr.attendance'
    
    employee_id = fields.Many2one('hr.employee', string="Employee", default=lambda self: self.env.user.employee_id.id, required=False, ondelete='cascade', index=True)
    badge_number = fields.Char(string='Badge Number')
    att_bioclock_id = fields.Many2one(comodel_name='hr.attendance.bioclock', string='Attendance Bioclock')
    attendance_timesheet_id = fields.Many2one(comodel_name='attendance.timesheet', string='Attendance Timesheets', readonly=True)
    attendance_timesheet_base_id = fields.Many2one(comodel_name='attendance.timesheet.base', string='Employee Timesheets', readonly=True, related='attendance_timesheet_id.attendance_timesheet_id')
    
    
    def _working_days(self, employee_id, date_filter=False):
        reference2 = 'at'
        found = False
        date_now = fields.datetime.now() if not date_filter else date_filter
        contract = self.env['hr.contract'].sudo().search([('employee_id','=',employee_id),('state','=','open'),('date_start','<=',date_now)],order='id desc', limit=1)
        
        if contract:
            weekday = str(int(date_now.strftime('%w'))-1)
            calendar = contract.resource_calendar_id
            if calendar.attendance_ids:
               found = calendar.attendance_ids.filtered(lambda r:r.dayofweek == weekday)
        if not found:
            reference2 = 'rd'
            
        return reference2
    
    
    def action_update_timesheet_attendance_lists(self):
        for id in self.ids:
            res = self.browse(id)
            res._update_timesheet_attendance()

    def action_update_timesheet_attendance(self):
        self._update_timesheet_attendance()

    def _update_timesheet_attendance(self):
        
        if not self.check_in or not self.check_out or not self.employee_id or self.check_in == self.check_out:
            return False
        
        employee_id = self.employee_id.id
        tz = pytz.timezone(self.env.user.tz)
        if self.employee_id.user_id.tz:
            tz = pytz.timezone(self.employee_id.user_id.tz) # pytz.utc
        # worked_hours = self.worked_hours
        # check_in = self.check_in
        check_in = pytz.utc.localize(self.check_in).astimezone(tz)
        # check_out = self.check_out
        check_out = pytz.utc.localize(self.check_out).astimezone(tz)
        
        year = check_in.strftime('%Y')
        mm = check_in.strftime('%m')
        d = check_in.strftime('%d')
        dd = str(calendar.monthrange(int(year),int(mm))[1])
        
        date_from = "%s-%s-01" % (year,mm)
        date_end = "%s-%s-%s" % (year,mm,dd.zfill(2))
        
        nextmonth = datetime.strptime(date_from,'%Y-%m-%d')+relativedelta(months=1)
        nextpayroll = nextmonth+relativedelta(months=1)
        year = nextmonth.strftime('%Y')
        mm = nextmonth.strftime('%m')
        dd = str(calendar.monthrange(int(year),int(mm))[1])
        
        payroll_date_from = nextmonth.strftime('%Y-%m-%d')
        payroll_date_end = "%s-%s-%s" % (year, mm, dd)
        
        reference = "%s,%s" % (self._name, self.id)
        attendance_date = check_in.date() # '%s-%s-%s' % (year,mm,d)
        reference2 = self._working_days(employee_id, date_filter=attendance_date)

        weekyr = int(attendance_date.strftime('%U'))
        dayofweek = attendance_date.strftime('%A')
        
        if get_month_week(attendance_date) >= 4: # weekyr%4 == 0:
            year = nextpayroll.strftime('%Y')
            mm = nextpayroll.strftime('%m')
            dd = str(calendar.monthrange(int(year),int(mm))[1])

            payroll_date_from = nextpayroll.strftime('%Y-%m-%d')
            payroll_date_end = "%s-%s-%s" % (year, mm, dd)
            
        data = {
            'attendance_date': attendance_date,
            'employee_id': employee_id,
            'reference': reference,
            'reference2': reference2,
            'weekofyear': weekyr,
            'dayofweek': dayofweek,
            'payroll_date_from': payroll_date_from,
            'payroll_date_end': payroll_date_end,
        }
                    
        # convert to float_time
        check_in = float(check_in.strftime('%H'))+(float(check_in.strftime('%M'))/60)
        check_out = float(check_out.strftime('%H'))+(float(check_out.strftime('%M'))/60)
        
        emp_data = {
            'date_from': date_from,
            'date_end': date_end,
            'payroll_date_from': payroll_date_from,
            'payroll_date_end': payroll_date_end,
            'employee_id': self.employee_id.id,
            'attendance_ids': [(0,0,data)]
        } 
        
        if  self.attendance_timesheet_id:
            exists = self.env['attendance.timesheet'].sudo().browse(self.attendance_timesheet_id.id)
        else:
            exists = self.env['attendance.timesheet'].sudo().search([('employee_id','=',employee_id),
            ('attendance_date','=',attendance_date)], limit=1)
        
        if exists.attendance_timesheet_id:
            base = self.env['attendance.timesheet.base'].browse(exists.attendance_timesheet_id.id)
        else:
            base = self.env['attendance.timesheet.base'].search([('employee_id','=',employee_id),
                                                                ('date_from','=',date_from),
                                                                ('date_end','=',date_end),], limit=1)
        
        if exists:
            if exists.state == 'draft':
                if not reference2:
                    data['reference2'] = reference2
                    data['worked_hours'] = 0.0 # reset
                
                if not exists.attendance_timesheet_id and base:
                    data['attendance_timesheet_id'] = base.id

                #am logs only
                if check_in > 0.0 and check_in < 12.0 and check_out < 13.0:
                    data['am_check_in'] = check_in
                    data['am_check_in_id'] = self.id
                    data['am_check_out'] = check_out
                    data['am_check_out_id'] = self.id
                    if exists.pm_check_in_id.id == self.id:
                        data['pm_check_in'] = 0.0
                        data['pm_check_in_id'] = False
                    if exists.pm_check_out_id.id == self.id:
                        data['pm_check_out'] = 0.0
                        data['pm_check_out_id'] = False
                    
                # no between logs
                elif check_in > 0.0 and check_in < 12.0 and check_out > 13.0:
                    data['am_check_in'] = check_in
                    data['am_check_in_id'] = self.id
                    data['pm_check_out'] = check_out
                    data['pm_check_out_id'] = self.id
                    if exists.am_check_out_id.id == self.id:
                        data['am_check_out'] = 0.0
                        data['am_check_out_id'] = False
                    if exists.pm_check_in_id.id == self.id:
                        data['pm_check_in'] = 0.0
                        data['pm_check_in_id'] = False
                    
                #pm logs only
                else:
                    data['pm_check_in'] = check_in
                    data['pm_check_in_id'] = self.id
                    data['pm_check_out'] = check_out
                    data['pm_check_out_id'] = self.id
                    if exists.am_check_in_id.id == self.id:
                        data['am_check_in'] = 0.0
                        data['am_check_in_id'] = False
                    if exists.am_check_out_id.id == self.id:
                        data['am_check_out'] = 0.0
                        data['am_check_out_id'] = False
                    
                exists.write(data)
                self.env.cr.commit()
                self.attendance_timesheet_id = exists.id
                self.env.cr.commit()
                    
        elif not exists:
                    
            if check_in >= 0.0 and check_in < 12.0 and check_out < 13.0:
                data['am_check_in'] = check_in
                data['am_check_in_id'] = self.id
                data['am_check_out'] = check_out
                data['am_check_out_id'] = self.id
                
            # No between Logs    
            elif check_in >= 0.0 and check_in < 12.0 and check_out > 13.0:
                data['am_check_in'] = check_in
                data['am_check_in_id'] = self.id
                data['pm_check_out'] = check_out
                data['pm_check_out_id'] = self.id
                
            else:
                data['pm_check_in'] = check_in
                data['pm_check_in_id'] = self.id
                data['pm_check_out'] = check_out
                data['pm_check_out_id'] = self.id
            
            if base:
                data['attendance_timesheet_id'] = base.id
                
            if not base:
                emp_data['attendance_ids'] = [(0,0,data)]
                ret = self.env['attendance.timesheet.base'].sudo().create(emp_data)
            else:
                emp_data = {}
                att_exist = False
                for att in base.attendance_ids:
                    if att.attendance_date == attendance_date:
                        att_exist = att
                        break
                        
                if not att_exist:
                    emp_data['attendance_ids'] = [(0,0,data)]
                    base.write(emp_data)
                    self.env.cr.commit()
                    self.attendance_timesheet_id = exists.id
                    self.env.cr.commit()
                else:
                    emp_data['attendance_ids'] = [(1,att_exist.id,data)]
                    base.write(emp_data)
                    self.env.cr.commit()
                    self.attendance_timesheet_id = att_exist.id
                    self.env.cr.commit()

class Hrleave(models.Model):
    _inherit = 'hr.leave'
    
    late_timesheet_id = fields.Many2one('attendance.timesheet', string='Late Timesheet Attendance')   
    
    def _leave_update_timesheet(self):
        if self.late_timesheet_id and self.state == 'confirm':
            if not self.late_timesheet_id.late_leave_id:
                self.late_timesheet_id.late_leave_id = self.id
            
        elif self.state == 'validate' and not self.late_timesheet_id:    
            timesheet_code = self.holiday_status_id.timesheet_code
            if not timesheet_code:
                return False
                
            employee = self.employee_id
            datefrom = self.request_date_from
            dateto = self.request_date_to
            ndays = int((dateto-datefrom).days)+1
            leave = 0.0
            
            for day in range(ndays):
                xday = day-1
                if xday:
                    leave_date = (datefrom+timedelta(days=xday))
                else:
                    leave_date = datefrom
                    
                timesheet = self.env['attendance.timesheet'].search([('employee_id','=',employee.id),
                    ('attendance_date','=',leave_date),('state','=','draft')], limit=1)
                if timesheet:
                    if self.late_timesheet_id:
                        leave += self.number_of_days
                    elif self.request_unit_half:
                        leave += 0.5
                    elif self.request_unit_hours:
                        leave += round((self.request_hour_to - self.request_hour_from)/8,2)
                    else:
                        leave += 1
                        
                    if timesheet.reference2 in ('','ua'):
                        timesheet.reference2 = timesheet_code
                    timesheet.leave = leave
                    timesheet.leave_id = self.id

    def _timesheet_apply_late_filing_leave(self):
        for rec in self:
            employee_id = rec.employee_id.id
            date_from = rec.request_date_from.date()
            date_to = rec.request_date_to.date()
            if rec.state in ('approve','validate') and rec.timesheet_code:
                for dt in daterange(date_from,date_to):
                    found = self.env['attendance.timesheet'].search([('attendance_date','=',dt),
                    ('employee_id','=',employee_id),
                    ('reference2','in',['','ua']),
                    ('state','=','draft')])
                    if found:
                        found.reference2 = rec.timesheet_code
                        found.leave_id = rec.id
                
    def _update_leave_attendance(self):
        employee_id = self.employee_id.id
        request_datefrom = self.request_date_from
        request_dateto = self.request_date_to
        contract = self.env['hr.contract'].search([('employee_id','=',employee_id),('state','=','open')],order='id desc',limit=1)
        datefrom = self.request_date_from.date()
        dateto = self.request_date_to.date()
        ndays = int((dateto-datefrom).days)
        reference = "%s,%s" % (self._name, self.id)
        reference2 = self.holiday_status_id.timesheet_code
        for day in range(ndays):
            leave_date = (datefrom+timedelta(days=day))
            dayofweek = str(int(leave_date.strftime('%w'))-1)
            active_contract = contract.filtered(lambda r: r.date_from <= leave_date)
            if active_contract:
                for rec in active_contract.resource_calendar_id.attendance_ids:
                    if dayofweek == rec.dayofweek:
                        Y = leave_date.strftime('%Y')
                        m = leave_date.strftime('%m')
                        d = leave_date.strftime('%d')
                        H = str(int(rec.hour_from)).zfill(2)
                        M = str(int((rec.hour_from % int(H))*60)).zfill(2)
                        S = "00"
                        dd = '%s-%s-%s %s:%s:%s' % (Y,m,d,H,M,S)
                        check_in = datetime(dd,DEFAULT_SERVER_DATETIME_FORMAT)
                        H = str(int(rec.hour_from)).zfill(2)
                        M = str(int((rec.hour_from % int(H))*60)).zfill(2)
                        S = "00"
                        dd = '%s-%s-%s %s:%s:%s' % (Y,m,d,H,M,S)
                        check_out = datetime(dd,DEFAULT_SERVER_DATETIME_FORMAT)
                        
                        if request_datefrom > check_out:
                            continue
                            
                        if leave_date == datefrom:
                            check_in = request_datefrom
                        elif leave_date == dateto:
                            check_out = request_dateto
                        
                        year = check_in.strftime('%Y')
                        mm = check_in.strftime('%m')
                        dd = str(calendar.monthrange(int(year),int(mm))[1])
                        
                        date_from = "%s-%s-01" % (year,mm)
                        date_end = "%s-%s-%s" % (year,mm,dd.zfill(2))
                        
                        nextmonth = datetime.strptime(date_from,'%Y-%m-%d')+relativedelta(months=1)
                        year = nextmonth.strftime('%Y')
                        mm = nextmonth.strftime('%m')
                        dd = str(calendar.monthrange(int(year),int(mm))[1])
                        
                        payroll_date_from = nextmonth.strftime('%Y-%m-%d')
                        payroll_date_end = "%s-%s-%s" % (year, mm, dd)
                        weekyr = int(leave_date.strftime('%U'))
                        dayofweek = leave_date.strftime('%A')
        
                        data = {
                            'attendance_date': leave_date,
                            'employee_id': employee_id,
                            'reference': reference,
                            'reference2': reference2,
                            'weekofyear':weekyr,
                            'dayofweek': dayofweek,
                            'payroll_date_from': payroll_date_from,
                            'payroll_date_end': payroll_date_end
                        }
                        
                            
                        exists = self.env['attendance.timesheet'].sudo().search([('reference','=',reference)])
                        found = self.env['attendance.timesheet.base'].search([('employee_id','=',employee_id),
                        ('date_from','=',datefrom),
                        ('date_end','=',dateto),])
            
                        emp_data = {
                            'date_from': date_from,
                            'date_end': date_end,
                            'payroll_date_from': payroll_date_from,
                            'payroll_date_end': payroll_date_end,
                            'employee_id': employee_id,
                            'attendance_ids': [(0,0,data)]
                        }    
                        
                        if exists:
                            if not exists.attendance_timesheet_id:
                                if found:
                                    data['attendance_timesheet_id'] = found.id
                            if exists.state == 'draft':
                                exists.write(data)
                        elif not exists: 
                            if not found:
                                ret = self.env['attendance.timesheet.base'].create(emp_data)
                            else:
                                att_exist = False
                                for att in found.attendance_ids:
                                    if att.attendance_date == leave_date:
                                        att_exist = att
                                if not att_exist:
                                    data['attendance_timesheet_id'] = found.id
                                    exists.create(data)
                                else:
                                    att_exist.write(data)
        
        # worked_hours = self.worked_hours
        # check_in = self.check_in
        # check_out = self.check_out
        # employee_id = self.employee_id.id
        # attendance_id = self.id 
            
        # year = check_in.strftime('%Y')
        # mm = check_in.strftime('%m')
        # dd = str(calendar.monthrange(int(year),int(mm))[1])
        
        # date_from = "%s-%s-01" % (year,mm)
        # date_end = "%s-%s-%s" % (year,mm,dd.zfill(2))
        
        # nextmonth = datetime.strptime(date_from,'%Y-%m-%d')+relativedelta(months=1)
        # year = nextmonth.strftime('%Y')
        # mm = nextmonth.strftime('%m')
        # dd = str(calendar.monthrange(int(year),int(mm))[1])
        
        # payroll_date_from = nextmonth.strftime('%Y-%m-%d')
        # payroll_date_end = "%s-%s-%s" % (year, mm, dd)
        
        # reference = "%s,%s" % (self._name, self.id)
        
        # if not check_out:
        #     return False
        # data = {
        #     'check_in': check_in,
        #     'check_out': check_out,
        #     'worked_hours': worked_hours,
        #     'employee_id': employee_id,
        #     'attendance_id': attendance_id,
        #     'reference': reference,
        # }
        
        # exists = self.env['attendance.timesheet'].sudo().search([('attendance_id','=',attendance_id)])
           
        # found = self.env['attendance.timesheet.base'].search([('employee_id','=',employee_id),
        # ('date_from','=',date_from),
        # ('date_end','=',date_end),])
        
        # emp_data = {
        #     'date_from': date_from,
        #     'date_end': date_end,
        #     'payroll_date_from': payroll_date_from,
        #     'payroll_date_end': payroll_date_end,
        #     'employee_id': self.employee_id.id,
        #     'attendance_ids': [(0,0,data)]
        # }    
        
        # if exists:
        #     if not exists.attendance_timesheet_id:
        #         if found:
        #             data['attendance_timesheet_id'] = found.id
        #     if exists.state == 'draft':
        #         exists.write(data)
        # elif not exists: 
        #     print('not exists')
        #     if not found:
        #         print('source5')        
        #         ret = self.env['attendance.timesheet.base'].create(emp_data)
        #     else:
        #         data['attendance_timesheet_id'] = found.id
        #         print('source6')        
        #         exists.create(data)
            
class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    employee_id = fields.Many2one('hr.employee',
                                  string='Related Employee', ondelete='restrict',
                                  help='Employee-related data of the user')
                                  
            

class ResourceCalendar(models.Model):
    _inherit = 'resource.calendar'
    
    flexitime = fields.Boolean(string='Flexible Time?')
class ResourceCalendarAttendance(models.Model):
    _inherit = 'resource.calendar.attendance' 

    flexitime = fields.Boolean(string='Flexible Time?', related="calendar_id.flexitime", invisible=True)
    
    start_hour_from = fields.Float(string='Start Flexible Work From')
    last_hour_from = fields.Float(string='End Flexible Work From')
    
    # start_hour_to = fields.Float(string='Flexible End Hour To')
    # last_hour_to = fields.Float(string='Flexible End Hour To')
        
    # def _compute_flexitime(self):
    #     for rec in self:
    #         rec.flexitime = rec.calendar_id.flexitime
    
    def init(self):
        self._cr.execute("""
        update resource_calendar_attendance
            set start_hour_from = 7.0, last_hour_from = 9.0
            where calendar_id in (
            select res_id from ir_model_data 
             where name = 'resource_calendar_std' and module = 'resource')
            and (start_hour_from = 0.0 or start_hour_from isnull)
            and (last_hour_from = 0.0 or last_hour_from isnull);
        """)
    
