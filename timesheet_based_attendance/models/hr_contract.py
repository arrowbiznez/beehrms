from odoo import api, fields, models, _

class HrContract(models.Model):
    _inherit = 'hr.contract'

    hourly_wage = fields.Monetary(string="Hourly Wage", readonly=True, compute='_compute_wage')
    
    @api.depends('wage')
    def _compute_wage(self):
        self.hourly_wage = self.wage/160 #

    def _update_timesheet_contract(self):
        for rec in self:
            employee_id = rec.employee_id.id
            contract_id = rec.id
            timesheet = self.env['attendance.timesheet.base'].search([('state','=','draft'),('employee_id','=',employee_id),('contract_id','=',False)])
            for tt in timesheet:
                tt.contract_id = contract_id
                tt.action_fix_attendance()
