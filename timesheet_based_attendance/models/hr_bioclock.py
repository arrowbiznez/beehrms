# -*- coding: utf-8 -*-
from odoo import fields, api, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import calendar
from pytz import timezone
import pytz

from odoo import api, fields, models
        
class HrAttendanceBioclock(models.Model):
    _name = 'hr.attendance.bioclock'
    
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    badge_number = fields.Char(string='Badge Number')
    check_in = fields.Datetime(string="Check In",required=True)
    check_out = fields.Datetime(string="Check Out")
    attendance_id = fields.One2many(comodel_name='hr.attendance', inverse_name='att_bioclock_id', string='Bioclock')
    state = fields.Selection(string='Status', selection=[('draft', 'Draft'), 
    ('done', 'Done'),])
    
    def _cron_update_attendance(self):
        for rec in self.search([('badge_number','!=',False),('state','=',False)]):
            data = []
            employee = self.env['hr.employee'].search([('badge_number','=',rec.badge_number)])
            if not employee:
                continue
            employee_id = employee.id
            check_in= rec.check_in
            check_out = rec.check_out
            badge_number = rec.badge_number
            state = 'draft' if not rec.check_out else 'done'
            data.append((0,0,{'employee_id': employee_id,
                'badge_number': rec.badge_number,
                'check_in': check_in,
                'check_out': check_out,
                'state': state
            }))
            
            found = self.env['hr.attendance'].search([('employee_id','=',employee_id),('att_bioclock_id','=',False),('check_in','=',check_in)])
            if not found:
                found.att_bioclock_id = rec.id
            else:
                rec.write({'employee_id': employee_id,
                'badge_number': badge_number,
                'attendance_id': data})
        
        for rec in self.search([('badge_number','!=',False),
            ('check_out','!=',False),
            ('state','=','draft')]):
        
            state = 'done'
            check_out = rec.check_out
            
            for att in rec.attendance_id:  
                if att.check_out == False:
                    att.check_out = check_out
            rec.state = state