# -*- coding:utf-8 -*-

from odoo import api, fields, models
from datetime import datetime, date

import calendar
from odoo.exceptions import ValidationError


class PdsModel(models.TransientModel):
    _name = 'hr.employee.pds'
    _description = 'pds wizard'

    employee_id = fields.Many2one(comodel_name='hr.employee')
    current_user = fields.Many2one(comodel_name='hr.employee', string='Employee',
                                   default=lambda self: self.env.user.employee_id.id)

    def print_pds_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['current_user'])[0]

        return self._print_payslip_report(data)

    def _print_payslip_report(self, data):
        return self.env.ref('nhcp_personal_info.report_pds').report_action(self, data=data)


class HrPDSReport(models.AbstractModel):
    _name = 'report.nhcp_personal_data_sheet.hr_profiling_template2'

    @api.model
    def _get_report_values(self, docids, data=None):

        current_user = data['form']['current_user']

        # print(current_user, current_user[0])
        user = self.env['hr.employee'].sudo().search([('id', '=', current_user[0])])

        docargs = {
            'docs': user,

            # 'user': user
        }
        return docargs
