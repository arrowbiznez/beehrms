# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class HrEmployeePersonalInfo(models.Model):
    _inherit = 'hr.employee'
    _name = 'ohr.employee'
    _order = 'request_date desc'
    
    request_date = fields.Datetime(string='Request Date', default=lambda self: fields.datetime.now())
    ref_employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee Reference',default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]))

    category_ids = fields.Many2many(related='ref_employee_id.category_ids', string="Employee Tags", readonly=False, related_sudo=False)
    id_attachment_id = fields.Many2many(related='ref_employee_id.id_attachment_id', string="Attachments")
    passport_attachment_id = fields.Many2many(related='ref_employee_id.passport_attachment_id', string="Attachments")
    
    children_ids = fields.One2many(comodel_name='ohr.employee.children', inverse_name='employee_id', string='Children')
    education_ids = fields.One2many(comodel_name='ohr.employee.education', inverse_name='employee_id', string='Children')
    workexp_ids = fields.One2many(comodel_name='ohr.employee.work.experience', inverse_name='employee_id', string='Work Experience')
    organization_ids = fields.One2many(comodel_name='ohr.employee.organization', inverse_name='employee_id', string='Organization')
    cs_ids = fields.One2many(comodel_name='ohr.employee.civil.service', inverse_name='employee_id', string='Civil Service')
    learning_dev_ids = fields.One2many(comodel_name='ohr.employee.learning.development', inverse_name='employee_id', string='Learning and Development')
    other_skills_ids = fields.One2many(comodel_name='ohr.employee.other.skills', inverse_name='employee_id', string='Skills')
    other_recognition_ids = fields.One2many(comodel_name='ohr.employee.other.recognition', inverse_name='employee_id', string='Non Academic Distinction/Recognition')
    other_association_ids = fields.One2many(comodel_name='ohr.employee.other.skills', inverse_name='employee_id', string='Membership in Association/Organization')
    reference_ids = fields.One2many(comodel_name='ohr.employee.references', inverse_name='employee_id', string='References')
    qa_ids = fields.One2many(comodel_name='ohr.employee.qa', inverse_name='employee_id', string='QA', default=False) #default=lambda self: self._insert_pds_question())

    state = fields.Selection(string='Status', selection=[('draft', 'Draft'),
                                                        ('confirm','Submitted'),
                                                        ('approve', 'Approved'),
                                                        ('reject','Rejected'),
                                                        ('cancel','Cancelled')], default='draft')
    @api.constrains('name')
    def _validate_name(self):
        id = self.id
        name = self.name
        
        res = self.search([['name', '=ilike', name], ['id', '!=', id],['state','in',['draft','confirm']]])
        if res:
            raise ValidationError(_("You have existing draft/pending request, please wait for approval."))

    
    def action_confirm(self):
        self.state = 'confirm'
        
    def action_cancel(self):
        self.state = 'cancel'
        
    def action_approve(self):
        self.state = 'approve'
        self.approve_info()
        
    def action_reject(self):
        self.state = 'reject'
    
    @api.onchange('ref_employee_id')
    def onchange_ref_employee_id(self):
    
        rec = self.sudo().ref_employee_id
        self.image_1920 = rec.image_1920
        self.prefix_id = rec.prefix_id.id
        self.firstname_id = rec.firstname_id.id
        self.middlename_id = rec.middlename_id.id
        self.lastname_id = rec.lastname_id.id
        self.suffix_id = rec.suffix_id.id
        self.name = rec.name
        self.job_title = rec.job_title
        
        self.sss = rec.sss
        self.gsis = rec.gsis
        self.philhealth = rec.philhealth
        self.hdmf = rec.hdmf
        self.tin_id = rec.tin_id
        self.mobile_phone = rec.mobile_phone
        self.work_phone = rec.work_phone
        self.work_email = rec.work_email
        self.work_location = rec.work_location
        self.identification_id = rec.identification_id
        
        self.signature = rec.signature
        self.private_address = rec.private_address
        self.private_address_block = rec.private_address_block
        self.private_address_street = rec.private_address_street
        self.private_address_subd = rec.private_address_subd
        self.private_address_bgry = rec.private_address_bgry
        self.private_address_city = rec.private_address_city
        self.private_address_province = rec.private_address_province
        
        self.private_address_zip = rec.private_address_zip
        self.private_phone = rec.private_phone
        self.private_email_add = rec.private_email_add
        
        self.gender = rec.gender
        self.birthday = rec.birthday
        self.place_of_birth = rec.place_of_birth
        self.country_of_birth = rec.country_of_birth
        self.marital = rec.marital
        
        self.position_title = rec.position_title
        self.employee_num = rec.employee_num
        self.item_num = rec.item_num
        self.salary_grade = rec.salary_grade
        self.effectivity_year = rec.effectivity_year
        self.account_number = rec.account_number
        self.level_of_employment = rec.level_of_employment
        self.level = rec.level
        self.date_of_orig_apntmnt = rec.date_of_orig_apntmnt
        self.date_of_last_promo = rec.date_of_last_promo
        self.promotion_year = rec.promotion_year
        self.step_increment = rec.step_increment
        
        self.contact_person_lname = rec.contact_person_lname
        self.contact_person_fname = rec.contact_person_fname
        self.contact_person_mname = rec.contact_person_mname
        self.contact_person_num = rec.contact_person_num
        self.contact_relationship = rec.contact_relationship
        
        self.spouse_firstname = rec.spouse_firstname
        self.spouse_middlename = rec.spouse_middlename
        self.spouse_lastname = rec.spouse_lastname
        self.spouse_occupation = rec.spouse_occupation
        self.spouse_employer_id = rec.spouse_employer_id
        self.mother_firstname = rec.mother_firstname
        self.mother_middlename = rec.mother_middlename
        self.mother_lastname = rec.mother_lastname
        self.father_firstname = rec.father_firstname
        self.father_middlename = rec.father_middlename
        self.father_lastname = rec.father_lastname
        
        children_ids = []
        for child in rec.children_ids:
            children_ids.append((0,0,{'order':child.order, 'birthdate':child.child_name}))
        self.children_ids = children_ids
        
        education_ids = []
        for educ in rec.education_ids:
            education_ids.append((0,0,{'order':educ.order, 'level':educ.level, 
            'school_name':educ.school_name, 'education':educ.education, 
            'school_year_from':educ.school_year_from, 'school_year_to':educ.school_year_to,
            'unit_earned':educ.unit_earned, 'year_graduated':educ.year_graduated, 'school_award':educ.school_award}))
        self.education_ids = education_ids
        
        workexp_ids = []
        for xp in rec.workexp_ids:
            workexp_ids.append((0,0,{'order':xp.order, 'date_from':xp.date_from, 'date_to':xp.date_to, 
            'position':xp.position, 'job':xp.job,'salary':xp.salary, 'salary_grade':xp.salary_grade,
            'app_status':xp.app_status, 'govt_service':xp.govt_service}))
        self.workexp_ids = workexp_ids
        
        organization_ids = []
        for org in rec.organization_ids:
            organization_ids.append((0,0,{'order':org.order, 'partner_id':org.partner_id.id, 'date_from':org.date_from, 'date_to':org.date_to, 
            'hours_no':org.hours_no, 'position':org.position}))
        self.organization_ids = organization_ids
        
        cs_ids = []
        for cs in rec.cs_ids:
            cs_ids.append((0,0,{'order':cs.order, 'career_service':cs.career_service, 'exam_rating':cs.exam_rating, 'exam_date':cs.exam_date, 
            'exam_place':cs.exam_place, 'license_no':cs.license_no, 'license_expiry_date':cs.license_expiry_date}))
        self.cs_ids = cs_ids
        
        learning_dev_ids = []
        for ld in rec.learning_dev_ids:
            learning_dev_ids.append((0,0,{'order':ld.order, 'title':ld.title, 'date_from':ld.date_from, 
             'date_to':ld.date_to, 'ld_type':ld.ld_type, 'sponsor':ld.sponsor}))
        self.learning_dev_ids = learning_dev_ids
        
        other_skills_ids = []
        for sk in rec.other_skills_ids:
            other_skills_ids.append((0,0,{'order':sk.order, 'name':sk.name}))
        self.other_skills_ids = other_skills_ids
        
        other_recognition_ids = []
        for req in rec.other_recognition_ids:
            other_recognition_ids.append((0,0,{'order':req.order, 'name':req.name}))
        self.other_recognition_ids = other_recognition_ids
        
        other_association_ids = []
        for ass in rec.other_association_ids:
            other_association_ids.append((0,0,{'order':ass.order, 'name':ass.name}))
        self.other_association_ids = other_association_ids
        
        reference_ids = []
        for ref in rec.reference_ids:
            reference_ids.append((0,0,{'order':ref.order, 'partner_id':ref.partner_id.id, 'address':address, 'contact':contact}))
        self.reference_ids = reference_ids
        
        qa_ids = []
        for qa in rec.qa_ids:
            qa_ids.append((0,0,{'question':qa.question.id, 'answer':qa.answer, 'support_answer':qa.support_answer}))
        self.qa_ids = qa_ids
        
    def approve_info(self):
        rec = self.ref_employee_id
        rec.image_1920 = self.image_1920
        rec.prefix_id = self.prefix_id.id
        rec.firstname_id = self.firstname_id.id
        rec.middlename_id = self.middlename_id.id
        rec.lastname_id = self.lastname_id.id
        rec.suffix_id = self.suffix_id.id
        
        rec.sss = self.sss
        rec.gsis = self.gsis
        rec.philhealth = self.philhealth
        rec.hdmf = self.hdmf
        rec.tin_id = self.tin_id
        rec.mobile_phone = self.mobile_phone
        rec.work_phone = self.work_phone
        rec.work_email = self.work_email
        rec.work_location = self.work_location
        rec.identification_id = self.identification_id
        
        rec.signature = self.signature
        rec.private_address_block = self.private_address_block
        rec.private_address_street = self.private_address_street
        rec.private_address_subd = self.private_address_subd
        rec.private_address_bgry = self.private_address_bgry
        rec.private_address_city = self.private_address_city
        rec.private_address_province = self.private_address_province
        
        rec.private_address_zip = self.private_address_zip
        rec.private_phone = self.private_phone
        rec.private_email_add = self.private_email_add
        
        rec.gender = self.gender
        rec.birthday = self.birthday
        rec.place_of_birth = self.place_of_birth
        rec.country_of_birth = self.country_of_birth
        rec.marital = self.marital
        
        rec.contact_person_lname = self.contact_person_lname
        rec.contact_person_fname = self.contact_person_fname
        rec.contact_person_mname = self.contact_person_mname
        rec.contact_person_num = self.contact_person_num
        rec.contact_relationship = self.contact_relationship
        
        rec.spouse_firstname = self.spouse_firstname
        rec.spouse_middlename = self.spouse_middlename
        rec.spouse_lastname = self.spouse_lastname
        rec.spouse_occupation = self.spouse_occupation
        rec.spouse_employer_id = self.spouse_employer_id
        rec.mother_firstname = self.mother_firstname
        rec.mother_middlename = self.mother_middlename
        rec.mother_lastname = self.mother_lastname
        rec.father_firstname = self.father_firstname
        rec.father_middlename = self.father_middlename
        rec.father_lastname = self.father_lastname
        
        
        rec.children_ids = [(5)]
        children_ids = []
        for child in self.children_ids:
            children_ids.append((0,0,{'order':child.order, 'birthdate':child.child_name}))
        rec.children_ids = children_ids
        
        rec.education_ids = [(5)]
        education_ids = []
        for educ in self.education_ids:
            education_ids.append((0,0,{'order':educ.order, 'level':educ.level, 
            'school_name':educ.school_name, 'education':educ.education, 
            'school_year_from':educ.school_year_from, 'school_year_to':educ.school_year_to,
            'unit_earned':educ.unit_earned, 'year_graduated':educ.year_graduated, 'school_award':educ.school_award}))
        rec.education_ids = education_ids
        
        rec.workexp_ids = [(5)]
        workexp_ids = []
        for xp in self.workexp_ids:
            workexp_ids.append((0,0,{'order':xp.order, 'date_from':xp.date_from, 'date_to':xp.date_to, 
            'position':xp.position, 'job':xp.job,'salary':xp.salary, 'salary_grade':xp.salary_grade,
            'app_status':xp.app_status, 'govt_service':xp.govt_service}))
        rec.workexp_ids = workexp_ids
        
        rec.organization_ids = [(5)]
        organization_ids = []
        for org in self.organization_ids:
            organization_ids.append((0,0,{'order':org.order, 'partner_id':org.partner_id.id, 'date_from':org.date_from, 'date_to':org.date_to, 
            'hours_no':org.hours_no, 'position':org.position}))
        rec.organization_ids = organization_ids
        
        rec.cs_ids = [(5)]
        cs_ids = []
        for cs in self.cs_ids:
            cs_ids.append((0,0,{'order':cs.order, 'career_service':cs.career_service, 'exam_rating':cs.exam_rating, 'exam_date':cs.exam_date, 
            'exam_place':cs.exam_place, 'license_no':cs.license_no, 'license_expiry_date':cs.license_expiry_date}))
        rec.cs_ids = cs_ids
        
        rec.learning_dev_ids = [(5)]
        learning_dev_ids = []
        for ld in self.learning_dev_ids:
            learning_dev_ids.append((0,0,{'order':ld.order, 'title':ld.title, 'date_from':ld.date_from, 
            'date_to':ld.date_to, 'ld_type':ld.ld_type, 'sponsor':ld.sponsor}))
        rec.learning_dev_ids = learning_dev_ids
        
        rec.other_skills_ids = [(5)]
        other_skills_ids = []
        for sk in self.other_skills_ids:
            other_skills_ids.append((0,0,{'order':sk.order, 'name':sk.name}))
        rec.other_skills_ids = other_skills_ids
        
        rec.other_recognition_ids = [(5)]
        other_recognition_ids = []
        for req in self.other_recognition_ids:
            other_recognition_ids.append((0,0,{'order':req.order, 'name':req.name}))
        rec.other_recognition_ids = other_recognition_ids
        
        rec.other_association_ids = [(5)]
        other_association_ids = []
        for ass in self.other_association_ids:
            other_association_ids.append((0,0,{'order':ass.order, 'name':ass.name}))
        rec.other_association_ids = other_association_ids
        
        rec.reference_ids = [(5)]
        reference_ids = []
        for ref in self.reference_ids:
            reference_ids.append((0,0,{'order':ref.order, 'partner_id':ref.partner_id.id, 'address':address, 'contact':contact}))
        rec.reference_ids = reference_ids
        
        rec.qa_ids = [(5)]
        qa_ids = []
        for qa in self.qa_ids:
            qa_ids.append((0,0,{'question':qa.question.id, 'answer':qa.answer, 'support_answer':qa.support_answer}))
        rec.qa_ids = qa_ids
        
    @api.onchange('firstname', 'middlename', 'lastname', 'suffix', 'prefix')
    def onchange_names(self):
        for rec in self:
            prefix = rec.prefix or ""
            firstname = rec.firstname or ""
            middlename = rec.middlename or ""
            lastname = rec.lastname or ""
            suffix = rec.suffix or ""
            partner = self.env['res.partner.name'].sudo()
            name = ""
            
            res_id = False
            if prefix:
                prefix = prefix.title().strip()
            
                name = "%s" % (prefix)
            
            res_id = False        
            if firstname:
                firstname = firstname.title().strip()

            if name:
                name = "%s %s" % (name, firstname)
            else:
                name = firstname
            
            res_id = False        
            if middlename:
                middlename = middlename.title().strip()
                if name:
                    name = "%s %s" % (name, middlename)
                else:
                    name = middlename
            
            res_id = False        
            if lastname:
                lastname = lastname.title().strip()

                if name:
                    name = "%s %s" % (name, lastname)
                else:
                    name = lastname
            
            res_id = False
            if suffix:
                suffix = suffix.title().strip()
                # res = partner.search([('name','ilike',suffix)])
                # if not res:
                #     res_id = partner.create({"name":suffix})
                
                # rec.suffix_id = res_id or res.id

                if name:
                    name = "%s %s" % (name, suffix)
                else:
                    name = suffix
            if name:
                rec.name = name        

    @api.model
    def year_selection(self):
        year = 2010  # replace 2000 with your a start year
        year_list = []
        while year != 2100:  # replace 2030 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list


    def _insert_pds_question(self):
        qa_ids = []
        que = self.env['hr.employee.question'].search([('active','=',True)])
        for rec in que:
            qa_ids.append((0,0,{'question':rec.id}))
        # self.qa_ids = qa_ids
        return qa_ids

class HrEmployeeReferences(models.Model):
    _inherit = 'hr.employee.references'
    _name = 'ohr.employee.references'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')

    
class HrEmployeeQA(models.Model):
    _inherit = 'hr.employee.qa'
    _name = 'ohr.employee.qa'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')

class HrEmployeeOtherAssoc(models.Model):
    _inherit = 'hr.employee.other.association'
    _name = 'ohr.employee.other.association'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')

class HrEmployeeOtherRecognition(models.Model):
    _inherit = 'hr.employee.other.recognition'
    _name = 'ohr.employee.other.recognition'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')
    
class HrEmployeeOtherSkills(models.Model):
    _inherit = 'hr.employee.other.skills'
    _name = 'ohr.employee.other.skills'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')


class HrEmployeeLearningDev(models.Model):
    _inherit = 'hr.employee.learning.development'
    _name = 'ohr.employee.learning.development'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')

class HrEmployeeCS(models.Model):
    _inherit = 'hr.employee.civil.service'
    _name = 'ohr.employee.civil.service'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')

class HrEmployeeWorkExperience(models.Model):
    _inherit = 'hr.employee.work.experience'
    _name = 'ohr.employee.work.experience'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')

class HrEmployeeOrganization(models.Model):
    _inherit = 'hr.employee.organization'
    _name = 'ohr.employee.organization'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')

class HrEmployeeSchool(models.Model):
    _inherit = 'hr.employee.education'
    _name = 'ohr.employee.education'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')

class HrEmployeeChildren(models.Model):
    _inherit = 'hr.employee.children'
    _name = 'ohr.employee.children'

    employee_id = fields.Many2one(comodel_name='ohr.employee', string='Employee')

class HrEmployeePerInfo(models.Model):
    _inherit = 'hr.employee'
    
    personal_info_count = fields.Integer(string='Update Request',compute="_compute_personal_info", store=False)

    def _compute_personal_info(self):
        for rec in self:
            rec.personal_info_count = self.env['ohr.employee'].search_count([('state','=','confirm'),('ref_employee_id','=',rec.id)]) or 0
    
    
    def open_personal_info(self):
        
        value = {
            'domain': [('state','=','confirm'),('ref_employee_id','=',self.id)],
            'view_mode': 'tree,form',
            'res_model': 'ohr.employee',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Information - Updates'),
        }
        
        return value
