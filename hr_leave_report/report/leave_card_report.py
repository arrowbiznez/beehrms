from odoo import api, models, fields
from datetime import datetime


class LeaveCardsReports(models.TransientModel):
    _name = 'hr.leavecard.rep'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee Name', required=True)

    current_user = fields.Many2one(comodel_name='hr.employee', string='In Charge',
                                   default=lambda self: self.env.user.employee_id.id)

    year = fields.Integer('Year', required=True, default=datetime.now().strftime('%Y'))

    def print_leave_card(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['employee_id', 'current_user', 'year'])[0]

        return self._print_leave_card(data)

    def _print_leave_card(self, data):
        return self.env.ref('hr_leave_report.hr_leave_card_report').report_action(self, data=data)


class LeaveCardReport(models.AbstractModel):
    _name = 'report.hr_leave_report.hr_leave_card_template'

    @api.model
    def _get_report_values(self, docids, data=None):
        # report_obj = self.env['ir.actions.report']
        # report = report_obj._get_report_from_name('module.report_name')
        employee = data['form']['employee_id']
        current_user = data['form']['current_user']
        year = data['form']['year']
        prev_year = year - 1
        user = self.env['hr.employee'].sudo().search([('id', '=', employee[0])])
        employee_rec = self.env['hr.leave.card.summary'].sudo().search(
            [('employee_id', '=', employee[0]), ('year', '=', year), ('filler', '=', False)],
            order='year, holiday_status_id, period_from asc,id')


        employee_rec_prev_year_VL = self.env['hr.leave.card.summary'].sudo().search(
            [('employee_id', '=', employee[0]), ('year', '=', year-1), ('filler', '=', False), ('code', '=', 'VL')],
            order='period_from desc', limit=1)

        employee_rec_prev_year_SL = self.env['hr.leave.card.summary'].sudo().search(
            [('employee_id', '=', employee[0]), ('year', '=', year-1), ('filler', '=', False), ('code', '=', 'SL')],
            order='period_from desc', limit=1)
        print(employee_rec_prev_year_VL,employee_rec_prev_year_SL)
        if employee_rec_prev_year_VL:
            employee_rec_prev_year_VL
            print(employee_rec_prev_year_VL.balance, employee_rec_prev_year_VL.remarks, employee_rec_prev_year_VL.period_from)
        else:
            employee_rec_prev_year_VL = 0

        if employee_rec_prev_year_SL:
            employee_rec_prev_year_SL
            print(employee_rec_prev_year_SL.balance, employee_rec_prev_year_SL.remarks, employee_rec_prev_year_SL.period_from)

        else:
            employee_rec_prev_year_SL = 0

        # print(employee_rec_prev_year_VL.balance)
        # print(employee_rec_prev_year_SL.balance)
        curr_user = self.env['hr.employee'].sudo().search([('id', '=', current_user[0])])

        user_name = user.lastname if user.lastname else '' + ', ' + user.firstname if user.firstname else '' + ' ' + \
                                                                                                          user.middlename[
                                                                                                              0] if user.middlename else ''
        curr_user_name = curr_user.firstname if curr_user.firstname else '' + ' ' + curr_user.middlename[
            0] if curr_user.middlename else '' + '. ' + curr_user.lastname if curr_user.lastname else ''

        # print(employee_rec.employee_id.id)
        # for each in employee_rec.sorted(key=lambda x: x.holiday_status_id):
        #    # if each.holiday_status_id.id == 1:
        #        # "rec.period_from.strftime('%B') == month and rec.holiday_status_id.id == 1 and rec.remarks != 'Availed Leave'" >
        #         print(each.id,each.month, each.earned,each.used,
        #               each.balance, each.code, each.holiday_status_id, each.filler, each.seq)
        docargs = {
            # 'doc_ids': docids,
            'employee_rec': employee_rec,
            'employee_name': user,
            'employee_marital': user.marital,
            'employee_tin_id': user.tin_id,
            'employee_current_user': curr_user,
            'employee_position': user.position_title.upper(),
            'curr_pos': curr_user.position_title.upper() if curr_user.position_title else '',
            'employee_rec_prev_year_VL': employee_rec_prev_year_VL.balance if employee_rec_prev_year_VL else 0,
            'employee_rec_prev_year_SL': employee_rec_prev_year_SL.balance if employee_rec_prev_year_SL else 0,
            'prev_year': prev_year,
            # 'doc_model': report.model,
            'docs': self,
        }
        return docargs


class HrLeaveSubFields(models.Model):
    _inherit = 'hr.leave'

    vac_special_leave = fields.Selection([
        ('within', 'Within the Philippines'),
        ('abroad', 'Abroad')])
    within_the_phil = fields.Char()
    abroad = fields.Char()
    sick_leave = fields.Selection([
        ('inhospital', 'In hospital'),
        ('outhospital', 'Out patient')])
    in_patient = fields.Char()
    out_patient = fields.Char()
    leave_for_women = fields.Char()
    study_leave = fields.Selection([
        ('masters', 'Completion of Masters degree'),
        ('board_exam', 'BAR/Board Exam')])
