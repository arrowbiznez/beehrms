# -*- coding: utf-8 -*-

{
    'name': 'Holiday Approvals',
    'version': '13.0.1.1.0',
    'summary': """Open HRMS Multilevel Approval Additional Fields""",
    'description': 'Multilevel Approval for Leaves, leave approval, multiple leave approvers, leave, approval',
    'category': 'Generic Modules/Human Resources',
    'author': 'Daryll Bangoy',
    'company': '',
    'website': "",
    'depends': ['hr','hr_holidays','ohrms_holidays_approval','timesheet_based_attendance'],
    'data': [
        'views/leave_request.xml',
        'security/ir.model.access.csv',
        # 'security/security.xml'
    ],
    'demo': [],
    'images': ['static/description/banner.png'],
    'license': "AGPL-3",
    'installable': True,
    'auto_install': False,
    'application': False,
}
