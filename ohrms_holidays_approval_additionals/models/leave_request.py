# -*- coding: utf-8 -*-
import re
from datetime import datetime, timedelta
from odoo import models, api, fields, _
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools import email_split

group_tags = [('employee','Employee'),
('sc','Section Chief'),
('dc','Division Chief'),
('deda','DEDA'),
('oed','OED'),
('FAD','FAD Chief'),
('cm','Chairman'),
('pmt','PMT')]

def daterange(date1, date2):
    for n in range(int ((date2 - date1).days)+1):
        yield date1 + timedelta(n) 
class HrLeaveValidatorsOIC(models.Model):
    """ Model for leave validators in Leave Types configuration """
    _name = 'hr.holidays.validators.oic.manager' 
    _order = 'date_start desc, date_end desc'
    _description = 'Division Chief OIC Leave Validators'
    
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', required=True, index=True)
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', required=True, index=True)
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', related='employee_id.job_id', store=True)
    date_start = fields.Date('Effectivity Start Date', default=lambda self: fields.datetime.now(), required=True, index=True) 
    date_end = fields.Date('Effectivity End Date', required=False, index=True) 
    active = fields.Boolean('Active', default=True)
    
    @api.constrains('department_id', 'employee_id', 'job_id')
    def _check_validity(self): 
        for rec in self:
            id = rec.id
            department_id = rec.department_id.id
            employee_id = rec.employee_id.id
            date_start = rec.date_start
            date_end = rec.date_end
            found = self.search([('id','!=',id),('department_id','=',department_id),('employee_id','=',employee_id),
                                 ('date_start','=',date_start),('active','=',True)], order='date_start desc')
            for res in found:
                if res.date_end:
                    if res.date_start == date_start and res.date_end == date_end:
                        raise ValidationError(("Record aleady exists!"))
                if res.date_start == date_start:
                    raise ValidationError(("Record Already Exists!"))
    
class HrLeaveValidators(models.Model):
    """ Model for leave validators in Leave Types configuration """
    _inherit = 'hr.holidays.validators' 
    _order = 'group_tag, order asc'
    _description = 'Leave Validators'
    
    validator_type = fields.Selection(string='Validator Type', selection=[('individual', 'Employee'), ('manager', 'Division Chief'),], default='individual')                       
    order = fields.Integer(string='Sequence')
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='holiday_validators.employee_id', store=True)
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', related='employee_id.job_id', store=True) 
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id', store=True)
    group_tag = fields.Selection(string='Approver For', selection=group_tags, default="employee")
    
    @api.depends('holiday_validators')
    def _compute_employee_info(self):
        for rec in self:
            rec.employee_id = self.env['hr.employee'].browse(rec.holiday_validators.id).id or False
            
class LeaveValidationStatus(models.Model):
    """ Model for leave validators and their status for each leave request """
    _inherit = 'leave.validation.status'
    _order = 'order'
    _description = 'Leave Validation Staus'
    
    order = fields.Integer(string='Sequence')
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='validating_users.employee_id', store=True)
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', related='employee_id.job_id', store=True) 
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id', store=True)
    group_tag = fields.Selection(string='Designation', selection=group_tags, related="employee_id.group_tag")
    
    @api.depends('validating_users')
    def _compute_employee_info(self):
        for rec in self:
            rec.employee_id = self.env['hr.employee'].search([('user_id','=',rec.validating_users.id)]).id
            
class HrLateMatrix(models.Model):
    _name = 'late.matrix'
    
    name = fields.Char(string='Name')
    matrix_ids = fields.One2many(comodel_name='hr.leave.type.late.matrix', inverse_name='late_matrix_id', string='Late Matrix Table')
    
class HrleaveTypeLateMatrix(models.Model):
    _name = 'hr.leave.type.late.matrix'

    late_matrix_id = fields.Many2one(comodel_name='late.matrix', string='Late Matrix')
    minutes = fields.Float(string='Minutes')
    days = fields.Float(string='days')
    active = fields.Boolean(string='Active', default=True)
    
    
class HrLeaveType(models.Model):
    _inherit = 'hr.leave.type'
    
    late_matrix_id = fields.Many2one(comodel_name='late.matrix', string='Late Matrix')
    timesheet_code = fields.Selection(string='Timesheet Appearance as', selection=[('vl', 'Vacation Leave'), 
    ('vl', 'Vacation Leave'),
    ('sl', 'Sick Leave'),
    ('mat', 'Maternity Leave'),
    ('pat', 'Paternity Leave'),
    ('spl', 'Special Previlige Leave'),
    ('par', 'Solo Parent Leave'),
    ('std', 'Study Leave'),
    ('10', '10-Day VAWC Leave'),
    ('reb', 'Rehabilitation Leave'),
    ('slw', 'Special Leave Benifits for Women'),
    ('emer', 'Special Emergency (Calamity) Leave'),
    ('adopt', 'Adoption Leave'),])
    
    multi_level_validation = fields.Boolean(string='Multiple Level Approval',
                                            help="If checked then multi-level approval is necessary", default=False)
    
    @api.onchange('validation_type')
    def enable_multi_level_validation(self):
        self.multi_level_validation = False
        if self.validation_type == 'multi':
            self.multi_level_validation = True
            self.double_validation = False
class HrLeave(models.Model):
    _inherit = 'hr.leave'
    _order = 'request_date_from desc, request_date_from desc'
         
    current_approver_level = fields.Integer(string='Current Approval Level', default=0)
    current_approver_user = fields.Many2one(comodel_name='res.users', string='Current Approver', compute="_compute_current_approver_user", store=True)
    
    can_approve = fields.Boolean(string='Can Approve', compute='_compute_current_approver_user', store=False, default=False)
    can_refuse = fields.Boolean(string='Can Refuse', compute='_compute_current_approver_user', store=False, default=False)
    can_reset = fields.Boolean(string='Can Reset', compute='_compute_current_approver_user', store=False, default=False)
    
    current_user = fields.Boolean(string='Current User?', compute='_compute_current_approver_user', store=False)
    
    
    def _compute_current_approver_user(self):
        for rec in self:
            can_approve = False
            can_refuse = False
            can_reset = False
            validating_users = False
            current_user = True
            if rec.multi_level_validation:
                res = self.env['leave.validation.status'].search([('holiday_status','=',rec.id),('order','=',rec.current_approver_level)])
                if res and rec.state:
                    current_user = True if self.env.user.id == res.validating_users.id else False
                    validating_users = res.validating_users.id
                    if rec.state in ['confirm','validate1'] and current_user:
                        can_approve = True 
                        can_refuse = True
                    if rec.state in ['confirm','+','cancel','validate']:
                        can_reset = True
            else:
                if rec.state == 'confirm':
                    can_approve = True
                if rec.state in ['confirm','validate','validate1']:
                    can_refuse = True
                
            rec.current_approver_user = validating_users
            rec.can_approve = can_approve
            rec.can_refuse = can_refuse
            rec.can_reset = can_reset
            rec.current_user = current_user
        
    def action_confirm(self):
        self.state = 'confirm'
        self.current_approver_level = 0
        print('confirm', self.state)
        ret = super(HrLeave, self).action_confirm()
        
    def action_approve(self):
        ret = super(HrLeave, self).action_approve()
        for rec in self:
            for approval in rec.leave_approvals:
                if approval.validating_users.id == self.env.user.id:
                    if not approval.validation_status:
                        approval.validation_status = True
        return ret
        
    def approval_check(self):
        """ Check all leave validators approved the leave request if approved
         change the current request stage to Approved"""
            
        if self.multi_level_validation:
            if not self.current_approver_user:
                raise UserError(_("No approver assigned."))
            if self.current_approver_user.id != self.env.user.id:
                raise UserError(_("You are not the current Approver or you already approved this request."))
        ret = super(HrLeave, self).approval_check()
        self.current_approver_level += 1
        # if ret:
        #     self.compute_total_base_leave()
        return ret
    
    def _get_approval_requests(self):
        """ Action for Approvals menu item to show approval
        requests assigned to current user """

        current_uid = self.env.uid
        domain = []
        hr_holidays = self.sudo().search([('current_approver_user','=',current_uid),('state','=','confirm')])
        li = []
        print('1. hr_holidays',hr_holidays)
        for l in hr_holidays:
            print('approvals',l.id,l.current_approver_user,current_uid,l.state)
            li.append(l.id)
        print('approvers',li)
        if li:
            domain.append(('id','in',li))
            print('domain',domain)
            
        value = {
            'domain': str(domain),
            'view_mode': 'tree,form',
            'res_model': 'hr.leave',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
        }
        print('value', value)
        return value
        
    @api.onchange('holiday_status_id','employee_id')
    def add_validators(self):
        """ Update the tree view and add new validators
        when permit type is changed in leave request form """
        for rec in self:
            
            if rec.holiday_status_id.validation_type == 'multi' and not rec.multi_level_validation:
                rec.multi_level_validation = True
                
            li = [(5, 0, 0)]
            
            xorder = 0
            group_tag = rec.employee_id.group_tag
            for l in rec.holiday_status_id.leave_validators.filtered(lambda r:r.group_tag==group_tag).sorted(key=lambda r: r.order):
                if l.validator_type == 'manager':
                    department_id = rec.employee_id.department_id.id
                    applied_date = rec.create_date
                    validating_users = False
                    
                    oic_manager = self.env['hr.holidays.validators.oic.manager'].search([('department_id','=',department_id),('date_start','<=',applied_date),('active','=',True)],order='date_start desc', limit=1)
                    if oic_manager:
                        if oic_manager.date_end >= applied_date or not oic_manager.date_end:
                            validating_users = oic_manager.employee_id.user_id.id
                            
                    if not validating_users and not rec.employee_id.parent_id:
                        raise UserError(_("No assigned Division Chief or Manager in your employee record."))    
                    
                    validating_users = rec.employee_id.parent_id.user_id if not validating_users else validating_users
                    if validating_users:
                        li.append((0, 0,{'validating_users': validating_users,
                            'order': xorder})) # type: ignore
                else:
                    li.append((0, 0, {'validating_users': l.holiday_validators.id,
                        'order': xorder})) # type: ignore
                xorder += 1
                    
            rec.leave_approvals = li
    
    
        