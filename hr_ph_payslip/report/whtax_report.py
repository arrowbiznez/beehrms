from odoo import models, api
from datetime import datetime


class WHTAXXlsx(models.AbstractModel):
    _name = 'report.hr_ph_payslip.whtax_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    @api.model
    def generate_xlsx_report(self, workbook, data, records):
        for obj in records:
            print(obj)
            # One sheet by partner
            sheet = workbook.add_worksheet("WHTAX")
            headers = ["No.", "MONTH", "SALARY GRADE", "STEP", "BASIC SALARY", "SALARY DIFF", "OVERTIME PAYS",
                       "HAZARD PAYS", "GSIS (9%)", "PAGIBIG", "PHILHEALTH (2%)", "MID-YEAR", "YEAR-END BONUS",
                       "CASH GIFT",
                       "SRI", "CNA"]
            alph = ["E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"]
            left_col = ["Basic Pays (Basic Salary, Salary Difference, Overtime Pays, Hazard Pays)",
                        "Others  (Mid-Year Bonus, Year-End Bonus, Cash Gift, SRI, CNA)", "Allowable Exemption",
                        "Others Total", "Gross Taxable Income",
                        "Mandatory Deduction (GSIS, PAGIBIG, PHILHEALTH)", "Net Taxable Income", "Tax Base for the First",
                        "Excess",
                        "Percentage of the Excess", "Added Tax", "Total Tax Due", "Taxes Withheld", "Remaining Tax Due",
                        "Monthly Withholding Tax"]
            style_center_sal = workbook.add_format(
                {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
                 'text_wrap': True, 'num_format': '#,##0.00'})
            style_center_total = workbook.add_format(
                {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
                 'text_wrap': True, 'num_format': '#,##0.00', 'bold': True, })
            style_center = workbook.add_format(
                {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
                 'text_wrap': True})
            style_center_month = workbook.add_format(
                {'font_size': 10, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
                 'text_wrap': True, 'bold': True})
            style_center_italic = workbook.add_format(
                {'font_size': 8, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
                 'text_wrap': True, 'italic': True})
            style_right = workbook.add_format(
                {'font_size': 9, 'color': '000000', 'align': 'right', 'valign': 'vcenter',
                 'text_wrap': True})
            style_left = workbook.add_format(
                {'font_size': 9, 'color': '000000', 'align': 'left', 'valign': 'vcenter',
                 'text_wrap': True, 'bold': True, 'num_format': '#,##0.00'})
            style_center_header = workbook.add_format(
                {'font_size': 10, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
                 'text_wrap': True, 'bold': True, 'bg_color': 'F6F6F6'})
            col_head = 0
            m_row = 2
            m_col = 0

            sheet.set_column('A:A', 4)
            sheet.set_column('B:B', 12)
            sheet.set_column('C:C', 12)
            sheet.set_column('D:D', 12)
            sheet.set_column('E:E', 12)
            sheet.set_column('F:F', 12)
            sheet.set_column('G:G', 12)
            sheet.set_column('H:H', 12)
            sheet.set_column('I:I', 12)
            sheet.set_column('J:J', 12)
            sheet.set_column('K:K', 12)
            sheet.set_column('L:L', 12)
            sheet.set_column('M:M', 12)
            sheet.set_column('N:N', 12)
            sheet.set_column('O:O', 12)
            sheet.set_column('P:P', 12)

            for head in headers:
                sheet.write(1, col_head, head, style_center_header)
                col_head += 1
            for each in records.line_ids.sorted(key=lambda x: x.month_no):
                sheet.write(m_row, 0, each.month_no, style_center)
                sheet.write(m_row, 1, each.month, style_center)
                sheet.write(m_row, 2, each.grade.name, style_center)
                sheet.write(m_row, 3, int(each.step), style_center)
                sheet.write(m_row, 4, each.basic_salary, style_center_sal)
                sheet.write(m_row, 5, each.salary_diff, style_center_sal)
                sheet.write(m_row, 6, each.overtime_pay, style_center_sal)
                sheet.write(m_row, 7, each.hazard_pay, style_center_sal)
                sheet.write(m_row, 8, each.gsis, style_center_sal)
                sheet.write(m_row, 9, each.hdmf, style_center_sal)
                sheet.write(m_row, 10, each.philhealth, style_center_sal)
                sheet.write(m_row, 11, each.midyr_bonus, style_center_sal)
                sheet.write(m_row, 12, each.yrend_bonus, style_center_sal)
                sheet.write(m_row, 13, each.cash_gift, style_center_sal)
                sheet.write(m_row, 14, each.sri, style_center_sal)
                sheet.write(m_row, 15, each.cna, style_center_sal)
                m_row += 1
            col_tot = 1
            for i in alph:
                sheet.write(14, 3 + col_tot, '=SUM({letter}3:{letter}14)'.format(letter=i), style_center_total)
                col_tot += 1

            values = [records.basic_pays, records.others, records.allowable_exemption,
                      records.others_total
                , records.gross_taxable_income, records.mandatory_deduction, records.net_taxable_income,
                      records.tax_for_the_first, records.excess, 0, records.added_tax, records.total_tax_due,
                      records.taxes_witheld, records.remaining_tax_due, records.monthly_witholding_tax]

            sheet.merge_range('F17:K17', records.month, style_center_month)
            left_col_row = 18
            for each in left_col:
                if left_col_row == 27:
                    pass
                else:
                    sheet.merge_range('F{i}:G{i}'.format(i=left_col_row), each, style_right)
                left_col_row += 1
            right_col_row = 18
            style_left.set_indent(2)
            for each in values:
                sheet.write('H{i}'.format(i=right_col_row), each, style_left)
                right_col_row += 1
                
            allowable_exemption = format(int(records.allowable_exemption),',')
            sheet.merge_range('I18:K18', 'Summation of Basic Pays', style_center_italic)
            sheet.merge_range('I19:K20',
                              'If the total amount of Others is above P%s less %s Allowable Exemption' % (allowable_exemption,allowable_exemption),
                              style_center_italic)
            sheet.merge_range('I21:K21', 'Summation of Total Other Benefits and Allowable Exemption',
                              style_center_italic)
            sheet.merge_range('I22:K24', 'Gross Taxable Income less Mandatory Deductions', style_center_italic)

            perc1 = ""
            perc2 = ""
            perc3 = 0.00
            over = 0
            not_over = 0
            added_tax_str = ""
            
            if records.tax_table_line_id:
                added_tax = int(records.tax_table_line_id.added_tax)
                if added_tax:
                    added_tax_str = "P%s + " % format(added_tax,',') if added_tax else ""
                tax = int(records.tax_table_line_id.excess_percentage)
                perc2 = "%s%s" % (tax,'%')
                perc1 = "%s%s excess"  % (added_tax_str,perc2)
                perc3 = records.tax_table_line_id.excess_percentage/100
                over = format(records.tax_table_line_id.net_taxable_income_from,',')
                not_over = format(records.tax_table_line_id.net_taxable_income_to,',')
                
            else:
                # if 250001 <= records.tax_for_the_first <= 400000:
                if records.tax_for_the_first == 250000:
                    perc1 = "20% excess"
                    perc2 = "20%"
                    perc3 = .20
                    over = 250000
                    not_over = 400000
                # elif 400000 <= records.tax_for_the_first <= 800000:
                elif records.tax_for_the_first == 400000:
                    perc1 = "P30,000 + 25% excess"
                    perc2 = "25%"
                    perc3 = .25
                    over = 400000
                    not_over = 800000
                # elif 800001 <= records.tax_for_the_first <= 2000000:
                elif records.tax_for_the_first == 800000:
                    perc1 = "P130,000 + 30% excess"
                    perc2 = "30%"
                    perc3 = .30
                    over = 800000
                    not_over = 2000000
                # elif 2000001 <= records.tax_for_the_first <= 8000000:
                elif records.tax_for_the_first == 2000000:
                    perc1 = "P490,000 + 32% of the excess over P2,000,000"
                    perc2 = "32%"
                    perc3 = .32
                    over = 2000000
                    not_over = 8000000

            long_month_name = records.month
            datetime_object = datetime.strptime(long_month_name, "%B")
            month_number = datetime_object.month

            sheet.merge_range('I25:K28',
                              'Over P{i} but not over P{j} ({k})'.format(i=str(over), j=str(not_over), k=perc1),
                              style_center_italic)
            sheet.merge_range('I29:K29', 'Summation of {} Excess Tax and Added Tax'.format(perc2), style_center_italic)
            sheet.merge_range('I32:K32', 'Total Tax divided by {} months'.format(str(12 - month_number)),
                              style_center_italic)
            sheet.merge_range('F27:G27', '{} of the Excess'.format(perc2), style_right)

            percentage_value = records.excess * perc3
            sheet.write('H27', percentage_value, style_left)

            sheet.set_row(17, 35)
            sheet.set_row(18, 30)
            sheet.set_row(22, 30)
            sheet.set_row(20, 30)
