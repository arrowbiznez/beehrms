# -*- coding:utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime, date

import calendar
from odoo.exceptions import ValidationError

MONTH_LIST = [('1', 'January'), ('2', 'February'), ('3', 'March'), ('4', 'April'), ('5', 'May'), ('6', 'June'),
              ('7', 'July'),
              ('8', 'August'), ('9', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December')]

default_month = int(datetime.now().strftime('%m'))


class PayslipGeneralReports(models.TransientModel):
    _name = 'hr.payslip.rep'
    _description = 'Payslip Details Report'

    line_id = fields.Many2one(comodel_name='hr.salary.rule', domain=[('id', '=', '17')])

    salary_rule = fields.Selection([('gsis', 'GSIS'),
                                    ('hdmf', 'HDMF'),
                                    ('whtax', 'WHTAX'),
                                    ('pera', 'PERA'),
                                    ('sal_diff', 'Salary Differential'),
                                    ('refund', 'Refund'),
                                    ('philhealth', 'Philhealth'),
                                    ('other', 'Other Deductions'),
                                    ('land', 'Landbank Salary Loan')], )
    month = fields.Selection(selection=MONTH_LIST, string='Month', required=True, default=str(default_month))

    year = fields.Integer('Year', required=True, default=datetime.now().strftime('%Y'))

    date_from = fields.Date('Date From', required=True)

    date_to = fields.Date('Date To', required=True)
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee Name')

    @api.onchange('month', 'year')
    def onchange_month(self):
        if self.month:
            month_date = date(self.year, int(self.month), 1)
            print(month_date)

            self.date_from = month_date.replace(day=1)

            self.date_to = month_date.replace(day=calendar.monthrange(month_date.year, month_date.month)[1])

    def print_emp_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'line_id', 'month', 'year'])[0]

        return self._print_report(data)

    def _print_report(self, data):
        return self.env.ref('hr_ph_payslip.report_general_report').report_action(self, data=data)

    def print_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'salary_rule', 'month', 'year'])[0]

        return self._report_print(data)

    def _report_print(self, data):
        return self.env.ref('hr_ph_payslip.other_payroll_report').report_action(self, data=data)

    def print_alphalist(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'salary_rule', 'month', 'year'])[0]

        return self._print_alphalist(data)

    def _print_alphalist(self, data):
        return self.env.ref('hr_ph_payslip.alphalist_xls').report_action(self, data=data)

    def print_tax_withheld(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['employee_id', 'year'])[0]

        return self._print_tax_withheld(data)

    def _print_tax_withheld(self, data):
        return self.env.ref('hr_ph_payslip.tax_withheld_xls').report_action(self, data=data)

    def print_manded(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'month', 'year', 'employee_id'])[0]

        return self._print_manded(data)

    def _print_manded(self, data):
        return self.env.ref('hr_ph_payslip.manded_xls').report_action(self, data=data)

    def print_manded_pdf(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'year', 'employee_id'])[0]

        return self._print_manded_pdf(data)

    def _print_manded_pdf(self, data):
        return self.env.ref('hr_ph_payslip.manded_pdf').report_action(self, data=data)


class HrPayrollReport(models.AbstractModel):
    _name = 'report.hr_ph_payslip.hr_general_payroll_template'

    @api.model
    def _get_report_values(self, docids, data=None):

        date_start = data['form']['date_from']
        date_end = data['form']['date_to']
        line = data['form']['line_id']

        month = data['form']['month']
        year = data['form']['year']

        lines = self.env['hr.salary.rule'].sudo().search([], order="id")

        rule_id = [14, 29, 30, 20, 19, 18, 101, 31, 32, 33, 34, 35, 36, 37, 38, 39, 21, 22, 40, 23, 24, 25, 26, 27, 28,
                   16, 2, 102, 103, 104, 105]

        nhcp_salary_structure_id = self.env.ref('hr_payroll_community.structure_base').id
        nhcp_salary_structure = self.env['hr.payroll.structure'].sudo().search([('id','=',nhcp_salary_structure_id)])

        rule_ids = []
        for rule in nhcp_salary_structure.rule_ids:
            print(rule)
            if rule.appears_on_payslip:
                rule_ids.append(rule.id)
        gen_report = True
        
        if rule_ids:
            rule_id = rule_ids

        # fetch records hr.payslip based on the parameters
        records = self.env['hr.payslip'].sudo().search(
            [('date_from', '=', date_start), ('date_to', '=', date_end), ('state', '=', 'done')]
        )
        if records:
            records

        else:
            raise ValidationError("No Records Found!")

        # get all department
        department_ids = []
        for dept in records:
            print()
            if dept.employee_id.department_id:
                if dept.employee_id.department_id not in department_ids:
                    department_ids.append(dept.employee_id.department_id)

        grandtotal = 0
        pera_sub = 0
        for each in department_ids:
            for rec in records:
                if each.id == rec.employee_id.department_id.id:
                    grandtotal = grandtotal + rec.employee_id.sudo().contract_id.wage
                    print(rec.employee_id.sudo().contract_id.wage)

        docargs = {
            'records': records,
            'month': calendar.month_name[int(month)],
            'year': year,
            'doc_ids': docids,
            'department': department_ids,
            'lines': lines,
            'rule_id': rule_id,
            'grandtotal': grandtotal,
            'docs': self,

        }
        return docargs


class HrOtherPayrollReport(models.AbstractModel):
    _name = 'report.hr_ph_payslip.hr_payroll_report_template'

    @api.model
    def _get_report_values(self, docids, data=None):

        date_start = data['form']['date_from']
        date_end = data['form']['date_to']
        salary_rule = data['form']['salary_rule']
        # line = data['form']['line_id']
        gsis_ids = [31, 32, 33, 34, 35, 36]
        gsis_contribution_id = self.env.ref('hr_ph_payslip.hr_contribution_register_gsis').id
        if gsis_contribution_id:
            gsis_ids = self.env['hr.salary.rule'].sudo().search([('register_id','=',gsis_contribution_id)]).ids

        hdmf_ids = [21, 22, 38, 39]
        hdmf_contribution_id = self.env.ref('hr_ph_payslip.hr_contribution_register_hdmf').id
        if hdmf_contribution_id:
            gsis_ids = self.env['hr.salary.rule'].sudo().search([('register_id','=',hdmf_contribution_id)]).ids

        # whtax = [20]
        whtax = [self.env.ref('hr_ph_payslip.hr_salary_rule_wtax').id]

        # pera = [14]
        pera = [self.env.ref('hr_ph_payslip.hr_salary_rule_pera').id]

        sal_dif = [29]
        sal_dif_contribution_id = self.env.ref('hr_ph_payslip.hr_salary_rule_category_adj').id
        if sal_dif_contribution_id:
            sal_dif = self.env['hr.salary.rule'].sudo().search([('register_id','=',sal_dif_contribution_id)]).ids

        refund = [30]
        refund_contribution_id = self.env.ref('hr_ph_payslip.hr_contribution_register_emp_refund').id
        if refund_contribution_id:
            refund = self.env['hr.salary.rule'].sudo().search([('register_id','=',refund_contribution_id)]).ids

        landbank = [37]
        pera = [self.env.ref('hr_ph_payslip.hr_salary_rule_lbp_loan_salary').id]

        philhealth = [18, 27]
        philhealth_contribution_id = self.env.ref('hr_ph_payslip.hr_contribution_register_philhealth').id
        if philhealth_contribution_id:
            philhealth = self.env['hr.salary.rule'].sudo().search([('register_id','=',philhealth_contribution_id)]).ids

        other = [23, 24, 25, 26, 28, 40]
        other_contribution_id = self.env.ref('hr_ph_payslip.hr_contribution_register_other_allowance').id
        if other_contribution_id:
            other = self.env['hr.salary.rule'].sudo().search([('register_id','=',other_contribution_id)]).ids

        month = data['form']['month']
        year = data['form']['year']
        deductions = ['GSIS', 'HDMF', 'WITHHOLDING TAX', 'LANDBANK SALARY LOAN', 'PHILHEALTH', 'OTHER DEDUCTIONS']
        allow = ['PERA', 'SALARY DIFFERENTIAL', 'REFUND']
        line_header = []
        otherr = False

        if salary_rule:
            if salary_rule == 'gsis':
                report_name = 'GSIS'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', gsis_ids)
                                                                  ], order="id")
                sal_rules = gsis_ids
            if salary_rule == 'hdmf':
                report_name = 'HDMF'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', hdmf_ids)
                                                                  ], order="id")
                sal_rules = hdmf_ids

            if salary_rule == 'whtax':
                report_name = 'WITHHOLDING TAX'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', whtax)
                                                                  ], order="id")
                sal_rules = whtax
            if salary_rule == 'pera':
                report_name = 'PERA'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', pera)
                                                                  ], order="id")
                sal_rules = pera
            if salary_rule == 'sal_diff':
                report_name = 'SALARY DIFFERENTIAL'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', sal_dif)
                                                                  ], order="id")
                sal_rules = sal_dif
            if salary_rule == 'refund':
                report_name = 'REFUND'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', refund)
                                                                  ], order="id")
                sal_rules = refund

            if salary_rule == 'land':
                report_name = 'LANDBANK SALARY LOAN'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', landbank)
                                                                  ], order="id")
                sal_rules = landbank

            if salary_rule == 'philhealth':
                report_name = 'PHILHEALTH'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', philhealth)
                                                                  ], order="id")
                sal_rules = philhealth

            if salary_rule == 'other':
                report_name = 'OTHER DEDUCTIONS'
                lines = self.env['hr.salary.rule'].sudo().search([('id', 'in', other)
                                                                  ], order="id")
                otherr = True
                sal_rules = other

            for each_line in lines:
                if each_line.code not in line_header:
                    print(each_line.id, each_line.code)
                    line_header.append(each_line.name)

        else:
            raise ValidationError("Please select salary rule!")

        # fetch records hr.payslip based on the parameters
        records = self.env['hr.payslip'].sudo().search(
            [('date_from', '=', date_start), ('date_to', '=', date_end), ('state', '=', 'done')]
        )
        if records:
            records

        else:
            raise ValidationError("No Records Found!")
        grandtotal = 0
        # get all department
        department_ids = []
        for dept in records:
            grandtotal = grandtotal + dept.employee_id.sudo().contract_id.wage
            if dept.employee_id.department_id:
                if dept.employee_id.department_id not in department_ids:
                    department_ids.append(dept.employee_id.department_id)

        docargs = {
            'records': records,
            'month': calendar.month_name[int(month)],
            'year': year,
            'department': department_ids,
            'lines': lines,
            'line_header': line_header,
            'report': report_name,
            'docs': self,
            'deduc': deductions,
            'allow': allow,
            'other': otherr,
            'sal_rules': sal_rules,
            'monthly_gt': grandtotal
        }
        return docargs


class HrAlphaListReportXlsx(models.AbstractModel):
    _name = 'report.hr_ph_payslip.alphalist_xls'
    _inherit = 'report.report_xlsx.abstract'

    @api.model
    def generate_xlsx_report(self, workbook, data, lines):
        # for obj in partners:
        # report_name = obj.name
        # One sheet by partner
        sheet = workbook.add_worksheet("Alphalist")
        style_left = workbook.add_format(
            {'font_size': 8, 'color': '000000', 'align': 'left', 'valign': 'vcenter',
             'text_wrap': True})
        style_center_big = workbook.add_format(
            {'font_size': 10, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True, })
        style_center = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True})

        sheet.merge_range('B1:D1', 'National Historical Commission of the Philippines', style_left)
        sheet.merge_range('B2:D2', 'Sample Withholding Tax Computation on Compensation', style_left)
        sheet.merge_range('B3:D3', 'As of June 30, 2021', style_left)
        style_center_big.set_border(1)
        sheet.merge_range('B4:C4', 'EMPLOYEE NAME', style_center_big)
        sheet.write('D4', 'TIN', style_center_big)
        sheet.write('E4', 'Jan', style_center_big)
        sheet.write('F4', 'Salary Differential', style_center_big)
        sheet.set_column('F:F', 17)
        sheet.set_column('B:D', 12)
        sheet.write('G4', 'SSL', style_center_big)
        sheet.write('H4', 'Feb', style_center_big)
        sheet.write('I4', '', style_center_big)
        sheet.write('J4', 'Mar', style_center_big)
        sheet.write('K4', 'Salary Differential', style_center_big)
        sheet.set_column('K:K', 17)
        sheet.write('L4', 'Apr', style_center_big)
        sheet.write('M4', 'Salary Differential', style_center_big)
        sheet.set_column('M:M', 17)
        sheet.write('N4', 'May', style_center_big)
        sheet.write('O4', 'Salary Differential', style_center_big)
        sheet.set_column('O:O', 17)
        sheet.write('P4', 'June', style_center_big)
        sheet.write('Q4', 'Salary Differential', style_center_big)
        sheet.set_column('Q:Q', 17)
        sheet.write('R4', 'July', style_center_big)
        sheet.write('S4', 'Salary Differential', style_center_big)
        sheet.set_column('S:S', 17)
        sheet.write('T4', 'Aug', style_center_big)
        sheet.write('U4', 'Salary Differential', style_center_big)
        sheet.set_column('U:U', 17)
        sheet.write('V4', 'Sept', style_center_big)
        sheet.write('W4', 'Step Increment', style_center_big)
        sheet.set_column('W:W', 17)
        sheet.write('X4', 'Oct', style_center_big)
        sheet.write('Y4', 'Step Increment', style_center_big)
        sheet.set_column('Y:Y', 17)
        sheet.write('Z4', 'Nov', style_center_big)
        sheet.write('AA4', 'Step Increment', style_center_big)
        sheet.set_column('AA:AA', 17)
        sheet.write('AB4', 'Dec', style_center_big)
        sheet.set_column('AC:AC', 17)
        sheet.write('AC4', 'Step Increment', style_center_big)
        sheet.write('AD4', 'Gross Comp.Inc.', style_center_big)
        sheet.set_column('AD:AD', 15)
        sheet.write('AE4', 'Collective Negotiation Agreement (2021)', style_center_big)
        sheet.set_column('AE:AE', 20)
        sheet.write('AF4', 'PEI', style_center_big)
        sheet.write('AG4', 'SRI 2020', style_center_big)
        sheet.write('AH4', 'MID YEAR', style_center_big)
        sheet.write('AI4', 'YEAR END', style_center_big)
        sheet.write('AJ4', 'Cash Gift', style_center_big)
        sheet.write('AK4', 'Total (BONUS & CASH GIFT)', style_center_big)
        sheet.set_column('AK:AK', 15)
        sheet.write('AL4', 'Grand Total (BONUS, CASH GIFT & PIB)', style_center_big)
        sheet.set_column('AL:AL', 18)
        sheet.write('AM4', 'In excess of de minimis benefit ceiling', style_center_big)
        sheet.set_column('AM:AM', 18)
        sheet.write('AN4', '> 90,000', style_center_big)
        sheet.set_column('AN:AN', 10)
        sheet.write('AO4', '> 10 days', style_center_big)
        sheet.write('AP4', 'Gr.Taxable Inc', style_center_big)
        sheet.set_column('AP:AP', 15)
        sheet.write('AQ4', 'Mandatory Deduction', style_center_big)
        sheet.set_column('AQ:AQ', 15)
        sheet.write('AR4', 'Income after Mandatory Deduction', style_center_big)
        sheet.set_column('AR:AR', 18)
        sheet.write('AS4', 'P.Exemption', style_center_big)
        sheet.set_column('AS:AS', 15)
        sheet.write('AT4', 'Net Taxable Inc.', style_center_big)
        sheet.set_column('AT:AT', 15)
        sheet.write('AU4', 'Tax due on 1st ', style_center_big)
        sheet.set_column('AU:AU', 15)
        sheet.write('AV4', 'Tax due on next', style_center_big)
        sheet.set_column('AV:AV', 15)
        sheet.write('AW4', 'Tax due on 1st', style_center_big)
        sheet.set_column('AW:AW', 15)
        sheet.write('AX4', 'Tax due on next', style_center_big)
        sheet.set_column('AX:AX', 15)
        sheet.write('AY4', 'Tax Due', style_center_big)
        sheet.set_column('AY:AY', 15)
        sheet.write('AZ4', 'Tax withheld as of June 30, 2021', style_center_big)
        sheet.set_column('AZ:AZ', 17)
        sheet.write('BA4', 'Tax still due (July to November 2021)', style_center_big)
        sheet.set_column('BA:BA', 17)
        sheet.write('BB4', 'For Refund', style_center_big)
        sheet.set_column('BB:BB', 15)

        for i in range(10):
            sheet.write(i + 4, 0, i + 1, style_center)
            sheet.set_column('A:A', 3)
            # print(type(i))


class HrTaxWithheldReportXlsx(models.AbstractModel):
    _name = 'report.hr_ph_payslip.tax_withheld_xls'
    _inherit = 'report.report_xlsx.abstract'

    @api.model
    def generate_xlsx_report(self, workbook, data, lines):
        employee = data['form']['employee_id']
        year = data['form']['year']

        if employee:
            recordss = self.env['hr.payslip.witholding.tax'].sudo().search(
                [('tax_year', '=', year), ('employee_id', '=', employee[0])],
                order="id"
            )
        else:
            recordss = self.env['hr.payslip.witholding.tax'].sudo().search(
                [('tax_year', '=', year)],
                order="id"
            )
        headers = ["No.", "MONTH", "SALARY GRADE", "STEP", "BASIC SALARY", "SALARY DIFF", "OVERTIME PAYS",
                   "HAZARD PAYS", "GSIS (9%)", "PAGIBIG", "PHILHEALTH (2%)", "MID-YEAR", "YEAR-END BONUS",
                   "CASH GIFT",
                   "SRI", "CNA"]
        alph = ["E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"]
        left_col = ["Basic Pays (Basic Salary, Salary Difference, Overtime Pays, Hazard Pays)",
                    "Others  (Mid-Year Bonus, Year-End Bonus, Cash Gift, SRI, CNA)", "Allowable Exemption",
                    "Others Total", "Gross Taxable Income",
                    "Mandatory Deduction (GSIS, PAGIBIG, PHILHEALTH)", "Net Taxable Income", "Tax For The First",
                    "Excess",
                    "Percentage of the Excess", "Added Tax", "Total Tax Due", "Taxes Withheld", "Remaining Tax Due",
                    "Monthly Withholding Tax"]
        style_center_sal = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True, 'num_format': '#,##0.00'})
        style_center_total = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True, 'num_format': '#,##0.00', 'bold': True, })
        style_center = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True})
        style_center_month = workbook.add_format(
            {'font_size': 10, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True, 'bold': True})
        style_center_italic = workbook.add_format(
            {'font_size': 8, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True, 'italic': True})
        style_right = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'right', 'valign': 'vcenter',
             'text_wrap': True})
        style_left = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'left', 'valign': 'vcenter',
             'text_wrap': True, 'bold': True, 'num_format': '#,##0.00'})
        style_center_header = workbook.add_format(
            {'font_size': 10, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True, 'bold': True, 'bg_color': 'F6F6F6'})

        for records in recordss:
            # One sheet by partner
            sheet = workbook.add_worksheet(records.employee_id.name)

            col_head = 0
            m_row = 2
            m_col = 0

            sheet.set_column('A:A', 4)
            sheet.set_column('B:B', 12)
            sheet.set_column('C:C', 12)
            sheet.set_column('D:D', 12)
            sheet.set_column('E:E', 12)
            sheet.set_column('F:F', 12)
            sheet.set_column('G:G', 12)
            sheet.set_column('H:H', 12)
            sheet.set_column('I:I', 12)
            sheet.set_column('J:J', 12)
            sheet.set_column('K:K', 12)
            sheet.set_column('L:L', 12)
            sheet.set_column('M:M', 12)
            sheet.set_column('N:N', 12)
            sheet.set_column('O:O', 12)
            sheet.set_column('P:P', 12)

            for head in headers:
                sheet.write(1, col_head, head, style_center_header)
                col_head += 1
            for each in records.line_ids.sorted(key=lambda x: x.month_no):
                sheet.write(m_row, 0, each.month_no, style_center)
                sheet.write(m_row, 1, each.month, style_center)
                sheet.write(m_row, 2, each.grade.name, style_center)
                sheet.write(m_row, 3, int(each.step), style_center)
                sheet.write(m_row, 4, each.basic_salary, style_center_sal)
                sheet.write(m_row, 5, each.salary_diff, style_center_sal)
                sheet.write(m_row, 6, each.overtime_pay, style_center_sal)
                sheet.write(m_row, 7, each.hazard_pay, style_center_sal)
                sheet.write(m_row, 8, each.gsis, style_center_sal)
                sheet.write(m_row, 9, each.hdmf, style_center_sal)
                sheet.write(m_row, 10, each.philhealth, style_center_sal)
                sheet.write(m_row, 11, each.midyr_bonus, style_center_sal)
                sheet.write(m_row, 12, each.yrend_bonus, style_center_sal)
                sheet.write(m_row, 13, each.cash_gift, style_center_sal)
                sheet.write(m_row, 14, each.sri, style_center_sal)
                sheet.write(m_row, 15, each.cna, style_center_sal)
                m_row += 1
            col_tot = 1
            for i in alph:
                sheet.write(14, 3 + col_tot, '=SUM({letter}3:{letter}14)'.format(letter=i), style_center_total)
                col_tot += 1

            values = [records.basic_pays, records.others, records.allowable_exemption,
                      records.others_total
                , records.gross_taxable_income, records.mandatory_deduction, records.net_taxable_income,
                      records.tax_for_the_first, records.excess, 0, records.added_tax, records.total_tax_due,
                      records.taxes_witheld, records.remaining_tax_due, records.monthly_witholding_tax]

            sheet.merge_range('F17:K17', records.month, style_center_month)
            left_col_row = 18
            for each in left_col:
                if left_col_row == 27:
                    pass
                else:
                    sheet.merge_range('F{i}:G{i}'.format(i=left_col_row), each, style_right)
                left_col_row += 1
            right_col_row = 18
            style_left.set_indent(2)
            for each in values:
                sheet.write('H{i}'.format(i=right_col_row), each, style_left)
                right_col_row += 1

            sheet.merge_range('I18:K18', 'Summation of Basic Pays', style_center_italic)
            sheet.merge_range('I19:K20',
                              'If the total amount of Others is above P90,000 less 90,000 Allowable Exemption',
                              style_center_italic)
            sheet.merge_range('I21:K21', 'Summation of Total Other Benefits and Allowable Exemption',
                              style_center_italic)
            sheet.merge_range('I22:K24', 'Gross Taxable Income less Mandatory Deductions', style_center_italic)

            perc1 = ""
            perc2 = ""
            perc3 = 0.00
            over = 0
            not_over = 0
            # if 250001 <= records.tax_for_the_first <= 400000:
            if records.tax_for_the_first == 250000:
                perc1 = "20% excess"
                perc2 = "20%"
                perc3 = .20
                over = 250000
                not_over = 400000
            # elif 400000 <= records.tax_for_the_first <= 800000:
            elif records.tax_for_the_first == 400000:
                perc1 = "P30,000 + 25% excess"
                perc2 = "25%"
                perc3 = .25
                over = 400000
                not_over = 800000
            # elif 800001 <= records.tax_for_the_first <= 2000000:
            elif records.tax_for_the_first == 800000:
                perc1 = "P130,000 + 30% excess"
                perc2 = "30%"
                perc3 = .30
                over = 800000
                not_over = 2000000
            # elif 2000001 <= records.tax_for_the_first <= 8000000:
            elif records.tax_for_the_first == 2000000:
                perc1 = "P490,000 + 32% of the excess over P2,000,000"
                perc2 = "32%"
                perc3 = .32
                over = 2000000
                not_over = 8000000

            long_month_name = records.month
            datetime_object = datetime.strptime(long_month_name, "%B")
            month_number = datetime_object.month

            sheet.merge_range('I25:K28',
                              'Over P{i} but not over P{j} ({k})'.format(i=str(over), j=str(not_over), k=perc1),
                              style_center_italic)
            sheet.merge_range('I29:K29', 'Summation of {} Excess Tax and Added Tax'.format(perc2), style_center_italic)
            sheet.merge_range('I32:K32', 'Total Tax divided by {} months'.format(str(12 - month_number)),
                              style_center_italic)
            sheet.merge_range('F27:G27', '{} of the Excess'.format(perc2), style_right)

            percentage_value = records.excess * perc3
            sheet.write('H27', percentage_value, style_left)

            sheet.set_row(17, 35)
            sheet.set_row(18, 30)
            sheet.set_row(22, 30)
            sheet.set_row(20, 30)


class HrMandedReportXlsx(models.AbstractModel):
    _name = 'report.hr_ph_payslip.manded_xls'
    _inherit = 'report.report_xlsx.abstract'

    @api.model
    def generate_xlsx_report(self, workbook, data, lines):
        # for obj in partners:
        # report_name = obj.name
        # One sheet by partner
        employee = data['form']['employee_id']
        year = data['form']['year']
        date_start = data['form']['date_from']
        date_end = data['form']['date_to']

        if employee:
            pass
        else:
            records = self.env['hr.payslip'].sudo().search(
                [('date_from', 'like', str(year) + '%'), ('date_to', 'like', str(year) + '%'), ('state', '=', 'done')],
                order="id"
            )
            # print(type(records))

        # sortedByName = sorted(records, key=lambda x: x.employee_id.lastname)
        # for each in records.sorted(key=lambda x: x.employee_id):

        sheet = workbook.add_worksheet("Manded")
        style_left = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'left', 'valign': 'vcenter',
             'text_wrap': True})
        style_left_noB = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'left', 'valign': 'vcenter',
             'text_wrap': True})
        style_center_big = workbook.add_format(
            {'font_size': 10, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True, })
        style_center = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True, 'num_format': '#,##0.00'})
        num_seq = workbook.add_format(
            {'font_size': 9, 'color': '000000', 'align': 'center', 'valign': 'vcenter',
             'text_wrap': True})
        if records:
            sheet.merge_range('B1:D1', 'National Historical Commission of the Philippines', style_left_noB)
            sheet.merge_range('B2:D2', 'Mandatory Deduction', style_left_noB)
            sheet.merge_range('B3:D3', 'As of 2022', style_left_noB)
            sheet.write('B5', 'Employee Name', style_center_big)
            sheet.write('C5', 'Position Title', style_center_big)
            sheet.merge_range('D4:G4', 'Jan', style_center_big)
            sheet.merge_range('H4:J4', 'Feb', style_center_big)
            sheet.merge_range('K4:M4', 'Mar', style_center_big)
            sheet.merge_range('N4:P4', 'Apr', style_center_big)
            sheet.merge_range('Q4:S4', 'May', style_center_big)
            sheet.merge_range('T4:V4', 'June', style_center_big)
            sheet.merge_range('W4:Y4', 'July', style_center_big)
            sheet.merge_range('Z4:AB4', 'Aug', style_center_big)
            sheet.merge_range('AC4:AE4', 'Sept', style_center_big)
            sheet.merge_range('AF4:AH4', 'Oct', style_center_big)
            sheet.merge_range('AI4:AK4', 'Nov', style_center_big)
            sheet.merge_range('AL4:AN4', 'Dec', style_center_big)
            sheet.write('AO5', 'Total', style_center_big)
            sheet.set_column('D:D', 15)

            deduction = ['GSIS', 'Philhealth', 'Pag-ibig']
            row = 7
            col = 4
            sheet.write('D5', 'GSIS', style_center_big)
            sheet.write('E5', 'Philhealth', style_center_big)
            sheet.write('F5', 'Pag-ibig', style_center_big)
            sheet.write('G5', 'GSIS SSL', style_center_big)
            for its in range(11):
                for each in deduction:
                    sheet.write(col, row, each, style_center)
                    row += 1

            f_col = 1
            f_row = 5
            liss = []
            jan = date(year, 1, 1)
            feb = date(year, 2, 1)
            mar = date(year, 3, 1)
            apr = date(year, 4, 1)
            may = date(year, 5, 1)
            june = date(year, 6, 1)
            july = date(year, 7, 1)
            aug = date(year, 8, 1)
            sept = date(year, 9, 1)
            oct = date(year, 10, 1)
            nov = date(year, 11, 1)
            dec = date(year, 12, 1)

            for each in records.sorted(key=lambda x: x.employee_id.lastname):
                # print(type(each.date_from))
                # print(each.employee_id.position_title)
                if each.employee_id.lastname in liss:
                    f_row -= 1
                    f_col += 1
                    if jan == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            f_col += 1
                            sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 4
                    if feb == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if mar == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if apr == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if may == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if june == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if july == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if aug == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if sept == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if oct == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if nov == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if dec == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    f_col = 1
                    f_row += 1
                else:
                    liss.append(each.employee_id.lastname)
                    sheet.write(f_row, f_col,
                                each.employee_id.lastname.capitalize() + ', ' + each.employee_id.firstname.capitalize() + ' ' +
                                each.employee_id.middlename[0].capitalize() + '.', style_left)
                    sheet.set_column('B:B', 25)
                    f_col += 1
                    sheet.write(f_row, f_col, each.employee_id.position_title, style_center)
                    # sheet.write(f_row, f_col + 2, str(each.date_from), style_center)
                    sheet.set_column('C:C', 18)
                    if jan == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            f_col += 1
                            sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 4
                    if feb == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if mar == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if apr == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if may == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if june == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if july == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if aug == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if sept == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if oct == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if nov == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    if dec == each.date_from:
                        for line in each.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                            if line.salary_rule_id.id == 17:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 18:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                            if line.salary_rule_id.id == 19:
                                f_col += 1
                                if line.amount:
                                    sheet.write(f_row, f_col, line.amount, style_center)
                                else:
                                    sheet.write(f_row, f_col, ' ', style_center)
                    else:
                        f_col += 3
                    # loop lines ids
                    #   check salary rules the same (gsis, sss, ph, etc..)
                    # sheet.write(f_row, f_col + 2, str(each.date_from), style_center)
                    # else:
                    #     # blank cells
                    #     print()
                    f_row += 1
                    f_col = 1
            t_row = 5
            t_col = 40
            for x in range(len(liss)):
                sheet.write(t_row, 0, x + 1, num_seq)
                sheet.set_column('A:A', 3)
                sheet.write(t_row, t_col, '=SUM(E' + str(6 + x) + ':AN' + str(6 + x) + ')', style_center)
                t_row += 1
        else:
            raise ValidationError("No Records Found!")


#         =SUM(E5:AO5)


class HrManded(models.AbstractModel):
    _name = 'report.hr_ph_payslip.hr_manded_template'

    @api.model
    def _get_report_values(self, docids, data=None):

        date_start = data['form']['date_from']
        date_end = data['form']['date_to']

        year = data['form']['year']
        employee = data['form']['employee_id']

        month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
        rules = ['GSIS', 'Philhealth', 'Pag-ibig']

        jan = date(year, 1, 1)
        feb = date(year, 2, 1)
        mar = date(year, 3, 1)
        apr = date(year, 4, 1)
        may = date(year, 5, 1)
        june = date(year, 6, 1)
        july = date(year, 7, 1)
        aug = date(year, 8, 1)
        sept = date(year, 9, 1)
        oct = date(year, 10, 1)
        nov = date(year, 11, 1)
        dec = date(year, 12, 1)

        dates = [jan, feb, mar, apr, may, june, july, aug, sept, oct, nov, dec]
        print(employee)
        if employee:
            records = self.env['hr.payslip'].sudo().search(
                [('date_from', 'like', str(year) + '%'), ('date_to', 'like', str(year) + '%'), ('state', '=', 'done'),
                 ('employee_id', '=', employee[0])],
                order="id"
            )
        else:
            records = self.env['hr.payslip'].sudo().search(
                [('date_from', 'like', str(year) + '%'), ('date_to', 'like', str(year) + '%'), ('state', '=', 'done')],
                order="id"
            )
        rec_list = []
        record_list = []
        emp_lastname = []
        for each in records.sorted(key=lambda x: x.employee_id.lastname):
            if each.employee_id.id not in rec_list:
                rec_list.append(each.employee_id.id)
                emp_lastname.append(
                    each.employee_id.lastname + ', ' + each.employee_id.firstname + ' ' + each.employee_id.middlename[
                        0] + '.')
                record_list.append(each)

        final_rec = []
        emp_lines = []
        emp_record = {}
        sal_rule_values = []
        for each in emp_lastname:
            emp_record[each] = {}

        print(emp_record)
        for each in emp_lastname:
            for each2 in dates:
                emp_record[each][str(each2)] = [0, 0, 0]

        # for each2 in dates:
        # emp_record[each][str(each2)] = {}

        for each in record_list:
            # emp_record[each.employee_id.lastname] = {}
            for each2 in records.sorted(key=lambda x: x.date_from):
                if each.employee_id.id == each2.employee_id.id:
                    emp_lines.append(each2)
            final_rec.append(emp_lines)
            emp_lines = []
        # for each in final_rec:
        #     for x in each:
        #         for each_item in dates:
        #             if each_item == x.date_from:
        #                 for line in x.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
        #                     if line.salary_rule_id.id == 17:
        #                         sal_rule_values.append(line.amount)
        #                     if line.salary_rule_id.id == 18:
        #                         sal_rule_values.append(line.amount)
        #                     if line.salary_rule_id.id == 19:
        #                         sal_rule_values.append(line.amount)
        #         emp_record[x.employee_id.lastname][str(x.date_from)] = sal_rule_values
        #         sal_rule_values = []
        for each in final_rec:
            for x in each:
                # for each_item in dates:
                #     if each_item == x.date_from:
                for line in x.line_ids.sorted(key=lambda x: x.salary_rule_id.id):
                    if line.salary_rule_id.id == 17:
                        sal_rule_values.append(line.amount)
                    if line.salary_rule_id.id == 18:
                        sal_rule_values.append(line.amount)
                    if line.salary_rule_id.id == 19:
                        sal_rule_values.append(line.amount)
                emp_record[
                    x.employee_id.lastname + ', ' + x.employee_id.firstname + ' ' + x.employee_id.middlename[0] + '.'][
                    str(x.date_from)] = sal_rule_values
                sal_rule_values = []

        docargs = {
            'year': year,
            'doc_ids': docids,
            'docs': self,
            'month_names': month_names,
            'rules': rules,
            'month_num': range(12),
            # 'record_list': record_list,
            # 'records': records,
            # 'dates': dates,
            'emp_record': emp_record

        }
        return docargs
