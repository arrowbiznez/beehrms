# -*- coding:utf-8 -*-

from odoo import api, fields, models
from datetime import datetime, date

import calendar
from odoo.exceptions import ValidationError

MONTH_LIST = [('1', 'January'), ('2', 'February'), ('3', 'March'), ('4', 'April'), ('5', 'May'), ('6', 'June'), ('7', 'July'),
              ('8', 'August'), ('9', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December')]

default_month = int(datetime.now().strftime('%m'))


class PayslipEmployee(models.TransientModel):
    _name = 'hr.payslip.emp'
    _description = 'Payslip Details Report'

    # line_id = fields.Many2one(comodel_name='hr.payslip.line', required=True)
    employee_id = fields.Many2one(comodel_name='hr.employee')
    month = fields.Selection(selection=MONTH_LIST, string='Month', required=True, default=str(default_month))

    year = fields.Integer('Year', required=True, default=datetime.now().strftime('%Y'))

    date_from = fields.Date('Date From', required=True)

    date_to = fields.Date('Date To', required=True)

    # current_user = fields.Many2one('res.users', 'In Charge', default=lambda self: self.env.user)

    current_user = fields.Many2one(comodel_name='hr.employee', string='In Charge', default=lambda self: self.env.user.employee_id.id)

    @api.onchange('month', 'year')
    def onchange_month(self):
        if self.month:
            month_date = date(self.year, int(self.month), 1)

            self.date_from = month_date.replace(day=1)

            self.date_to = month_date.replace(day=calendar.monthrange(month_date.year, month_date.month)[1])

    def print_emp_payslip_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'employee_id', 'month', 'year','current_user'])[0]

        return self._print_payslip_report(data)

    def _print_payslip_report(self, data):
        return self.env.ref('hr_ph_payslip.report_employee_payslip').report_action(self, data=data)


class HrPayslipReport(models.AbstractModel):
    _name = 'report.hr_ph_payslip.hr_employee_payslip_template'

    @api.model
    def _get_report_values(self, docids, data=None):
        date_start = data['form']['date_from']
        date_end = data['form']['date_to']

        month = data['form']['month']
        year = data['form']['year']
        current_user = data['form']['current_user']
        # line = data['form']['line_id']
        selected_employee = data['form']['employee_id']
        month_name = calendar.month_name[int(month)]

        # print(current_user, current_user[0])
        user = self.env['hr.employee'].sudo().search([('id', '=', current_user[0])])
        # print(user.middlename[0],user.middlename, user.firstname, user.lastname, user.position_title)
        if selected_employee:
            employee = self.env['hr.payslip'].sudo().search([('employee_id', '=', selected_employee[0]),
                                                             ('date_from', '=', date_start), ('date_to', '=', date_end),
                                                             ('state', '=', 'done')])
            if employee:
                employee
                print(employee)
            else:
                raise ValidationError("No Records Found!")
        else:
            employee = self.env['hr.payslip'].sudo().search([('date_from', '=', date_start), ('date_to', '=', date_end),
                                                             ('state', '=', 'done')])
            if employee:
                employee
                print(employee)
            else:
                raise ValidationError("No Records Found!")

        docargs = {
            'docs': self,
            'month_name': str(month_name),
            'year': str(year),
            'employee': employee,
            'firstname': user.firstname,
            'lastname': user.lastname,
            'position': user.position_title,
            'middle_initial': user.middlename[0],
            'run_date': datetime.now().strftime("%m/%d/%Y %H:%M:%S")
            # 'user': user
        }
        return docargs
