from mailbox import MMDF
from odoo import models, fields, api, _
from datetime import datetime
from io import StringIO
import base64, shutil

class hrbankreport(models.TransientModel):
    _name = 'hr.bank.report'
    _description = 'Payroll Bank Report'
    _rec_name = 'payslip_run_id'
    
    payslip_run_id = fields.Many2one(comodel_name='hr.payslip.run', string='Payslip Batch', domain="[('state','=','close')]")
    payroll_week = fields.Selection(string='Payroll Week', selection=[('1', '1st Week'), 
                                                                ('2', '2nd Week'), 
                                                                ('3', '3rd Week'), 
                                                                ('4', '4th Week')], default='1')
    bank_reports_lines = fields.One2many(comodel_name='hr.bank.report.line', inverse_name='bank_report_id', string='Reports Details')
    bank_report = fields.Text(string='Bank Reports')
    
    account_number = fields.Char(string='Account Number', related='payslip_run_id.account_number')
    account_name = fields.Char(string='Account Name', related='payslip_run_id.account_name')
    account_code = fields.Char(string='Bank Code', related='payslip_run_id.bank_branchcode')
    account_serial = fields.Char(string='Account Serial', related='payslip_run_id.account_serial')
    account_serial2 = fields.Char(string='Account Serial', related='payslip_run_id.account_serial2')
    account_amount = fields.Float(string='Amount')
    account_amount_text = fields.Char(string='Amount text')
    account_count = fields.Char(string='Account Count')
    deposit_date = fields.Char(string='Account date')
    
    attachment_id = fields.Many2one(comodel_name='ir.attachment', string='Attachments')
    
    
    # @api.onchange('payslip_run_id','payroll_week')
    # def _onchange_bank_reports_lines(self):
    #     bank_reports_lines = [(5,0)]
    #     for rec in self:
    #         for line in rec.payslip_run_id.slip_ids:
    #             if line.state == 'done':
    #                 if rec.payroll_week == '1':
    #                     amount_text = line.week1_amount_text
    #                 elif rec.payroll_week == '2':
    #                     amount_text = line.week2_amount_text
    #                 elif rec.payroll_week == '3':
    #                     amount_text = line.week3_amount_text
    #                 else:
    #                     amount_text = line.week4_amount_text
    #                 bank_reports_lines.append((0,0,{'account_number':line.account_number,
    #                 'account_name':line.account_name,
    #                 'account_amount_text': amount_text,
    #                 'account_code': line.bank_branchcode}))
                    
    #         rec.bank_reports_lines = bank_reports_lines
        
    def generate_bank_report(self):
        if not self.payslip_run_id:
            return { 
                'type' : 'ir.actions.do_nothing'
             }
        output = StringIO("")
        bank_text = ""
        counter = 0
        for rec in self.payslip_run_id:
            for line in rec.slip_ids:
                if line.state != 'done':
                    continue
                if self.payroll_week == '1':
                    amount_text = line.week1_amount_text
                    deposit_date = rec.week1_deposit_date
                elif self.payroll_week == '2':
                    amount_text = line.week2_amount_text
                    deposit_date = rec.week2_deposit_date
                elif self.payroll_week == '3':
                    amount_text = line.week3_amount_text
                    deposit_date = rec.week3_deposit_date
                else:
                    amount_text = line.week4_amount_text
                    deposit_date = rec.week4_deposit_date
                counter += 1
                account_number = line.account_number or ''
                account_name = line.account_name or ''
                bank_branchcode = line.bank_branchcode or 999
                bank_text = "%s%s%s%s\n" % (account_number.replace('-','').zfill(10),account_name.ljust(40,' '),amount_text.zfill(15),bank_branchcode.ljust(16,' '))
                output.write(bank_text)
            
            if self.payroll_week == '1':
                amount_text = rec.week1_amount_text
                deposit_date = rec.week1_deposit_date
            elif self.payroll_week == '2':
                amount_text = rec.week2_amount_text
                deposit_date = rec.week2_deposit_date
            elif self.payroll_week == '3':
                amount_text = rec.week3_amount_text
                deposit_date = rec.week3_deposit_date
            else:
                amount_text = rec.week4_amount_text
                deposit_date = rec.week4_deposit_date
            ctr = rec.account_count or ''
            account_number = rec.account_number or ''
            account_name = rec.account_name or ''
            account_serial = rec.account_serial or ''
            account_serial2 = rec.account_serial2 or '00000'
            bank_text = "%s%s%s%s%s%s %s" % (account_number.zfill(10),account_name.ljust(25,' '),amount_text.zfill(15),deposit_date,account_serial.zfill(9),ctr.zfill(8), account_serial2)
            output.write(bank_text)
        output.seek(0)
        
        fname = "payroll_%s.txt" % deposit_date
        url = ('/tmp/%s' % fname)
        with open(url, 'w') as fd:
            shutil.copyfileobj(output, fd)
            fd.close()
            
        with open(url) as fd:
             file = fd.read()
        
        data = {
            'name': fname,
            'store_fname': "%s: %s" % (self.payslip_run_id.name,deposit_date),
            'res_model': self._name,
            'res_id': self.id,
            'type': 'binary',
            'datas': base64.b64encode(file.encode('ascii')),
        }        
        ret = self.env['ir.attachment'].sudo().create(data)
        self.attachment_id = ret
        
        download_url = '/web/content/' + str(ret.id) + '?download=True'
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        return {
                "type": "ir.actions.act_url",
                "url": str(base_url)  +  str(download_url),
                "target": "new",
             }
             
    def print_bank_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.payslip_run_id.id
        data['model'] = 'hr.payslip.run'
        data['form'] = self.read(['payslip_run_id', 'payroll_week'])[0]
        print('data = ',data)
        return self.env.ref('hr_ph_payslip.payroll_bank_report_report').report_action(self, data=data)

    
class hrbankreport(models.TransientModel):
    _name = 'hr.bank.report.line'
    _description = 'Payroll Bank Report Lines'
    
    bank_report_id = fields.Many2one(comodel_name='hr.bank.report', string='Bank Report')
    account_number = fields.Char(string='Account Number')
    account_name = fields.Char(string='Account Name')
    account_amount = fields.Float(string='Amount')
    account_amount_text = fields.Char(string='Amount Text')
    account_code = fields.Char(string='Bank Code')