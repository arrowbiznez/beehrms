# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from io import StringIO
import math, base64, shutil
from odoo.exceptions import ValidationError

class hrEmployee(models.Model):
    _inherit = 'res.company'
    
    payroll_bank = fields.Char(string='Bank Name')
    account_name = fields.Char(string='Account Name')
    bank_branchcode = fields.Char(string='Branch Code')
    account_number = fields.Char(string='Account Number')
    account_serial = fields.Char(string='Account Serial')
    account_serial2 = fields.Char(string='Account Serial2')
    
class hrEmployee(models.Model):
    _inherit = 'hr.employee'

    account_number = fields.Char(string='Account Number')

class HrPayslip(models.Model):
    _inherit = 'hr.payslip.run'
    _order = 'date_start desc, date_end desc, name'
    
    # _sql_constraints = [
    #     (
    #         "payroll_period_uniq",
    #         "unique (date_start, date_end)",
    #         "A Payroll Period must be unique!",
    #     )
    # ]
    
    payroll_bank = fields.Char(string='Bank Name', related='company_id.payroll_bank' , store=True)
    account_name = fields.Char(string='Account Name', related='company_id.account_name', store=True)
    account_number = fields.Char(string='Account Number', related='company_id.account_number', store=True)
    bank_branchcode = fields.Char(string='Branch Code', related='company_id.bank_branchcode', store=True)
    account_serial = fields.Char(string='Account Serial', related='company_id.account_serial', store=True)
    account_serial2 = fields.Char(string='Account Serial 2', related='company_id.account_serial2', store=True)
    
    week1_amount_text = fields.Char(string='Week 1 Amount', compute='_compute_state', store=True)
    week2_amount_text = fields.Char(string='Week 2 Amount', compute='_compute_state', store=True)
    week3_amount_text = fields.Char(string='Week 3 Amount', compute='_compute_state', store=True)
    week4_amount_text = fields.Char(string='Week 4 Amount', compute='_compute_state', store=True)
    
    account_count = fields.Char(string='Account Count', compute='_compute_state', store=True)
    
    week1_deposit_date = fields.Char(string='Week 1 Account date', compute='_compute_state', store=True)
    week2_deposit_date = fields.Char(string='Week 2 Account date', compute='_compute_state', store=True)
    week3_deposit_date = fields.Char(string='Week 3 Account date', compute='_compute_state', store=True)
    week4_deposit_date = fields.Char(string='Week 4 Account date', compute='_compute_state', store=True)
    
    
    @api.constrains('date_start','date_end','state')
    def _validate_payroll_period(self):
        for rec in self:
            id = rec.id
            date_start = rec.date_start
            date_end = rec.date_end
            found = self.search([('state','=','close'),('date_start','=',date_start),('date_end','=',date_end),('id','!=',id)])
            if found:
                raise ValidationError(_('Found duplicate of Payroll Period: "%s to %s".' % (date_start.strftime('%Y-%m-%d'), date_end.strftime('%Y-%m-%d'))))
                
    def recompute_bank_report(self, id):
        total_pera  = 0.0
        total_net = 0.0
        for rec in self.browse(id):
            if rec.state != 'close':
                continue
            print('executing... recompute_bank_report')
            salary_date  = rec.date_start 
            mm = salary_date.strftime('%m').zfill(2)
            YY = salary_date.strftime('%Y')
            rec.week1_deposit_date = '%s%s%s' % (mm,'1',YY)
            rec.week2_deposit_date = '%s%s%s' % (mm,'2',YY)
            rec.week3_deposit_date = '%s%s%s' % (mm,'3',YY)
            rec.week4_deposit_date = '%s%s%s' % (mm,'4',YY)
            
            count = 0
            for slip in rec.slip_ids:
                if slip.state == 'done':
                    count += 1        
                    for line in slip.line_ids.filtered(lambda r: r.code in ('NET','PERA')):
                        if line.code == 'PERA':
                            total_pera += line.total/2
                        if line.code == 'NET':
                            total_net += line.total/4
            total_net -= total_pera
            amount_pera = '{0:.2f}'.format(total_net).replace('.','').zfill(15)
            amount_total = '{0:.2f}'.format(total_net - total_pera).replace('.','').zfill(15)
            rec.week1_amount_text = amount_total
            rec.week2_amount_text = amount_pera
            rec.week3_amount_text = amount_total
            rec.week4_amount_text = amount_pera
            rec.account_count = str(count).zfill(8)
                
    @api.depends('state')
    def _compute_state(self):
        total_pera  = 0.0
        total_net = 0.0
        for rec in self:
            if rec.state != 'close':
                continue
            salary_date  = rec.date_start 
            mm = salary_date.strftime('%m').zfill(2)
            YY = salary_date.strftime('%Y')
            rec.week1_deposit_date = '%s%s%s' % (mm,'1',YY)
            rec.week2_deposit_date = '%s%s%s' % (mm,'2',YY)
            rec.week3_deposit_date = '%s%s%s' % (mm,'3',YY)
            rec.week4_deposit_date = '%s%s%s' % (mm,'4',YY)
            
            count = 0
            for slip in rec.slip_ids:
                if slip.state == 'done':
                    count += 1        
                    for line in slip.line_ids.filtered(lambda r: r.code in ('NET','PERA')):
                        if line.code == 'PERA':
                            total_pera += line.total/2
                        if line.code == 'NET':
                            total_net += line.total/4
            total_net -= total_pera
            amount_pera = '{0:.2f}'.format(total_net).replace('.','').zfill(15)
            amount_total = '{0:.2f}'.format(total_net - total_pera).replace('.','').zfill(15)
            rec.week1_amount_text = amount_total
            rec.week2_amount_text = amount_pera
            rec.week3_amount_text = amount_total
            rec.week4_amount_text = amount_pera
            rec.account_count = str(count).zfill(8)
            
class HrPayslip(models.Model):
    _inherit = 'hr.payslip'
    _order = 'date_from desc, date_to desc, number'
    
    account_number = fields.Char(string='Account Number', related='employee_id.account_number', store=True)
    account_name = fields.Char(string='Account Name', compute='_compute_state', store=True)
    bank_branchcode = fields.Char(string='Branch Code', related='employee_id.company_id.bank_branchcode', store=True)
    week1_amount_text = fields.Char(string='Week 1 Amount', compute='_compute_state', store=True)
    week2_amount_text = fields.Char(string='Week 2 Amount', compute='_compute_state', store=True)
    week3_amount_text = fields.Char(string='Week 3 Amount', compute='_compute_state', store=True)
    week4_amount_text = fields.Char(string='Week 4 Amount', compute='_compute_state', store=True)
    
    #whtax
    whtax_held = fields.Float(string='Tax Witheld', compute='_compute_witholding_tax', default=0.0)
    whtax = fields.Float(string='Witholding Tax', compute='_compute_witholding_tax', default=0.0)
    salary_diff = fields.Float(string='Salary Differential', required=False, compute='_compute_witholding_tax', default=0.0)
    overtime_pay = fields.Float(string='Overtime Pays', required=False, compute='_compute_witholding_tax', default=0.0)
    hazard_pay = fields.Float(string='Hazard Pays', required=False, compute='_compute_witholding_tax', default=0.0)
    gsis = fields.Float(string='GSIS', required=False, compute='_compute_witholding_tax', default=0.0)
    hdmf = fields.Float(string='Pag-ibig', required=False, compute='_compute_witholding_tax', default=0.0)
    philhealth = fields.Float(string='PhilHealth', required=False, compute='_compute_witholding_tax', default=0.0)
    midyr_bonus = fields.Float(string='Mid-Year Bonus', required=False, compute='_compute_witholding_tax', default=0.0)
    yrend_bonus = fields.Float(string='Year-End Bonus', required=False, compute='_compute_witholding_tax', default=0.0)
    cash_gift = fields.Float(string='Cash Gift', required=False, compute='_compute_witholding_tax', default=0.0)
    sri = fields.Float(string='SRI', required=False)
    cna = fields.Float(string='CNA', required=False)
    whtax_id = fields.Many2one(comodel_name='hr.payslip.witholding.tax', string='Witholding Tax Table', compute='_compute_witholding_tax')
    whtax_line_id = fields.Many2one(comodel_name='hr.payslip.witholding.tax.line', string='Witholding Tax Line', compute='_compute_witholding_tax')
    
    tax_held_count = fields.Integer(string='Tax Held Count', compute='_compute_tax_held_count', default=0)
    prev_whtax = fields.Float(string='Prev WHTAX', compute='_compute_tax_held_count', default=0.0)
    

    def action_close_payslip(self):
        return {
        'type': 'ir.actions.client',
        'tag': 'reload',
        }

    def _compute_witholding_tax(self):
        for rec in self:
            employee_id = rec.employee_id.id
            date_from = rec.date_from
            date_to = rec.date_to
            year = date_from.year
            whtax = 0.0
            whtax_held = 0.0
            salary_diff = 0.0
            overtime_pay = 0.0
            hazard_pay = 0.0
            gsis = 0.0
            hdmf = 0.0
            philhealth = 0.0
            midyr_bonus = 0.0
            yrend_bonus = 0.0
            cash_gift = 0.0
            sri = 0.0
            cna = 0.0
            whtax_id = False
            whtax_line_id = False
            
            found = self.env['hr.payslip.witholding.tax.line'].search([('employee_id','=',employee_id),('date_from','=',date_from),('date_to','=',date_to),('active','=',True)],order='date_to desc, date_from desc',limit=1)
            if found:
                if not found.whtax_id.active:
                    found = False
            if not found:
                self.env['hr.payslip.witholding.tax'].compute_whtax_held_employee(employee_id,year)
                self.env.cr.commit()
                found = self.env['hr.payslip.witholding.tax.line'].search([('employee_id','=',employee_id),('date_from','=',date_from),('date_to','=',date_to),('active','=',True)],order='date_to desc, date_from desc',limit=1)
            if found:
                whtax_held = found.whtax_held
                whtax = found.whtax
                salary_diff = found.salary_diff
                overtime_pay = found.overtime_pay
                hazard_pay = found.hazard_pay
                gsis = found.gsis
                hdmf = found.hdmf
                philhealth = found.philhealth
                midyr_bonus = found.midyr_bonus
                yrend_bonus = found.yrend_bonus
                cash_gift = found.cash_gift
                sri = found.sri
                cna = found.cna
                whtax_line_id = found.id
                whtax_id = found.whtax_id.id
            else:
                raise ValidationError(
                    _(
                        "The %s Annual Witholding Tax Table of %s is in-active or not generated yet." % (year, rec.employee_id.name)
                    )
                )
                
            rec.whtax_held = whtax_held
            rec.whtax = whtax
            rec.gsis = gsis
            rec.hdmf = hdmf
            rec.philhealth = philhealth
            
            rec.salary_diff = salary_diff
            rec.overtime_pay = overtime_pay
            rec.hazard_pay = hazard_pay
            rec.midyr_bonus = midyr_bonus
            rec.yrend_bonus = yrend_bonus
            rec.cash_gift = cash_gift
            rec.sri = sri
            rec.cna = cna
            rec.whtax_line_id = whtax_line_id
            rec.whtax_id = whtax_id
    
    def _compute_tax_held_count(self):
        for rec in self:
            rec.tax_held_count = (12 - rec.date_to.month)
            employee_id = rec.employee_id.id
            date_from = rec.date_from
            date_to = rec.date_to
            prev_whtax = 0.0
            if rec.date_to.month > 1:
                found = self.search([('employee_id','=',employee_id),('date_to','<',date_to)],order='date_to desc, date_from desc')
                for line in found.line_ids:
                    if line.code == 'WHTAX':
                        prev_whtax = line.amount
                        break
                if not found:
                    found = self.env['hr.payslip.witholding.tax.line'].search([('employee_id','=',employee_id),('date_from','=',date_from),('date_to','=',date_to),('active','=',True)],order='date_to desc, date_from desc',limit=1)
                    # found = self.env['hr.payslip.beginning.ref'].search([('employee_id','=',employee_id),('date_to','<',date_to),('active','=',True)],order='date_to desc, date_from desc',limit=1)
                    if found:
                        if found.whtax_id.active:
                            prev_whtax = found.whtax
            rec.prev_whtax = prev_whtax
    
    def action_slip_done(self):
        if self.payslip_run_id:
            if self.payslip_run_id.state == 'done':
                raise ValidationError("Parent Payslip Batch is already closed!")
        return super(HrPayslip, self).action_slip_done()
            
        
    @api.depends('state')
    def _compute_state(self):
        pera  = 0.0
        total = 0.0
        for rec in self:
            if rec.state != 'done':
                continue
            account_name = '%s, %s %s' % (rec.employee_id.lastname or '',rec.employee_id.firstname or '',rec.employee_id.middlename[:1] if rec.employee_id.middlename else '')
            account_name = account_name.upper().ljust(40,' ')
            rec.account_name = account_name
            for line in rec.line_ids.filtered(lambda r: r.code in ('PERA','NET')):
                if line.code == 'PERA':
                    pera = line.total/2
                else:
                    total = line.total/4
                    
            amount_pera = '{0:.2f}'.format(total).replace('.','').zfill(15)
            amount_total = '{0:.2f}'.format(total-pera).replace('.','').zfill(15)
            rec.week1_amount_text = amount_total
            rec.week2_amount_text = amount_pera
            rec.week3_amount_text = amount_total
            rec.week4_amount_text = amount_pera