from odoo import api, fields, models
from datetime import datetime, date
from calendar import monthrange
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError, UserError

steps = [
    ('1', 'Step 1'), ('2', 'Step 2'),
    ('3', 'Step 3'), ('4', 'Step 4'),
    ('5', 'Step 5'), ('6', 'Step 6'),
    ('7', 'Step 7'), ('8', 'Step 8')]

class HrSalaryRegister(models.Model):
    _inherit = 'hr.salary.rule'
    
    register = fields.Char(string='Register', related='register_id.name')

class HrPayslipInput(models.Model):
    _inherit = 'hr.payslip.input'
    
    # sdiff_line_id = fields.Many2one('hr.payslip.adjustment.sdiff', string="Salary Diffirential", help="Salary Differential of Employee")
    sdiff_line_id = fields.Many2one('hr.payslip.witholding.tax.line', string="Salary Diffirential", help="Salary Differential of Employee", domain=[('active','=',True)])


class HrPayslipAdjustmentSalaryDifferential(models.Model):
    _name = 'hr.payslip.adjustment.sdiff'
    _description = 'Salary Differential - Change of Salary Grade'
    _order = 'payroll_date desc, date_start asc'

    contract_id = fields.Many2one(comodel_name='hr.contract', string='Contract', required=True, domain=[('active','=',True)])
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='contract_id.employee_id')
    name = fields.Char(string='Name', related='employee_id.name', store=True)
    rule_id = fields.Many2one(comodel_name='hr.salary.rule', string='Adjustment Name', required=True,
    domain=[('register','=',('Salary Differential (due to salary step increment and promotion)')),('active','=',True)],
    default=lambda self: self.env.ref('hr_ph_payslip.hr_salary_rule_emp_sdiff'), readonly=True)
    amount = fields.Float(string='Adjustment Amount', required=True)
    date_from = fields.Date(string='Payroll Date From', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,1))
    date_to = fields.Date(string='Payroll Date To', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,monthrange(fields.datetime.now().year,fields.datetime.now().month)[1]))
    date_start = fields.Date(string='Payroll Date From', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,1))
    date_end = fields.Date(string='Payroll Date To', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,monthrange(fields.datetime.now().year,fields.datetime.now().month)[1]))
    payroll_date = fields.Date(string='Payroll Date (End)', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,monthrange(fields.datetime.now().year,fields.datetime.now().month)[1])+relativedelta(months=1))
    active = fields.Boolean(string='Active', default=True)
    code = fields.Char(string='Code', related='rule_id.code')
    payslip_id = fields.Many2one(comodel_name='hr.payslip', string='Payslip', readonly=True)
    

# class HrPayslipWitholdingBeginning(models.Model):
#     _name = 'hr.payslip.witholding.tax.beginning'
#     _description = 'Witholding Tax Beginning'
#     _order = 'date_start desc, name asc'

#     contract_id = fields.Many2one(comodel_name='hr.contract', string='Contract', required=True)
#     employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='contract_id.employee_id')
#     name = fields.Char(string='Name', related='employee_id.name', store=True)
#     amount = fields.Float(string='Witholding Tax', required=True)
#     date_from = fields.Date(string='Date From', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,1))
#     date_to = fields.Date(string='Date To', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,monthrange(fields.datetime.now().year,fields.datetime.now().month)[1]))
#     active = fields.Boolean(string='Active', default=True)


class HrPayslipBeginning(models.Model):
    _name = 'hr.payslip.beginning.ref'
    _description = 'Payslip Beginning Reference'
    _order = 'date_from desc, date_to desc, name asc'
    
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    name = fields.Char(string='Name', related='employee_id.name', store=True)
    date_from = fields.Date(string='Date From', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,1))
    date_to = fields.Date(string='Date To', required=True, default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,monthrange(fields.datetime.now().year,fields.datetime.now().month)[1]))
    grade = fields.Many2one(comodel_name='hr.salary.grade', string='Grade', required=True, track_visibility="onchange")
    step = fields.Selection(string='Step', selection=steps, default=1, required=True, track_visibility="onchange")
    basic_salary = fields.Float(string='Basic Salary', required=True)
    salary_diff = fields.Float(string='Salary Differential', required=False)
    overtime_pay = fields.Float(string='Overtime Pays', required=False)
    hazard_pay = fields.Float(string='Hazard Pays', required=False)
    gsis = fields.Float(string='GSIS', required=False)
    hdmf = fields.Float(string='Pag-ibig', required=False)
    philhealth = fields.Float(string='PhilHealth', required=False)
    midyr_bonus = fields.Float(string='Mid-Year Bonus', required=False)
    yrend_bonus = fields.Float(string='Year-End Bonus', required=False)
    cash_gift = fields.Float(string='Cash Gift', required=False)
    sri = fields.Float(string='SRI', required=False)
    cna = fields.Float(string='CNA', required=False)
    whtax = fields.Float(string='Witholding Tax', required=False)
    active = fields.Boolean(string='Active', default=True)
