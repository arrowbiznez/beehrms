from odoo import api, fields, models, _
from datetime import datetime, date
from calendar import month, monthrange
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError, UserError

class HrPayslipDeductionLoan(models.Model):
    _name = 'hr.payslip.deduction.loan'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Payslip Deductions - Loan"
    _order = 'date desc'
    
    @api.model
    def default_get(self, field_list):
        result = super(HrPayslipDeductionLoan, self).default_get(field_list)
        if result.get('user_id'):
            ts_user_id = result['user_id']
        else:
            ts_user_id = self.env.context.get('user_id', self.env.user.id)
        result['employee_id'] = self.env['hr.employee'].search([('user_id', '=', ts_user_id)], limit=1).id
        return result

    def _compute_loan_amount(self):
        total_paid = 0.0
        for loan in self.filtered(lambda r: r.active==True):
            for line in loan.loan_lines:
                if line.paid:
                    total_paid += line.amount
            balance_amount = loan.loan_amount - total_paid
            loan.total_amount = loan.loan_amount
            loan.balance_amount = balance_amount
            loan.total_paid_amount = total_paid

    name = fields.Char(string="Loan Name", default="/", readonly=True, help="Name of the loan")
    date = fields.Date(string="Date", default=fields.Date.today(), readonly=True, help="Date")
    employee_id = fields.Many2one('hr.employee', string="Employee", required=True, help="Employee")
    department_id = fields.Many2one('hr.department', related="employee_id.department_id", readonly=True,
                                    string="Department", help="Employee")
    installment = fields.Integer(string="No Of Installments", default=1, help="Number of installments")
    payment_date = fields.Date(string="Start Date", required=True, default=fields.Date.today(), help="Start Date of the paymemt")
    payment_date_end = fields.Date(string="End Date", required=True, default=fields.Date.today()+relativedelta(months=1), store=True, help="End Date of the paymemt")
    loan_lines = fields.One2many('hr.payslip.deduction.loan.line', 'loan_id', string="Loan Line", index=True)
    company_id = fields.Many2one('res.company', 'Company', readonly=True, help="Company",
                                 default=lambda self: self.env.user.company_id,
                                 states={'draft': [('readonly', False)]})
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, help="Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)
    job_position = fields.Many2one('hr.job', related="employee_id.job_id", readonly=True, string="Job Position",
                                   help="Job position")
    loan_amount = fields.Float(string="Loan Amount", required=True, help="Loan amount")
    ma_amount = fields.Float(string="Monthly Amortization", required=True, help="Monthly Amortization")
    total_amount = fields.Float(string="Total Amount", store=True, readonly=True, compute='_compute_loan_amount',
                                help="Total loan amount")
    balance_amount = fields.Float(string="Balance Amount", store=True, compute='_compute_loan_amount', help="Balance amount")
    total_paid_amount = fields.Float(string="Total Paid Amount", store=True, compute='_compute_loan_amount',
                                     help="Total paid amount")
    
    rule_id = fields.Many2one(comodel_name='hr.salary.rule', string='Loan Name', required=True,
    domain=[('note','ilike',('Loan Deduction')),('active','=',True)])
    code = fields.Char(string='Code', related='rule_id.code')
    active = fields.Boolean(string='Active', default=True)

    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed'),
        ('cancel', 'Canceled'),
    ], string="Status", default='draft', track_visibility='onchange', copy=False, )
    
    @api.onchange('payment_date','payment_date_end','ma_amount')
    def _onchange_payment_date(self):
        for rec in self:
            if rec.state == 'draft':
                if rec.payment_date_end:                  
                    st_date = datetime.strptime(str(rec.payment_date), '%Y-%m-%d') 
                    end_date = datetime.strptime(str(rec.payment_date_end), '%Y-%m-%d')
                    if st_date < end_date and end_date:
                        rec.installment = (end_date.year - st_date.year) * 12 + (end_date.month - st_date.month)
                if rec.ma_amount:
                    rec.loan_amount = rec.ma_amount*rec.installment
                # installments = rec.installment-1 or 1
                # rec.payment_date_end = rec.payment_date + relativedelta(months=installments)

    
    @api.onchange('state')
    def _onchange_payment_state(self):
        for rec in self:
            if rec.state != 'cancel':
                end_date = datetime.strptime(str(rec.payment_date_end), '%Y-%m-%d')
                for line in rec.loan_lines:
                    if line.date > end_date and not line.paid:
                        line.active = False
                    if line.date <= end_date and not line.paid:
                        line.active = True

    @api.model
    def create(self, values):
        rule = self.env['hr.salary.rule'].sudo().browse(values['rule_id'])
        loan_count = self.env['hr.payslip.deduction.loan'].search_count(
            [('employee_id', '=', values['employee_id']), ('state', '=', 'approve'),
             ('balance_amount', '!=', 0),('rule_id','=',values['rule_id'])])
        if loan_count:
            raise ValidationError(_("The employee has already a pending %s installment") % rule.name)
            
        seq = False
        if 'employee_id' not in values:
            emp_id = self.env.user.employee_id.id
        else:
            emp_id = values['employee_id']
        emp = self.env['hr.employee'].browse(emp_id)
        if emp:
            if emp.department_id and 'dept_code' in self.env['hr.department']._fields:
                if emp.department_id.dept_code:
                    seq = self.env['ir.sequence'].next_by_code_by_employee(rule.code,self._name,emp) or '/'
        if not seq:
            seq = self.env['ir.sequence'].next_by_code(self._name) or '/'
        values['name'] = seq
        
        res = super(HrPayslipDeductionLoan, self).create(values)
        return res
        
    def write(self, values):
        end_date = datetime.strptime(values['payment_date_end'],'%Y-%m-%d') if 'payment_date_end' in values else False
        state = values['state'] if 'state' in values else self.state
        ret = super(HrPayslipDeductionLoan, self).write(values)
        if end_date and state != 'cancel':
            for line in self.loan_lines:
                if line.date > end_date.date() and not line.paid:
                    line.active = False
                elif line.date <= end_date.date() and not line.paid:
                    line.active = True
        return ret
    

    def compute_installment(self):
        """This automatically create the installment the employee need to pay to
        company based on payment start date and the no of installments.
            """
        for loan in self:
            date_start = datetime.strptime(str(loan.payment_date), '%Y-%m-%d')
            if loan.loan_amount:
                amount = loan.loan_amount / loan.installment
            else:
                amount = loan.ma_amount
            lines = [(5,0)]
            for i in range(1, loan.installment + 1):
                lines.append((0,0,{
                    'date': date_start,
                    'amount': amount,
                    'employee_id': loan.employee_id.id,}))
                date_start = date_start + relativedelta(months=1)
            loan.loan_lines = lines
            loan._compute_loan_amount()
        return True

    def action_cancel(self):
        self.write({'state': 'cancel'})

    def action_confirm(self):
        for data in self:
            if not data.loan_lines:
                raise ValidationError(_("Please Compute installment"))
            else:
                self.write({'state': 'confirm'})

    def unlink(self):
        for loan in self:
            if loan.state not in ('draft', 'cancel'):
                raise UserError(
                    'You cannot delete a loan which is not in draft or cancelled state')
        return super(HrPayslipDeductionLoan, self).unlink()

class InstallmentLine(models.Model):
    _name = "hr.payslip.deduction.loan.line"
    _description = "Installment Line"
    _order = 'employee_id, date asc'

    date = fields.Date(string="Payroll Date", required=True, help="Date of the payment")
    employee_id = fields.Many2one('hr.employee', string="Employee", help="Employee")
    amount = fields.Float(string="Amount", required=True, help="Amount")
    paid = fields.Boolean(string="Paid", help="Paid")
    loan_id = fields.Many2one('hr.payslip.deduction.loan', string="Loan Ref.", help="Loan")
    payslip_id = fields.Many2one('hr.payslip', string="Payslip Ref.", help="Payslip")
    active = fields.Boolean(string='active', default=True)
    name = fields.Char(string='Name', related='loan_id.name')
    
class HrSalaryRegister(models.Model):
    _inherit = 'hr.salary.rule'
    
    register = fields.Char(string='Register', related='register_id.name')

class HrModifiedPagibigContribution(models.Model):
    _name = 'hr.payslip.deduction.mpc'
    _description = 'Payslip Deductions - Modified Pag-ibig Contribution'
    _order = 'date_start desc'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', required=True)
    name = fields.Char(string='Name', related='employee_id.name', store=True)
    rule_id = fields.Many2one(comodel_name='hr.salary.rule', string='Deduction Name', required=True,
    domain=[('register','=',('Modified Pag-ibig Contribution')),('active','=',True)])
    amount = fields.Float(string='Monthly Deduction', required=True)
    date_start = fields.Date(string='Start Date', required=True, default=lambda self: fields.datetime.now(), help='Must be equeal to Payslip Date From.')
    date_end = fields.Date(string='End Date', required=True, help='Must be equeal to Payslip Date To.')
    active = fields.Boolean(string='Active', default=True)
    code = fields.Char(string='Code', related='rule_id.code')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, help="Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)
    line_ids = fields.One2many('hr.payslip.deduction.contribution.line', 'mpc_id', string="MPC Line", index=True)
    
    total_amount = fields.Float(string='Total Amount', compute='_compute_amount')
    running_balance = fields.Float(string='Running Balance', compute='_compute_amount')
    deducted_amount = fields.Float(string='Deducted Amount', compute='_compute_amount')
    
    # payslip_ids = fields.One2many(
    #     string='Payslips',
    #     comodel_name='hr.payslip',
    #     inverse_name='employee_id',
    #     domain=[('state','=','done'),('employee_id','=',employee_id),('date_from','>=',date_start),('date_to','<=',date_end),('mpc_amount','>',0.0)])

    def _compute_amount(self):
        for rec in self:
            deducted_amount = 0.0
            for line in rec.line_ids:
                deducted_amount += line.amount
            date_end = rec.date_end if rec.date_end else fields.datetime.now().date()
            months = (date_end.year - rec.date_start.year) * 12 + (date_end.month - rec.date_start.month)
            total_amount = months*rec.amount
            rec.total_amount = total_amount
            rec.deducted_amount = deducted_amount
            rec.running_balance = total_amount - deducted_amount

class HrNHCPEACContribution(models.Model):
    _name = 'hr.payslip.deduction.eac'
    _description = 'Payslip Deductions - NHCP EAC Contribution'
    _order = 'date_start desc'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', required=True)
    name = fields.Char(string='Name', related='employee_id.name', store=True)
    rule_id = fields.Many2one(comodel_name='hr.salary.rule', string='Salary Rule', required=True,
    default=lambda self: self.env.ref('hr_ph_payslip.hr_salary_rule_nhcp_eac'), readonly=True)
    amount = fields.Float(string='Monthly Deduction', required=True)
    date_start = fields.Date(string='Start Date', required=True, default=lambda self: fields.datetime.now())
    date_end = fields.Date(string='End Date')
    active = fields.Boolean(string='Active', default=True)
    code = fields.Char(string='Code', related='rule_id.code')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, help="Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)
    line_ids = fields.One2many('hr.payslip.deduction.contribution.line', 'eac_id', string="EAC Line", index=True)
    
    total_amount = fields.Float(string='Total Amount', compute='_compute_amount')
    running_balance = fields.Float(string='Running Balance', compute='_compute_amount')
    deducted_amount = fields.Float(string='Deducted Amount', compute='_compute_amount')

    def _compute_amount(self):
        for rec in self:
            deducted_amount = 0.0
            for line in rec.line_ids:
                deducted_amount += line.amount
            date_end = rec.date_end if rec.date_end else fields.datetime.now().date()
            months = (date_end.year - rec.date_start.year) * 12 + (date_end.month - rec.date_start.month)
            total_amount = months*rec.amount
            rec.total_amount = total_amount
            rec.deducted_amount = deducted_amount
            rec.running_balance = total_amount - deducted_amount

class HrNHCPHWPContribution(models.Model):
    _name = 'hr.payslip.deduction.hwp'
    _description = 'Payslip Deductions - NHCP HWP Contribution'
    _order = 'date_start desc'

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', required=True)
    name = fields.Char(string='Name', related='employee_id.name', store=True)
    rule_id = fields.Many2one(comodel_name='hr.salary.rule', string='Deduction Name', required=True,
    default=lambda self: self.env.ref('hr_ph_payslip.hr_salary_rule_nhcp_hwp'), readonly=True)
    amount = fields.Float(string='Monthly Deduction', required=True)
    date_start = fields.Date(string='Start Date', required=True, default=lambda self: fields.datetime.now())
    date_end = fields.Date(string='End Date')
    active = fields.Boolean(string='Active', default=True)
    code = fields.Char(string='Code', related='rule_id.code')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, help="Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)
    line_ids = fields.One2many('hr.payslip.deduction.contribution.line', 'hwp_id', string="HWP Line", index=True)

    total_amount = fields.Float(string='Total Amount', compute='_compute_amount')
    running_balance = fields.Float(string='Running Balance', compute='_compute_amount')
    deducted_amount = fields.Float(string='Deducted Amount', compute='_compute_amount')

    def _compute_amount(self):
        for rec in self:
            deducted_amount = 0.0
            for line in rec.line_ids:
                deducted_amount += line.amount
            date_end = rec.date_end if rec.date_end else fields.datetime.now().date()
            months = (date_end.year - rec.date_start.year) * 12 + (date_end.month - rec.date_start.month)
            total_amount = months*rec.amount
            rec.total_amount = total_amount
            rec.deducted_amount = deducted_amount
            rec.running_balance = total_amount - deducted_amount

class HrPayslipDeductionContributionLine(models.Model):
    _name = "hr.payslip.deduction.contribution.line"
    _description = "Contributions Line"
    _order = 'employee_id, date asc'

    date = fields.Date(string="Payment Date", required=True, help="Date of the payment", default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,monthrange(fields.datetime.now().year,fields.datetime.now().month)[1])+relativedelta(months=1))
    employee_id = fields.Many2one('hr.employee', string="Employee", help="Employee")
    amount = fields.Monetary(string="Amount", required=True, help="Amount")
    mpc_id = fields.Many2one('hr.payslip.deduction.mpc', string="MPC Ref.", help="Modified Pag-ibig Contribution")
    eac_id = fields.Many2one('hr.payslip.deduction.eac', string="EAC Ref.", help="NHCP EAC Contribution")
    hwp_id = fields.Many2one('hr.payslip.deduction.hwp', string="HWP Ref.", help="NHCP HWP Contribution")
    payslip_id = fields.Many2one('hr.payslip', string="Payslip Ref.", help="Payslip")
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, help="Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)
    # payslip_input_id = fields.Many2one('hr.payslip.input', string="Payslip Input Ref.", help="Payslip Input")

class HrOtherAllowance(models.Model):
    _name = 'hr.payslip.other.allowance'
    _description = 'Payslip Allowance - Other Allowance'
    _order = 'date_start desc'

    contract_id = fields.Many2one(comodel_name='hr.contract', string='Contract', required=True, domain=[('active','=',True)])
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='contract_id.employee_id')
    name = fields.Char(string='Name', related='employee_id.name', store=True)
    rule_id = fields.Many2one(comodel_name='hr.salary.rule', string='Allowance Name', required=True,
    default=lambda self: self.env.ref('hr_ph_payslip.hr_salary_rule_other_allowance'), readonly=True)
    amount = fields.Float(string='Monthly Allowance', required=True)
    date_start = fields.Date(string='Start Date', required=True, default=lambda self: fields.datetime.now())
    date_end = fields.Date(string='End Date')
    active = fields.Boolean(string='Active', default=True)
    code = fields.Char(string='Code', related='rule_id.code')
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, help="Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)
    line_ids = fields.One2many('hr.payslip.other.allowance.line', 'other_allowance_id', string="Other Allowance Lines", index=True)

class HrOtherAllowanceLine(models.Model):
    _name = "hr.payslip.other.allowance.line"
    _description = "Payslip Allowance - Other Allowance Line"
    _order = 'employee_id, date asc'

    date = fields.Date(string="Payment Date", required=True, help="Date of the payment", default=lambda self: fields.datetime.now())
    employee_id = fields.Many2one('hr.employee', string="Employee", help="Employee")
    amount = fields.Monetary(string="Amount", required=True, help="Amount")
    other_allowance_id = fields.Many2one('hr.payslip.other.allowance', string="Other Allowance Ref.", help="Other Allowance Contribution")
    payslip_id = fields.Many2one('hr.payslip', string="Payslip Ref.", help="Payslip")
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, help="Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)


class HrPayslipInput(models.Model):
    _inherit = 'hr.payslip.input'
    
    ps_loan_id = fields.Many2one('hr.payslip.deduction.loan', string="Loan Installment", help="Loan installment", store=True,) # compute='_compute_contribution_id')
    ps_loan_line_id = fields.Many2one('hr.payslip.deduction.loan.line', string="Loan Installment Line", help="Loan installment", store=True,) # compute='_compute_contribution_id')
    mpc_line_id = fields.Many2one('hr.payslip.deduction.mpc', string="Modified Pagibig", help="Modified Pag-ibig Contribution", store=True,) # compute='_compute_contribution_id')
    nhcp_eac_id = fields.Many2one('hr.payslip.deduction.eac', string="Employee Association Contribution", help="NHCP Employee Association Contribution", store=True,) # compute='_compute_contribution_id')
    nhcp_hwp_id = fields.Many2one('hr.payslip.deduction.hwp', string="Health & Welfare Plan", help="NHCP Health & Welfare Plan", store=True,) # compute='_compute_contribution_id')
    other_allowance_id = fields.Many2one('hr.payslip.other.allowance', string="Other Allowance", help="Other Allowance for the Employee", store=True,) # compute='_compute_contribution_id')

    def _compute_contribution_id(self):
        for rec in self:
            if rec.amount > 0.0:
                amount = rec.amount
                code = rec.code
                date_start = rec.payslip_id.date_from
                date_end = rec.payslip_id.date_to
                employee_id = rec.payslip_id.employee_id.id
                skip = False

                if not rec.other_allowance_id:
                    found = self.env['hr.payslip.other.allowance'].search([('code','=',code),('employee_id', '=', employee_id), ('active', '=', True), ('date_start','<=', date_start),('amount','=',amount)], order='date_start desc, id desc')
                    for ff in found:
                        if ff.date_end:
                            if ff.date_end < date_end:
                                continue
                        rec.other_allowance_id = ff.id
                        skip = True
                        break

                if skip:
                    continue

                if not rec.mpc_line_id:
                    found = self.env['hr.payslip.deduction.mpc'].search([('code','=',code),('employee_id', '=', employee_id), ('active', '=', True), ('date_start','<=', date_start),('amount','=',amount)], order='date_start desc, id desc')
                    for ff in found:
                        if ff.date_end:
                            if ff.date_end < date_end:
                                continue
                        rec.mpc_line_id = ff.id
                        skip = True
                        break
                
                if skip:
                    continue

                if not rec.nhcp_eac_id:
                    found = self.env['hr.payslip.deduction.eac'].search([('code','=',code),('employee_id', '=', employee_id), ('active', '=', True), ('date_start','<=', date_start),('amount','=',amount)], order='date_end desc, id desc')
                    for ff in found:
                        if ff.date_end:
                            if ff.date_end < date_end:
                                continue
                        rec.nhcp_eac_id = ff.id
                    skip = True
                    break
                
                if skip:
                    continue

                if not rec.nhcp_hwp_id:
                    found = self.env['hr.payslip.deduction.hwp'].search([('code','=',code),('employee_id', '=', employee_id), ('active', '=', True), ('date_start','<=', date_start),('amount','=',amount)], order='date_start desc, id desc')
                    for ff in found:
                        if ff.date_end:
                            if ff.date_end < date_end:
                                continue
                        rec.nhcp_hwp_id = ff.id
                        skip = True
                        break
                
                if skip:
                    continue

                if not rec.ps_loan_id:
                    # found = self.env['hr.payslip.deduction.loan'].search([('code','=',code),('employee_id', '=', employee_id), ('state', '!=', 'cancel'), ('payment_date','<=', date_start), ('payment_date_end','>=', date_start)], order='payment_date desc, payment_date_end desc, id desc')
                    found = self.env['hr.payslip.deduction.loan'].search([('code','=',code),('employee_id', '=', employee_id), ('state', '=', 'confirm'),('payment_date','<=', date_start),('payment_date_end','>=',date_end)], order='payment_date desc, payment_date_end desc, id desc')
                    for ff in found:
                        for line in ff.loan_lines:
                            if date_start <= line.date <= date_end and not line.paid and line.active:
                            # if line.date == date_start and line.amount == amount:
                                rec.ps_loan_id = ff.id
                                rec.ps_loan_line_id = line.id
                                print('ps_loan_line_id', line.id)
                                skip = True
                                break
                        if skip:
                            break



    
class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    def action_payslip_done(self):
        ret = super(HrPayslip, self).action_payslip_done()
        for rec in self:
            employee_id = rec.employee_id.id
            date_from = rec.date_from
            date_to = rec.date_to
            payslip_id = rec.id

            data = {'payslip_id': payslip_id,
                'employee_id': employee_id,}

            for input in rec.input_line_ids:
                amount = input.amount
                if amount <= 0.0:
                    continue

                deduction_line = []
                data['amount'] = amount
                
                if input.ps_loan_line_id:
                    input.ps_loan_line_id.payslip_id = payslip_id
                    input.ps_loan_line_id.paid = True

                elif input.mpc_line_id:
                    found = input.mpc_line_id.line_ids.filtered(lambda r: r.payslip_id == payslip_id)
                    for res in found:
                        res.append((2,found.id))
                    deduction_line.append((0,0,data))
                    input.mpc_line_id.line_ids = deduction_line

                elif input.nhcp_eac_id:
                    found = input.nhcp_eac_id.line_ids.filtered(lambda r: r.payslip_id == payslip_id)
                    for res in found:
                        res.append((2,found.id))
                    deduction_line.append((0,0,data))
                    input.nhcp_eac_id.line_ids = deduction_line

                elif input.nhcp_hwp_id:
                    found = input.nhcp_hwp_id.line_ids.filtered(lambda r: r.payslip_id == payslip_id)
                    for res in found:
                        res.append((2,found.id))
                    deduction_line.append((0,0,data))
                    input.nhcp_hwp_id.line_ids = deduction_line

                elif input.other_allowance_id:
                    found = input.other_allowance_id.line_ids.filtered(lambda r: r.payslip_id == payslip_id)
                    for res in found:
                        res.append((2,found.id))
                    deduction_line.append((0,0,data))
                    input.other_allowance_id.line_ids = deduction_line
        
        return ret
        
        for recd in self.overtime_ids:
            if recd.type == 'cash':
                recd.payslip_paid = True
        return super(PayslipOverTime, self).action_payslip_done()

    def fill_auto_inputs_lines(self):
        for rec in self:
            employee_id = rec.employee_id.id
            date_from = rec.date_from
            date_to = rec.date_to
            payslip_id = rec.id
            for inputs in rec.input_line_ids:
                amount = inputs.amount
                if amount <= 0.0:
                    continue
                data = {
                        'date': date_from,
                        'employee_id': employee_id,
                        'amount': amount, 
                        'payslip_id': payslip_id}
                if inputs.ps_loan_line_id:
                    print('ps_loan_line_id', inputs.ps_loan_line_id)
                    for term in inputs.line_ids:
                        if term.date == date_from:
                            term.paid = True
                    # inputs.ps_loan_line_id.loan_id._compute_loan_amount()
                if inputs.mpc_line_id:
                    print('mpc_line_id', inputs.mpc_line_id)
                    # data['mpc_id'] = inputs.mpc_line_id.id
                    inputs.mpc_line_id.line_ids = [(0,0,data)]
                if inputs.nhcp_eac_id:
                    print('nhcp_eac_id', inputs.nnhcp_eac_line_id)
                    # data['eac_id'] = inputs.nhcp_eac_id.id
                    inputs.nhcp_eac_id.line_ids = [(0,0,data)]
                if inputs.nhcp_hwp_id:
                    print('nhcp_hwp_id', inputs.nhcp_hwp_line_id)
                    # data['hwp_id'] = inputs.nhcp_hwp_id.id
                    inputs.nhcp_hwp_id.line_ids = [(0,0,data)]
                if inputs.other_allowance_id:
                    print('other_allowance_id', inputs.other_allowance_id)
                    # data['other_allowance_id'] = inputs.other_allowance_id.id
                    inputs.other_allowance_id.line_ids = [(0,0,data)]


    def get_inputs(self, contract_ids, date_from, date_to):
        """This Compute the other inputs to employee payslip.
                           """
        print('get_inputs')
        inputs = super(HrPayslip, self).get_inputs(contract_ids, date_from, date_to)
        contract_obj = self.env['hr.contract']
        emp_id = contract_obj.browse(contract_ids[0].id).employee_id
        
        for n in range(0,len(inputs)-1):
            code = inputs[n].get('code')

            sdiff_obj = self.env['hr.payslip.adjustment.sdiff'].search([('code','=',code),('employee_id', '=', emp_id.id), ('active', '=', True), ('date_from','=', date_from), ('date_to','=', date_to)])
            if sdiff_obj:
                print('sdiff_obj', sdiff_obj)
            for sdiff in sdiff_obj:
                if not sdiff.payslip_id:
                    sdiff.payslip_id = self.id
                    inputs[n]['amount'] = sdiff.amount
                    inputs[n]['sdiff_line_id'] = sdiff.id

            other_allowance_obj = self.env['hr.payslip.other.allowance'].search([('code','=',code),('contract_id', '=', contract_obj.id), ('active', '=', True),('date_start','<=', date_from)])
            if other_allowance_obj:
                print('other_allowance_obj', other_allowance_obj)
            for oallowance in other_allowance_obj:
                if oallowance.date_end:
                    if oallowance.date_end < date_to:
                        continue
                inputs[n]['amount'] = oallowance.amount
                inputs[n]['other_allowance_id'] = oallowance.id
            
            mpc_obj = self.env['hr.payslip.deduction.mpc'].search([('code','=',code),('employee_id', '=', emp_id.id), ('active', '=', True),('date_start','<=', date_from)])
            if mpc_obj:
                print('mpc_obj', mpc_obj)
            for mpc in mpc_obj:
                if mpc.date_end:
                    if mpc.date_end < date_to:
                        continue
                inputs[n]['amount'] = mpc.amount
                inputs[n]['mpc_line_id'] = mpc.id
                        
            eac_obj = self.env['hr.payslip.deduction.eac'].search([('code','=',code),('employee_id', '=', emp_id.id), ('active', '=', True),('date_start','<=', date_from)])
            if eac_obj:
                print('eac_obj', eac_obj)
            for eac in eac_obj:
                if eac.date_end:
                    if eac.date_end < date_to:
                        continue
                inputs[n]['amount'] = eac.amount
                inputs[n]['nhcp_eac_id'] = eac.id
                        
            hwp_obj = self.env['hr.payslip.deduction.hwp'].search([('code','=',code),('employee_id', '=', emp_id.id), ('active', '=', True),('date_start','<=', date_from)])
            if hwp_obj:
                print('hwp_obj', hwp_obj)
            for hwp in hwp_obj:
                if hwp.date_end:
                    if hwp.date_end < date_to:
                        continue
                inputs[n]['amount'] = hwp.amount
                inputs[n]['hwp_line_id'] = hwp.id
                        
            loan_obj = self.env['hr.payslip.deduction.loan'].search([('code','=',code),('employee_id', '=', emp_id.id), ('state', '=', 'confirm'),('payment_date','<=', date_from),('payment_date_end','>=',date_from)])
            if loan_obj:
                print('loan_obj', loan_obj)
            for loan in loan_obj:
                for loan_line in loan.loan_lines:
                    if date_from <= loan_line.date <= date_to and not loan_line.paid and loan_line.active:
                        inputs[n]['amount'] = loan_line.amount
                        inputs[n]['ps_loan_line_id'] = loan_line.id
                        inputs[n]['ps_loan_id'] = loan.id
        
        print('inputs_inputs', inputs)
        return inputs
