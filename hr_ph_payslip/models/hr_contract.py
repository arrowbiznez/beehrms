# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class HrContract(models.Model):
    _inherit = 'hr.contract'
    
    cola = fields.Monetary('COLA')
    allowance = fields.Monetary('PERA')
    # other_allowance = fields.Monetary('Other Allowance')
    
    
    def get_phhealth_contribution(self):
            premium = 2.75 
            year = self.date_from.year()
            
            if year == 2020:
                premium = 3.0
            elif year == 2021:
                premium = 3.5 
            elif year == 2022:
                premium = 4.0    
            elif year == 2023:
                premium = 4.5
            elif year >= 2024 and year <= 2025:
                premium = 5.0 
            
            return (self.wage*(premium/100))/2
            
    def  get_hdmf_contribution(self):
            # HMDF Employee
            premium = 1 
            if self.wage > 1500:
                premium = 2.0 
                
            return (self.wage*(premium/100))/2
            
    def  get_gsis_contribution(self):
            # HMDF Employee
            premium = 9 
            return (self.wage*(premium/100))/2
    
    def get_wtax_contribution(self):
            # HMDF Employee
            deductions = self.get_absences+self.get_gsis_contribution()+self.get_hdmf_contribution()+self.get_phhealth_contribution()
            wtax = 0.0
            base_wage = 0.00
            wage = self.wage-deductions
            if wage > 20833 and wage <= 33332:
                base_wage = 20833
                wtax = 20.0 if self.date_from < '2023-01-01' else 15.0
            elif wage > 33333 and wage <= 66666:
                base_wage = 33333
                wtax = 25.0  if self.date_from < '2023-01-01' else 20.0
            elif wage > 66667 and wage <= 166666:
                base_wage = 0.00
                wtax = 30.0  if self.date_from < '2023-01-01' else 25.0
            elif wage > 166666 and wage <= 666666:
                base_wage = 66667
                wtax = 32.0 if self.date_from < '2023-01-01' else 30.0
            elif wage > 666667:
                base_wage = 666667
                wtax = 35.0 
                
            if wtax > 0.0: 
                return (wage-base_wage)*(wtax/100)
            else:
                return 0.0

    