# -*- coding: utf-8 -*-

from ..report import hr_payslip_bank_report
from . import hr_contract, hr_payslip_employee, hr_payslip, hr_adjustments, hr_deductions, hr_payslip_witholding_tax, hr_payslip_matrix