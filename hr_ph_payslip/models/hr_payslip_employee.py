from odoo import api, fields, models, _

from datetime import datetime

class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'

    emp_count = fields.Integer(string="Count")
    

    # Fill in Employee Payslip
    @api.model
    def default_get(self, fields):
        rec = super(HrPayslipEmployees, self).default_get(fields)
        context = self._context
        active_id = self.env.context.get('active_id')

        if 'company_id' in context:
            company_id = context['company_id']
        else:
            company_id = ''

        if context.get('date_start'):
            date_start = context['date_start']
        else:
            date_start = datetime.strftime(datetime.now(),"%Y-%m-%d")

        if 'date_end' in context:
            date_end = context['date_end']
        else:
            date_end = ''

        # print contract_ids
        company = self.env['res.company'].search([('partner_id', '=', company_id)], limit=1)
        contracts = self.env['hr.contract'].search([('state','=','open'),'|',('date_start','<=',date_start),('date_end','>',date_start),('date_end','=',False)])
        emp_ids = []
        count = 0
        for contract in contracts:
            count += 1
            emp_ids.append(contract.employee_id.id)
            
        # employee_ids = self.env['hr.employee'].search([('company_id', '=', company.id)])

        # emp_ids = []
        # count = 0
        # for rec_emp in employee_ids:
        #     payslip_id = self.env['hr.payslip'].search([('employee_id', '=', rec_emp.id),
        #                                                 ('date_from', '=', date_start),
        #                                                 ('date_to', '=', date_end),
        #                                                 ('state', '=', 'draft')])
        #     if not payslip_id:
        #         contract = self.env['hr.contract']
        #         clause_1 = ['&', ('date_end', '<=', date_end), ('date_end', '>=', date_start)]
        #         clause_2 = ['&', ('date_start', '<=', date_end), ('date_start', '>=', date_start)]
        #         clause_3 = ['&', ('date_start', '<=', date_start), '|', ('date_end', '=', False),
        #                     ('date_end', '>=', date_end)]
        #         clause_final = [('employee_id', '=', rec_emp.id), '|',
        #                         '|'] + clause_1 + clause_2 + clause_3
        #         cont_res = contract.search(clause_final, limit=1, order='date_start desc')
        #         previous_contract = self.env['hr.contract'].search([('state', '=', 'close'),
        #                                                             ('employee_id', '=', rec_emp.id)],
        #                                                            order='date_start desc', limit=1)
        #         if cont_res:
        #             if rec_emp.id not in emp_ids:
        #                 emp_ids.append(rec_emp.id)
        #         if not cont_res and previous_contract:
        #             if rec_emp.id not in emp_ids:
        #                 emp_ids.append(rec_emp.id)

        #   count += 1
        rec['emp_count'] = count
        rec['employee_ids'] = emp_ids

        return rec