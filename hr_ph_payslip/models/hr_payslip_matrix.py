from odoo import api, fields, models, _
from datetime import datetime, date
from calendar import month, monthrange
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError, UserError

class HrModifiedPagibigContribution(models.Model):
    _name = 'hr.payslip.manded'
    _description = 'Government Mandatory Deductions'
    _order = 'date_start desc'
    
    name = fields.Char(string='Name', store=True, required=True)
    # rule_id = fields.Many2one(comodel_name='hr.salary.rule', string='Deduction Name', required=True,
    # domain=[('register','=',('Modified Pag-ibig Contribution')),('active','=',True)])
    contribution = fields.Selection([
        ('philhealth', 'PhilHealth Contribution'),
        ('hdmf', 'HDMF Contribution'),
        ('gsis', 'GSIS Contribution')
    ], string='Contribution Name', required=True)
    ee_code = fields.Char(string='Rule Code (Employee)', required=True)
    er_code = fields.Char(string='Rule Code (Employer)', required=True)
    
    date_start = fields.Date(string='Start Date', required=True, default=lambda self: fields.datetime.now())
    date_end = fields.Date(string='End Date', required=False)
    matrix_ids = fields.One2many('hr.payslip.manded.matrix', 'manded_id', string='Matrix')
    active = fields.Boolean(string='Active', default=True)
    
class HrModifiedPagibigContributionMatrix(models.Model):
    _name = 'hr.payslip.manded.matrix'
    _description = 'Payslip Deductions - Modified Pag-ibig Contribution'
    _order = 'sequence desc'

    manded_id = fields.Many2one('hr.payslip.manded', string='Mandatory Contribution')
    sequence = fields.Integer('sequence')
    name = fields.Char(string='Name', store=True, related='manded_id.name')
    contribution = fields.Selection([
        ('philhealth', 'PhilHealth Contribution'),
        ('hdmf', 'HDMF Contribution'),
        ('gsis', 'GSIS Contribution')
    ], string='Contribution Name', related='manded_id.contribution')
    date_start = fields.Date(string='Start Date', required=True, related="manded_id.date_start")
    range_min = fields.Float(string='Minimum Range', required=True)
    range_max = fields.Float(string='Maximum Range', required=True)
    
    is_fix_amount = fields.Boolean('Fix Amount?', default=True)
    
    ee_code = fields.Char(string='Rule Code (Employee)', related="manded_id.ee_code")
    er_code = fields.Char(string='Rule Code (Employer)', related="manded_id.er_code")
    
    ee_amount = fields.Monetary(string='Employee Share', required=False)
    er_amount = fields.Monetary(string='Employer Share', required=False)
    
    ee_wage_percentage = fields.Float(string='Employee % Share', required=False)
    er_wage_percentage = fields.Float(string='Employer % Share', required=False)
    
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, help="Currency",
                                  default=lambda self: self.env.user.company_id.currency_id)
    active = fields.Boolean(string='Active', related="manded_id.active")