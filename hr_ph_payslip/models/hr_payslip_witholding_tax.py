from odoo import api, fields, models, tools, _
from datetime import datetime, date
from calendar import month, monthrange
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError, UserError
from odoo.tools.safe_eval import safe_eval

steps = [
    ('1', 'Step 1'), ('2', 'Step 2'),
    ('3', 'Step 3'), ('4', 'Step 4'),
    ('5', 'Step 5'), ('6', 'Step 6'),
    ('7', 'Step 7'), ('8', 'Step 8')]

class HrPayslipTaxTableBase(models.Model):
    _name = 'hr.payslip.tax.table.base'
    _description = 'Payroll Tax Table Base'
    _order = 'date_start desc, date_end asc'
    
    
    name = fields.Char(string='Number')
    date_start = fields.Date(string='Date From', required=True, default=lambda self: date(fields.datetime.now().year,1,1))
    date_end = fields.Date(string='Date To', required=False, default=False)
    allowable_exemption = fields.Float(string='Allowable Exemption', default=0.00)
    tax_table_ids = fields.One2many(comodel_name='hr.payslip.tax.table', inverse_name='tax_table_id', string='Tax Table Line')
        
    @api.model
    def create(self, values):
        values['name'] = self.env['ir.sequence'].next_by_code(self._name) or '/'
        return super(HrPayslipTaxTableBase, self).create(values)
    
    def action_generate_employee_whtax(self):
        self.env['hr.payslip.witholding.tax'].cron_fillup_whtax()
    
    def action_regenerate_employee_whtax(self):
        self.env['hr.payslip.witholding.tax'].cron_fillup_whtax(regenerate=True)
        
class HrPayslipTaxTable(models.Model):
    _name = 'hr.payslip.tax.table'
    _description = 'Payroll Tax Table'
    _order = 'net_taxable_income_from asc, net_taxable_income_to asc'
    
    tax_table_id = fields.Many2one(comodel_name='hr.payslip.tax.table.base', string='Tax Table')
    date_start = fields.Date(string='Date From', required=True, related='tax_table_id.date_start')
    date_end = fields.Date(string='Date To', required=False, related='tax_table_id.date_end')
    net_taxable_income_from = fields.Float(string='Net Taxable Income From',default=1.00)
    net_taxable_income_to = fields.Float(string='Net Taxable Income To', default=9999999999.99)
    tax_for_the_first = fields.Float(string='Tax Base for the First')
    added_tax = fields.Float(string='Tax for the First')
    excess_percentage = fields.Float(string='Excess Percentage (%)')

class HrPayslipWitholdingTax(models.Model):
    _name = 'hr.payslip.witholding.tax'
    _description = 'Witholding Tax'
    _order = 'tax_year desc, name asc'
    
    name = fields.Char(string='Number', default=False)
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    employee_name = fields.Char(string='Employee Name', related='employee_id.name', store=True)
    tax_year = fields.Char(string='Year', default=lambda self: fields.datetime.now().year)
    line_ids = fields.One2many(comodel_name='hr.payslip.witholding.tax.line', inverse_name='whtax_id', string='Monthly Witholding Tax')
    
    tax_table_line_id = fields.Many2one('hr.payslip.tax.table', string='Tax Table Line', compute='_compute_monthly_taxes')
    month_no = fields.Integer(string='#', compute='_compute_monthly_taxes')
    month = fields.Char(string='Month', compute='_compute_monthly_taxes')
    basic_pays = fields.Monetary(string='Basic Pays', compute='_compute_monthly_taxes')
    others = fields.Monetary(string='Others', compute='_compute_monthly_taxes')
    allowable_exemption = fields.Monetary(string='Allowable Exemption', compute='_compute_monthly_taxes')
    others_total = fields.Monetary(string='Others Total', compute='_compute_monthly_taxes') 
    gross_taxable_income = fields.Monetary(string='Gross Taxable Income', compute='_compute_monthly_taxes')
    mandatory_deduction = fields.Monetary(string='Mandatory Deduction', compute='_compute_monthly_taxes')
    net_taxable_income = fields.Monetary(string='Net Taxable Income', compute='_compute_monthly_taxes')
    tax_for_the_first = fields.Monetary(string='Tax Base for the First', compute='_compute_monthly_taxes') 
    excess = fields.Monetary(string='Tax Base for the Excess', compute='_compute_monthly_taxes') 
    excess_percent = fields.Float(string='%', compute='_compute_monthly_taxes') 
    excess_percent_amount = fields.Monetary(string='Tax for the Excess', compute='_compute_monthly_taxes')
    added_tax = fields.Monetary(string='Tax for the First', compute='_compute_monthly_taxes')
    total_tax_due = fields.Monetary(string='Total Tax Due', compute='_compute_monthly_taxes')
    taxes_witheld = fields.Monetary(string='Taxes Witheld', compute='_compute_monthly_taxes')
    remaining_tax_due = fields.Monetary(string='Remaining Tax Due', compute='_compute_monthly_taxes')
    monthly_witholding_tax = fields.Monetary(string='Monthly Witholding Tax', compute='_compute_monthly_taxes')
    currency_id = fields.Many2one('res.currency', string='Currency',
                                required=True, readonly=True,
                                default=lambda
                                    self: self.env.company.currency_id.id)
    state = fields.Selection(string='Status', selection=[('open', 'Open'), ('lock', 'Locked'),], default="open")
    active = fields.Boolean(string='Active', default=True)
    
    def action_open(self):
        for rec in self:
            rec.state = 'open'
            
    def action_lock(self):
        for rec in self:
            rec.state = 'lock'
    
    def _cron_compute_whtax_held(self):
        year = datetime.now().year
        for res in self.search([('tax_year','=',year),('active','=',True)],order='employee_id, tax_year'):
            self.compute_whtax_held_employee(res.employee_id.id, year)
            
    def action_compute_whtax_held(self):
        for rec in self:
            tax_year = rec.tax_year
            self.compute_whtax_held_employee(rec.employee_id.id, rec.tax_year)    
            
    def compute_whtax_held_employee(self, employee_id, year, month=False, regenerate=False):
        print('xxx:',employee_id, year, month, regenerate)
        current_month = 1
        if not regenerate:
            current_month = month if month else datetime.now().month
            for line in self.line_ids:
                contract_id = line.contract_id.id
                self.cron_fillup_whtax(contract_id=contract_id)
                break
        for res in self.search([('employee_id','=',employee_id),('tax_year','=',year),('active','=',True)], order='employee_id, tax_year'):
            if res.state == 'lock' and not regenerate:
                continue
            create_date = res.create_date or datetime.now().date
            basic_pays = 0.0
            others = 0.0
            mandatory_deduction = 0.0
            tax_base = self.env['hr.payslip.tax.table.base'].sudo().search([('date_start','<=',create_date)], order="date_start desc", limit=1)
            allowable_exemption = tax_base.allowable_exemption or 90000
            for line in res.line_ids.sorted(key=lambda r: r.month_no):
                basic_pays += (line.basic_salary+line.salary_diff+line.overtime_pay+line.hazard_pay)
                others += (line.midyr_bonus+line.yrend_bonus+line.cash_gift+line.sri+line.cna)
                mandatory_deduction += (line.gsis+line.hdmf+line.philhealth)
            others_total = others-allowable_exemption if others > allowable_exemption else 0.00
            gross_taxable_income = basic_pays+others_total # others+allowable_exemption+others_total
            net_taxable_income = gross_taxable_income - mandatory_deduction
            
            tax_for_the_first = 0.0
            added_tax = 0.0
            excess_percent = 0.0
            
            # Tax table
            tax_table = self.env['hr.payslip.tax.table'].sudo().search([('tax_table_id','=',tax_base.id),('date_start','<=',create_date),('net_taxable_income_from','<=',net_taxable_income),('net_taxable_income_to','>=',net_taxable_income)], order="id desc, net_taxable_income_from asc, net_taxable_income_to asc")
            if tax_table:
                for table in tax_table:
                    if net_taxable_income >= table.net_taxable_income_from and (net_taxable_income < table.net_taxable_income_to or not table.net_taxable_income_to):
                        tax_for_the_first = table.tax_for_the_first
                        added_tax = table.added_tax
                        excess_percent = table.excess_percentage/100
                        break
                    
            excess = net_taxable_income-tax_for_the_first
            excess_percent_amount = excess*excess_percent
            excess_percent = excess_percent*100
            total_tax_due = excess_percent_amount+added_tax 
            remaining_tax_due = total_tax_due
            whtax_held = 0.0
            
            for line in res.line_ids.sorted(key=lambda r: r.month_no):
                remaining_tax_due = total_tax_due-whtax_held
                if line.whtax <= 0.0 or line.month_no >= current_month:
                    line.whtax_held = whtax_held
                    line.whtax = remaining_tax_due/(12-line.month_no) if (12-line.month_no) > 0 else 0.00
                    print('whtax:',line.month_no, line.whtax)
                whtax_held += line.whtax
    
    def _compute_monthly_taxes(self):
        for rec in self:
            month_no = 12
            if datetime.now().year == int(rec.tax_year):
                month_no = datetime.now().month
            rec.month_no = month_no
            create_date = rec.create_date
            whtax_held = 0.0
            for line in rec.line_ids:
                if line.month_no == month_no:
                    whtax_held = line.whtax_held
                    rec.month = line.month
                rec.basic_pays += line.basic_salary+line.salary_diff+line.overtime_pay+line.hazard_pay
                rec.others += line.midyr_bonus+line.yrend_bonus+line.cash_gift+line.sri+line.cna
                rec.mandatory_deduction += line.gsis+line.hdmf+line.philhealth
                
            tax_base = self.env['hr.payslip.tax.table.base'].sudo().search([('date_start','<=',create_date)], order="date_start desc", limit=1)
            rec.allowable_exemption = tax_base.allowable_exemption or 90000 # Fix
            rec.others_total = rec.others-rec.allowable_exemption if rec.others > rec.allowable_exemption else 0.00
            rec.gross_taxable_income = rec.basic_pays+rec.others_total #rec.others+rec.allowable_exemption+rec.others_total
            rec.net_taxable_income = rec.gross_taxable_income - rec.mandatory_deduction
            
            # Tax table
            net_taxable_income = rec.net_taxable_income
            # tax_table = self.env['hr.payslip.tax.table'].sudo().search([('tax_table_id','=',tax_base.id),('net_taxable_income_from','<=',net_taxable_income),('net_taxable_income_to','>=',net_taxable_income)], order="date_start desc, date_end desc, net_taxable_income_from asc, net_taxable_income_to asc")
            tax_table = self.env['hr.payslip.tax.table'].sudo().search([('tax_table_id','=',tax_base.id),('net_taxable_income_from','<=',net_taxable_income),('net_taxable_income_to','>=',net_taxable_income)], order="date_start desc, date_end desc, net_taxable_income_from asc, net_taxable_income_to asc", limit=1)
            if tax_table:
                rec.tax_for_the_first = tax_table.tax_for_the_first
                rec.added_tax = tax_table.added_tax
                rec.excess_percent = tax_table.excess_percentage/100
                rec.tax_table_line_id = tax_table.id 
                # for table in tax_table:
                #     if net_taxable_income >= table.net_taxable_income_from and (net_taxable_income < table.net_taxable_income_to or not table.net_taxable_income_to):
                #         rec.tax_for_the_first = table.tax_for_the_first
                #         rec.added_tax = table.added_tax
                #         rec.excess_percent = table.excess_percentage/100
                #         rec.tax_table_line_id = tax_table.id 
                #         break
            else:    
                if rec.net_taxable_income >= 250000:
                    if rec.net_taxable_income >= 250000 and rec.net_taxable_income < 400000:
                        rec.tax_for_the_first = 250000
                        rec.added_tax = 0.00
                        rec.excess_percent = 0.20
                    elif rec.net_taxable_income >= 400000 and rec.net_taxable_income < 800000:
                        rec.tax_for_the_first = 400000
                        rec.added_tax = 30000
                        rec.excess_percent = 0.25
                    elif rec.net_taxable_income >= 800000 and rec.net_taxable_income < 2000000:
                        rec.tax_for_the_first = 800000
                        rec.added_tax = 130000
                        rec.excess_percent = 0.30
                    if rec.net_taxable_income >= 2000000 and rec.net_taxable_income < 8000000:
                        rec.tax_for_the_first = 2000000
                        rec.added_tax = 490000
                        rec.excess_percent = 0.32
                    if rec.net_taxable_income >= 8000000:
                        rec.tax_for_the_first = 8000000
                        rec.added_tax = 2410000
                        rec.excess_percent = 0.35
                    
            rec.excess = rec.net_taxable_income-rec.tax_for_the_first
            rec.excess_percent_amount = rec.excess*rec.excess_percent
            rec.total_tax_due = rec.excess_percent_amount+rec.added_tax 
            rec.taxes_witheld = whtax_held
            rec.remaining_tax_due = rec.total_tax_due-rec.taxes_witheld
            rec.monthly_witholding_tax =  rec.remaining_tax_due/(12-month_no) if (12-month_no) > 0 else 0.00
           
    @api.model
    def create(self, values):
        values['name'] = self.env['ir.sequence'].next_by_code(self._name) or '/'
        return super(HrPayslipWitholdingTax, self).create(values)
    
    # Frequency/Timing of execution should be determine
    def cron_fillup_whtax(self, year = datetime.now().year, contract_id=False, regenerate=False):
        
        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            localdict['categories'].dict[category.code] = category.code in localdict['categories'].dict and \
                                                          localdict['categories'].dict[category.code] + amount or amount
            return localdict
            
        class BrowsableObject(object):
            def __init__(self, employee_id, dict, env):
                self.employee_id = employee_id
                self.dict = dict
                self.env = env

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0


        print('contract_id', contract_id)
        year = datetime.now().year if not year else year
        date_filter = date(year,1,1)
        date_begin = date(year,1,31)
        date_end = date(year,12,31)
        
        if contract_id:
            contracts = self.env['hr.contract'].browse(contract_id)
        else:
            contracts = self.env['hr.contract'].search(['|',('state','=','open'),('date_end','>=',date_filter)],order='write_date desc, employee_id, date_start')
        
        for contract in contracts:
            print('contract', contract)
            if contract.state != 'open' or contract.date_start > date_end:
                continue

            contract_id = contract.id
            employee_id = contract.employee_id.id
            wage = contract.wage
            salary_diff = 0.0
            if contract.date_start > date_begin:
                month = contract.date_start.month
                exists = self.env['hr.payslip.witholding.tax.line'].sudo().search([('employee_id','=',employee_id),('tax_year','=',year),('month_no','=',month),('active','=',True)],order="date_to desc",limit=1)
                if exists:
                    salary_diff = wage - exists.basic_salary
            
            grade = contract.job_grade.id
            salary_grade = contract.salary_grade.id
            step = contract.salary_step
            tax_lines = []
            whtax = self.search([('employee_id','=',employee_id),('tax_year','=',(year)),('active','=',True)])
            
            # start at current month
            month_start = datetime.now().month if whtax and not regenerate else 1
            if whtax and not regenerate:
                for line in whtax:
                    if line.month_no != 1:
                        month_start = 1 
                        break
                    
            # start at contract month
            month_start = contract.date_start.month if contract.date_start >= date_begin else month_start
            month_end = contract.date_end.month+1 if contract.date_end else 13
            
            ### From Salary Rule Start ####
            mancon = {'GSISEE': False, 'HDMFEE': False, 'PHEE': False}
            rules_dict = {}
            rules = BrowsableObject(employee_id, rules_dict, self.env)
            baselocaldict = {'rules': rules}
            
            structure_id = self.env.ref('hr_ph_payslip.whtax_mandatory_configuraton_base', False)
            employee = contract.employee_id
            localdict = dict(baselocaldict, employee=employee, contract=contract)
            for rule in structure_id.rule_ids:
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                localdict['result_rate'] = 100
                if rule._satisfy_condition(localdict) and rule.id:
                    amount, qty, rate = rule._compute_rule(localdict)
                    # check if there is already a rule computed with that code
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    # set/overwrite the amount computed for this rule in the localdict
                    tot_rule = amount * qty * rate / 100.0
                    mancon[rule.code] = tot_rule
                    
            #Base on new Matrix   
            for key in mancon.keys():
                found = self.env['hr.payslip.manded.matrix'].sudo().search([('ee_code','=',key),('date_start','<=',date_begin),('range_min','<=',wage),('range_max','>=',wage),('active','=',True)], order='date_start desc', limit=1)
                if found:
                    if found.is_fix_amount:
                        mancon[key] = found.ee_amount
                    else:
                        mancon[key] = wage*found.ee_wage_percentage
                    
            ### From Salary Rule End ####
            for month in range(month_start,13):
                date_from = date(year,month,1)
                date_to = date(year,month,monthrange(year,month)[1])
                overtime_pay = 0.0
                hazard_pay = 0.0
                gsis = mancon['GSISEE'] # or (wage*0.09)
                hdmf = mancon['HDMFEE'] # or 100
                philhealth = mancon['PHEE'] # or (wage*0.02 if (wage*0.02)<1600 else 1600)
                
                # #Base on new Matrix   
                # manded = mancon
                # for key in manded.keys():
                #     found = self.env['hr.payslip.manded.matrix'].sudo().search([('ee_code','=',key),('date_start','<=',date_begin),('active','=',True)], order='date_start desc', limit=1)
                #     if found:
                #         if found.is_fix_amount:
                #             manded[key] = found.ee_amount
                #         else:
                #             manded[key] = wage*found.ee_wage_percentage
                           
                # gsis = manded['GSISEE']
                # hdmf = manded['HDMFEE']
                # philhealth = manded['PHEE'] 
                
                # saldiff = self.env['hr.payslip.adjustment.sdiff'].sudo().search([('employee_id','=',employee_id),('date_start','=',date_from),('date_end','=',date_to),('active','=',True)])
                # salary_diff = saldiff.amount if saldiff else 0.00
                
                line_data = {
                        'contract_id': contract_id,
                        'employee_id': employee_id,
                        'date_from': date_from,
                        'date_to': date_to,
                        'tax_year': year,
                        'grade': grade,
                        'salary_grade': salary_grade,
                        'step': step,
                        'basic_salary': wage,
                        'salary_diff': salary_diff,
                        'whtax': 0.00,
                        'whtax_held': 0.00,
                        'sri': 0.00,
                        'cna': 0.00,
                        'overtime_pay': overtime_pay,
                        'hazard_pay': hazard_pay,
                        'midyr_bonus': 0.00,
                        'yrend_bonus': 0.00,
                        'cash_gift': 0.00,
                        'gsis': gsis,
                        'hdmf': hdmf,
                        'philhealth': philhealth,
                    }
                    
                if month == 5:
                    line_data['midyr_bonus'] = wage
                elif month == 11:
                    line_data['yrend_bonus'] = wage
                elif month == 12:
                    line_data['cash_gift'] = 5000
                    line_data['cna'] = 25000
                
                if not whtax:
                    if month <= month_end:
                        tax_lines.append((0,0,line_data))
                else:
                    found_taxline = False
                    for tax_line in whtax.line_ids.filtered(lambda r: r.date_from == date_from and r.date_to == date_to):
                        # Check if exists
                        if tax_line.month_no > month_end:
                                tax_line.sudo().unlink()
                        if not tax_line.locked or regenerate:
                            if tax_line.salary_grade.id != salary_grade:
                                # update the tax line.
                                tax_lines.append((1,tax_line.id,line_data))
                            else: 
                                if tax_line.gsis != gsis:
                                    tax_line.gsis = gsis
                                if tax_line.hdmf != hdmf:
                                    tax_line.hdmf = hdmf
                                if tax_line.philhealth != philhealth:
                                    tax_line.philhealth = philhealth
                        found_taxline = True
                    if not found_taxline:
                        tax_lines.append((0,0,line_data))
                salary_diff = 0.0
            
            if not whtax:
                self.create( {
                'employee_id': employee_id,
                'tax_year': year,
                'line_ids': tax_lines,
                })
            else:
                if tax_lines:
                    whtax.line_ids = tax_lines
            self.env.cr.commit()
            
            if regenerate:
                self.compute_whtax_held_employee(employee_id, year, regenerate=regenerate)
                print('compute_whtax_held_employee:', employee_id, year, regenerate)
                self.env.cr.commit()
            
                   
    
class HrPayslipWitholdingLine(models.Model):
    _name = 'hr.payslip.witholding.tax.line'
    _description = 'Witholding Tax Line'
    _order = 'date_from desc, date_to desc, name asc'
    
    whtax_id = fields.Many2one(comodel_name='hr.payslip.witholding.tax', string='Witholding Tax', domain=[('active','=',True)])
    contract_id = fields.Many2one(comodel_name='hr.contract', string='Contract', required=True)
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    name = fields.Char(string='Name', related='employee_id.name', store=True)
    date_from = fields.Date(string='Date From', required=True, 
        default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,1), 
        states={'lock': [('readonly', True)]})
    date_to = fields.Date(string='Date To', required=True, 
        default=lambda self: date(fields.datetime.now().year,fields.datetime.now().month,monthrange(fields.datetime.now().year,fields.datetime.now().month)[1]), 
        states={'lock': [('readonly', True)]})
    tax_year = fields.Integer(string='Year', default=lambda self: self.datetime.now().date, states={'lock': [('readonly', True)]})
    month_no = fields.Integer(string='#', compute="_compute_month", store=True)
    month = fields.Char(string='Month', compute="_compute_month", store=True)
    grade = fields.Many2one(comodel_name='hr.salary.grade', string='Grade', 
        required=True, track_visibility="onchange", domain="[('active','=',True)]")
    salary_grade = fields.Many2one(comodel_name='hr.salary.matrix', string='Salary Grade', 
        domain="[('grade','=',grade),('active','=',True)]",
        states={'lock': [('readonly', True)]}, help="To Change please end the current appointment and create a new one.")
    step = fields.Selection(string='Step', selection=steps, default=1, required=True, track_visibility="onchange", related="salary_grade.step")    
    basic_salary = fields.Float(string='Basic Salary', required=True, states={'lock': [('readonly', True)]})
    salary_diff = fields.Float(string='Salary Differential', required=False, states={'lock': [('readonly', True)]})
    overtime_pay = fields.Float(string='Overtime Pays', required=False, states={'lock': [('readonly', True)]})
    hazard_pay = fields.Float(string='Hazard Pays', required=False, states={'lock': [('readonly', True)]})
    gsis = fields.Float(string='GSIS', required=False, store=True) #compute='_compute_mandatory_deduction')
    hdmf = fields.Float(string='Pag-ibig', required=False,  store=True) #compute='_compute_mandatory_deduction')
    philhealth = fields.Float(string='PhilHealth', required=False, store=True) #compute='_compute_mandatory_deduction')
    midyr_bonus = fields.Float(string='Mid-Year Bonus', required=False, states={'lock': [('readonly', True)]})
    yrend_bonus = fields.Float(string='Year-End Bonus', required=False, states={'lock': [('readonly', True)]})
    cash_gift = fields.Float(string='Cash Gift', required=False, states={'lock': [('readonly', True)]})
    sri = fields.Float(string='SRI', required=False, states={'lock': [('readonly', True)]})
    cna = fields.Float(string='CNA', required=False, states={'lock': [('readonly', True)]})
    whtax_held = fields.Float(string='Tax Witheld', required=False, store=True, readonly=True)
    whtax = fields.Float(string='Witholding Tax', required=False, store=True, readonly=True)
    active = fields.Boolean(string='Active', default=True)
    payslip_id = fields.Many2one(comodel_name='hr.payslip', string='Payslip', states={'lock': [('readonly', True)]})
    sdiff_id = fields.Many2one(comodel_name='hr.payslip.adjustment.sdiff', string='Salary Differential Reference')
    locked = fields.Boolean(string='Lock Field', compute="_compute_lock", default=False)
    state = fields.Selection(string='Status', selection=[('open', 'Open'), ('lock', 'Locked'),], related="whtax_id.state")
    
    # def init(self):
    #     self._cr.execute("""
    #     update hr_payslip_witholding_tax_line t
    #             set salary_grade = c.salary_grade,
    #                 grade = c.grade,
    #                 basic_salary = wage 
    #             from (select h.id, h.wage, h.salary_grade, m.grade from hr_contract h inner join hr_salary_matrix m on h.salary_grade = m.id)  c 
    #         where t.contract_id = c.id;
    #     """)
    
                         
    def open_whtax_view(self):                                
        for rec in self:
            domain = str([('whtax_line_id', '', rec.id)])
            context = {}
            
            value = {
                'domain': domain,
                'view_mode': 'form',
                'res_model': 'hr.payslip.witholding.view',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'name': _('Witholding Tax Computation'),
                'res_id': self.id,
                'target': 'new',
                'context': context
            }
        return value          
            
    def action_compute_whtax_line(self):
        for rec in self:
            self.env['hr.payslip.witholding.tax'].compute_whtax_held_employee(rec.employee_id.id, rec.tax_year, rec.month_no)    
    
    def _compute_lock(self):
        for rec in self:
            year = datetime.now().year
            month = datetime.now().month
            rec.locked = True if (rec.tax_year == year and rec.month_no < month) or rec.tax_year < year else False
        
    @api.depends('basic_salary')
    def _compute_mandatory_deduction(self):
        for rec in self:
            rec.gsis = rec.basic_salary*0.09
            rec.hdmf = 100
            rec.philhealth = round(rec.basic_salary*0.02,2) if (rec.basic_salary*0.02)<1600 else 1600
    
    @api.depends('date_from','date_to')
    def _compute_month(self):
        for rec in self:
            if not rec.date_from:
                rec.month = rec.date_from.strftime('%B') or False
                rec.month_no = rec.date_from.month
            else:
                rec.month = rec.date_to.strftime('%B') or False
                rec.month_no = rec.date_to.month
            rec.whtax_view_id = rec.id or False
                

class HrPayslipWitholdingTaxView(models.Model):
    _name = 'hr.payslip.witholding.view'
    _description = 'Witholding Tax View'
    _rec_name = 'month'
    _auto = False
    
    whtax_line_id = fields.Many2one(comodel_name='hr.payslip.witholding.tax.line', domain=[('active','=',True)])
    whtax_id = fields.Many2one(comodel_name='hr.payslip.witholding.tax', string='Witholding Tax Table', domain=[('active','=',True)])
    tax_table_line_id = fields.Many2one('hr.payslip.tax.table', string='Tax Table Line', compute='_compute_monthly_taxes')
    month_no = fields.Integer(string='#', compute='_compute_monthly_taxes')
    month = fields.Char(string='Month', compute='_compute_monthly_taxes')   
    tax_year = fields.Char(string='Year', compute='_compute_monthly_taxes')   
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', compute='_compute_monthly_taxes')
    basic_pays = fields.Monetary(string='Basic Pays', compute='_compute_monthly_taxes')
    others = fields.Monetary(string='Others', compute='_compute_monthly_taxes')
    allowable_exemption = fields.Monetary(string='Allowable Exemption', compute='_compute_monthly_taxes')
    others_total = fields.Monetary(string='Others Total', compute='_compute_monthly_taxes') 
    gross_taxable_income = fields.Monetary(string='Gross Taxable Income', compute='_compute_monthly_taxes')
    mandatory_deduction = fields.Monetary(string='Mandatory Deduction', compute='_compute_monthly_taxes')
    net_taxable_income = fields.Monetary(string='Net Taxable Income', compute='_compute_monthly_taxes')
    tax_for_the_first = fields.Monetary(string='Tax Base for the First', compute='_compute_monthly_taxes') 
    excess = fields.Monetary(string='Tax Base for the Excess', compute='_compute_monthly_taxes') 
    excess_percent = fields.Float(string='%', compute='_compute_monthly_taxes') 
    excess_percent_amount = fields.Monetary(string='Tax for the Excess', compute='_compute_monthly_taxes')
    added_tax = fields.Monetary(string='Tax for the First', compute='_compute_monthly_taxes')
    total_tax_due = fields.Monetary(string='Total Tax Due', compute='_compute_monthly_taxes')
    taxes_witheld = fields.Monetary(string='Taxes Witheld', compute='_compute_monthly_taxes')
    remaining_tax_due = fields.Monetary(string='Remaining Tax Due', compute='_compute_monthly_taxes')
    monthly_witholding_tax = fields.Monetary(string='Monthly Witholding Tax', compute='_compute_monthly_taxes')
    currency_id = fields.Many2one('res.currency', string='Currency',
                                required=True, readonly=True, compute='_compute_monthly_taxes')
         
    def init(self):
        tools.drop_view_if_exists(self._cr, 'hr_payslip_witholding_view')
        self._cr.execute("""
            CREATE or REPLACE view hr_payslip_witholding_view as (
                select id, id as whtax_line_id, whtax_id 
                from hr_payslip_witholding_tax_line where active = True)""")
            
    def _compute_monthly_taxes(self):
        for rec in self:
            rec.month_no = month_no = rec.whtax_line_id.month_no
            rec.month = rec.whtax_line_id.month
            rec.currency_id = rec.whtax_id.currency_id.id
            rec.employee_id = rec.whtax_id.employee_id.id
            rec.tax_year = rec.whtax_id.tax_year
            create_date = rec.whtax_id.create_date
            whtax_held = 0.0
            for line in rec.whtax_id.line_ids:
                if line.month_no == month_no:
                    whtax_held = line.whtax_held
                    rec.month = line.month
                rec.basic_pays += line.basic_salary+line.salary_diff+line.overtime_pay+line.hazard_pay
                rec.others += line.midyr_bonus+line.yrend_bonus+line.cash_gift+line.sri+line.cna
                rec.mandatory_deduction += line.gsis+line.hdmf+line.philhealth
            tax_base = self.env['hr.payslip.tax.table.base'].sudo().search([('date_start','<=',create_date)], order="date_start desc", limit=1)
            rec.allowable_exemption = tax_base.allowable_exemption or 90000 # Fix
            rec.others_total = rec.others-rec.allowable_exemption if rec.others > rec.allowable_exemption else 0.00
            rec.gross_taxable_income = rec.basic_pays+rec.others_total #rec.others+rec.allowable_exemption+rec.others_total
            rec.net_taxable_income = rec.gross_taxable_income - rec.mandatory_deduction
            
            # Tax table
            net_taxable_income = rec.net_taxable_income
            # tax_table = self.env['hr.payslip.tax.table'].sudo().search([('tax_table_id','=',tax_base.id),('net_taxable_income_from','<=',net_taxable_income),('net_taxable_income_to','>=',net_taxable_income)], order="date_start desc, date_end desc, net_taxable_income_from asc, net_taxable_income_to asc")
            tax_table = self.env['hr.payslip.tax.table'].sudo().search([('tax_table_id','=',tax_base.id),('net_taxable_income_from','<=',net_taxable_income),('net_taxable_income_to','>=',net_taxable_income)], order="date_start desc, date_end desc, net_taxable_income_from asc, net_taxable_income_to asc", limit=1)
            if tax_table:
                rec.tax_for_the_first = tax_table.tax_for_the_first
                rec.added_tax = tax_table.added_tax
                rec.excess_percent = tax_table.excess_percentage/100
                rec.tax_table_line_id = tax_table.id or False
                # for table in tax_table:
                #     if net_taxable_income >= table.net_taxable_income_from and (net_taxable_income < table.net_taxable_income_to or not table.net_taxable_income_to):
                #         rec.tax_for_the_first = table.tax_for_the_first
                #         rec.added_tax = table.added_tax
                #         rec.excess_percent = table.excess_percentage/100
                #         rec.tax_table_line_id = tax_table.id or False
                #         break
            else:    
                if rec.net_taxable_income >= 250000:
                    if rec.net_taxable_income >= 250000 and rec.net_taxable_income < 400000:
                        rec.tax_for_the_first = 250000
                        rec.added_tax = 0.00
                        rec.excess_percent = 0.20
                    elif rec.net_taxable_income >= 400000 and rec.net_taxable_income < 800000:
                        rec.tax_for_the_first = 400000
                        rec.added_tax = 30000
                        rec.excess_percent = 0.25
                    elif rec.net_taxable_income >= 800000 and rec.net_taxable_income < 2000000:
                        rec.tax_for_the_first = 800000
                        rec.added_tax = 130000
                        rec.excess_percent = 0.30
                    if rec.net_taxable_income >= 2000000 and rec.net_taxable_income < 8000000:
                        rec.tax_for_the_first = 2000000
                        rec.added_tax = 490000
                        rec.excess_percent = 0.32
                    if rec.net_taxable_income >= 8000000:
                        rec.tax_for_the_first = 8000000
                        rec.added_tax = 2410000
                        rec.excess_percent = 0.35
                    
            rec.excess = rec.net_taxable_income-rec.tax_for_the_first
            rec.excess_percent_amount = rec.excess*rec.excess_percent
            rec.total_tax_due = rec.excess_percent_amount+rec.added_tax 
            rec.taxes_witheld = whtax_held
            rec.remaining_tax_due = rec.total_tax_due-rec.taxes_witheld
            rec.monthly_witholding_tax =  rec.remaining_tax_due/(12-month_no) if (12-month_no) > 0 else 0.00 
           