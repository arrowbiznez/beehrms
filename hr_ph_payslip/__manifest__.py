# -*- coding: utf-8 -*-
{
    'name': "Payslip for Philippines",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Daryll Gay Bangoy",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['hr_payroll_community',
                'hr_salary_grade',
                'base_automation',],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/salary_rule.xml',
        'data/salary_rule2.xml',
        'data/salary_rule3.xml',
        'data/hr_payslip_tax_table_data.xml',
        'views/hr_contract.xml',
        'views/templates.xml',
        'views/views.xml',
        'views/hr_adjustments.xml',
        'views/hr_deductions.xml',
        'views/hr_payslip_matrix.xml',
        'views/hr_payslip_witholding_tax.xml',
        'report/hr_payroll_report_temp.xml',
        'report/general_report_view.xml',
        'report/hr_employee_payslip_temp.xml',
        'report/hr_payslip_bank_report.xml',
        'report/templates/general_payroll_template.xml',
        'report/templates/manded_template.xml',
        'report/templates/hr_payslip_indiv.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}
