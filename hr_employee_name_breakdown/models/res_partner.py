# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class ResPartner(models.Model):
    _name = 'res.partner.name'
    _description = 'Name List for auto pop-up'
    
    name = fields.Char(string='Name', index=True)

    @api.constrains('name')
    def _validate_name(self):
        id = self.id
        name = self.name
        
        res = self.search([['name', '=ilike', name], ['id', '!=', id]])
        if res:
            raise ValidationError(_("Record already exists."))
    
class ResPartnerSuggest(models.Model):
    _inherit = 'res.partner'
        
    prefix_id = fields.Many2one(comodel_name='res.partner.name', string='Prefix ID')
    firstname_id = fields.Many2one(comodel_name='res.partner.name', string='First Name ID')
    middlename_id = fields.Many2one(comodel_name='res.partner.name', string='Middle Name ID')
    lastname_id = fields.Many2one(comodel_name='res.partner.name', string='Last Name ID')
    suffix_id = fields.Many2one(comodel_name='res.partner.name', string='Suffix ID')
    
    prefix = fields.Char(string='Prefix', store=True)
    firstname = fields.Char(string='Firstname', store=True)
    middlename = fields.Char(string='Middlename', store=True)
    lastname = fields.Char(string='Lastname', store=True)
    suffix = fields.Char(string='Suffix', store=True)
    
    @api.onchange('firstname', 'middlename', 'lastname', 'suffix', 'prefix')
    def onchange_names(self):
        for rec in self:
            prefix = rec.prefix or ""
            firstname = rec.firstname or ""
            middlename = rec.middlename or ""
            lastname = rec.lastname or ""
            suffix = rec.suffix or ""
            partner = self.env['res.partner.name'].sudo()
            name = ""
            
            res_id = False
            if prefix:
                prefix = prefix.title().strip()
                res = partner.search([('name','ilike',prefix)])
                if not res:
                    res_id = partner.create({"name":prefix})
                
                rec.prefix_id = res_id or res.id
            
                name = "%s" % (prefix)
            
            res_id = False        
            if firstname:
                firstname = firstname.title().strip()
                res = partner.search([('name','ilike',firstname)])
                if not res:
                    partner.create({"name":firstname})
                
                rec.firstname_id = res_id or res.id

            if name:
                name = "%s %s" % (name, firstname)
            else:
                name = firstname
            
            res_id = False        
            if middlename:
                middlename = middlename.title().strip()
                res = partner.search([('name','ilike',middlename)])
                if not res:
                    partner.create({"name":middlename})
                
                rec.middlename_id = res_id or res.id

                if name:
                    name = "%s %s" % (name, middlename)
                else:
                    name = middlename
            
            res_id = False        
            if lastname:
                lastname = lastname.title().strip()
                res = partner.search([('name','ilike',lastname)])
                if not res:
                    partner.create({"name":lastname})
                rec.lastname_id = res_id or res.id

                if name:
                    name = "%s %s" % (name, lastname)
                else:
                    name = lastname
            
            res_id = False
            if suffix:
                suffix = suffix.title().strip()
                res = partner.search([('name','ilike',suffix)])
                if not res:
                    partner.create({"name":suffix})
                
                rec.suffix_id = res_id or res.id

                if name:
                    name = "%s %s" % (name, suffix)
                else:
                    name = suffix
                    
            if name:
                rec.name = name
            
    # @api.onchange('firstname_id', 'middlename_id', 'lastname_id', 'suffix_id', 'prefix_id')
    # def onchange_name_break(self):
    #     for s in self:
    #         prefix = s.prefix_id.name or ""
    #         firstname = s.firstname_id.name or ""
    #         middlename = s.middlename_id.name or ""
    #         lastname = s.lastname_id.name or ""
    #         suffix = s.suffix_id.name or ""
    #         name = False

    #         if prefix:
    #             prefix = prefix.title().strip()
            
    #         name = "%s" % (prefix)

    #         if firstname:
    #             firstname = firstname.title().strip()

    #         if name:
    #             name = "%s %s" % (name, firstname)
    #         else:
    #             name = firstname

    #         if middlename:
    #             middlename = middlename.title().strip()
                
    #         if name: 
    #             name = '%s %s' % (name, middlename)
    #         else:
    #             name = middlename
            
    #         if lastname:
    #             lastname = lastname.title().strip()
            
    #         if name:
    #             name = "%s %s" % (name, lastname)
    #         else:
    #             name = lastname

    #         if suffix:
    #             suffix = suffix.title().strip()
            
    #         if name:
    #             name = "%s %s" % (name, suffix)
    #         else:
    #             name = suffix

    #         s.name = name

    @api.model
    def create(self, values):
        print('values',values)
        name = False
        ids = [values.get('prefix_id',False), values.get('firstname_id',False), values.get('middlename_id',False), values.get('lastname_id',False), values.get('suffix_id',False)]
        for id in ids:
            if id:
                print('xid',id)
                rec = self.env['res.partner.name'].browse(id)
                if name:
                    name = '%s %s' % (name, rec.name)
                else:
                    name = rec.name
                    
        if not name:
            ids = [values.get('prefix',False), values.get('firstname',False), values.get('middlename',False), values.get('lastname',False), values.get('suffix',False)]
            for id in ids:
                if id:
                    print('xid2',id)
                    if name:
                        name = '%s %s' % (name, id)
                    else:
                        name = id
                        
        if name:
            values['name'] = name.title()

        return super(ResPartnerSuggest, self).create(values)

    # @api.multi
    def write(self, values):
        for r in self:
            name = False
            ids = [values.get('prefix_id',r.prefix_id.id or False), values.get('firstname_id',r.firstname_id.id or False), 
            values.get('middlename_id',r.middlename_id.id or False), values.get('lastname_id',r.lastname_id.id or False), 
            values.get('suffix_id',r.suffix_id.id or False)]
            for id in ids:
                if id:
                    rec = self.env['res.partner.name'].browse(id)
                    if name:
                        name = '%s %s' % (name, rec.name)
                    else:
                        name = rec.name
            if name:
                values['name'] = name.title()
                        
            elif not name:
                ids = [values.get('prefix',r.prefix or False), values.get('firstname',r.firstname or False), 
                values.get('middlename',r.middlename or False), values.get('lastname',r.lastname or False), 
                values.get('suffix',r.suffix or False)]
                for id in ids:
                    if id:
                        if name:
                            name = '%s %s' % (name, id)
                        else:
                            name = id
            if name:
                values['name'] = name.title()
                    
        return super(ResPartnerSuggest, self).write(values)
    
    

