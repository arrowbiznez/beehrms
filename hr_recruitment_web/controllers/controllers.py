# -*- coding: utf-8 -*-
import base64

from odoo import http
from odoo.http import request


class HrRecruitmentWeb(http.Controller):
    @http.route('/form_application', auth='public', type='http', website='true')
    def application_form(self, **kw):
        # return "Hello, world"
        return http.request.render('hr_recruitment_web.application_forms', {})

    @http.route('/create/application', type='http', website='true', auth='public')
    def create_application(self, **post):
        applicant_details = {
            'name': post.get('name'),
            'partner_name': post.get('partner_name'),
            'email_from': post.get('email_from'),
            'partner_phone': post.get('partner_phone'),
            'partner_mobile': post.get('partner_mobile'),
        }
        applicant_id = request.env['hr.applicant'].sudo().create(applicant_details)
        # application = request.env['hr.applicant'].sudo()

        # applicant = request.env['hr.applicant']
        print(applicant_id.id)
        Attachments = request.env['ir.attachment'].sudo()
        name = post.get('attachment').filename
        file = post.get('attachment')
        if name:
            attachment_id = Attachments.create({
                'name': name,
                'type': 'binary',
                'datas': base64.b64encode(file.read()),
                'res_model': 'hr.applicant',
                'res_id': applicant_id.id
            })
            request.env['hr.applicant'].sudo().update({

                'attachment_ids': [(4, attachment_id.id)],

            })
        return http.request.render('hr_recruitment_web.application_sent', {})

        # request.env['ir.attachment'].sudo().create(attachment_id)

    # @http.route('/hr_recruitment_web/hr_recruitment_web/objects/<model("hr_recruitment_web.hr_recruitment_web"):obj>/', auth='public')
    # def object(self, obj, **kw):
    #     return http.request.render('hr_recruitment_web.object', {
    #         'object': obj
    #     })
