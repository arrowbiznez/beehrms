# -*- coding: utf-8 -*-

from dateutil import relativedelta
import pandas as pd
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.addons.resource.models.resource import HOURS_PER_DAY


class HrOverTime(models.Model):
    _inherit = 'hr.overtime'

    ot_type = fields.Many2one(comodel_name='overtime.type', string='Overtime Type', compute="_get_overtime_type", store=True)
    overtime_approvals = fields.One2many('overtime.validation.status',
                                      'overtime_status',
                                      string='Overtime Validators',
                                      track_visibility='always', help="Overtime approvals")
                                      
    multi_level_validation = fields.Boolean(string='Multiple Level Approval',
                                            help="If checked then multi-level approval is necessary", store=True)
                                                 
    current_approver_level = fields.Integer(string='Current Approval Level', default=-1)
    current_approver_user = fields.Many2one(comodel_name='res.users', string='Current Approver', compute="_compute_current_approver_user", store=True)
    is_current_approver_user = fields.Boolean(string='User is Current Approval?', compute="_compute_current_approver_user", store=True)
    
    
    approver_comment = fields.Text(string="Approvers' Comment")
    
    validation_type = fields.Selection(selection=[('fin','Finance'),('multi', 'Multi Level Approval')],
    related="ot_type.validation_type", store=True, default="fin")
    
    state = fields.Selection(add_selection=[('cancel', 'Cancel'),])
    
    @api.depends('current_approver_level')
    def _compute_current_approver_user(self):
        for rec in self:
            rec.current_approver_user = False
            rec.is_current_approver_user = False
            if rec.multi_level_validation:
                print('current_approver_level',rec.current_approver_level)
                rec.current_approver_user = self.env['overtime.validation.status'].search([('hr_overtime_status','=',rec.id),('order','=',rec.current_approver_level)]).validating_users.id or False
                print('current_approver_user',rec.current_approver_user)
                rec.is_current_approver_user = True if rec.current_approver_user == self.env.user.id else False
    @api.model
    def create(self, values):
        seq = False
        if 'employee_id' not in values:
            emp_id = self.env.user.employee_id.id
        else:
            emp_id = values['employee_id']
        emp = self.env['hr.employee'].browse(emp_id)
        if emp:
            if emp.department_id and 'dept_code' in self.env['hr.department']._fields:
                if emp.department_id.dept_code:
                    seq = self.env['ir.sequence'].next_by_code_by_employee('OVT',self._name,emp) or '/'
        if not seq:
            seq = self.env['ir.sequence'].next_by_code('hr.overtime') or '/'
        values['name'] = seq
        return super(HrOverTime, self.sudo()).create(values)
        
    def write(self, values):
        if self.multi_level_validation:
            current_approver_level = self.current_approver_level
            
            if values.get('state') == 'f_approve':
                current_approver_level = 0 
            elif self.state == 'confirm' and 'overtime_approvals' in values:
                for overtime_approvals in values['overtime_approvals']:
                    for stat in overtime_approvals:
                        if isinstance(stat, dict):
                            status = stat.get('validation_status','')
                            if status == True:
                                current_approver_level = current_approver_level+1
                            elif status == False:
                                current_approver_level = -1
            elif values.get('state') == 'draft':
                current_approver_level = -1
            values['current_approver_level'] = current_approver_level
        return super(HrOverTime, self).write(values)
                                        
    @api.onchange('duration_type','type')
    def _get_overtime_type(self):
        for rec in self:
            dur = ''
            type = rec.type
            dur = rec.duration_type
            ot_type = self.env['overtime.type'].search([('type','=',type),('duration_type', '=', dur)], order="id desc", limit=1)
            rec.ot_type = ot_type.id
            rec.multi_level_validation = ot_type.multi_level_validation
            
    
    @api.onchange('ot_type','employee_id')
    def add_validators(self):
        """ Update the tree view and add new validators
        when permit type is changed in leave request form """
        li = [(5, 0, 0)]
                
        order = -1
        found = False
        for l in self.ot_type.overtime_validators.sorted(key=lambda r: r.order):
            if l.order != 0:
                if l.overtime_validators.id == self.env.user.id:
                    found = True
                if l.validator_type == 'manager':
                    if not self.employee_id.parent_id:
                        raise UserError(_("No assigned Division Chief or Manager in your employee record.")) 
                    if self.employee_id.parent_id.user_id.id == self.env.user.id:
                        found = True
            if found:
                order = l.order
                break
                
        next_order = 0
        xorder = 0
        for l in self.ot_type.overtime_validators.sorted(key=lambda r: r.order):
            if next_order != l.order:
                continue
            if l:
                if l.validator_type == 'manager':
                    if self.employee_id.parent_id.user_id:
                        li.append((0, 0, {
                            'validating_users': self.employee_id.parent_id.user_id.id,
                            'order': xorder,
                        }))
                else:
                    li.append((0, 0, {
                        'validating_users': l.overtime_validators.id,
                        'order': xorder,
                    }))
                xorder += 1
            if order > 0:
                next_order = order
                order = 0
            next_order += 1
                
        self.overtime_approvals = li
        
    def action_confirm(self):
        self.state = 'f_approve'
        
    def action_cancel(self):
        self.state = 'cancel'
        
    def action_approve(self):
        """ Chack if any pending tasks is added if so reassign the pending
        task else call approval """
        if any(overtime.state != 'confirm' for overtime in self):
            raise UserError(_(
                'Overtime request must be confirmed in order to approve it.'))
        return self.approval_check()

    def approval_check(self):
        """ Check all overtime validators approved the overtime request if approved
         change the current request stage to Approved"""
         
        
        if self.multi_level_validation and self.current_approver_user.id != self.env.user.id:
            raise UserError(_("You are not the current Approver."))

        current_employee = self.env['hr.employee'].search(
            [('user_id', '=', self.env.uid)], limit=1)
        li = []
        if self.env.context.get('active_id'):
            active_id = self.env.context.get('active_id')
        else:
            active_id = self.id
        user = self.env['hr.overtime'].search([('id', '=', active_id)], limit=1)
        for user_obj in user.overtime_approvals:
            li.append(user_obj.validating_users.id)
        if self.env.uid in li:
            validation_obj = user.overtime_approvals.search(
                [('overtime_status', '=', user.id),
                 ('validating_users', '=', self.env.uid)])
            validation_obj.validation_status = True
            validation_obj.overtime_comments = self.approver_comment
            self.approver_comment = ""
        approval_flag = True
        for user_obj in user.overtime_approvals:
            if not user_obj.validation_status:
                approval_flag = False
        if approval_flag:
            self.approve()
        return approval_flag
        
    def _get_approval_requests(self):
        """ Action for Approvals menu item to show approval
        requests assigned to current user """

        current_uid = self.env.uid
        hr_overtime = self.env['hr.overtime'].search([])
        li = []
        for l in hr_overtime:
            if l.state == 'f_approve':
                for user in l.overtime_approvals.sorted(key=lambda r: r.order):
                    if user.validating_users.id == current_uid and l.current_approver_level == user.order:
                        li.append(l.id)
        value = {
            'domain': str([('id', 'in', li)]),
            'view_mode': 'tree,form',
            'res_model': 'hr.overtime',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context':"{'search_default_to_approve': 1}",
        }
        return value
    
    def _get_overtime_all_requests(self):
    
        value = {
            'domain': str([('state', '!=', 'draft'),('type','=','cash')]),
            'view_mode': 'tree,form',
            'res_model': 'hr.overtime',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('All Records'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context':"{'default_type': 'cash'}",
        }
        return value
                                            
class OvertimeValidationStatus(models.Model):
    """ Model for overtime validators and their status for each overtime request """
    _name = 'overtime.validation.status'

    overtime_status = fields.Many2one('hr.overtime')
    validating_users = fields.Many2one('res.users', string='Overtime Validators', help="Overtime validators",
                                       domain="[('share','=',False)]")
    validation_status = fields.Boolean(string='Approve Status', readonly=True,
                                       default=False,
                                       track_visibility='always', help="Status")
    overtime_comments = fields.Text(string='Comments', help="Comments")
    
    order = fields.Integer(string='Sequence')
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='validating_users.employee_id', store=True)
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', related='employee_id.job_id', store=True) 
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id', store=True)
    
    @api.depends('validating_users')
    def _compute_employee_info(self):
        for rec in self:
            rec.employee_id = self.env['hr.employee'].search([('user_id','=',rec.validating_users.id)]).id
            
    @api.onchange('validating_users')
    def prevent_change(self):
        """ Prevent Changing overtime validators from Overtime request form """
        raise UserError(_(
            "Changing overtime validators is not permitted. You can only change "
            "it from Overtime Types Configuration"))
            
class HrOverTimeType(models.Model):
    _inherit = 'overtime.type'
    
    multi_level_validation = fields.Boolean(string='Multiple Level Approval',
                                            help="If checked then multi-level approval is necessary")
    
    validation_type = fields.Selection(selection=[('fin','Finance'),('multi', 'Multi Level Approval')],
    default='fin')
    overtime_validators = fields.One2many('hr.overtime.validators',
                                       'hr_overtime_status',
                                       string='Overtime Validators', help="Overtime validators")
    
    @api.onchange('validation_type')
    def enable_multi_level_validation(self):
        """ Enabling the boolean field of multilevel validation"""
        if self.validation_type == 'multi':
            self.multi_level_validation = True
        else:
            self.multi_level_validation = False
            
class HrOvertimeValidators(models.Model):
    """ Model for overtime validators in Overtime Types configuration """
    _name = 'hr.overtime.validators'

    hr_overtime_status = fields.Many2one('overtime.type')

    overtime_validators = fields.Many2one('res.users',
                                         string='Overtime Validators', help="Overtime validators",
                                         domain="[('share','=',False)]")
                                         
    order = fields.Integer(string='Sequence')
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='overtime_validators.employee_id', store=True)
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', related='employee_id.job_id', store=True) 
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id', store=True)
     
    validator_type = fields.Selection(string='Validator Type', selection=[('individual', 'Employee'), ('manager', 'Manager'),], default='individual')                                        
    
    @api.depends('overtime_validators')
    def _compute_employee_info(self):
        for rec in self:
            rec.employee_id = self.env['hr.employee'].browse(rec.overtime_validators.id).id or False
                                                    


