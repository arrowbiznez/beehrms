#-*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

office_leave = 'Official Leave The Office'
office_leave_code = 'OPLO'
office_leave_personal = 'Personal Leave The Office'
office_leave_personal_code = 'PPLO'
office_stay = 'Stay in the Office'
office_stay_code = 'PSO'
office_report = 'Report in the Office'
office_report_code = 'PRO'
group_tags = [('employee','Employee'),
('sc','Section Chief'),
('dc','Division Chief'),
('deda','DEDA'),
('oed','OED'),
('FAD','FAD Chief'),
('cm','Chairman'),
('pmt','PMT')]


class HrJob(models.Model):
    _inherit = 'hr.job'
    
    group_tag = fields.Selection(string='Designation', selection=group_tags, default="employee")
    
class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    
    group_tag = fields.Selection(string='Designation', selection=group_tags, related="job_id.group_tag")

class PermitValidationStatus(models.Model):
    """ Model for Permit validators and their status for each permit request """
    _name = 'permit.validation.status'

    hr_permit_status = fields.Many2one('hr.permit')
    validating_users = fields.Many2one('res.users', string='Permit Validators', help="Permit validators")
    validation_status = fields.Boolean(string='Approve Status', readonly=True,
                                       default=False,
                                       track_visibility='always', help="Status")
    permit_comments = fields.Text(string='Comments', help="Comments")
    
    order = fields.Integer(string='Sequence')
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='validating_users.employee_id', store=True)
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', related='employee_id.job_id', store=True) 
    group_tag = fields.Selection(string='Designation', selection=group_tags, related="employee_id.group_tag")
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id', store=True)
    
    @api.depends('validating_users')
    def _compute_employee_info(self):
        for rec in self:
            rec.employee_id = self.env['hr.employee'].search([('user_id','=',rec.validating_users.id)]).id
            
    # @api.onchange('validating_users')
    # def prevent_change(self):
    #     """ Prevent Changing permit validators from Permit request form """
    #     raise UserError(_(
    #         "Changing permit validators is not permitted. You can only change "
    #         "it from Permit Types Configuration"))

class HrPermitEmployeeType(models.Model):
    _name = 'hr.permit.type'
    _description = 'HR Employee Permit Type'
    _inherit = 'mail.thread'
    
    name = fields.Char(string='Permit Type')
    
    code = fields.Char(string='Permit Type Code')
    
    group_tag = fields.Selection(string='Designation', selection=group_tags, default="employee")
    
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', help="Blank means applicable to Regular Employee", invisible=True)
    
    # multi_level_validation = fields.Boolean(string='Multiple Level Approval',
    #                                         help="If checked then multi-level approval is necessary", default=True)
    
    # validation_type = fields.Selection(selection=[('fin','Finance'),('multi', 'Multi Level Approval')],
    # default='fin')
    
    permit_validators = fields.One2many('hr.permit.validators',
                                       'hr_permit_status',
                                       string='Permit Validators', help="Permit validators")
    
    # @api.constrains('name', 'group_tag')
    # def _validate_name(self):
    #     id = self.id
    #     name = self.name
    #     group_tag = self.group_tag
        
    #     res = self.search_count([('name', '=ilike', name), ('group_tag','=ilike',group_tag), ('id', '!=', id)])
    #     if res:
    #         raise ValidationError(_("Record with same information already exists. If you are using import, there is duplication of information in your file."))


    # @api.onchange('validation_type')
    # def enable_multi_level_validation(self):
    #     """ Enabling the boolean field of multilevel validation"""
    #     if self.validation_type == 'multi':
    #         self.multi_level_validation = True
    #     else:
    #         self.multi_level_validation = False


class HrPermitValidators(models.Model):
    """ Model for permit validators in Permit Types configuration """
    _name = 'hr.permit.validators'

    hr_permit_status = fields.Many2one('hr.permit.type')

    permit_validators = fields.Many2one('res.users',
                                         string='Permit Validators', help="Permit validators",
                                         domain="[('share','=',False)]")
                                         
    order = fields.Integer(string='Sequence')
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', related='permit_validators.employee_id', store=True)
    job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', related='employee_id.job_id', store=True) 
    group_tag = fields.Selection(string='Designation', selection=group_tags, related="employee_id.group_tag")
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id', store=True)
     
    validator_type = fields.Selection(string='Validator Type', selection=[('individual', 'Employee'), ('manager', 'Manager'),], default='individual')
     
     
    @api.depends('permit_validators')
    def _compute_employee_info(self):
        for rec in self:
            rec_id = self.env['hr.employee'].browse(rec.permit_validators.id).id or False
                                                    
class HrPermitEmployee(models.Model):
    _name = 'hr.permit'
    _description = 'HR Permit'
    _inherit = 'mail.thread'
    _order = 'id desc'
    _rec_name = 'name'

    name = fields.Char(string='Name', store=True, default='New')
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', required=True,
                    default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]))
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id', store=True)
    manager_id = fields.Many2one(comodel_name='hr.employee', string='Manager', related='department_id.manager_id', store=True)
    
    permit_type_id = fields.Many2one(comodel_name='hr.permit.type', string='Permit To', required=True, readonly=True, 
                    default=lambda self: self.env.ref('hr_permit_employee.hr_permit_type_office_leave').id)
    permit_type = fields.Char(string='Type', related='permit_type_id.name', store=True, invisible=True)
    
    transaction_date = fields.Datetime(string='Filing Date', default=lambda self: fields.datetime.now(), readonly=True)
    
    request_date = fields.Date(string='Request Date')
    
    request_time_depart = fields.Float(string='Estimated Depart Time',)
    request_time_arrive = fields.Float(string='Estimated Return Time',)
    
    
    approval_type = fields.Selection(string='Approval Type', selection=[('official', 'Official'), ('personal', 'Personnal'),])
    
    destination = fields.Text(string='Destination')
    purpose = fields.Text(string='Purpose')
    
    actual_time_arrival = fields.Float(string='Arrival')
    
    actual_time_departure = fields.Float(string='Departure')
    
    can_approve = fields.Boolean(string='Can Approve', compute='_compute_current_approver_user', store=True)
    can_refuse = fields.Boolean(string='Can Refuse', compute='_compute_current_approver_user', store=True)
    can_reset = fields.Boolean(string='Can Reset', compute='_compute_current_approver_user', store=True)
    
    state = fields.Selection(string='State', selection=[('draft', 'To Submit'), 
    ('cancel', 'Cancelled'), ('confirm', 'Confirm'), 
    ('approve', 'Approved'), 
    ('refuse', 'Refused'),], default='draft')
    
    permit_approvals = fields.One2many('permit.validation.status',
                                      'hr_permit_status',
                                      string='Permit Validators',
                                      track_visibility='always', help="Permit approvals")
                                                 
    current_approver_level = fields.Integer(string='Current Approval Level', default=-1)
    current_approver_user = fields.Many2one(comodel_name='res.users', string='Current Approver', compute="_compute_current_approver_user", store=True)
    
    company_id = fields.Many2one(comodel_name='res.company', string='Company', default=lambda self: self.env.user.company_id.id)
    
    approver_comment = fields.Text(string="Approvers' Comment")

    document_code = fields.Char(string="Document Code")
    rev_no = fields.Char(string="Rev. No.")
    document_title = fields.Char(string="Document Title")
    
    @api.model
    def create(self, values):
        seq = False
        permit_type = False
        context = self.env.context
        print('values',context)
        if 'default_permit_type_id' in context:
            permit_type_id = context['default_permit_type_id']
            permit_type = self.env['hr.permit.type'].browse(permit_type_id).code or ''
        if 'employee_id' not in values:
            emp_id = self.env.user.employee_id.id
        else:
            emp_id = values['employee_id']
        emp = self.env['hr.employee'].browse(emp_id)
        print('emp',emp, permit_type)
        if emp and permit_type:
            if emp.department_id and 'dept_code' in self.env['hr.department']._fields:
                if emp.department_id.dept_code:
                    seq = self.env['ir.sequence'].next_by_code_by_employee(permit_type.upper(),self._name,emp) or '/'
        if not seq:
            seq = self.env['ir.sequence'].next_by_code('hr.permit') or '/'
        values['name'] = seq
        
        return super(HrPermitEmployee, self).create(values)
    
    
    def _open_permit_to_leave(self):
        """ Action for Permit to Leave Official"""
        employee = self.env.user.employee_id
        uid = employee.id
        group_tag = employee.group_tag
            
        default_permit_type_id = self.env.ref('hr_permit_employee.hr_permit_type_office_leave').id
        res = self.env['hr.permit.type'].sudo().search([('name','=', office_leave),('group_tag','=',group_tag)],order='id desc')
        ctr = 1
        ids = []
        for rec in res:
            ids.append(rec.id)
            if ctr == 1:
                default_permit_type_id = res.id
            ctr += 1
            
        
        domain = str([('permit_type_id.id', 'in', ids),('employee_id.id','=',uid)])
        context = {'default_permit_type_id': default_permit_type_id}
        
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Official Permit to Leave The Office'),
            'res_id': self.id,
            'target': 'current',
            'context': context
        }
        
        return value
        
    def _open_permit_to_leave_personal(self):
        """ Action for Permit to Leave Personal"""
        employee = self.env.user.employee_id
        uid = employee.id
        group_tag = employee.group_tag
            
        default_permit_type_id = self.env.ref('hr_permit_employee.hr_permit_type_office_leave_personal').id
        res = self.env['hr.permit.type'].sudo().search([('name','=', office_leave_personal),('group_tag','=',group_tag)],order='id desc')
        ctr = 1
        ids = []
        for rec in res:
            ids.append(rec.id)
            if ctr == 1:
                default_permit_type_id = res.id
            ctr += 1
            
        
        domain = str([('permit_type_id.id', 'in', ids),('employee_id.id','=',uid)])
        context = {'default_permit_type_id': default_permit_type_id}
        
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Personal Permit to Leave The Office'),
            'res_id': self.id,
            'target': 'current',
            'context': context
        }
        
        return value
        
    def _open_permit_to_report(self):
        """ Action for Permit to Report """
        employee = self.env.user.employee_id
        uid = employee.id
        group_tag = employee.group_tag
        
        default_permit_type_id = self.env.ref('hr_permit_employee.hr_permit_type_office_report').id
        res = self.env['hr.permit.type'].sudo().search([('name','=', office_report),('group_tag','=',group_tag)],order='id desc')
        ctr = 1
        ids = []
        for rec in res:
            ids.append(rec.id)
            if ctr == 1:
                default_permit_type_id = res.id
            ctr += 1
            
        
        domain = str([('permit_type_id.id', 'in', ids),('employee_id.id','=',uid)])
        context = {'default_permit_type_id': default_permit_type_id}
        
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Permit to Report in the Office'),
            'res_id': self.id,
            'target': 'current',
            'context': context
        }
        
        return value
    
    def _open_permit_to_stay(self):
        """ Action for Permit to Stay """
        employee = self.env.user.employee_id
        uid = employee.id
        group_tag = employee.group_tag
        
        default_permit_type_id = self.env.ref('hr_permit_employee.hr_permit_type_office_stay').id
        res = self.env['hr.permit.type'].sudo().search([('name','=', office_stay),('group_tag','=',group_tag)],order='id desc')
        ctr = 1
        ids = []
        for rec in res:
            ids.append(rec.id)
            if ctr == 1:
                default_permit_type_id = res.id
            ctr += 1
            
        
        domain = str([('permit_type_id.id', 'in', ids),('employee_id.id','=',uid)])
        context = {'default_permit_type_id': default_permit_type_id}
        
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Permit to Stay in the Office'),
            'res_id': self.id,
            'target': 'current',
            'context': context
        }
        # 
        return value
        
    def _open_permit_all_to_leave(self):
        """ Action for Permit to Leave Official"""
        
        context = {'default_name': office_leave}
        hr_permit_type = self.env['hr.permit.type'].search([('name','=',office_leave)])
        permit_type_ids = []
        for l in hr_permit_type:
            permit_type_ids.append(l.id)
        
        li = []
        uid = self.env.user.id
        permit = self.search([('permit_type_id', 'in', permit_type_ids)])
        for p in permit:
            for u in p.permit_approvals:
                if u.validating_users.id == uid:
                    li.append(p.id)
            
        domain = str([('state','!=','draft'),('id','in',li)])
        
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Official Permit to Leave The Office'),
            'res_id': self.id,
            'target': 'current',
            'context':context,
        }
        return value
    
    def _open_permit_all_to_leave_personal(self):
        """ Action for Permit to Leave Personal"""
        
        context = {'default_name': office_leave_personal}
        hr_permit_type = self.env['hr.permit.type'].search([('name','=',office_leave_personal)])
        permit_type_ids = []
        for l in hr_permit_type:
            permit_type_ids.append(l.id)
        
        li = []
        uid = self.env.user.id
        permit = self.search([('permit_type_id', 'in', permit_type_ids)])
        for p in permit:
            for u in p.permit_approvals:
                if u.validating_users.id == uid:
                    li.append(p.id)
            
        domain = str([('state','!=','draft'),('id','in',li)])
        
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Personal Permit to Leave The Office'),
            'res_id': self.id,
            'target': 'current',
            'context':context,
        }
        return value
          
    def _open_permit_all_to_report(self):
        """ Action for Permit to Leave """
        
        context = {'default_name': office_report}
        hr_permit_type = self.env['hr.permit.type'].search([('name','=',office_report)])
        permit_type_ids = []
        for l in hr_permit_type:
            permit_type_ids.append(l.id)
        
        li = []
        uid = self.env.user.id
        permit = self.search([('permit_type_id', 'in', permit_type_ids)])
        for p in permit:
            for u in p.permit_approvals:
                if u.validating_users.id == uid:
                    li.append(p.id)
            
        domain = str([('state','!=','draft'),('id','in',li)])
        
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Permit to Reprt in the Office'),
            'res_id': self.id,
            'target': 'current',
            'context':context,
        }
        
        return value
    
    def _open_permit_all_to_stay(self):
        """ Action for Permit to Stay """
        
        context = {'default_name': office_stay}
        hr_permit_type = self.env['hr.permit.type'].search([('name','=',office_stay)])
        permit_type_ids = []
        for l in hr_permit_type:
            permit_type_ids.append(l.id)
        
        li = []
        uid = self.env.user.id
        permit = self.search([('permit_type_id', 'in', permit_type_ids)])
        for p in permit:
            for u in p.permit_approvals:
                if u.validating_users.id == uid:
                    li.append(p.id)
            
        domain = str([('state','!=','draft'),('id','in',li)])
        
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Permit to Stay in the Office'),
            'res_id': self.id,
            'target': 'current',
            'context':context,
        }
        return value
        
    
    def _get_approval_requests_leave(self):
        """ Action for Approvals menu item to show approval
        requests assigned to current user """

        current_uid = self.env.uid
        
        hr_permit = self.env['hr.permit'].search([('name','=',office_leave)])
        li = []
        for l in hr_permit:
            if l.state == 'confirm':
                if l.current_approver_user.id == current_uid:
                    li.append(l.id)
        
        value = {
            'domain': [('id', 'in', li)],
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Personnel Pass Slip Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context':"{'search_default_to_approve': 1, }"
        }
        print('value', value)
        return value
        
    
    def _get_approval_requests_leave_personal(self):
        """ Action for Approvals menu item to show approval
        requests assigned to current user """

        current_uid = self.env.uid
        
        hr_permit = self.env['hr.permit'].search([('name','=',office_leave_personal)])
        li = []
        for l in hr_permit:
            if l.state == 'confirm':
                if l.current_approver_user.id == current_uid:
                    li.append(l.id)
        
        value = {
            'domain': [('id', 'in', li)],
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Personal Permit to Leave Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context':"{'search_default_to_approve': 1, }"
        }
        print('value', value)
        return value
        
    
    def _get_approval_requests_report(self):
        """ Action for Approvals menu item to show approval
        requests assigned to current user """

        current_uid = self.env.uid
        
        # permit_type_id = self.env.ref('hr_permit_employee.hr_permit_type_office_report').id
        hr_permit = self.env['hr.permit'].search([('name','=',office_report)])
        li = []
        for l in hr_permit:
            if l.current_approver_user.id == current_uid:
                li.append(l.id)
                
            # if l.state == 'confirm':
            #     for user in l.permit_approvals:
            #         if user.validating_users.id == current_uid:
            #             li.append(l.id)
                        
        domain = str([('id', 'in', li)])
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Permit to Report Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context':"{'search_default_to_approve': 1, }"
        }
        return value
    
    def _get_approval_requests_stay(self):
        """ Action for Approvals menu item to show approval
        requests assigned to current user """

        current_uid = self.env.uid
        
        # permit_type_id = self.env.ref('hr_permit_employee.hr_permit_type_office_stay').id
        hr_permit = self.env['hr.permit'].search([('name','=',office_stay)])
        li = []
        for l in hr_permit:
            if l.state == 'confirm':
                if l.current_approver_user.id == current_uid:
                    li.append(l.id)
                    
                # if l.state == 'confirm':
                #     for user in l.permit_approvals:
                #         if user.validating_users.id == current_uid:
                #             li.append(l.id)
           
        domain = str([('id', 'in', li)])
        value = {
            'domain': domain,
            'view_mode': 'tree,form',
            'res_model': 'hr.permit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Permit to Stay Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context':"{'search_default_to_approve': 1, }"
        }
        return value
        
    
    def action_cancel(self): 
        self.state = 'cancel'
    
    def action_confirm(self):
        if self.current_approver_level < 0:
            self.current_approver_level = 0
        self.state = 'confirm'
    
    def action_reject(self):
        if self.can_approve:
            self.state = 'refuse'
        else:
            raise UserError(_("You are not the current Approver."))
        
    def action_draft(self):
        self.state = 'draft'
    
    def action_approve(self):
        """ Check if any pending tasks is added if so reassign the pending
        task else call approval """
        
        if any(permit.state != 'confirm' for permit in self):
            raise UserError(_(
                'Permit request must be confirmed in order to approve it.'))
                
        uid = self.env.user.id
        for permit in self:
            if permit.employee_id.user_id:
                if permit.employee_id.user_id.id == uid:
                    raise UserError(_('You cannot approve your own Request')) 
        
        return self.approval_check()

    def approval_check(self):
        """ Check all permit validators approved the permit request if approved
         change the current request stage to Approved"""
        
        if self.current_approver_user.id != self.env.user.id:
            raise UserError(_("You are not the current Approver."))

        # current_employee = self.env['hr.employee'].search(
        #     [('user_id', '=', self.env.uid)], limit=1)
        li = []
        if self.env.context.get('active_id'):
            active_id = self.env.context.get('active_id')
        else:
            active_id = self.id
        user = self.env['hr.permit'].search([('id', '=', active_id)], limit=1)
        for user_obj in user.permit_approvals:
            li.append(user_obj.validating_users.id)
        if self.env.uid in li:
            validation_obj = user.permit_approvals.search(
                [('hr_permit_status', '=', user.id),
                 ('validating_users', '=', self.env.uid)])
            validation_obj.validation_status = True
            validation_obj.permit_comments = self.approver_comment
            self.approver_comment = False
        approval_flag = True
        for user_obj in user.permit_approvals:
            if not user_obj.validation_status:
                approval_flag = False
        if approval_flag:
            self.approve()
        self.current_approver_level += 1
        return approval_flag
        
    def approve(self):
        self.state = 'approve'
        
        timesheet_based_attendance = False
        print('name',self.permit_type)
        if self.permit_type in [office_leave,office_leave_personal]:
            timesheet_based_attendance = self.env['ir.module.module'].sudo().search(
            [('name', '=', 'timesheet_based_attendance')],
            limit=1).state or False
        
        print('timesheet_based_attendance',timesheet_based_attendance)
        if timesheet_based_attendance:
            if timesheet_based_attendance == 'installed':
                employee_id = self.employee_id.id
                request_date = self.request_date or self.transaction_date.date()
                # transaction_date = self.transaction_date
                # request_time_depart = self.request_time_depart
                # request_time_arrive = self.request_time_arrive
                found = self.env['attendance.timesheet'].sudo().search([('employee_id','=',employee_id),
                                    ('attendance_date','=',request_date),
                                    ('state','=','draft')])
                print('found',found)
                if found:
                    state = 'oplo'
                    if self.permit_type == office_leave:
                        state = 'pplo'
                    found.reference2 = state
    
    @api.depends('current_approver_level')    
    def _compute_current_approver_user(self):
        for rec in self:
            can_approve = False
            can_refuse = False
            can_reset = False
            res = self.env['permit.validation.status'].search([('hr_permit_status','=',rec.id),('order','=',rec.current_approver_level)])
            if res:
                current_approver_user = res.validating_users.id
                can_approve = True
                can_refuse = True
                can_reset = True
            else:
                current_approver_user = False
            rec.current_approver_user = current_approver_user
            rec.can_approve = can_approve
            rec.can_refuse = can_refuse
    #         rec.can_reset = can_reset
    
    
    @api.onchange('permit_type_id','employee_id')
    def add_validators(self):
        print('Add Validators')
        """ Update the tree view and add new validators
        when permit type is changed in permit request form """
        li = [(5, 0, 0)]
        xorder = 0
        for l in self.permit_type_id.permit_validators.sorted(key=lambda r: r.order):
            if l:
                if l.validator_type == 'manager':
                    if not self.employee_id.parent_id:
                        raise UserError(_("No assigned Division Chief or Manager in your employee record.")) 
                    elif self.employee_id.parent_id.user_id:
                        li.append((0, 0, {
                            'validating_users': self.employee_id.parent_id.user_id.id,
                            'order': xorder,
                        }))
                else:
                    li.append((0, 0, {
                        'validating_users': l.permit_validators.id,
                        'order': xorder,
                    }))
                xorder += 1
                
        self.permit_approvals = li
    
    
    
    
