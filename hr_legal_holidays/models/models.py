# -*- coding: utf-8 -*-

from odoo import api, fields, models, SUPERUSER_ID, tools, _
from odoo.tools import float_compare
from odoo.exceptions import ValidationError

class HrLeaveType(models.Model):
    _inherit = 'hr.leave.type'

    timesheet_code = fields.Selection(selection_add=[('holiday', 'Holiday'),
    ('sholiday', 'Special Holiday'),])
    

class HrLegalHoliday(models.Model):
    _inherit = 'hr.leave'
    
    @api.model
    def default_get(self, fields_list):
        context = self._context
        defaults = super(HrLegalHoliday, self).default_get(fields_list)
        defaults = self._default_get_request_parameters(defaults)

        LeaveType = self.env['hr.leave.type'].with_context(employee_id=defaults.get('employee_id'), default_date_from=defaults.get('date_from', fields.Datetime.now()))
        if context.get('legal_holidays'):
            lt = LeaveType.search([('code', '=', 'HOL')], limit=1)
        else:
            lt = LeaveType.search([('valid', '=', True)], limit=1)

        defaults['holiday_status_id'] = lt.id if lt else defaults.get('holiday_status_id')
        defaults['state'] = 'confirm' if lt and lt.validation_type != 'no_validation' else 'draft'
        return defaults
    
    @api.constrains('state', 'number_of_days', 'holiday_status_id')
    def _check_holidays(self):
        mapped_days = self.mapped('holiday_status_id').get_employees_days(self.mapped('employee_id').ids)
        for holiday in self:
            # print('xxx_aloc',holiday.holiday_status_id.allocation_type)
            if holiday.holiday_type != 'employee' or not holiday.employee_id or holiday.holiday_status_id.allocation_type == 'no':
                continue
            if holiday.holiday_status_id.allocation_type == 'fixed' and holiday.holiday_status_id.code in ['HOL','SHOL']:
                continue
            leave_days = mapped_days[holiday.employee_id.id][holiday.holiday_status_id.id]
            if holiday.late_timesheet_id or holiday.absent_timesheet_id:
                continue
            if float_compare(leave_days['remaining_leaves'], 0, precision_digits=2) == -1 or float_compare(leave_days['virtual_remaining_leaves'], 0, precision_digits=2) == -1:
                raise ValidationError(_('The number of remaining time-off is not sufficient for this time off type.\n'
                                        'Please check for pending time-off allocations that needs an approval.'))
    