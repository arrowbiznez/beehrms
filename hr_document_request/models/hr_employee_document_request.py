# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning, ValidationError

# class  HRDocumentUplaodRequest(models.Model):
#     _name = "hr.document.upload.request"
#     _inherit = 'mail.thread'
    

# class  HRDocumentUplaodRequestEmp(models.Model):
#     _name = "hr.document.upload.request.employee"
#     _inherit = 'mail.thread'
    
# class  HRDocumentUplaod(models.Model):
#     _name = "hr.document.upload"
#     _inherit = 'mail.thread'

class HRDocumentCheckList(models.Model):
    _name = 'hr.document.checklist'
    _inherit = 'mail.thread'
    _description = 'HR Training Checklist'
    
    name = fields.Char(string='Document Name', tracking=True, required=True)
    document_type = fields.Many2one(comodel_name='document.type', string='Document Type', required=True)
    active = fields.Boolean(string='Active', default=True, tracking=True)
     
    @api.constrains('name')
    def _check_name(self):
        name = self.name
        id = self.id
        found = self.search([('name','ilike',name),('id','!=',id)])
        if found:
            raise ValidationError("Document Already Exists in checklist.")
            
class HREmployeeDocumentRequest(models.Model):
    _name = 'hr.employee.document.request'
    _inherit = 'mail.thread'
    _description = 'HR Employee Document Request'
    _order = 'date_requested desc, id'
    
    name = fields.Char(string='Name', default='New', readonly=True)
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Requested By', 
    default=lambda self: self.env.user.employee_id.id, tracking=True, readonly=True)
    date_requested = fields.Date(string='Date Request', default=lambda self: fields.datetime.now(), tracking=True, required=True)
    state = fields.Selection(string='State', selection=[('draft', 'To Submit'), ('confirm', 'Submitted'),
    ('done', 'Done'),('refuse','Refused'),('cancel', 'Cancel'),], tracking=True, default='draft')
    document_ids = fields.Many2many(comodel_name='hr.document.checklist', string='Documents Checklist', tracking=True, domain="[('active','=',True)]")
    hr_document_ids = fields.One2many(comodel_name='hr.employee.document', inverse_name='emp_doc_request_id', string='Requested Documents')
    requested_for = fields.Char(string='Requested for', required=True)
    
    
    @api.model
    def create(self, values):
        seq = False
        if 'employee_id' not in values:
            emp_id = self.env.user.employee_id.id
        else:
            emp_id = values['employee_id']
        emp = self.env['hr.employee'].browse(emp_id)
        if emp:
            if emp.department_id and 'dept_code' in self.env['hr.department']._fields:
                if emp.department_id.dept_code:
                    seq = self.env['ir.sequence'].next_by_code_by_employee('DOCR',self._name,emp) or '/'
        if not seq:
            seq = self.env['ir.sequence'].next_by_code('hr.employee.document.request') or '/'
        values['name'] = seq
        return super(HREmployeeDocumentRequest, self.sudo()).create(values)
    
    def action_confirm(self):
        self.state = 'confirm'
        
    def action_done(self):
        self.state = 'done'

    def action_draft(self):
        self.state = 'draft'
        
    def action_reject(self):
        self.rejected_by = self.env.user.id
        self.state = 'reject'
        
    def action_cancel(self):
        self.state = 'Cancel'
        
class HREmployeeDocument(models.Model):
    _inherit = 'hr.employee.document'
    
    emp_doc_request_id = fields.Many2one(comodel_name='hr.employee.document.request', string='Employee Document Request')
    

class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    employee_id = fields.Many2one('hr.employee',
                                  string='Related Employee', ondelete='restrict', 
                                  help='Employee-related data of the user')