# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _


class HrDeduction(models.Model):
    _inherit = 'hr.payslip'

    deduction_ids = fields.One2many('hr.payslip.deduction', 'special_deduction_id', readonly=True,
                                           help="Payslip worked days",
                                           states={'draft': [('readonly', False)]})


class HrDeductionsPayroll(models.Model):
    _name = 'hr.payslip.deduction'

    salary_rule_id = fields.Many2one('hr.salary.rule', string='Salary Rule', domain=[('category_id.code','=','DED')])
    desc = fields.Char(string="Description")
    code = fields.Char(string="Code")
    amount = fields.Float(string="Amount")
    contract_id = fields.Many2one('hr.contract')
    payslip_id = fields.Many2one('hr.payslip', string="Payslip ID")
    special_deduction_id = fields.Many2one('hr.payslip')

    @api.onchange('salary_rule_id')
    def _compute_input_details(self):
        for line in self:
            line.name = line.salary_rule_id.name
            line.code = line.salary_rule_id.code


