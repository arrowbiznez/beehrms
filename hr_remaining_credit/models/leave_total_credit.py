import datetime

from odoo import models, fields, api, tools, _


class TotalLeaveCredit(models.Model):
    _name = 'total.leave.credit'
    _description = 'Leaves Credit Report'
    # _auto = False

    employee_id = fields.Many2one('hr.employee')
    allocation_date = fields.Datetime(string="Allocation Date")
    holiday_status_id = fields.Many2one('hr.leave.type')
    total_allocation = fields.Float(string="Total Allocation")
    total_base_leave = fields.Float(string='Total Leaves')
    total_payslip_leave = fields.Float(string="Total Payslip Leave")
    total_remaining = fields.Float(string="Total Remaining", compute='compute_total_remaining')
    total_remaining_leave = fields.Float(string="total leave credit", store=True)
    
    # def init(self):
    #     tools.drop_view_if_exists(self._cr, 'total_leave_credit')
    #     self._cr.execute("""CREATE or REPLACE view total_base_leave as (
    #     with leave_allocation as (select employee_id, holiday_status_id, 
    #                 0.0 as total_base_leave, 
    #                 sum(number_of_days*8) as total_allocation,
    #                 sum(number_of_days) total_remaining_leave, 
    #                 max(date_from) allocation_date
    #                 from hr_leave_allocation where state = 'validate' group by 1,2),
    #             leave_availed as (select employee_id, holiday_status_id, 
    #                 sum(total_hours_leave) total_base_leave,
    #                 0.0 as total_allocation,
    #                 sum(-total_hours_leave) total_remaining_leave, 
    #                 to_date('1999-01-01','YYYY-MM-DD')::date as allocation_date
    #                 from hr_leave where state = 'validate' group by 1,2)
	# 			select concat(employee_id, holiday_status_id) id, employee_id, holiday_status_id, 
    #                 sum(total_base_leave) as total_base_leave,
    #                 sum(total_allocation) total_allocation,
    #                 sum(total_remaining_leave) total_remaining_leave,
    #                 (sum(total_allocation) - sum(total_base_leave)) total_remaining,
    #                 max(allocation_date) allocation_date,
    #                 0.0 as total_payslip_leave
    #                 from (select * from leave_allocation 
	# 			union all select * from leave_availed) xxx group by 1,2,3 order by employee_id);""")
    
    def compute_total_remaining(self):
        for line in self:
            total_leaves = line.total_payslip_leave + line.total_base_leave
            line.total_remaining = line.total_allocation - total_leaves

class HrLeave(models.Model):
    _inherit = 'hr.leave'

    total_hours_leave = fields.Float(string="total hours leave", related='number_of_days_display', store=True)
    approval_date = fields.Datetime(string="Approval Date")

    def write(self, values):
        prev_state = self.state
        state = False
        if 'state' in values:
            state = values['state']
        ret = super(HrLeave, self).write(values)
        if ret:
            if state == 'validate':
                self.compute_total_base_leave()
            elif state == 'cancel' and prev_state == 'validate':
                self.compute_total_base_leave(deduct=True)
        return ret
    
    def compute_total_base_leave(self, deduct=False):
        for line in self:
            line.approval_date = datetime.datetime.now()
            total_credit = self.env['total.leave.credit'].search([('employee_id', '=', line.employee_id.id),
                                                                  ('holiday_status_id', '=', line.holiday_status_id.id)])
            if total_credit:
                if deduct:
                    total_credit.total_base_leave -= line.total_hours_leave
                    total_credit.total_remaining_leave += line.total_hours_leave
                else:
                    total_credit.total_base_leave += line.total_hours_leave
                    total_credit.total_remaining_leave -= line.total_hours_leave
                
            else:
                self.env['total.leave.credit'].create({'employee_id': line.employee_id.id,
                                                       'total_base_leave': line.total_hours_leave,
                                                       'holiday_status_id': line.holiday_status_id.id})


class HrLeaveAllocation(models.Model):
    _inherit = 'hr.leave.allocation'

    approval_date = fields.Datetime(string="Approval Date")
    
    def action_approve(self):
        self.compute_total_allocation()
        return super(HrLeaveAllocation, self).action_approve()

    def compute_total_allocation(self, deduct=False):
        for line in self:
            line.approval_date = datetime.datetime.now()
            total_credit = self.env['total.leave.credit'].search([('employee_id', '=', line.employee_id.id),
                                                                ('holiday_status_id', '=',
                                                                line.holiday_status_id.id)], limit=1, order='id desc')
            if total_credit:
                if deduct:
                    total_credit.total_allocation -= line.number_of_hours_display
                    total_credit.total_remaining_leave -= line.number_of_days_display
                else:
                    total_credit.total_allocation += line.number_of_hours_display
                    total_credit.total_remaining_leave += line.number_of_days_display
                    total_credit.allocation_date = datetime.datetime.now()
            else:
                self.env['total.leave.credit'].create({'employee_id': line.employee_id.id,
                                                    'total_allocation': line.number_of_days_display,
                                                    'total_remaining_leave': line.number_of_days_display,
                                                    'holiday_status_id': line.holiday_status_id.id})


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    total_force_leave = fields.Integer(string="Force Leave")

    def compute_force_leave(self):
        employees = self.env['hr.employee'].search([])
        for employee in employees:
            employee.write({'total_force_leave': 0})
            leave_credit = self.env['hr.leave.allocation'].search([('employee_id','=', employee.id),('holiday_status_id.code','=','VL',('holiday_status_id.active','=',True))])
            format = '%Y-%m-%d'
            start_dt = datetime.datetime.now().replace(month=1, day=1)
            end_dt = datetime.datetime.now().replace(month=12, day=31)

            current_alloc = [allocation for allocation in leave_credit if allocation.approval_date and
                             start_dt < allocation.approval_date < end_dt]

            total_current_allocation = sum(allocation.number_of_days_display for allocation in current_alloc)

            total_leaves = self.env['hr.leave'].search([('employee_id','=', employee.id),('holiday_status_id.code','=', 'VL'),('holiday_status_id.active','=',True)])
            current_leave = [leave for leave in total_leaves if leave.approval_date and
                             start_dt < datetime.datetime.strptime(leave.approval_date, format) < end_dt]

            total_current_leave = sum(leave.number_of_days_display for leave in current_leave)
            total = total_current_allocation - total_current_leave
            if total > 10:
                current_force_leave = total - 10
                employee.write({'total_force_leave': current_force_leave})


