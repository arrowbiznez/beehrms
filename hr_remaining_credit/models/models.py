#-*- coding: utf-8 -*-

from odoo import models, fields, api, tools, _

class HRLeave(models.Model):
    _inherit = "hr.leave"
    
    def log_leave_allocation(self):
        data = []
        for rec in self:
            if rec.state != "validate":
                break
            remarks = 'Availed Leave'
            if 'Tardiness' in rec.report_note or 'Undertime' in rec.report_note:
                remarks = 'Late and/or Undertime Conversion to Leave'
            elif 'Absences' in rec.report_note:
                remarks = 'Absences Conversion to Leave'
            elif 'Holiday' in rec.report_note:
                break

            xdata = {
                'employee_id': rec.employee_id,
                'holiday_status_id': rec.holiday_status_id.id,
                'transaction_date': rec.approval_date or rec.create_date,
                'used': rec.total_hours_leave,
                'leave_id': rec.id,
                'period_from': rec.date_from,
                'period_to': rec.date_to,
                'expiry': False,
                'seq': 2,
                'remarks': remarks
            }

            xdata.update(self.env['hr.leave.card.summary'].get_prev_balance(rec))
            data.append(xdata)

        if data:
            self.env['hr.leave.card.summary'].sudo().create(data)


class HRLeaveRequest(models.Model):
    _inherit = "hr.leave.allocation"

    def log_leave_allocation(self):
        data = []
        for rec in self:
            xdata = {
                'employee_id': rec.employee_id,
                'holiday_status_id': rec.holiday_status_id.id,
                'transaction_date': rec.approval_date or rec.create_date,
                'earned': rec.number_of_days,
                'allocation_id': rec.id,
                'period_from': rec.date_from or rec.create_date,
                'period_to': rec.date_to or rec.create_date,
                'expiry': True if rec.date_to else False,
                'seq': 1  
            }
            xdata.update(self.env['hr.leave.card.summary'].get_prev_balance(rec))
            data.append(xdata)

        if data:
            self.env['hr.leave.card.summary'].sudo().create(data)

class HRLeaveCardSummary(models.Model):
    _name = "hr.leave.card.summary"
    _description = "HR Leave Card Summary"
    _auto = True
    _order = "period_from asc, seq asc"

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', index=True)
    name = fields.Char('name', related='employee_id.name', store=True)
    transaction_date = fields.Date(string='Approval Date', index=True)
    period_from = fields.Date(string='Date From', index=True)
    period_to = fields.Date(string='Date To', index=True)
    expiry = fields.Boolean(string='With Expiry?', index=True)
    expiry_date = fields.Date(string='Expiry Date', compute="_compute_balance", store=True, default=False, index=True)
    year = fields.Char(string='Year', index=True)
    holiday_status_id = fields.Many2one(comodel_name='hr.leave.type', string='Leave Type', index=True)
    forwarded = fields.Float(string='Forwarded', compute="_compute_balance", store=True, default=0.0)
    earned = fields.Float(string='Earned')
    used = fields.Float(string='Used')
    balance = fields.Float(string='Balance', compute="_compute_balance", store=True, default=0.0)
    absences = fields.Float(string='Absences')
    absences_nopay = fields.Float(string='Absences w/out Pay')
    remarks = fields.Char(string='Remarks')
    leave_id = fields.Many2one(comodel_name='hr.leave', string='leave', index=True)
    allocation_id = fields.Many2one(comodel_name='hr.leave.allocation', string='leave', index=True)
    type = fields.Selection([
        ('ua', 'Unauthorized Absent')
    ], string='Type', index=True)
    code = fields.Char('Code')
    filler = fields.Boolean('Filler', compute="_compute_balance", store=True, default=False, index=True)
    seq = fields.Integer('Sequence', index=True)
    month = fields.Char('Month', index=True)
    # source_document = fields.Reference (selection = [('hr.leave.allocation', 'Leave Allocation'), 
    #                                                 ('hr.leave', 'Leave'),
    #                                                 ('hr.leave.type', 'Leave Type'),
    #                                                 ('attendancet.imesheet', 'Timesheet')],  string = "Source Document") 
    
    
    
    # # add absences without leave
    # def init(self):
    #     tools.drop_view_if_exists(self._cr, 'hr_leave_card_summary')
    #     self._cr.execute("""
    #         update attendance_timesheet t set leave_id = l.id
    #             from hr_leave l where t.employee_id = l.employee_id and t.leave_id isnull and 
    #             t.attendance_date between date_from and date_to and l.state = 'validate';
                         
    #         CREATE or REPLACE view hr_leave_card_summary as (
    #             with alloc as (select employee_id, holiday_status_id, 
    #                     coalesce(approval_date::date, a.create_date::date) transaction_date,
    #                     number_of_days earned, 
    # 					0.0 as used,
    # 					a.id as allocation_id,
    # 					cast(null as integer) as leave_id,
    # 					coalesce(date_from::date, a.create_date::date) as period_from,
    # 					coalesce(date_to::date, coalesce(date_from::date, a.create_date::date)) as period_to,
    # 					(case when date_to isnull then 0 else 1 end) as expiry,
    # 					t.code,
    # 					0.0 as absences,
    # 					0.0 as absences_nopay,
    # 					'' as type, 1 as seq
    #                     from hr_leave_allocation a inner join hr_leave_type t on a.holiday_status_id = t.id 
    #                     where state = 'validate' order by a.id),
	# leave as ( select employee_id, holiday_status_id, 
    #                     coalesce(approval_date::date, hl.create_date::date)::date transaction_date,
    #                     0.0 earned, 
    # 					(total_hours_leave) as used,
    # 					cast(null as integer) as allocation_id,
    # 					hl.id as leave_id, 
    # 					date_from as period_from,
    # 					date_to as period_to,
    # 					0 as expiry, (case when (report_note like '%Tardiness%' or report_note like '%Undertime%') then 'Late and/or Undertime Conversion to Leave'
    # 					when report_note like '%Absences%' then 'Absences Conversion to Leave' else 'Availed Leave' end) as remarks
    #         from hr_leave hl where hl.state = 'validate' and total_hours_leave > 0),
    #     -- dayoff as (select l.employee_id, period_from, period_to, 
    #     --     sum(coalesce(absence,0.0))*(case when sum(coalesce(absence,0))<0 then -1 else 1 end)::integer as total_absence
    #     -- from attendance_timesheet tt inner leaft leave as l
    #     --     where tt.attendance_date between period_from and period_to
    #     --     and tt.leave_id = l.id tt.employee_id = l.employee_id 
    #     --     group by 1,2,3),
    # absences as (select t.employee_id, (select distinct holiday_status_id from leave limit 1) as holiday_status_id,
    #                     t.date_from as period_from, 
    #                     t.date_end as period_to, 
    #                     sum(coalesce(tt.absence,0.0))*(case when sum(coalesce(tt.absence,0))<0 then -1 else 1 end)::integer as total_absence 
	# 		from attendance_timesheet tt inner join attendance_timesheet_base t on t.id = tt.attendance_timesheet_id
	# 						group by 1,2,3,4),
	# filler as (select l.employee_id, t.id holiday_status_id,
    #                     date_from as period_from, 
    #                     date_end as period_to, 
    #                     0 as total_absence 
	# 		from hr_leave_type t, attendance_timesheet tt, attendance_timesheet_base l 
	# 		where t.code in ('VL','SL','SLP') and l.id = tt.attendance_timesheet_id 
	# 						group by 1,2,3,4),
    # leave_type as (select id, code from hr_leave_type)

    # SELECT row_number() over(ORDER BY leave_summ.employee_id, leave_summ.holiday_status_id, leave_summ.period_from, leave_summ.seq, leave_summ.allocation_id) as id, *, date_part('year', period_from) as year, to_char(period_from, 'Month') as month 
	# 		from (select *, False as filler, 'Leave Allocation' remarks from alloc 
	# union all select l.employee_id, l.holiday_status_id, l.transaction_date, l.earned, 
    #     l.used, l.allocation_id, l.leave_id, l.period_from, l.period_to, l.expiry, 
	# t.code, 0.0 as absences, -- cast(b.total_absence as numeric) absences, 
	# 0.0 as absences_nopay, '' as type, 2 as seq, False as filler, remarks
    #     from leave l inner join leave_type t on l.holiday_status_id = t.id
    #     -- left outer join dayoff b on l.employee_id = b.employee_id
	# 	-- 	where l.period_from = b.period_from and l.period_to = b.period_to
 	# union all select employee_id, holiday_status_id, 
    #                     null as transaction_date, 
    #                     0.0 earned, 
    #                     0.0 as used, 
    #                     cast(null as integer) as allocation_id, 
    #                     cast(null as integer) as leave_id,
    #                     period_from, 
    #                     period_to, 
    #                     0 as expiry, 
    #                     t.code,
    #                     cast(total_absence as numeric) absences, 
    #                     0.0 as absences_nopay,
    # 				    '' as type, 3  as seq,
    # 				    False as filler, 'Absences without Leave Application' remarks 
    #                     from absences a inner join leave_type t on a.holiday_status_id = t.id
 	# union all select employee_id, holiday_status_id, 
    #                     null as transaction_date, 
    #                     0.0 earned, 
    #                     0.0 as used, 
    #                     cast(null as integer) as allocation_id, 
    #                     cast(null as integer) as leave_id,
    #                     period_from, 
    #                     period_to, 
    #                     0 as expiry,
    #                     '' as code, 
    #                     cast(total_absence as numeric) absences, 
    #                     0.0 as absences_nopay,
    #                     '' as type, 99 as seq, True as filler, 'Filler - Just to forward the balance.' remarks 
    #                     from filler
    #                     ) leave_summ
    # group by 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
    # order by employee_id, holiday_status_id, period_from asc, seq asc)""")

    # 	### To Correct dashboard				
    #     self._cr.execute("""with emp_credits as (select employee_id, holiday_status_id, sum(used) as used, sum(earned) as earned, sum(earned-used) as balance
    # from hr_leave_card_summary group by 1,2)
    #     insert into total_leave_credit (employee_id, holiday_status_id, total_allocation, total_base_leave, total_remaining_leave)
    #                     select * from emp_credits where concat(employee_id,holiday_status_id) not in (select concat(employee_id,holiday_status_id) from total_leave_credit)""")
        
    #     self._cr.execute("""update total_leave_credit c 
    #                     set total_allocation = earned,
    #                     total_base_leave = used,
    #                     total_remaining_leave = balance
    #                     from (select employee_id, holiday_status_id, sum(used) as used, sum(earned) as earned, sum(earned-used) as balance
    # 				    from hr_leave_card_summary group by 1,2) s
    # 				    where c.employee_id = s.employee_id and c.holiday_status_id = s.holiday_status_id
    # 				    """)
        
    def create_leave_card_summary_data(self):
        tools.drop_view_if_exists(self._cr, 'hr_leave_card_summary')
        self._cr.execute("""
                with alloc as (select employee_id, holiday_status_id, 
                        coalesce(approval_date::date, a.create_date::date) transaction_date,
                        number_of_days earned, 
    					0.0 as used,
    					a.id as allocation_id,
    					cast(null as integer) as leave_id,
    					coalesce(date_from::date, a.create_date::date) as period_from,
    					coalesce(date_to::date, coalesce(date_from::date, a.create_date::date)) as period_to,
    					(case when date_to isnull then false else true end) as expiry,
    					t.code,
    					0.0 as absences,
    					0.0 as absences_nopay,
    					'' as type, 1 as seq
                        from hr_leave_allocation a inner join hr_leave_type t on a.holiday_status_id = t.id 
                        where state = 'validate' order by a.id ),
	leave as (select employee_id, holiday_status_id, 
                        coalesce(approval_date::date, hl.create_date::date)::date transaction_date,
                        0.0 earned, 
    					(total_hours_leave) as used,
    					cast(null as integer) as allocation_id,
    					hl.id as leave_id, 
    					date_from as period_from,
    					date_to as period_to,
    					false as expiry, (case when (report_note like '%Tardiness%' or report_note like '%Undertime%') then 'Late and/or Undertime Conversion to Leave'
    					when report_note like '%Absences%' then 'Absences Conversion to Leave' else 'Availed Leave' end) as remarks
            from hr_leave hl where hl.state = 'validate' and total_hours_leave > 0),
    absences as (select t.employee_id, (select distinct holiday_status_id from leave limit 1) as holiday_status_id,
                        t.date_from as period_from, 
                        t.date_end as period_to, 
                        sum(coalesce(tt.absence,0.0))*(case when sum(coalesce(tt.absence,0))<0 then -1 else 1 end)::integer as total_absence 
			from attendance_timesheet tt inner join attendance_timesheet_base t on t.id = tt.attendance_timesheet_id
							group by 1,2,3,4),
	filler as (select l.employee_id, t.id holiday_status_id,
                        date_from as period_from, 
                        date_end as period_to, 
                        0 as total_absence 
			from hr_leave_type t, attendance_timesheet tt, attendance_timesheet_base l 
			where t.code in ('VL','SL','SLP') and l.id = tt.attendance_timesheet_id 
							group by 1,2,3,4),
    leave_type as (select id, code from hr_leave_type)

    insert into hr_leave_card_summary(employee_id, holiday_status_id, transaction_date, earned, used, allocation_id, 
                         leave_id, period_from, period_to, expiry, code, absences, 
                         absences_nopay, type, seq, filler, remarks, year, month)
    SELECT *, date_part('year', period_from) as year, to_char(period_from, 'Month') as month 
			from (select *, False as filler, 'Leave Allocation' remarks from alloc 
	union all select l.employee_id, l.holiday_status_id, l.transaction_date, l.earned, 
        l.used, l.allocation_id, l.leave_id, l.period_from, l.period_to, l.expiry, 
	t.code, 0.0 as absences, 0.0 as absences_nopay, '' as type, 2 as seq, False as filler, remarks
        from leave l inner join leave_type t on l.holiday_status_id = t.id
 	union all select employee_id, holiday_status_id, 
                        null as transaction_date, 
                        0.0 earned, 
                        0.0 as used, 
                        cast(null as integer) as allocation_id, 
                        cast(null as integer) as leave_id,
                        period_from, 
                        period_to, 
                        false as expiry, 
                        t.code,
                        cast(total_absence as numeric) absences, 
                        0.0 as absences_nopay,
    				    '' as type, 3  as seq,
    				    False as filler, 'Absences without Leave Application' remarks 
                        from absences a inner join leave_type t on a.holiday_status_id = t.id
 	union all select employee_id, holiday_status_id, 
                        null as transaction_date, 
                        0.0 earned, 
                        0.0 as used, 
                        cast(null as integer) as allocation_id, 
                        cast(null as integer) as leave_id,
                        period_from, 
                        period_to, 
                        false as expiry,
                        '' as code, 
                        cast(total_absence as numeric) absences, 
                        0.0 as absences_nopay,
                        '' as type, 99 as seq, True as filler, 'Filler - Just to forward the balance.' remarks 
                        from filler
                        ) leave_summ
    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
    order by employee_id, holiday_status_id, period_from asc, seq asc""")
        
        self._cr.execute("""with res as (select *, id+1 as xid from hr_leave_card_summary order by id desc)
     
        update hr_leave_card_summary rec
        set expiry_date = (case when not res.expiry isnull then res.period_to end),
            balance = (case when rec.expiry and not res.period_to isnull then 
                    (case when rec.period_from < rec.expiry_date or rec.period_from < res.period_to then res.balance when rec.balance > 0 then rec.balance else 0 end)
                    end),
            forwarded = (case when rec.period_from < rec.expiry_date or rec.period_from < res.period_to then res.balance when rec.balance > 0 then rec.balance else 0 end)
        from res where rec.id = res.xid""")

    	### To Correct dashboard				
        self._cr.execute("""with emp_credits as (select employee_id, holiday_status_id, sum(used) as used, sum(earned) as earned, sum(earned-used) as balance
    from hr_leave_card_summary group by 1,2)
        insert into total_leave_credit (employee_id, holiday_status_id, total_allocation, total_base_leave, total_remaining_leave)
                        select * from emp_credits where concat(employee_id,holiday_status_id) not in (select concat(employee_id,holiday_status_id) from total_leave_credit)""")
        
        self._cr.execute("""update total_leave_credit c 
                        set total_allocation = earned,
                        total_base_leave = used,
                        total_remaining_leave = balance
                        from (select employee_id, holiday_status_id, sum(used) as used, sum(earned) as earned, sum(earned-used) as balance
    				    from hr_leave_card_summary group by 1,2) s
    				    where c.employee_id = s.employee_id and c.holiday_status_id = s.holiday_status_id
    				    """)

    def _compute_balance(self):
        for rec in self:
            holiday_status_id = rec.holiday_status_id.id
            employee_id = rec.employee_id.id
            id = rec.id
            period_from = rec.period_from
            res = self.search([('id','<',id),('period_from','<=',period_from),('holiday_status_id','=',holiday_status_id),('employee_id','=',employee_id)],order='id desc', limit=1)
            balance = 0.0
            if res:
                if res.expiry:
                    rec.expiry_date = res.period_to or False
                if rec.expiry_date:
                    balance = 0.0
                    if rec.period_from < rec.expiry_date:
                        balance = res.balance 
                else: 
                    balance = res.balance
            if balance:
                rec.forwarded = balance
            rec.balance = rec.forwarded + rec.earned - rec.used
            # rec.filler = True if (rec.earned+rec.used+rec.absences) <= 0.0 else False

    def get_prev_balance(self, rec):
        data = {}
        holiday_status_id = rec.holiday_status_id.id
        employee_id = rec.employee_id.id
        id = rec.id
        period_from = rec.period_from
        res = self.search([('id','<',id),('period_from','<=',period_from),('holiday_status_id','=',holiday_status_id),('employee_id','=',employee_id)],order='id desc', limit=1)
        balance = 0.0
        if res:
            if res.expiry:
                rec.expiry_date = res.period_to or False
            if rec.expiry_date:
                balance = 0.0
                if rec.period_from < rec.expiry_date:
                    balance = res.balance 
            else: 
                balance = res.balance
        if balance:
            data['forwarded'] = balance
        
        data['expiry_date'] = rec.expiry_date
        data['balance'] = balance + rec.earned-rec.used

        return data

