# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning, ValidationError

score_warning = "Invalid Score, highest score is 5."
            
class HREmployeeFunctions(models.Model):
    _name = 'hr.employee.functions'
    _inherit = 'mail.thread'
    _description = 'HR Employee Functions'
    
    name = fields.Char(string='Description', )
    type = fields.Selection(string='Type', selection=[('core', 'Core Functions'), 
    ('strategic', 'Strategic Functions'),
    ('support', 'Support Functions'),])
    active = fields.Boolean(string='Active', default=True)
    
class HREmployeeFunctionCore(models.Model):
    _name='hr.employee.function.core'
    _description = 'HR Employee Evaluation Core Function'
    
    order = fields.Integer(string='Sequence')
    name = fields.Text(string='Core Functions')
    function_id = fields.Many2one(comodel_name='hr.employee.functions', string='Core Function', required=False,
    domain=[('type','=','core'),('active','=',True)], context="{'default_type':'core'}")
    accomplishment = fields.Text(string='Actual Accomplishments', required=False)
    quality = fields.Float(string='Q1')
    efficiency = fields.Float(string='E2')
    timeliness = fields.Float(string='T3')
    average = fields.Float(string='A4', compute='_compute_avg')
    remarks = fields.Text(string='Remarks')
    empeval_id = fields.Many2one(comodel_name='hr.employee.evaluation', string='Employee Evaluation')
    state = fields.Selection(string='State', selection=[('draft', 'To Submit'), 
    ('confirm', 'Waiting'),
    ('approve', 'Accomplistments'),
    ('evaluate', 'Evaluation'),
    ('rating', 'Rating Approval'),
    ('done','Done')], related='empeval_id.state') 
    
    @api.onchange('quality')
    def _onchange_score(self):
        if self.quality > 5:
            raise ValidationError(_(score_warning))
    
    @api.onchange('efficiency')
    def _onchange_score(self):
        if self.efficiency > 5:
            raise ValidationError(_(score_warning))
            
    @api.onchange('timeliness')
    def _onchange_score(self):
        if self.timeliness > 5:
            raise ValidationError(_(score_warning))
    
    @api.depends('quality','efficiency','timeliness')
    def _compute_avg(self):
        for rec in self:
            rec.average = (rec.quality+rec.efficiency+rec.timeliness)/3

class HREmployeeFunctionStrategic(models.Model):
    _name='hr.employee.function.strategic'
    _description = 'HR Employee Evaluation Strategic Function'
    
    order = fields.Integer(string='Sequence')
    name = fields.Text(string='Strategic Functions')
    function_id = fields.Many2one(comodel_name='hr.employee.functions', string='Strategic Function', required=False,
    domain=[('type','=','strategic'),('active','=',True)], context="{'default_type':'strategic'}")
    accomplishment = fields.Text(string='Actual Accomplishments', required=False)
    quality = fields.Float(string='Q1')
    efficiency = fields.Float(string='E2')
    timeliness = fields.Float(string='T3')
    average = fields.Float(string='A4', compute='_compute_avg')
    remarks = fields.Text(string='Remarks')
    empeval_id = fields.Many2one(comodel_name='hr.employee.evaluation', string='Employee Evaluation')
    state = fields.Selection(string='State', selection=[('draft', 'To Submit'), 
    ('confirm', 'Waiting'),
    ('approve', 'Accomplistments'),
    ('evaluate', 'Evaluation'),
    ('rating', 'Rating Approval'),
    ('done','Done')], related='empeval_id.state') 
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Requested By', related="empeval_id.employee_id")
    
    @api.onchange('quality')
    def _onchange_score(self):
        if self.quality > 5:
            raise ValidationError(_(score_warning))
    
    @api.onchange('efficiency')
    def _onchange_score(self):
        if self.efficiency > 5:
            raise ValidationError(_(score_warning))
            
    @api.onchange('timeliness')
    def _onchange_score(self):
        if self.timeliness > 5:
            raise ValidationError(_(score_warning))
            
    @api.depends('quality','efficiency','timeliness')
    def _compute_avg(self):
        for rec in self:
            rec.average = (rec.quality+rec.efficiency+rec.timeliness)/3
        
class HREmployeeFunctionSupport(models.Model):
    _name='hr.employee.function.support'
    _description = 'HR Employee Evaluation Support Function'
    
    order = fields.Integer(string='Sequence')
    name = fields.Text(string='Strategic Functions')
    function_id = fields.Many2one(comodel_name='hr.employee.functions', string='Strategic Function', required=False,
    domain=[('type','=','support'),('active','=',True)], context="{'default_type':'support'}")
    accomplishment = fields.Text(string='Actual Accomplishments', required=False)
    quality = fields.Float(string='Q1')
    efficiency = fields.Float(string='E2')
    timeliness = fields.Float(string='T3')
    average = fields.Float(string='A4', compute='_compute_avg')
    remarks = fields.Text(string='Remarks')
    empeval_id = fields.Many2one(comodel_name='hr.employee.evaluation', string='Employee Evaluation')
    state = fields.Selection(string='State', selection=[('draft', 'To Submit'), 
    ('confirm', 'Waiting'),
    ('approve', 'Accomplistments'),
    ('evaluate', 'Evaluation'),
    ('rating', 'Rating Approval'),
    ('done','Done')], related='empeval_id.state')     
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Requested By', related="empeval_id.employee_id")

    @api.onchange('quality')
    def _onchange_score(self):
        if self.quality > 5:
            raise ValidationError(_(score_warning))
    
    @api.onchange('efficiency')
    def _onchange_score(self):
        if self.efficiency > 5:
            raise ValidationError(_(score_warning))
            
    @api.onchange('timeliness')
    def _onchange_score(self):
        if self.timeliness > 5:
            raise ValidationError(_(score_warning))
    
    @api.depends('quality','efficiency','timeliness')
    def _compute_avg(self):
        for rec in self:
            rec.average = (rec.quality+rec.efficiency+rec.timeliness)/3
    
class HREmployeeEvaluation(models.Model):
    _name = 'hr.employee.evaluation'
    _inherit = 'mail.thread'
    _description = 'HR Employee Evaluation'
    _order = 'transaction_date desc, id desc'
    
    name = fields.Char(string='Name', default='New', readonly=True, tracking=True)
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee', 
    default=lambda self: self.env.user.employee_id.id, tracking=True, readonly=True, copy=True)
    transaction_date = fields.Date(string='Transaction Date', tracking=True, required=True, copy=False, default=lambda self: fields.datetime.now())
    evaluation_date = fields.Date(string='Evaluation Date From', tracking=True, required=True, copy=False)
    evaluation_date_to = fields.Date(string='Evaluation Date To', tracking=True, required=True, copy=False)
    score = fields.Float(string='Rating', default=0.0, compute='_compute_score')
    
    state = fields.Selection(string='State', 
    selection=[('draft', 'To Submit'), 
    ('confirm', 'Waiting'),
    ('approve', 'Accomplishments'),
    ('evaluate', 'Evaluation'),
    ('rating', 'Rating Approval'),
    ('done','Done')], 
    tracking=True, default='draft', copy=False)
    
    approved_by = fields.Many2one(comodel_name='res.users', string='Approved By', tracking=True, copy=False)
    cancelled_by = fields.Many2one(comodel_name='res.users', string='Cancelled By', tracking=True, copy=False)    
    assessed_by = fields.Many2one(comodel_name='res.users', string='Assessed By', tracking=True, copy=False)   

    is_supervisor = fields.Boolean(string='Supervisor?', compute="_compute_manager")
    is_manager = fields.Boolean(string='Manager?', compute="_compute_manager")
    job_id = fields.Many2one(comodel_name='hr.job', string='Position', related='employee_id.job_id')
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id')
    
    supervisor_id = fields.Many2one(comodel_name='hr.employee', string='Immediate Superior', required=True)
    supervisor_job_id = fields.Many2one(comodel_name='hr.job', string='Job Position', related='supervisor_id.job_id')
    manager_id = fields.Many2one(comodel_name='hr.employee', string='Manager', required=True)
    manager_job_id = fields.Many2one(comodel_name='hr.job', string='Head Position', related='manager_id.job_id')
    
    core_ids = fields.One2many(comodel_name='hr.employee.function.core', inverse_name='empeval_id', string='Core Function', copy=True)
    strategic_ids = fields.One2many(comodel_name='hr.employee.function.strategic', inverse_name='empeval_id', string='Strategic Function', copy=True)
    support_ids = fields.One2many(comodel_name='hr.employee.function.support', inverse_name='empeval_id', string='Support Function', copy=True)
    
    # @api.depends('field_name')
    def _compute_score(self):
        score = 0
        ctr = 0
        for rec in self.core_ids:
            ctr += 1
            score += rec.average
            
        
        for rec in self.strategic_ids:
            ctr += 1
            score += rec.average
            
        for rec in self.support_ids:
            ctr += 1
            score += rec.average
            
        if ctr > 0:
            score = score/ctr
        
        self.score = score
    
    def  _compute_manager(self):
        uid = self.env.user.id
        is_supervisor = False
        is_manager = False
        if self.state in ['confirm','evaluate','rating']:
            if self.employee_id.user_id.id != uid:
                is_supervisor  = (self.supervisor_id.user_id.id == uid or False)
                # if self.assessed_by:
                is_manager  = (self.manager_id.user_id.id == uid or False)
        
        self.is_supervisor  = is_supervisor
        self.is_manager  = is_manager
    
    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        supervisor_id = False
        manager_id = False
        if self.employee_id.parent_id:
            manager_id = self.employee_id.parent_id.id
        if self.employee_id.coach_id:
            supervisor_id = self.employee_id.coach_id.id
            
                
        self.supervisor_id = supervisor_id
        self.manager_id = manager_id
                
    # @api.model
    # def create(self, values):
    #     seq = self.env['ir.sequence'].next_by_code('hr.employee.evaluation') or '/'
    #     values['name'] = seq
    #     return super(HREmployeeEvaluation, self.sudo()).create(values)
    
    # Employee
    def action_confirm(self):
        self.state = 'confirm'
        
    def action_draft(self):
        self.state = 'draft'
              
    # Manager
    def action_approve(self):
        self.state = 'approve'
       
    # Manager
    def action_setback_approve(self):
        self.state = 'approve'
    
    # Employee to Supervisor  
    def action_evaluate(self):
        self.state = 'evaluate'
        
    #Employee
    def action_fillup(self):
        self.state = 'approve'
        
    # Supervisor       
    def action_confirm_rating(self):
        self.action_done()
             
    def action_done_review(self):
        self.action_done()
        
    # Manager
    def action_done(self):
        if self.state == 'evaluate':
            if self.assessed_by == False:
                if self.is_supervisor:
                    self.assessed_by = self.env.user.id
            self.state = 'rating'
            
        elif self.state == 'rating':
            if self.approved_by == False:
                if self.is_manager:
                    self.approved_by = self.env.user.id
            self.state = 'done'
                
    def action_reject(self):
        self.rejected_by = self.env.user.id
        self.state = 'reject'
        
    def action_cancel(self):
        self.cancelled_by = self.env.user.id
        self.state = 'Cancel'
        
class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    employee_id = fields.Many2one('hr.employee',
                                  string='Related Employee', ondelete='restrict', 
                                  help='Employee-related data of the user')