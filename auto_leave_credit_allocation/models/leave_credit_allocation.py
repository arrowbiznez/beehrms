# -*- coding: utf-8 -*-
import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api
import math

CREDIT_DECREASE_BY_ABSENCE = {"30":1.25,"29.5":1.229,"29":1.208,"28.5":1.188,"28":1.167,"27.5":1.146,"27":1.125,
                               "26.5":1.104,"26":1.083,"25.5":1.063,"25":1.042,"24.5":1.021,"24":1,"23.5":0.979,
                               "23":0.958,"22.5":0.938,"22":0.917,"21.5":0.896,"21":0.875,"20.5":0.854,"20":0.833,
                               "19.5":0.813,"19":0.792,"18.5":0.771,"18":0.75,"17.5":0.729,"17":0.708,"16.5":0.687,
                               "16":0.667,"15.5":0.646,"15":0.625,"14.5":0.604,"14":0.583,"13.5":0.562,"13":0.542,
                               "12.5":0.521,"12":0.5,"11.5":0.479,"11":0.458,"10.5":0.437,"10":0.417,"9.5":0.396,
                               "9":0.375,"8.5":0.354,"8":0.333,"7.5":0.312,"7":0.292,"6.5":0.271,"6":0.25,"5.5":0.229,
                               "5":0.208,"4.5":0.187,"4":0.167,"3.5":0.146,"3":0.125,"2.5":0.104,"2":0.083,"1.5":0.062,
                               "1":0.042,"0.5":0.021,"0":0}

CREDIT_DECREASE_BY_MINUTES = {"1":0.002,"2":0.004,"3":0.006,"4":0.008,"5":0.01,"6":0.012,"7":0.015,"8":0.017,"9":0.019,
                               "10":0.021,"11":0.023,"12":0.025,"13":0.027,"14":0.029,"15":0.031,"16":0.033,"17":0.035,
                               "18":0.037,"19":0.4,"20":0.042,"21":0.044,"22":0.046,"23":0.48,"24":0.05,"25":0.052,
                               "26":0.054,"27":0.056,"28":0.058,"29":0.06,"30":0.062,"31":0.065,"32":0.067,"33":0.069,
                               "34":0.071,"35":0.073,"36":0.075,"37":0.077,"38":0.079,"39":0.081,"40":0.083,"41":0.085,
                               "42":0.087,"43":0.09,"44":0.092,"45":0.094,"46":0.096,"47":0.098,"48":0.1,"49":0.102,
                               "50":0.104,"51":0.106,"52":0.108,"53":0.11,"54":0.112,"55":0.115,"56":0.117,"57":0.119,
                               "58":0.121,"59":0.123,"60":0.125}

CREDIT_DECREASE_BY_HOURS = {"1":0.125,"2":0.25,"3":0.375,"4":0.5,"5":0.625,"6":0.75,"7":0.875,"8":1}

class AttendanceTimesheetBase(models.Model):
    _inherit = 'attendance.timesheet.base'
    
    leave_allocation_ids = fields.One2many('hr.leave.allocation', 'att_timesheet_id', string='Leave Allocation')
    
    def compute_employee_leave_credit(self):
        
        alloc = self.env['hr.leave.allocation'].sudo()
        vacation_leave_id = self.env['hr.leave.type'].search([('code', '=', 'VL'),('active','=',True)], limit=1)
        sick_leave_id = self.env['hr.leave.type'].search([('code', '=', 'SL'),('active','=',True)], limit=1)
        for rec in self:
            if rec.date_from.month==12: # No Credit Allocation for December
                break
            exists = alloc.sudo().search([('att_timesheet_id','=',rec.id)])
            if exists:
                continue
            total_late = rec.total_undertime + rec.total_late
            total_absence = rec.total_absences
            approval_date = rec.date_from
            date_from = rec.date_end + relativedelta(days=1)
            total_hours,total_minutes = math.modf(total_late)

            decrease_hours_credit = CREDIT_DECREASE_BY_HOURS.get(total_hours, 0)
            decrease_minutes_credit = CREDIT_DECREASE_BY_HOURS.get(total_minutes, 0)
            decrease_days_credit = CREDIT_DECREASE_BY_HOURS.get(total_absence, 0)

            total_decrease = decrease_days_credit+decrease_minutes_credit+decrease_hours_credit

            total_credit = 1.25 - total_decrease
            # date_to = '%s-12-31 23:59:59' % date_from.year
            
            vacation_leave = alloc.create({'employee_id': rec.employee_id.id,
                                        'name': '%s Allocation for %s from timesheet %s-%s' % (
                                        vacation_leave_id.name, rec.employee_id.name, rec.date_from.strftime('%Y-%m-%d'), rec.date_end.strftime('%Y-%m-%d')),
                                        'holiday_status_id': vacation_leave_id.id,
                                        'holiday_type': 'employee',
                                        'number_of_days': total_credit,
                                        'att_timesheet_id': rec.id,
                                        'approval_date': approval_date,
                                        'date_from': date_from,
                                        })
                                        # 'date_to': date_to

            sick_leave = alloc.create({'employee_id': rec.employee_id.id,
                                    'name': '%s Allocation for %s from timesheet %s-%s' % (
                                    sick_leave_id.name, rec.employee_id.name, rec.date_from.strftime('%Y-%m-%d'), rec.date_end.strftime('%Y-%m-%d')),
                                    'holiday_status_id': sick_leave_id.id,
                                    'holiday_type': 'employee',
                                    'number_of_days': total_credit,
                                    'att_timesheet_id': rec.id,
                                    'approval_date': approval_date,
                                    'date_from': date_from,
                                    })
                                    # 'date_to': date_to
                                    
            vacation_leave.action_approve()
            sick_leave.action_approve() 
class AutomatedLeaveCreditAllocation(models.Model):
    _inherit = 'hr.leave.allocation'
    # _description = 'Automated Credit Allocation'
    
    att_timesheet_id = fields.Many2one('attendance.timesheet.base', string='timesheet')
    approval_date = fields.Datetime(string="Approval Date")
    state = fields.Selection(selection_add=[('expired', 'Expired')])
    
    def _automated_spl_credit(self):
        spl_id = self.env['hr.leave.type'].search([('code','=','SPL')])
        if not spl_id:
            return False
        
        approval_date = datetime.now()
        date_from = '%s-01-31 00:00:00' % approval_date.year
        date_to = '%s-12-31 23:59:59' % approval_date.year
        
        spl_id.validity_start = date_from
        spl_id.validity_stop = date_to
        
        params = self.env['ir.config_parameter'].sudo()
        spl_days = params.get_param('hr_leave_type.spl_credit_allocation_days',
                                                 default='3')
        
        for contract in self.env['hr.contract'].search([('state','=','open')]):
            name = '%s Allocation for %s in %s to %s' % (
                                        spl_id.name, contract.employee_id.name, date_from, date_to)
            duplicate = self.search([('name','=',name)])
            if not duplicate:
                spl = self.create({'employee_id': contract.employee_id.id,
                                            'name': name,
                                            'holiday_status_id': spl_id.id,
                                            'holiday_type': 'employee',
                                            'number_of_days': int(spl_days),
                                            'approval_date': approval_date,
                                            'date_from': date_from,
                                            'date_to': date_to
                                            })
                        
                spl.action_approve()

        
    
    def _automated_leave_credit(self):
        first_day = datetime.datetime.now().replace(day=1)
        # if first_day.month==12: # No Credit Allocation for December
        #     return False
        last_day = first_day + relativedelta(months=1)-relativedelta(days=1)
        
        employee_ids = self.env['hr.employee'].search([('contract_id', '!=', False)])
        vacation_leave_id = self.env['hr.leave.type'].search([('code', '=', 'VL'),('active','=',True)], limit=1)
        sick_leave_id = self.env['hr.leave.type'].search([('code', '=', 'SL'),('active','=',True)], limit=1)

        for employee in employee_ids:
            active_timesheet = self.env['attendance.timesheet.base'].search(
                [("date_from", '=', first_day), ("date_end", '=', last_day), ("employee_id", '=', employee.id)], limit=1)
            if active_timesheet:
                att_timesheet_id = active_timesheet.id
                exists = self.search([('att_timesheet_id','=',att_timesheet_id)])
                if exists:
                    continue
                total_late = active_timesheet.total_undertime + active_timesheet.total_late
                total_absence = active_timesheet.total_absences
                approval_date = active_timesheet.date_from
                date_from = active_timesheet.date_end + relativedelta(days=1)
                total_hours,total_minutes = math.modf(total_late)
    
                decrease_hours_credit = CREDIT_DECREASE_BY_HOURS.get(total_hours, 0)
                decrease_minutes_credit = CREDIT_DECREASE_BY_HOURS.get(total_minutes, 0)
                decrease_days_credit = CREDIT_DECREASE_BY_HOURS.get(total_absence, 0)
    
                total_decrease = decrease_days_credit+decrease_minutes_credit+decrease_hours_credit
    
                total_credit = 1.25 - total_decrease
                # date_to = '%s-12-31 23:59:59' % date_from.year
    
                vacation_leave = self.create({'employee_id': employee.id,
                                            'name': '%s Allocation for %s in %s' % (
                                            vacation_leave_id.name, employee.name, datetime.datetime.now()),
                                            'holiday_status_id': vacation_leave_id.id,
                                            'holiday_type': 'employee',
                                            'number_of_days': total_credit,
                                            'att_timesheet_id': att_timesheet_id.id,
                                            'approval_date': approval_date,
                                            'date_from': date_from,
                                            })
                                            # 'date_to': date_to
    
                sick_leave = self.create({'employee_id': employee.id,
                                        'name': '%s Allocation for %s in %s' % (
                                        sick_leave_id.name, employee.name, datetime.datetime.now()),
                                        'holiday_status_id': sick_leave_id.id,
                                        'holiday_type': 'employee',
                                        'number_of_days': total_credit,
                                        'att_timesheet_id': att_timesheet_id.id,
                                        'approval_date': approval_date,
                                        'date_from': date_from,
                                        })
                                        # 'date_to': date_to
                                        
                vacation_leave.action_approve()
                sick_leave.action_approve()