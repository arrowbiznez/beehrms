# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class HrCoc(models.Model):
    _name = 'hr.compensatory.ot.credit'
    _inherits = {'hr.employee.document':'hr_document_id'}
    _inherit = 'mail.thread'
    _description = 'HR Compensatory Overtime Credit'
    
    hr_document_id = fields.Many2one('hr.employee.document', 'Employee Document',
                                 required=True, ondelete="cascade")
    title = fields.Char(string='Document Title', required=True, placeholder='Document Title')
    transaction_date = fields.Date(string='Transaction Date', required=True, default=lambda self: fields.datetime.now())
    # employee_id = fields.Many2one(comodel_name='hr.employee', string='employee', required=True, default=lambda self: self.env['hr.employee'].search([('user_id','=',self.env.user.id)]))
    # coc_ids = fields.Many2many(comodel_name="ir.attachment", relation="hr_coc_ir_attachment_relation",column1="hr_to_id", column2="attachment_id", string="Attachments") 
    doc_count = fields.Integer(string='Attachments', compute='_compute_doc_count')
    state = fields.Selection(string='State', selection=[('draft', 'To Submit'), ('cancel', 'Cancelled'), ('confirm', 'Confirm'), ('approve', 'Approved'), ('refuse', 'Refused'),], default='draft')
    
    def _get_coc_employee(self):
    
        document_type = self.env.ref('hr_travel_order.document_type_compensatory_ot_credit').id
        employee_id = self.env.user.employee_id.id
        context = {'default_document_type': document_type, 'default_employee_ref': employee_id, 'default_name':'New'}
        
        value = {
            'domain': str([('employee_ref', '=', employee_id)]),
            'view_mode': 'tree,form',
            'res_model': 'hr.compensatory.ot.credit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('COC Requests'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context': context
        }
        return value
    
    def _get_coc_manager_approval(self):
    
        document_type = self.env.ref('hr_travel_order.document_type_compensatory_ot_credit').id
        employee_id = self.env.user.employee_id.id
        context = {'default_document_type': document_type, 'default_employee_ref': employee_id, 'default_name':'New'}
        
        value = {
            'domain': str([('state', '=', 'confirm')]),
            'view_mode': 'tree,form',
            'res_model': 'hr.compensatory.ot.credit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('COC Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context': context
        }
        return value
        
    def _get_coc_manager_all(self):
    
        document_type = self.env.ref('hr_travel_order.document_type_compensatory_ot_credit').id
        employee_id = self.env.user.employee_id.id
        context = {'default_document_type': document_type, 'default_employee_ref': employee_id, 'default_name':'New'}
        
        value = {
            'domain': str([('state', '!=', 'draft')]),
            'view_mode': 'tree,form',
            'res_model': 'hr.compensatory.ot.credit',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('All COC'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context': context
        }
        return value
    
    @api.model
    def create(self, values):
        seq = False
        if 'employee_id' not in values:
            emp_id = self.env.user.employee_id.id
        else:
            emp_id = values['employee_id']
        emp = self.env['hr.employee'].browse(emp_id)
        if emp:
            if emp.department_id and 'dept_code' in self.env['hr.department']._fields:
                if emp.department_id.dept_code:
                    seq = self.env['ir.sequence'].next_by_code_by_employee('COC',self._name,emp) or '/'
        if not seq:
            seq = self.env['ir.sequence'].next_by_code('hr.compensatory.ot.credit') or '/'
        values['name'] = seq
        return super(HrCoc, self.sudo()).create(values)
    
    
    # @api.depends('hr_attachment_id')
    def _compute_doc_count(self):
        ctr = 0
        for rec in self.doc_attachment_id:
            ctr += 1
        self.doc_count = ctr
    
    def action_confirm(self):
        self.state = 'confirm'
    
    def action_reject(self):
        self.state = 'refuse'
        
    def action_draft(self):
        self.state = 'draft'
    
    def action_approve(self): 
        self.state = 'approve' 
        
    def action_cancel(self): 
        self.state = 'cancel' 
        
class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    employee_id = fields.Many2one('hr.employee',
                                  string='Related Employee', ondelete='restrict', 
                                  help='Employee-related data of the user')
                                  