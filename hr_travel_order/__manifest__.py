# -*- coding: utf-8 -*-
{
    'name': "HR Travel Order",

    'summary': """
        Uploading of travel Order Form only""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Daryll Gay S. Bangoy",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','hr_holidays','oh_employee_documents_expiry'],

    # always loaded
    'data': [
        # 'security/security.xml',
        'security/ir.model.access.csv',
        'data/travel_order.xml',
        'views/travel_order.xml',
        'views/compensatory_ot_credit.xml',
        'views/templates.xml',
        'report/cert_of_appearance.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
