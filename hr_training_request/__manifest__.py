# -*- coding: utf-8 -*-
{
    'name': "HR Training & Development Documents Request",

    'summary': """
        Uploading of training Request Form only""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Daryll Gay S. Bangoy",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','oh_employee_documents_expiry'],

    # always loaded
    'data': [
        # 'security/security.xml',
        'security/ir.model.access.csv',
        'data/training_request.xml',
        'views/hr_employee_training_request.xml',
        'views/hr_training_document_request.xml',
        'views/hr_training_document_employee_upload.xml',
        'views/templates.xml',
        'report/hr_training_request_report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
