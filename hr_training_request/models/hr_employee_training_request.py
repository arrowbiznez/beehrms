# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning, ValidationError

class HRTrainingCheckList(models.Model):
    _name = 'hr.training.checklist'
    _inherit = 'mail.thread'
    _description = 'HR Training Checklist'
    
    name = fields.Char(string='Training', tracking=True)
    active = fields.Boolean(string='Active', default=True, tracking=True)
     
    @api.constrains('name')
    def _check_name(self):
        name = self.name
        id = self.id
        found = self.search([('name','ilike',name),('id','!=',id)])
        if found:
            raise ValidationError("Training Already Exists in checklist.")
            
class HREmployeeTrainingRequestCompentency(models.Model):
    _name = 'hr.training.request.competency'
    _inherit = 'mail.thread'
    _description = 'HR Employee Training Request Competency'
    
    request_id = fields.Many2one(comodel_name='hr.employee.training.request', string='Competency')
    target_competency = fields.Text(string='Target Competency (1)')
    target_indicators = fields.Text(string='Target Indicators (2)')
    activity = fields.Text(string='Development Activity/Intervention (3)')
    support_needed = fields.Text(string='Support Needed (4)')
    trainer_provider = fields.Char(string='Trainer/Provider (5)')
    completion_date = fields.Date(string='Expected Date of Completion (6)')
    
class HREmployeeTrainingRequestPurpsose(models.Model):
    _name = 'hr.training.request.purpose'
    _inherit = 'mail.thread'
    _description = 'HR Employee Training Request Purpose'
    
    name = fields.Char(string='Purpose')
    active = fields.Boolean(string='Active', default=True)
    
class HREmployeeTrainingRequest(models.Model):
    _name = 'hr.employee.training.request'
    _inherit = 'mail.thread'
    _description = 'HR Employee Training Request'
    _order = 'date_requested desc, id'
    
    name = fields.Char(string='Name', default='New', readonly=True, tracking=True)
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Requested By', 
    default=lambda self: self.env.user.employee_id.id, tracking=True, readonly=True)
    date_requested = fields.Date(string='Date Request', default=lambda self: fields.datetime.now(), tracking=True, required=True)
    state = fields.Selection(string='State', selection=[('draft', 'To Submit'), 
                                                        ('f_approve', 'Waiting'),
                                                        ('approve', 'Approved'),
                                                        ('verify','Verified'),
                                                        ('refuse','Refused'),
                                                        ('cancel', 'Cancel'),], tracking=True, default='draft')
    attendee_ids = fields.Many2many(comodel_name='hr.employee', string='Attendees', relation="hr_training_request_employee_attendees_relation", 
    column1='training_request', column2='employee_id', tracking=True, default=lambda self: self.env.user.employee_id.ids or False)
    training_ids = fields.Many2many(comodel_name='hr.training.checklist', string='Trainings', tracking=True, domain="[('active','=',True)]")
    reviewed_by = fields.Many2one(comodel_name='res.users', string='Reviewed By', tracking=True)
    approved_by = fields.Many2one(comodel_name='res.users', string='Approved By', tracking=True)
    rejected_by = fields.Many2one(comodel_name='res.users', string='Rejected By', tracking=True)
    is_hrmo = fields.Boolean(string='HRMO?', compute="_compute_manager")
    is_manager = fields.Boolean(string='Manager?', compute="_compute_manager")
    job_id = fields.Many2one(comodel_name='hr.job', string='Position', related='employee_id.job_id')
    manager_id = fields.Many2one(comodel_name='hr.employee', string='Manager', related='employee_id.parent_id', readonly=True, required=True)
    supervisor_id = fields.Many2one(comodel_name='hr.employee', string='Supervisor', default=lambda self: self.env.user.employee_id.parent_id.id, required=True)
    department_id = fields.Many2one(comodel_name='hr.department', string='Department', related='employee_id.department_id', required=True)
    purpose = fields.Many2many(comodel_name='hr.training.request.purpose', string='Purpose', domain="[('active','=',True)]")
    other_purpose = fields.Char(string='Others, please specify')
    purpose_list = fields.Many2many(comodel_name='hr.training.request.purpose', relation="hr_employee_training_request_purpuse_rel", column1="request_id", column2="purpose_id", string='Purpose', domain="[('active','=',True)]")
    compentency_ids = fields.One2many(comodel_name='hr.training.request.competency', inverse_name='request_id', string='Competencies')
    
    def _compute_manager(self):
        self.is_hrmo = False
        self.is_manager = False
        if self.state != 'reject':
            if self.state == 'f_approve':
                if not self.reviewed_by:
                    self.is_hrmo = self.env.user.has_group('hr.group_hr_user')
            else:
                if not self.approved_by and self.manager_id.user_id.id == self.env.user.id:    
                    self.is_manager = self.employee_id.parent_id.user_id.id == self.user.id
    
    @api.model
    def create(self, values):
        seq = False
        if 'employee_id' not in values:
            emp_id = self.env.user.employee_id.id
        else:
            emp_id = values['employee_id']
        emp = self.env['hr.employee'].browse(emp_id)
        if emp:
            if emp.department_id and 'dept_code' in self.env['hr.department']._fields:
                if emp.department_id.dept_code:
                    seq = self.env['ir.sequence'].next_by_code_by_employee("ETAD",self._name,emp) or '/'
        if not seq:
            seq = self.env['ir.sequence'].next_by_code('hr.employee.training.request') or '/'
        values['name'] = seq
        
        purpose_list =[]
        res = self.env['hr.training.request.purpose'].search([('active','=',True)])
        for rec in res:
            purpose_list.append((4,rec.id))
        if purpose_list:
            values['purpose_list'] = purpose_list
            
        return super(HREmployeeTrainingRequest, self.sudo()).create(values)
    
    
    def action_confirm(self):
        self.rejected_by = False
        self.reviewed_by = False
        self.approved_by = False
        if not self.attendee_ids:
            self.attendee_ids = [(6,0,[self.employee_id.id])]
        self.state = 'f_approve'
        
    def action_approve(self):
        if not self.reviewed_by:
            if self.env.user.has_group('hr.group_hr_user'):
                self.reviewed_by = self.env.user.id
            else:
                raise ValidationError(_("Must be reviewed by HRMO first."))
            
        else:
            if not self.approved_by:
                if self.employee_id.parent_id.employee_id.user_id.id == self.env.user.id:
                    self.approved_by = self.env.user.id
                    self.state = 'approve'
                else:
                    raise ValidationError(_("You must be his/her Division Chief."))
                
    def action_draft(self):
        self.state = 'draft'
        
    def action_reject(self):
        self.rejected_by = self.env.user.id
        self.state = 'reject'
        
    def action_cancel(self):
        self.state = 'cancel'
        
    def action_verify(self):
        self.state = 'verify'
        
class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    employee_id = fields.Many2one('hr.employee',
                                  string='Related Employee', ondelete='restrict',
                                  help='Employee-related data of the user')