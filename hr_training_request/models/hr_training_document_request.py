# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning

class HrTrainingDocumentUpload(models.Model):
    _name = 'hr.training.document.upload'
    _inherit = 'mail.thread'
    _description = 'HR Training Document Upload'
    _order = "id desc"
    
    # name = fields.Char(string='Name', default='New', readonly=True)
    request_id = fields.Many2one(comodel_name='hr.training.document.request', string='Request')
    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee')
    doc_request_ids = fields.Many2many(comodel_name="hr.training.checklist", relation="hr__training_doc_upload_training_checklist_rel",column1="req_id", column2="training_id", string="Training Documents") 
    doc_count = fields.Integer(string='No of Docs', compute='_compute_doc_count')
    ref = fields.Reference(string='Reference', selection='_get_ref_model')
    attachment_count = fields.Integer(string='No of Attachments', compute='_compute_doc_attachment')
    upload_state = fields.Selection(string='Upload Status', selection=[('init',''),('draft', 'To Submit'), ('confirm', 'Confirm'), ('done', 'Done'), ], compute='_compute_doc_attachment')
    
    # @api.model
    # def create(self, values):
    #     seq = self.env['ir.sequence'].next_by_code('hr.training.document.upload') or '/'
    #     values['name'] = seq
    #     return super(HrTrainingDocumentUpload, self.sudo()).create(values)
    
    def open_document_upload(self):
        id = self.ref
        
        value = {
            'domain': str([('id', '=',id)]),
            'view_id': self.env.ref('hr_document_request.hr_document_upload_view_form',
                                    False).id,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'hr.training.document.upload',
            'type': 'ir.actions.act_window',
            'name': _('Document Upload'),
            'res_id': id,
            'target': 'current',
            'create': False,
            'edit': False,
        }
        
        return value
    
    @api.model
    def _get_ref_model(self):   
       models = self.env ['ir.model']. search ([])
       return [(model.model, model.info) for model in models]
    
    @api.depends('doc_request_ids')
    def _compute_doc_count(self):
        ctr = 0
        for rec in self.doc_request_ids:
            ctr += 1
        self.doc_count = ctr
        
    def _compute_doc_attachment(self):
        for res in self:
            ctr = 0
            state = 'init'
            if res.ref:
                for rec in res.ref:
                    state = rec.state
                    for s in rec.hr_document_ids:
                        ctr += 1
            res.attachment_count = ctr
            res.upload_state = state

class HrTrainingDocumentRequest(models.Model):
    _name = 'hr.training.document.request'
    _inherit = 'mail.thread'
    _description = 'HR Training Document Request'
    
    name = fields.Char(string='Description', required=True, default="New", readonly=True)
    request_date = fields.Date(string='Request Date', required=True, default=lambda self: fields.datetime.now())
    state = fields.Selection(string='State', selection=[('draft', 'Draft'), ('confirm', 'On-Going'), 
    ('done', 'Completed'), ('cancel', 'Cancel'),], default='draft')
    employee_ids = fields.One2many(comodel_name='hr.training.document.upload', inverse_name='request_id', string='Employee')
    
    @api.model
    def create(self, values):
        seq = self.env['ir.sequence'].next_by_code('hr.training.document.request') or '/'
        values['name'] = seq
        return super(HrTrainingDocumentRequest, self.sudo()).create(values)
    
    def action_confirm(self):
        for rec in self.employee_ids:
            doc_attachment_ids = [(5, 0, 0)]
            hr_document_ids = [(5, 0, 0)]
            empty = True
            for doc in rec.doc_request_ids:
                empty = False
                doc_attachment_ids.append((4,doc.id))
                hr_document_ids.append((0,0,{'name': doc.name, 'employee_ref': rec.employee_id, 'document_type': doc.id}))
            if not empty:
                print('attachments',doc_attachment_ids)
                ref = self.env['hr.training.document.employee.upload'].create({'employee_ref': rec.employee_id.id,'doc_attachment_ids': doc_attachment_ids})
                rec.ref = "%s,%s" % ('hr.training.document.employee.upload',ref.id)
            else:
                break
            
        if not empty:
            self.state = "confirm"
        else:
            raise Warning(_("Please Fill Employee and the Documents to comply."))
    
    def action_cancel(self):
        self.state = 'cancel'
    
    def action_done(self):
        self.state = 'cancel'
        
    def action_done(self):
        self.state = 'done'
        
    def action_draft(self):
        self.state = 'draft'