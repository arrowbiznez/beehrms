# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class HrTrainingDocumentUploadEmployee(models.Model):
    _name = 'hr.training.document.upload.employee'

class HrTrainingDocumentEmployeeUpload(models.Model):
    _name = 'hr.training.document.employee.upload'
    _inherit = 'mail.thread'
    _description = 'HR Employee Document Upload'
    _rec_name = 'name'
    _order = "id desc"
    
    name = fields.Char(string='Name', default='New', readonly=True)
    employee_ref = fields.Many2one(comodel_name='hr.employee', string='Employee', required='True')
    # hr_document_id = fields.Many2one('hr.employee.document', 'Employee Document',required=True, ondelete="cascade")
    transaction_date = fields.Date(string='Transaction Date', required=True, default=lambda self: fields.datetime.now())
    doc_attachment_ids = fields.Many2many(comodel_name="hr.training.checklist", relation="hr_training_doc_emp_upload_training_checklist_rel",
            column1="training_upload_id", column2="doc_id", string="Documents Checklist", copy=False) 
    doc_count = fields.Integer(string='Attachments', compute='_compute_doc_count')
    state = fields.Selection(string='State', selection=[('draft', 'To Submit'), ('confirm', 'Submit'), ('done', 'Done'), ], default='draft')
    hr_document_ids = fields.One2many(comodel_name='hr.employee.document', inverse_name='training_doc_upload_id', string='Employee Documents')
    
    # attachment_ids = fields.One2many('ir.attachment','res_id', domain=[('res_model', '=', 'hr.training.document.employee.upload')],
    #                                  string='Attachments', copy=False)

    # # @api.multi
    # def action_get_attachment_tree_view(self):
    #     attachment_action = self.env.ref('base.action_attachment')
    #     action = attachment_action.read()[0]
    #     action['context'] = {'default_res_model': self._name, 'default_res_id': self.ids[0]}
    #     action['domain'] = str(['&', ('res_model', '=', self._name), ('res_id', 'in', self.ids)])
    #     action['search_view_id'] = (self.env.ref('hr_training_request.ir_attachment_view_search_notifications').id, )
    #     return action
    
    @api.model
    def create(self, values):
        seq = self.env['ir.sequence'].next_by_code('hr.training.document.employee.upload') or '/'
        values['name'] = seq
        return super(HrTrainingDocumentEmployeeUpload, self.sudo()).create(values)
    
    def _get_document_upload_request_employee(self):
    
        # document_type = self.env.ref('hr_document_upload.document_type_training_and_development').id
        employee_id = self.env.user.employee_id.id
        context = {'default_employee_ref': employee_id}
    
        value = {
            'domain': str([('employee_ref', '=', employee_id)]),
            'view_mode': 'tree,form',
            'res_model': 'hr.training.document.employee.upload',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Document Upload'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context': context
        }
        return value
    
    def _get_document_upload_request_manager_approval(self):
    
        document_type = self.env.ref('hr_document_upload.document_type_training_and_development').id
        employee_id = self.env.user.employee_id.id
        context = {'default_document_type': document_type, 'default_employee_ref': employee_id}
        
        value = {
            'domain': str([('state', '=', 'confirm')]),
            'view_mode': 'tree,form',
            'res_model': 'hr.training.document.employee.upload',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Document Upload Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context': context
        }
        return value
        
    def _get_document_upload_request_manager_all(self):
    
        document_type = self.env.ref('hr_document_upload.document_type_training_and_development').id
        employee_id = self.env.user.employee_id.id
        context = {'default_document_type': document_type, 'default_employee_ref': employee_id}
        
        value = {
            'domain': str([('state', '!=', 'draft')]),
            'view_mode': 'tree,form',
            'res_model': 'hr.training.document.employee.upload',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'name': _('Document Upload Approvals'),
            'res_id': self.id,
            'target': 'current',
            'create': False,
            'edit': False,
            'context': context
        }
        return value
    
    @api.depends('hr_document_ids')
    def _compute_doc_count(self):
        ctr = 0
        for rec in self.hr_document_ids:
            ctr += 1
        self.doc_count = ctr
    
    def action_confirm(self):
        self.state = 'confirm'
    
    def action_reject(self):
        self.state = 'refuse'
        
    def action_draft(self):
        self.state = 'draft'
    
    def action_approve(self): 
        self.state = 'approve' 
        
    def action_cancel(self): 
        self.state = 'cancel' 
        
    def action_done(self): 
        self.state = 'Done' 
        
class HREmployeeDocument(models.Model):
    _inherit = 'hr.employee.document'
    
    training_doc_upload_id = fields.Many2one(comodel_name='hr.training.document.employee.upload', string='Employee Document Upload')
    
class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    employee_id = fields.Many2one('hr.employee',
                                  string='Related Employee', ondelete='restrict', 
                                  help='Employee-related data of the user')