  # -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class ResUsersInherit(models.Model):
    _inherit = 'res.users'

    employee_id = fields.Many2one('hr.employee',
                                  string='Related Employee', ondelete='restrict',
                                  help='Employee-related data of the user')

    @api.model
    def create(self, vals):
        """This code is to create an employee while creating an user."""
        if 'employee_id' not in vals:
            employee = self.env['hr.employee'].sudo().search([('name','ilike',vals['name'])]) 
            if employee:
                vals['employee_id'] = employee.id
            result = super(ResUsersInherit, self).create(vals)
            if not employee:
                result['employee_id'] = self.env['hr.employee'].sudo().create({'name': result['name'],
                                                                            'user_id': result['id'],
                                                                            'address_home_id': result['partner_id'].id})
            elif not employee.user_id and not vals.get('employee_id'): #never mind
                employee.write({'user_id': result['id'],})
                result['employee_id'] = employee.id
        else:
            result = super(ResUsersInherit, self).create(vals)
            
        return result
        
        
